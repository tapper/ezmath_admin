webpackJsonp(["car.module"],{

/***/ "./src/app/cars/add/add.component.css":
/***/ (function(module, exports) {

module.exports = ".FormClass\r\n{\r\n    text-align: right;\r\n    direction: rtl;\r\n}\r\n\r\n.row\r\n{\r\n    margin-top: 20px;\r\n}\r\n\r\n.textWhite\r\n{\r\n    background-color: white;\r\n}\r\n\r\ninput.ng-invalid.ng-touched\r\n{\r\n    border:1px solid red;\r\n}"

/***/ }),

/***/ "./src/app/cars/add/add.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"row FormClass\">\r\n  <div class=\"col-lg-12\">\r\n    <div class=\"card\">\r\n      <div class=\"card-header\">\r\n        הוסף חברה\r\n      </div>\r\n      <div class=\"card-body\">\r\n        <form (ngSubmit)=\"onSubmit(f)\" #f=\"ngForm\">\r\n          <div class=\"row\">\r\n            <div class=\"form-group\" class=\"col-lg-12\">\r\n              <label for=\"formGroupExampleInput\">הכנס שם חברה </label>\r\n              <input type=\"text\" class=\"form-control textWhite\" id=\"formGroupExampleInput\" name=\"name\" ngModel required>\r\n            </div>\r\n          </div>\r\n\r\n          <div class=\"row\">\r\n            <div class=\"form-group\" class=\"col-lg-6\">\r\n              <label for=\"formGroupExampleInput\">הכנס כתובת </label>\r\n              <input type=\"text\" class=\"form-control textWhite\" id=\"formGroupExampleInput\" name=\"address\"\r\n                     ngModel required>\r\n            </div>\r\n            <div class=\"form-group\" class=\"col-lg-6\">\r\n              <label for=\"formGroupExampleInput2\">הכנס טלפון</label>\r\n              <input type=\"text\" class=\"form-control textWhite\" id=\"formGroupExampleInput2\" name=\"phone\"\r\n                     ngModel required>\r\n            </div>\r\n          </div>\r\n\r\n          <div class=\"row\">\r\n            <div class=\"form-group\" class=\"col-lg-6\">\r\n              <label for=\"formGroupExampleInput\">הזן סיסמה</label>\r\n              <input type=\"text\" class=\"form-control textWhite\" id=\"formGroupExampleInput\" name=\"password\" ngModel required>\r\n            </div>\r\n            <div class=\"form-group\" class=\"col-lg-6\">\r\n              <label for=\"formGroupExampleInput2\">הכנס אימייל</label>\r\n              <input type=\"text\" class=\"form-control textWhite\" id=\"formGroupExampleInput2\" name=\"email\" ngModel required email>\r\n            </div>\r\n          </div>\r\n\r\n          <div class=\"row\">\r\n            <div class=\"form-group\" class=\"col-lg-6\">\r\n              <label for=\"formGroupExampleInput\">הכנס אתר אינטרנט</label>\r\n              <input type=\"text\" class=\"form-control textWhite\" id=\"formGroupExampleInput\" name=\"website\"\r\n                     ngModel>\r\n            </div>\r\n            <div class=\"form-group\" class=\"col-lg-6\">\r\n              <label for=\"formGroupExampleInput2\">הכנס WAZE</label>\r\n              <input type=\"text\" class=\"form-control textWhite\" id=\"formGroupExampleInput2\" name=\"waze\"\r\n                     ngModel>\r\n            </div>\r\n          </div>\r\n          <div class=\"row\">\r\n            <div class=\"form-group\" class=\"col-lg-6\">\r\n              <label for=\"formGroupExampleInput\">הכנס פייסבוק</label>\r\n              <input type=\"text\" class=\"form-control textWhite\" id=\"formGroupExampleInput\" name=\"facebook\"\r\n                     ngModel>\r\n            </div>\r\n            <div class=\"form-group\" class=\"col-lg-6\">\r\n              <label for=\"formGroupExampleInput2\">הכנס subDomain</label>\r\n              <input type=\"text\" class=\"form-control textWhite\" id=\"formGroupExampleInput2\" name=\"subdomain\"\r\n                     ngModel required>\r\n            </div>\r\n          </div>\r\n          <div class=\"row\">\r\n            <div class=\"form-group\" class=\"col-lg-12\">\r\n              <label for=\"formGroupExampleInput\">פרטים נוספים</label>\r\n              <textarea  rows=\"4\" cols=\"50\"  class=\"form-control textWhite\" id=\"formGroupExampleInput\" name=\"description\" ngModel required> </textarea>\r\n            </div>\r\n          </div>\r\n          <div class=\"row\">\r\n            <label class=\"uploader\">\r\n              <img [src]=\"imageSrc\"  [class.loaded]=\"imageLoaded\" style=\"width: 100px;\"/>\r\n              <input type=\"file\" name=\"file\" accept=\"image/*\" (change)=\"handleInputChange($event)\">\r\n            </label>\r\n          </div>\r\n          <div class=\"row\" align=\"left\" style=\"float: left; margin-left: 5px\">\r\n            <button [disabled]=\"!f.valid\" type=\"submit\" class=\"btn btn-primary btn-icon loading-demo mr-1 mb-1\"\r\n                    style=\"padding:0 20px !important; cursor: pointer; width: 205px; text-align: center; font-weight: bold; font-size: 16px\">\r\n              <span style=\"text-align: center; padding: 10px;  width: 100% !important;\">שלח טופס</span>\r\n            </button>\r\n          </div>\r\n        </form>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./src/app/cars/add/add.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__("./node_modules/@angular/http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__car_service__ = __webpack_require__("./src/app/cars/car.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AddComponent = (function () {
    function AddComponent(route, http, service, router) {
        this.route = route;
        this.http = http;
        this.service = service;
        this.router = router;
        this.navigateTo = '/company/index';
        this.imageSrc = '';
    }
    AddComponent.prototype.onSubmit = function (form) {
        var _this = this;
        console.log(form.value);
        this.service.AddCompany('AddCompany1', form.value).then(function (data) {
            console.log("AddCompany : ", data);
            _this.router.navigate([_this.navigateTo]);
        });
    };
    AddComponent.prototype.handleInputChange = function (e) {
        var file = e.dataTransfer ? e.dataTransfer.files[0] : e.target.files[0];
        var pattern = /image-*/;
        var reader = new FileReader();
        if (!file.type.match(pattern)) {
            alert('invalid format');
            return;
        }
        reader.onload = this._handleReaderLoaded.bind(this);
        reader.readAsDataURL(file);
        console.log("2 : ", reader);
    };
    AddComponent.prototype._handleReaderLoaded = function (e) {
        var reader = e.target;
        this.imageSrc = reader.result;
        console.log("3 : ", this.imageSrc);
    };
    AddComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-add',
            template: __webpack_require__("./src/app/cars/add/add.component.html"),
            styles: [__webpack_require__("./src/app/cars/add/add.component.css")]
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__car_service__["a" /* CarService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__car_service__["a" /* CarService */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */]) === "function" && _d || Object])
    ], AddComponent);
    return AddComponent;
    var _a, _b, _c, _d;
}());

//# sourceMappingURL=add.component.js.map

/***/ }),

/***/ "./src/app/cars/car.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CarModule", function() { return CarModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common__ = __webpack_require__("./node_modules/@angular/common/@angular/common.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__car_routing__ = __webpack_require__("./src/app/cars/car.routing.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__swimlane_ngx_datatable__ = __webpack_require__("./node_modules/@swimlane/ngx-datatable/release/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__swimlane_ngx_datatable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4__swimlane_ngx_datatable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_http__ = __webpack_require__("./node_modules/@angular/http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__edit_edit_component__ = __webpack_require__("./src/app/cars/edit/edit.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__index_index_component__ = __webpack_require__("./src/app/cars/index/index.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__add_add_component__ = __webpack_require__("./src/app/cars/add/add.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ng_bootstrap_ng_bootstrap__ = __webpack_require__("./node_modules/@ng-bootstrap/ng-bootstrap/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_ng2_file_upload__ = __webpack_require__("./node_modules/ng2-file-upload/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_ng2_file_upload___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_10_ng2_file_upload__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__angular_forms__ = __webpack_require__("./node_modules/@angular/forms/@angular/forms.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__car_service__ = __webpack_require__("./src/app/cars/car.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13_ng_sidebar__ = __webpack_require__("./node_modules/ng-sidebar/lib/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13_ng_sidebar___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_13_ng_sidebar__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};














var CarModule = (function () {
    function CarModule() {
    }
    CarModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_2__angular_common__["CommonModule"],
                __WEBPACK_IMPORTED_MODULE_1__angular_router__["d" /* RouterModule */].forChild(__WEBPACK_IMPORTED_MODULE_3__car_routing__["a" /* CarRoutes */]),
                __WEBPACK_IMPORTED_MODULE_4__swimlane_ngx_datatable__["NgxDatatableModule"],
                __WEBPACK_IMPORTED_MODULE_5__angular_http__["c" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_9__ng_bootstrap_ng_bootstrap__["g" /* NgbModule */],
                __WEBPACK_IMPORTED_MODULE_10_ng2_file_upload__["FileUploadModule"],
                __WEBPACK_IMPORTED_MODULE_11__angular_forms__["FormsModule"],
                __WEBPACK_IMPORTED_MODULE_13_ng_sidebar__["SidebarModule"]
            ],
            declarations: [
                __WEBPACK_IMPORTED_MODULE_6__edit_edit_component__["a" /* EditComponent */],
                __WEBPACK_IMPORTED_MODULE_7__index_index_component__["a" /* IndexComponent */],
                __WEBPACK_IMPORTED_MODULE_8__add_add_component__["a" /* AddComponent */]
            ],
            providers: [__WEBPACK_IMPORTED_MODULE_12__car_service__["a" /* CarService */]]
        })
    ], CarModule);
    return CarModule;
}());

//# sourceMappingURL=car.module.js.map

/***/ }),

/***/ "./src/app/cars/car.routing.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CarRoutes; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__edit_edit_component__ = __webpack_require__("./src/app/cars/edit/edit.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__add_add_component__ = __webpack_require__("./src/app/cars/add/add.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__index_index_component__ = __webpack_require__("./src/app/cars/index/index.component.ts");



var CarRoutes = [{
        path: '',
        children: [{
                path: 'index',
                component: __WEBPACK_IMPORTED_MODULE_2__index_index_component__["a" /* IndexComponent */],
                data: {
                    heading: 'Car'
                }
            }, {
                path: 'edit',
                component: __WEBPACK_IMPORTED_MODULE_0__edit_edit_component__["a" /* EditComponent */],
                data: {
                    heading: 'Edit Car'
                }
            }, {
                path: 'add',
                component: __WEBPACK_IMPORTED_MODULE_1__add_add_component__["a" /* AddComponent */],
                data: {
                    heading: 'Edit Car'
                }
            }]
    }];
/*

component: CompanyComponent,
    data: {
        heading: 'Company',
        removeFooter: true
    },



*/ 
//# sourceMappingURL=car.routing.js.map

/***/ }),

/***/ "./src/app/cars/car.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CarService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_http__ = __webpack_require__("./node_modules/@angular/http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__ = __webpack_require__("./node_modules/rxjs/Rx.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__email_mock_messages__ = __webpack_require__("./src/app/email/mock-messages.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ServerUrl = "http://www.tapper.co.il/salecar/laravel/public/api/";
var CarService = (function () {
    function CarService(http) {
        this.http = http;
        this.headers = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/x-www-form-urlencoded' });
        this.options = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["f" /* RequestOptions */]({ headers: this.headers });
        this.Companies = [];
        this.CompanyArray = [];
        this.Cars = [];
    }
    ;
    CarService.prototype.getMessages = function () {
        return Promise.resolve(__WEBPACK_IMPORTED_MODULE_3__email_mock_messages__["a" /* MESSAGES */]);
    };
    CarService.prototype.GetAllCars = function (url) {
        var _this = this;
        var body = new FormData();
        body.append('id', '26');
        return this.http.post(ServerUrl + '' + url, body).map(function (res) { return res.json(); }).do(function (data) { _this.Cars = data; }).toPromise();
    };
    CarService.prototype.AddCompany = function (url, Company) {
        this.CompanyArray = Company;
        var body = 'company=' + JSON.stringify(this.CompanyArray);
        return this.http.post(ServerUrl + '' + url, body, this.options).map(function (res) { return res; }).do(function (data) { }).toPromise();
    };
    CarService.prototype.EditCompany = function (url, Company) {
        this.CompanyArray = Company;
        var body = 'company=' + JSON.stringify(this.CompanyArray);
        return this.http.post(ServerUrl + '' + url, body, this.options).map(function (res) { return res; }).do(function (data) { }).toPromise();
    };
    CarService.prototype.DeleteCompany = function (url, Id) {
        var body = 'id=' + Id;
        return this.http.post(ServerUrl + '' + url, body, this.options).map(function (res) { return res.json(); }).do(function (data) { }).toPromise();
    };
    CarService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__angular_http__["b" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_http__["b" /* Http */]) === "function" && _a || Object])
    ], CarService);
    return CarService;
    var _a;
}());

;
//# sourceMappingURL=car.service.js.map

/***/ }),

/***/ "./src/app/cars/edit/edit.component.css":
/***/ (function(module, exports) {

module.exports = ".FormClass\r\n{\r\n    text-align: right;\r\n    direction: rtl;\r\n}\r\n\r\n.row\r\n{\r\n    margin-top: 20px;\r\n}\r\n\r\n.textWhite\r\n{\r\n    background-color: white;\r\n}\r\n\r\ninput.ng-invalid.ng-touched\r\n{\r\n    border:1px solid red;\r\n}"

/***/ }),

/***/ "./src/app/cars/edit/edit.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"row FormClass\">\r\n    <div class=\"col-lg-12\">\r\n        <div class=\"card\">\r\n            <div class=\"card-header\">\r\n                ערוך חברה\r\n            </div>\r\n            <div class=\"card-body\">\r\n                <form (ngSubmit)=\"onSubmit(f)\" #f=\"ngForm\">\r\n                    <div class=\"row\">\r\n                        <div class=\"form-group\" class=\"col-lg-12\">\r\n                            <label for=\"formGroupExampleInput\">הכנס שם חברה </label>\r\n                            <input type=\"text\" class=\"form-control textWhite\" [(ngModel)]=\"Company.name\" id=\"formGroupExampleInput\" name=\"name\" ngModel required>\r\n                        </div>\r\n\r\n                    </div>\r\n\r\n                    <div class=\"row\">\r\n                        <div class=\"form-group\" class=\"col-lg-6\">\r\n                            <label for=\"formGroupExampleInput\">הכנס כתובת </label>\r\n                            <input type=\"text\" class=\"form-control textWhite\" [(ngModel)]=\"Company.address\" id=\"formGroupExampleInput\" name=\"address\"\r\n                                   ngModel>\r\n                        </div>\r\n                        <div class=\"form-group\" class=\"col-lg-6\">\r\n                            <label for=\"formGroupExampleInput2\">הכנס טלפון</label>\r\n                            <input type=\"text\" class=\"form-control textWhite\" [(ngModel)]=\"Company.phone\" id=\"formGroupExampleInput2\" name=\"phone\"\r\n                                   ngModel>\r\n                        </div>\r\n                    </div>\r\n\r\n\r\n                    <div class=\"row\">\r\n                        <div class=\"form-group\" class=\"col-lg-6\">\r\n                            <label for=\"formGroupExampleInput\">הזן סיסמה</label>\r\n                            <input type=\"text\" class=\"form-control textWhite\" [(ngModel)]=\"Company.password\" id=\"formGroupExampleInput\" name=\"pass\" ngModel>\r\n                        </div>\r\n                        <div class=\"form-group\" class=\"col-lg-6\">\r\n                            <label for=\"formGroupExampleInput2\">הכנס אימייל</label>\r\n                            <input type=\"text\" class=\"form-control textWhite\" [(ngModel)]=\"Company.email\" id=\"formGroupExampleInput2\" name=\"email\" ngModel required email>\r\n                        </div>\r\n                    </div>\r\n\r\n                    <div class=\"row\">\r\n                        <div class=\"form-group\" class=\"col-lg-6\">\r\n                            <label for=\"formGroupExampleInput\">הכנס אתר אינטרנט</label>\r\n                            <input type=\"text\" class=\"form-control textWhite\" [(ngModel)]=\"Company.website\" id=\"formGroupExampleInput\" name=\"web\"\r\n                                   ngModel>\r\n                        </div>\r\n                        <div class=\"form-group\" class=\"col-lg-6\">\r\n                            <label for=\"formGroupExampleInput2\">הכנס WAZE</label>\r\n                            <input type=\"text\" class=\"form-control textWhite\" [(ngModel)]=\"Company.waze\" id=\"formGroupExampleInput2\" name=\"waze\"\r\n                                   ngModel>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"row\">\r\n                        <div class=\"form-group\" class=\"col-lg-6\">\r\n                            <label for=\"formGroupExampleInput\">הכנס פייסבוק</label>\r\n                            <input type=\"text\" class=\"form-control textWhite\" [(ngModel)]=\"Company.facebook\" id=\"formGroupExampleInput\" name=\"facebook\"\r\n                                   ngModel>\r\n                        </div>\r\n                        <div class=\"form-group\" class=\"col-lg-6\">\r\n                            <label for=\"formGroupExampleInput2\">הכנס subDomain</label>\r\n                            <input type=\"text\" class=\"form-control textWhite\" [(ngModel)]=\"Company.subdomain\" id=\"formGroupExampleInput2\" name=\"sub\"\r\n                                   ngModel>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"row\">\r\n                        <div class=\"form-group\" class=\"col-lg-12\">\r\n                            <label for=\"formGroupExampleInput\">פרטים נוספים</label>\r\n                            <textarea  rows=\"4\" cols=\"50\"  class=\"form-control textWhite\" id=\"formGroupExampleInput\" [(ngModel)]=\"Company.description\" name=\"description\" ngModel required> </textarea>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"row\">\r\n                        <label class=\"uploader\">\r\n                            <img [src]=\"imageSrc\"  [class.loaded]=\"imageLoaded\" style=\"width: 100px;\"/>\r\n                            <input type=\"file\" name=\"file\" accept=\"image/*\" (change)=\"handleInputChange($event)\">\r\n                        </label>\r\n                    </div>\r\n                    <div class=\"row\" align=\"left\" style=\"float: left; margin-left: 5px\">\r\n                        <button [disabled]=\"!f.valid\" type=\"submit\" class=\"btn btn-primary btn-icon loading-demo mr-1 mb-1\"\r\n                                style=\"padding:0 20px !important; cursor: pointer; width: 205px; text-align: center; font-weight: bold; font-size: 16px\">\r\n                            <span style=\"text-align: center; padding: 10px;  width: 100% !important;\">שלח טופס</span>\r\n                        </button>\r\n                    </div>\r\n                </form>\r\n\r\n\r\n\r\n\r\n                <!-- <img src=\"{{imageSrc}}\" style=\"width: 100%\" /> <div class=\"row\" align=\"left\"  style=\"float: left; margin-left: 5px\">\r\n                     <button type=\"button\"  class=\"btn btn-primary btn-icon loading-demo mr-1 mb-1\" style=\"padding:0 20px !important; cursor: pointer; width: 205px; text-align: center; font-weight: bold; font-size: 16px\">\r\n                         <span style=\"text-align: center; padding: 10px;  width: 100% !important;\">שלח טופס</span>\r\n                     </button>\r\n                 </div>-->\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>"

/***/ }),

/***/ "./src/app/cars/edit/edit.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EditComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__("./node_modules/@angular/http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__car_service__ = __webpack_require__("./src/app/cars/car.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var URL = 'https://evening-anchorage-3159.herokuapp.com/api/';
var EditComponent = (function () {
    function EditComponent(route, http, service, router) {
        var _this = this;
        this.route = route;
        this.http = http;
        this.service = service;
        this.router = router;
        this.navigateTo = '/company/index';
        this.imageSrc = '';
        this.Company = [];
        this.route.params.subscribe(function (params) {
            _this.Id = params['id'];
            _this.Company = _this.service.Companies[_this.Id];
            console.log("CP : ", _this.service.Companies);
        });
    }
    EditComponent.prototype.onSubmit = function (form) {
        var _this = this;
        console.log("Edit : ", this.Company);
        this.service.EditCompany('EditCompany', this.Company).then(function (data) {
            console.log("AddCompany : ", data);
            _this.router.navigate([_this.navigateTo]);
        });
    };
    EditComponent.prototype.handleInputChange = function (e) {
        var file = e.dataTransfer ? e.dataTransfer.files[0] : e.target.files[0];
        var pattern = /image-*/;
        var reader = new FileReader();
        if (!file.type.match(pattern)) {
            alert('invalid format');
            return;
        }
        reader.onload = this._handleReaderLoaded.bind(this);
        reader.readAsDataURL(file);
        console.log("2 : ", reader);
    };
    EditComponent.prototype._handleReaderLoaded = function (e) {
        var reader = e.target;
        this.imageSrc = reader.result;
        console.log("3 : ", this.imageSrc);
    };
    EditComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-edit',
            template: __webpack_require__("./src/app/cars/edit/edit.component.html"),
            styles: [__webpack_require__("./src/app/icons/fontawesome/fontawesome.component.scss"), __webpack_require__("./src/app/components/buttons/buttons.component.scss"), __webpack_require__("./src/app/cars/edit/edit.component.css")]
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__car_service__["a" /* CarService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__car_service__["a" /* CarService */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */]) === "function" && _d || Object])
    ], EditComponent);
    return EditComponent;
    var _a, _b, _c, _d;
}());

//# sourceMappingURL=edit.component.js.map

/***/ }),

/***/ "./src/app/cars/index/index.component.css":
/***/ (function(module, exports) {

module.exports = ".card\r\n{\r\n    text-align: right;\r\n    direction: rtl;\r\n}\r\n\r\n.card-body\r\n{\r\n    border-bottom: 1px solid #f2f1f2;\r\n}\r\n\r\n.mr-auto\r\n{\r\n    text-align: right;\r\n    direction: rtl;\r\n    float: right;\r\n}\r\n\r\n.mr-3\r\n{\r\n    background-color: green;\r\n    float: right;\r\n}\r\n\r\n.IconClass\r\n{\r\n    margin-top: 6px;\r\n    text-align: center;\r\n    padding-left: -13px !important;\r\n}\r\n\r\n.d-icon{\r\n    margin-top: -20px;\r\n}\r\n\r\n.card-title\r\n{\r\n    font-size: 16px;\r\n}\r\n\r\n.card-text\r\n{\r\n    margin-top: -10px;\r\n}"

/***/ }),

/***/ "./src/app/cars/index/index.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\" card-body\" style=\"width: 100% !important; overflow: hidden; background-color: #f1f1f1\">\r\n    <div style=\"float:right; width:40%; text-align: right\">\r\n        <h6 class=\"text-uppercase\">הרכבים של רויאל קאר</h6>\r\n        <span>כרגע יש 4 חברות במערכת</span>\r\n    </div>\r\n    <div style=\"float:left; width:30%; \">\r\n        <button [routerLink]=\"['/', 'company' , 'add' ]\" class=\"btn btn-icon btn-facebook mb-1 mr-1 \" style=\"width: 170px; background-color:#3b5998; cursor: pointer; color: white; \">\r\n            <i class=\"fa fa-plus-circle\"></i>\r\n            חזרה לעמוד החברה\r\n        </button>\r\n        <button [routerLink]=\"['/', 'car' , 'add' ]\" class=\"btn btn-icon btn-facebook mb-1 mr-1 \" style=\"width: 140px; background-color:#3b5998; cursor: pointer; color: white; \">\r\n            <i class=\"fa fa-plus-circle\"></i>\r\n            הוסף רכב חדש\r\n        </button>\r\n    </div>\r\n</div>\r\n\r\n\r\n<div class=\"row\" style=\"margin-top: 30px;\">\r\n    <div class=\"col-lg-12\" align=\"center\">\r\n        <div class=\"col-lg-10\">\r\n\r\n            <div class=\"card-deck-wrapper\">\r\n                <div class=\"card-deck\">\r\n                    <div class=\"card\" *ngFor=\"let item of ItemsArray let i=index\">\r\n                        <img class=\"card-img-top img-fluid\" src=\"{{host}}{{item.images[0].url}}\" alt=\"Card image\"/>\r\n                        <div class=\"card-body\">\r\n                            <h5 class=\"card-title\">\r\n                                {{item.carName}} - {{item.carModelName}}\r\n                            </h5>\r\n                            <p class=\"card-text\">\r\n                                {{item.ComapnyName}}\r\n                            </p>\r\n                            <p class=\"card-text\">\r\n                                <small class=\"text-muted\">\r\n                                    שנה: {{item.year}} | יד {{item.hand}} | מנוע {{item.size}} סמ\"ק\r\n                                </small>\r\n                            </p>\r\n                            <hr>\r\n                            <button [routerLink]=\"['/', 'car' , 'edit' , { id: i}]\"\r\n                                    class=\"btn btn-icon btn-facebook mb-1 mr-1 \"\r\n                                    style=\"width:80px; font-size: 10px; cursor: pointer; background-color: #3b5998; color: white\">\r\n                                <i class=\"fa fa-edit\"></i>\r\n                                Edit\r\n                            </button>\r\n                            <button class=\"btn btn-icon btn-instagram mb-1 mr-1 \"\r\n                                    style=\"width:80px; font-size: 10px; background-color:lightgrey; cursor: pointer;\"\r\n                                    (click)=\"DeleteItem(i)\">\r\n                                <i class=\"fa fa-close\"></i>\r\n                                Delete\r\n                            </button>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>"

/***/ }),

/***/ "./src/app/cars/index/index.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return IndexComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__car_service__ = __webpack_require__("./src/app/cars/car.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__settings_settings_service__ = __webpack_require__("./src/settings/settings.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var IndexComponent = (function () {
    function IndexComponent(CarService, settings) {
        var _this = this;
        this.CarService = CarService;
        this.ItemsArray = [];
        this.host = '';
        this.settings = '';
        this.messageOpen = false;
        this.isOpened = true;
        this._autoCollapseWidth = 991;
        this.CarService.GetAllCars('GetAllCars').then(function (data) {
            console.log("getAllCars : ", data),
                _this.ItemsArray = data,
                _this.selectedItem = _this.ItemsArray[0];
            _this.host = settings.host,
                console.log(_this.ItemsArray[0].logo);
        });
    }
    IndexComponent.prototype.DeleteItem = function (i) {
        console.log("Del 1 : ", this.ItemsArray[i].id);
        /*this.CarService.DeleteCompany('DeleteCompany', this.ItemsArray[i].id).then((data: any) => {
            this.ItemsArray = data , console.log("Del 2 : ", data);
        })*/
    };
    IndexComponent.prototype.ngOnInit = function () {
        if (this.isOver()) {
            this.isOpened = false;
        }
    };
    IndexComponent.prototype.ngAfterContentInit = function () {
    };
    IndexComponent.prototype.toogleSidebar = function () {
        this.isOpened = !this.isOpened;
    };
    IndexComponent.prototype.isOver = function () {
        return window.matchMedia("(max-width: 991px)").matches;
    };
    IndexComponent.prototype.onSelect = function (message) {
        this.selectedItem = message;
        if (this.isOver()) {
            this.isOpened = false;
        }
    };
    IndexComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-index',
            template: __webpack_require__("./src/app/cars/index/index.component.html"),
            styles: [__webpack_require__("./src/app/icons/fontawesome/fontawesome.component.scss"), __webpack_require__("./src/app/media/list/list.component.scss"), __webpack_require__("./src/app/email/email.component.scss"), __webpack_require__("./src/app/cars/index/index.component.css")],
            providers: [__WEBPACK_IMPORTED_MODULE_1__car_service__["a" /* CarService */]]
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__car_service__["a" /* CarService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__car_service__["a" /* CarService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__settings_settings_service__["a" /* SettingsService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__settings_settings_service__["a" /* SettingsService */]) === "function" && _b || Object])
    ], IndexComponent);
    return IndexComponent;
    var _a, _b;
}());

//# sourceMappingURL=index.component.js.map

/***/ })

});
//# sourceMappingURL=car.module.chunk.js.map