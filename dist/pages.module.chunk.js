webpackJsonp(["pages.module"],{

/***/ "./src/app/pages/activty/activty.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"card card-body pb-0\">\r\n  <div class=\"activity-stream\">\r\n    <div class=\"card\">\r\n      <div class=\"card-header\">\r\n        <img class=\"card-avatar\" src=\"assets/images/face1.jpg\">\r\n        <div class=\"card-header-text\">\r\n          <div class=\"card-title\">Received a call from Joel</div>\r\n          <div class=\"card-subtitle\">Header subtitle</div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"card\">\r\n      <div class=\"card-header\">\r\n        <div class=\"card-avatar widget-icon rounded-circle bg-orange text-white icon icon-basic-paperplane\"></div>\r\n        <div class=\"card-header-text\">\r\n          <div class=\"card-title\">Header with icon</div>\r\n          <div class=\"card-subtitle\">Header subtitle</div>\r\n        </div>\r\n      </div>\r\n      <div class=\"card-body\">\r\n        <p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Donec id elit non mi porta gravida at eget metus. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit.</p>\r\n        <a href=\"javascript:;\" class=\"mr-1\">Like</a>\r\n        <a href=\"javascript:;\">Comment</a>\r\n      </div>\r\n    </div>\r\n    <div class=\"card\">\r\n      <div class=\"card-header\">\r\n        <img class=\"card-avatar\" src=\"assets/images/face2.jpg\">\r\n        <div class=\"card-header-text\">\r\n          <div class=\"card-title\">Header title</div>\r\n          <div class=\"card-subtitle\">Added a photo</div>\r\n        </div>\r\n      </div>\r\n      <div class=\"card-body\">\r\n        <img src=\"assets/images/unsplash/20.jpg\" style=\"width: 300px;\">\r\n      </div>\r\n    </div>\r\n    <div class=\"card\">\r\n      <div class=\"card-header\">\r\n        <img class=\"card-avatar\" src=\"assets/images/face3.jpg\">\r\n        <div class=\"card-header-text\">\r\n          <div class=\"card-title\">Header title</div>\r\n          <div class=\"card-subtitle\">Header subtitle</div>\r\n        </div>\r\n      </div>\r\n      <div class=\"card-body\">\r\n        <p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Donec id elit non mi porta gravida at eget metus. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit.</p>\r\n      </div>\r\n    </div>\r\n    <div class=\"card\">\r\n      <div class=\"card-header\">\r\n        <img class=\"card-avatar\" src=\"assets/images/face4.jpg\">\r\n        <div class=\"card-header-text\">\r\n          <div class=\"card-title\">Uploaded new file</div>\r\n          <div class=\"card-subtitle\">Header subtitle</div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"card\">\r\n      <div class=\"card-header\">\r\n        <div class=\"card-avatar widget-icon rounded-circle bg-purple text-white icon icon-basic-accelerator\"></div>\r\n        <div class=\"card-header-text\">\r\n          <div class=\"card-title\">Header with icon</div>\r\n          <div class=\"card-subtitle\">Header subtitle</div>\r\n        </div>\r\n      </div>\r\n      <div class=\"card-body\">\r\n        <p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Donec id elit non mi porta gravida at eget metus. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit.</p>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./src/app/pages/activty/activty.component.scss":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/pages/activty/activty.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ActivtyComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var ActivtyComponent = (function () {
    function ActivtyComponent() {
    }
    ActivtyComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-activty',
            template: __webpack_require__("./src/app/pages/activty/activty.component.html"),
            styles: [__webpack_require__("./src/app/pages/activty/activty.component.scss")]
        })
    ], ActivtyComponent);
    return ActivtyComponent;
}());

//# sourceMappingURL=activty.component.js.map

/***/ }),

/***/ "./src/app/pages/blank/blank.component.html":
/***/ (function(module, exports) {

module.exports = "<p>\r\n  blank works!\r\n</p>\r\n"

/***/ }),

/***/ "./src/app/pages/blank/blank.component.scss":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/pages/blank/blank.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BlankComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var BlankComponent = (function () {
    function BlankComponent() {
    }
    BlankComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-blank',
            template: __webpack_require__("./src/app/pages/blank/blank.component.html"),
            styles: [__webpack_require__("./src/app/pages/blank/blank.component.scss")]
        })
    ], BlankComponent);
    return BlankComponent;
}());

//# sourceMappingURL=blank.component.js.map

/***/ }),

/***/ "./src/app/pages/forum/forum.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"card card-body mb-0\">\r\n  <h6 class=\"text-uppercase\">Welcome to the forums</h6>\r\n  <span>Currently viewing 10 topics</span>\r\n</div>\r\n\r\n<div class=\"p-3\">\r\n  <div class=\"card\">\r\n\r\n    <div class=\"card-header\">\r\n      <div class=\"card-header-text w-100\">\r\n        <div class=\"card-title\">\r\n          General Topics\r\n        </div>\r\n        <div class=\"card-subtitle text-capitalize ff-sans\">\r\n          Total posts: 73,564\r\n        </div>\r\n      </div>\r\n    </div>\r\n\r\n    <div class=\"card-body\">\r\n      <div class=\"d-flex align-items-center\">\r\n        <div class=\"mr-3\">\r\n          <div class=\"widget-icon rounded-circle bg-red text-white icon icon-basic-headset\"></div>\r\n        </div>\r\n        <div class=\"mr-auto\">\r\n          <a href=\"javascript:;\" class=\"ff-headers text-color\">Forum Rules</a>\r\n          <div>Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Maecenas faucibus mollis interdum.</div>\r\n        </div>\r\n        <div class=\"d-flex\">\r\n          <div class=\"forum-stat\">\r\n            <h6>890</h6>\r\n            <small class=\"d-block\">Views</small>\r\n          </div>\r\n          <div class=\"forum-stat\">\r\n            <h6>465</h6>\r\n            <small class=\"d-block\">Topics</small>\r\n          </div>\r\n          <div class=\"forum-stat\">\r\n            <h6>375</h6>\r\n            <small class=\"d-block\">Replies</small>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n\r\n    <div class=\"card-body\">\r\n      <div class=\"d-flex align-items-center\">\r\n        <div class=\"mr-3\">\r\n          <div class=\"widget-icon rounded-circle bg-purple text-white icon icon-basic-elaboration-message-check\"></div>\r\n        </div>\r\n        <div class=\"mr-auto\">\r\n          <a href=\"javascript:;\" class=\"ff-headers text-color\">Official Announcements</a>\r\n          <div>Nulla vitae elit libero, a pharetra augue. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.</div>\r\n        </div>\r\n        <div class=\"d-flex\">\r\n          <div class=\"forum-stat\">\r\n            <h6>46</h6>\r\n            <small class=\"d-block\">Views</small>\r\n          </div>\r\n          <div class=\"forum-stat\">\r\n            <h6>78</h6>\r\n            <small class=\"d-block\">Topics</small>\r\n          </div>\r\n          <div class=\"forum-stat\">\r\n            <h6>22</h6>\r\n            <small class=\"d-block\">Replies</small>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n\r\n    <div class=\"card-body\">\r\n      <div class=\"d-flex align-items-center\">\r\n        <div class=\"mr-3\">\r\n          <div class=\"widget-icon rounded-circle bg-brown text-white icon icon-basic-elaboration-message-plus\"></div>\r\n        </div>\r\n        <div class=\"mr-auto\">\r\n          <a href=\"javascript:;\" class=\"ff-headers text-color\">Technical Announcements</a>\r\n          <div>Nullam quis risus eget urna mollis ornare vel eu leo. Sed posuere consectetur est at lobortis.</div>\r\n        </div>\r\n        <div class=\"d-flex\">\r\n          <div class=\"forum-stat\">\r\n            <h6>567</h6>\r\n            <small class=\"d-block\">Views</small>\r\n          </div>\r\n          <div class=\"forum-stat\">\r\n            <h6>686</h6>\r\n            <small class=\"d-block\">Topics</small>\r\n          </div>\r\n          <div class=\"forum-stat\">\r\n            <h6>982</h6>\r\n            <small class=\"d-block\">Replies</small>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n\r\n    <div class=\"card-body\">\r\n      <div class=\"d-flex align-items-center\">\r\n        <div class=\"mr-3\">\r\n          <div class=\"widget-icon rounded-circle bg-indigo text-white icon icon-basic-elaboration-mail-next\"></div>\r\n        </div>\r\n        <div class=\"mr-auto\">\r\n          <a href=\"javascript:;\" class=\"ff-headers text-color\">Marketing</a>\r\n          <div>Nulla vitae elit libero, a pharetra augue. Cras justo odio, dapibus ac facilisis in, egestas eget quam.</div>\r\n        </div>\r\n        <div class=\"d-flex\">\r\n          <div class=\"forum-stat\">\r\n            <h6>4653</h6>\r\n            <small class=\"d-block\">Views</small>\r\n          </div>\r\n          <div class=\"forum-stat\">\r\n            <h6>6785</h6>\r\n            <small class=\"d-block\">Topics</small>\r\n          </div>\r\n          <div class=\"forum-stat\">\r\n            <h6>9456</h6>\r\n            <small class=\"d-block\">Replies</small>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n\r\n  </div>\r\n\r\n  <div class=\"card\">\r\n\r\n    <div class=\"card-header\">\r\n      <div class=\"card-header-text w-100\">\r\n        <div class=\"card-title\">\r\n          Client Topics\r\n        </div>\r\n        <div class=\"card-subtitle text-capitalize ff-sans\">\r\n          Total posts: 78,548\r\n        </div>\r\n      </div>\r\n    </div>\r\n\r\n    <div class=\"card-body\">\r\n      <div class=\"d-flex align-items-center\">\r\n        <div class=\"mr-3\">\r\n          <div class=\"widget-icon rounded-circle bg-grey text-white icon icon-basic-accelerator\"></div>\r\n        </div>\r\n        <div class=\"mr-auto\">\r\n          <a href=\"javascript:;\" class=\"ff-headers text-color\">Wordpress support</a>\r\n          <div>Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Maecenas faucibus mollis interdum.</div>\r\n        </div>\r\n        <div class=\"d-flex\">\r\n          <div class=\"forum-stat\">\r\n            <h6>890</h6>\r\n            <small class=\"d-block\">Views</small>\r\n          </div>\r\n          <div class=\"forum-stat\">\r\n            <h6>465</h6>\r\n            <small class=\"d-block\">Topics</small>\r\n          </div>\r\n          <div class=\"forum-stat\">\r\n            <h6>375</h6>\r\n            <small class=\"d-block\">Replies</small>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n\r\n    <div class=\"card-body\">\r\n      <div class=\"d-flex align-items-center\">\r\n        <div class=\"mr-3\">\r\n          <div class=\"widget-icon rounded-circle bg-grey-400 text-white icon icon-basic-gear\"></div>\r\n        </div>\r\n        <div class=\"mr-auto\">\r\n          <a href=\"javascript:;\" class=\"ff-headers text-color\">HTML Templates</a>\r\n          <div>Nulla vitae elit libero, a pharetra augue. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.</div>\r\n        </div>\r\n        <div class=\"d-flex\">\r\n          <div class=\"forum-stat\">\r\n            <h6>46</h6>\r\n            <small class=\"d-block\">Views</small>\r\n          </div>\r\n          <div class=\"forum-stat\">\r\n            <h6>78</h6>\r\n            <small class=\"d-block\">Topics</small>\r\n          </div>\r\n          <div class=\"forum-stat\">\r\n            <h6>22</h6>\r\n            <small class=\"d-block\">Replies</small>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n\r\n    <div class=\"card-body\">\r\n      <div class=\"d-flex align-items-center\">\r\n        <div class=\"mr-3\">\r\n          <div class=\"widget-icon rounded-circle bg-grey-300 text-white icon icon-basic-lock\"></div>\r\n        </div>\r\n        <div class=\"mr-auto\">\r\n          <a href=\"javascript:;\" class=\"ff-headers text-color\">UI/UX Designs</a>\r\n          <div>Nullam quis risus eget urna mollis ornare vel eu leo. Sed posuere consectetur est at lobortis.</div>\r\n        </div>\r\n        <div class=\"d-flex\">\r\n          <div class=\"forum-stat\">\r\n            <h6>567</h6>\r\n            <small class=\"d-block\">Views</small>\r\n          </div>\r\n          <div class=\"forum-stat\">\r\n            <h6>686</h6>\r\n            <small class=\"d-block\">Topics</small>\r\n          </div>\r\n          <div class=\"forum-stat\">\r\n            <h6>982</h6>\r\n            <small class=\"d-block\">Replies</small>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n\r\n    <div class=\"card-body\">\r\n      <div class=\"d-flex align-items-center\">\r\n        <div class=\"mr-3\">\r\n          <div class=\"widget-icon rounded-circle bg-green text-white icon icon-basic-elaboration-mail-next\"></div>\r\n        </div>\r\n        <div class=\"mr-auto\">\r\n          <a href=\"javascript:;\" class=\"ff-headers text-color\">Customizations</a>\r\n          <div>Nulla vitae elit libero, a pharetra augue. Cras justo odio, dapibus ac facilisis in, egestas eget quam.</div>\r\n        </div>\r\n        <div class=\"d-flex\">\r\n          <div class=\"forum-stat\">\r\n            <h6>4653</h6>\r\n            <small class=\"d-block\">Views</small>\r\n          </div>\r\n          <div class=\"forum-stat\">\r\n            <h6>6785</h6>\r\n            <small class=\"d-block\">Topics</small>\r\n          </div>\r\n          <div class=\"forum-stat\">\r\n            <h6>9456</h6>\r\n            <small class=\"d-block\">Replies</small>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/pages/forum/forum.component.scss":
/***/ (function(module, exports) {

module.exports = ".forum-stat {\n  width: 70px;\n  text-align: center; }\n\n:host {\n  padding: 0 !important; }\n"

/***/ }),

/***/ "./src/app/pages/forum/forum.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ForumComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var ForumComponent = (function () {
    function ForumComponent() {
    }
    ForumComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-forum',
            template: __webpack_require__("./src/app/pages/forum/forum.component.html"),
            styles: [__webpack_require__("./src/app/pages/forum/forum.component.scss")]
        })
    ], ForumComponent);
    return ForumComponent;
}());

//# sourceMappingURL=forum.component.js.map

/***/ }),

/***/ "./src/app/pages/invoice/invoice.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"card\">\r\n  <div class=\"card-body\">\r\n    <img src=\"assets/images/logo.png\" class=\"avatar-xs mb-3\" alt=\"\">\r\n\r\n    <div class=\"row mb-3\">\r\n      <div class=\"col\">\r\n        <h5><strong>Invoice #0001</strong></h5>\r\n        <p class=\"mb-0\">Issued on 01 Jun 2016</p>\r\n        <p class=\"mb-0\">Payment due by 31 May 2016</p>\r\n      </div>\r\n    </div>\r\n\r\n    <div class=\"row mb-3\">\r\n      <div class=\"col\">\r\n        <p class=\"mb-0\">Company LLC</p>\r\n        <p class=\"mb-0\">company@address.com</p>\r\n        <p class=\"mb-0\">+012 345 678 90</p>\r\n      </div>\r\n    </div>\r\n\r\n    <div class=\"mt-5 mb-5\">\r\n      <p class=\"mb-0\"><strong>Client Details</strong></p>\r\n      <p class=\"mb-0\">John Smith</p>\r\n      <p class=\"mb-0\">1234 Main</p>\r\n      <p class=\"mb-0\">Apt. 4B</p>\r\n      <p class=\"mb-0\">Springfield, ST 54321</p>\r\n    </div>\r\n  </div>\r\n    <div class=\"table-responsive mb-5\">\r\n      <table class=\"table table-hover mb-0 align-middle\">\r\n        <thead class=\"thead-inverse\">\r\n          <tr>\r\n            <th>Description</th>\r\n            <th>Unit Price</th>\r\n            <th>Quantity</th>\r\n            <th>Amount</th>\r\n          </tr>\r\n        </thead>\r\n        <tbody>\r\n          <tr *ngFor=\"let item of invoiceItems\">\r\n            <td>\r\n              <strong>{{item.title}}</strong><br/>\r\n              <span class=\"text-muted\">{{item.subtitle}}</span>\r\n            </td>\r\n            <td>{{item.price | currency:'USD':true:'1.2-2'}}</td>\r\n            <td>{{item.quantity}}</td>\r\n            <td>{{ (item.price * item.quantity) | currency:'USD':true:'1.2-2'}}</td>\r\n          </tr>\r\n        </tbody>\r\n      </table>\r\n    </div>\r\n  <div class=\"card-body\">\r\n    \r\n    <div class=\"row mb-5\">\r\n      <div class=\"col mt-1 mb-1\">\r\n        <p class=\"text-uppercase m-0\"><strong>Subtotal</strong></p>\r\n        <h3 class=\"m-0\">{{getSubTotal() | currency:'USD':true:'1.2-2'}}</h3>\r\n      </div>\r\n      <div class=\"col mt-1 mb-1 text-sm-right text-md-left\">\r\n        <p class=\"text-uppercase m-0\"><strong>Tax (15%)</strong></p>\r\n        <h3 class=\"m-0\">{{getCalculatedTax() | currency:'USD':true:'1.2-2'}}</h3>\r\n      </div>\r\n      <div class=\"col mt-1 mb-1\">\r\n        <p class=\"text-uppercase m-0\"><strong>Discount</strong></p>\r\n        <h3 class=\"m-0\">$0.00</h3>\r\n      </div>\r\n      <div class=\"col mt-1 mb-1 text-xs-left text-sm-right\">\r\n        <p class=\"text-uppercase m-0\"><strong>Total</strong></p>\r\n        <h3 class=\"m-0 text-success\">{{ getTotal() | currency:'USD':true:'1.2-2'}}</h3>\r\n      </div>\r\n    </div>\r\n\r\n    <small class=\"p-t-2\">\r\n      <strong>\r\n        PAYMENT TERMS AND POLICIES\r\n      </strong>\r\n      All accounts are to be paid within 7 days from receipt of invoice. To be paid by cheque or credit card or direct payment online. If account is not paid within 7 days the credits details supplied as confirmation of work undertaken will be charged the agreed\r\n      quoted fee noted above. If the Invoice remails unpaid. our dept recovery agency, Urban, may charge you a fee of 25% of the unpaid portion of the\r\n      invoice amount and other legal and collection costs not covered by the fee.\r\n    </small>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./src/app/pages/invoice/invoice.component.scss":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/pages/invoice/invoice.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InvoiceComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var InvoiceComponent = (function () {
    function InvoiceComponent() {
        this.invoiceItems = [{
                'title': 'Maintanance',
                'subtitle': 'Monthly web updates for http://www.themeforest.net',
                'price': 250.00,
                'quantity': 1
            }, {
                'title': 'Recurring Bill (Hosting)',
                'subtitle': 'Monthly dedicated VPN web hosting (1 Jan - 30 Jan, 2014)',
                'price': 652.87,
                'quantity': 3
            }, {
                'title': 'Recurring Bill (Domain)',
                'subtitle': '2 year domain name purchase',
                'price': 239.00,
                'quantity': 3
            }, {
                'title': 'Web design',
                'subtitle': 'PSD to HTML Conversion (3 pages)',
                'price': 958.00,
                'quantity': 1
            }];
        this.invoiceTotals = [{
                'subtotal': this.getSubTotal(),
                'tax': this.getCalculatedTax(),
                'discount': 0.00,
                'total': 0
            }];
    }
    InvoiceComponent.prototype.getSubTotal = function () {
        var total = 0.00;
        for (var i = 1; i < this.invoiceItems.length; i++) {
            total += (this.invoiceItems[i].price * this.invoiceItems[i].quantity);
        }
        return total;
    };
    InvoiceComponent.prototype.getCalculatedTax = function () {
        return ((15 * this.getSubTotal()) / 100);
    };
    InvoiceComponent.prototype.getTotal = function () {
        return (this.getSubTotal() + this.getCalculatedTax());
    };
    InvoiceComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-invoice',
            template: __webpack_require__("./src/app/pages/invoice/invoice.component.html"),
            styles: [__webpack_require__("./src/app/pages/invoice/invoice.component.scss")]
        })
    ], InvoiceComponent);
    return InvoiceComponent;
}());

//# sourceMappingURL=invoice.component.js.map

/***/ }),

/***/ "./src/app/pages/pages.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PagesModule", function() { return PagesModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common__ = __webpack_require__("./node_modules/@angular/common/@angular/common.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pages_routing__ = __webpack_require__("./src/app/pages/pages.routing.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__invoice_invoice_component__ = __webpack_require__("./src/app/pages/invoice/invoice.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__timeline_timeline_component__ = __webpack_require__("./src/app/pages/timeline/timeline.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pricing_pricing_component__ = __webpack_require__("./src/app/pages/pricing/pricing.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__forum_forum_component__ = __webpack_require__("./src/app/pages/forum/forum.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__activty_activty_component__ = __webpack_require__("./src/app/pages/activty/activty.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__blank_blank_component__ = __webpack_require__("./src/app/pages/blank/blank.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};










var PagesModule = (function () {
    function PagesModule() {
    }
    PagesModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_2__angular_common__["CommonModule"],
                __WEBPACK_IMPORTED_MODULE_1__angular_router__["d" /* RouterModule */].forChild(__WEBPACK_IMPORTED_MODULE_3__pages_routing__["a" /* PagesRoutes */])
            ],
            declarations: [__WEBPACK_IMPORTED_MODULE_4__invoice_invoice_component__["a" /* InvoiceComponent */], __WEBPACK_IMPORTED_MODULE_5__timeline_timeline_component__["a" /* TimelineComponent */], __WEBPACK_IMPORTED_MODULE_6__pricing_pricing_component__["a" /* PricingComponent */], __WEBPACK_IMPORTED_MODULE_7__forum_forum_component__["a" /* ForumComponent */], __WEBPACK_IMPORTED_MODULE_8__activty_activty_component__["a" /* ActivtyComponent */], __WEBPACK_IMPORTED_MODULE_9__blank_blank_component__["a" /* BlankComponent */]]
        })
    ], PagesModule);
    return PagesModule;
}());

//# sourceMappingURL=pages.module.js.map

/***/ }),

/***/ "./src/app/pages/pages.routing.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PagesRoutes; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__invoice_invoice_component__ = __webpack_require__("./src/app/pages/invoice/invoice.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__timeline_timeline_component__ = __webpack_require__("./src/app/pages/timeline/timeline.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__activty_activty_component__ = __webpack_require__("./src/app/pages/activty/activty.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pricing_pricing_component__ = __webpack_require__("./src/app/pages/pricing/pricing.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__forum_forum_component__ = __webpack_require__("./src/app/pages/forum/forum.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__blank_blank_component__ = __webpack_require__("./src/app/pages/blank/blank.component.ts");






var PagesRoutes = [
    {
        path: '',
        children: [{
                path: 'invoice',
                component: __WEBPACK_IMPORTED_MODULE_0__invoice_invoice_component__["a" /* InvoiceComponent */],
                data: {
                    heading: 'Invoice'
                }
            }, {
                path: 'forum',
                component: __WEBPACK_IMPORTED_MODULE_4__forum_forum_component__["a" /* ForumComponent */],
                data: {
                    heading: 'Forum'
                }
            }, {
                path: 'timeline',
                component: __WEBPACK_IMPORTED_MODULE_1__timeline_timeline_component__["a" /* TimelineComponent */],
                data: {
                    heading: 'Timeline'
                }
            }, {
                path: 'activity',
                component: __WEBPACK_IMPORTED_MODULE_2__activty_activty_component__["a" /* ActivtyComponent */],
                data: {
                    heading: 'Activity'
                }
            }, {
                path: 'pricing',
                component: __WEBPACK_IMPORTED_MODULE_3__pricing_pricing_component__["a" /* PricingComponent */],
                data: {
                    heading: 'Pricing'
                }
            }, {
                path: 'blank',
                component: __WEBPACK_IMPORTED_MODULE_5__blank_blank_component__["a" /* BlankComponent */],
                data: {
                    heading: 'Blank'
                }
            }]
    }
];
//# sourceMappingURL=pages.routing.js.map

/***/ }),

/***/ "./src/app/pages/pricing/pricing.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"text-ce-center\">\r\n  <label class=\"switch m-b\">\r\n    <div class=\"slide-toggle-bar\">\r\n      <input type=\"checkbox\" checked>\r\n      <span></span>\r\n    </div>\r\n    <span class=\"slide-toggle-content\">Pay annual save up to 25%</span>\r\n  </label>\r\n</div>\r\n\r\n<div class=\"card-group pricing\">\r\n  <div class=\"card\">\r\n    <div class=\"card-header\">\r\n      <div class=\"card-header-text w-100\">\r\n        <div class=\"card-title\">Developer</div>\r\n        <div class=\"card-subtitle\">For testing purposes</div>\r\n      </div>\r\n    </div>\r\n    <div class=\"card-body\">\r\n      <h1>FREE</h1>\r\n      <ul class=\"plan-features\">\r\n        <li>Secure storage</li>\r\n        <li>Limited to 1 user</li>\r\n        <li>Data analytics</li>\r\n        <li class=\"plan-feature-inactive text-muted\">Full search access</li>\r\n        <li class=\"plan-feature-inactive text-muted\">Automatic backups</li>\r\n      </ul>\r\n      <button class=\"btn btn-primary\">Choose plan</button>\r\n    </div>\r\n  </div>\r\n  <div class=\"card\">\r\n    <div class=\"card-header\">\r\n      <div class=\"card-header-text w-100\">\r\n        <div class=\"card-title\">Basic</div>\r\n        <div class=\"card-subtitle\">For freelancers</div>\r\n      </div>\r\n    </div>\r\n    <div class=\"card-body\">\r\n      <h1>\r\n        <span class=\"plan-price text-primary\">\r\n          <span class=\"plan-price-symbol\">$</span>\r\n          <span>10</span>\r\n          <span class=\"plan-price-period\">/ month</span>\r\n        </span>\r\n      </h1>\r\n      <ul class=\"plan-features\">\r\n        <li>Secure storage</li>\r\n        <li>5 concurrent users</li>\r\n        <li>Data analytics</li>\r\n        <li class=\"plan-feature-inactive text-muted\">Full search access</li>\r\n        <li class=\"plan-feature-inactive text-muted\">Automatic backups</li>\r\n      </ul>\r\n      <button class=\"btn btn-primary\">Choose plan</button>\r\n    </div>\r\n  </div>\r\n  <div class=\"card\">\r\n    <div class=\"card-header\">\r\n      <div class=\"card-header-text w-100\">\r\n        <div class=\"card-title\">Managed</div>\r\n        <div class=\"card-subtitle\">For small businesses</div>\r\n      </div>\r\n    </div>\r\n    <div class=\"card-body\">\r\n      <h1>\r\n        <span class=\"plan-price text-primary\">\r\n          <span class=\"plan-price-symbol\">$</span>\r\n          <span>49</span>\r\n          <span class=\"plan-price-period\">/ month</span>\r\n        </span>\r\n      </h1>\r\n      <ul class=\"plan-features\">\r\n        <li>Secure storage</li>\r\n        <li>Unlimited users</li>\r\n        <li>Data analytics</li>\r\n        <li>Full search access</li>\r\n        <li class=\"plan-feature-inactive text-muted\">Automatic backups</li>\r\n      </ul>\r\n      <button class=\"btn btn-primary\">Choose plan</button>\r\n    </div>\r\n  </div>\r\n  <div class=\"card\">\r\n    <div class=\"card-header\">\r\n      <div class=\"card-header-text w-100\">\r\n        <div class=\"card-title\">Enterprise</div>\r\n        <div class=\"card-subtitle\">For large businesses</div>\r\n      </div>\r\n    </div>\r\n    <div class=\"card-body\">\r\n      <h1>\r\n        <span class=\"plan-price text-primary\">\r\n          <span class=\"plan-price-symbol\">$</span>\r\n          <span>99</span>\r\n          <span class=\"plan-price-period\">/ month</span>\r\n        </span>\r\n      </h1>\r\n      <ul class=\"plan-features\">\r\n        <li>Secure storage</li>\r\n        <li>Unlimited users</li>\r\n        <li>Data analytics</li>\r\n        <li>Full search access</li>\r\n        <li>Automatic backups</li>\r\n      </ul>\r\n      <button class=\"btn btn-primary\">Choose plan</button>\r\n    </div>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/pages/pricing/pricing.component.scss":
/***/ (function(module, exports) {

module.exports = "/* $colors\r\n ------------------------------------------*/\n.pricing {\n  padding-top: 1rem;\n  padding-bottom: 1rem;\n  text-align: center; }\n.pricing .card {\n    border: 1px solid theme-colors(\"primary\");\n    margin-left: -1px; }\n.pricing.card-group .card + .card {\n    margin-left: -1px;\n    border-left: 1px solid theme-colors(\"primary\"); }\n.pricing .pricing-plan {\n    text-align: center;\n    overflow: hidden;\n    cursor: default; }\n.pricing .plan-price {\n    font-size: 2.5rem;\n    font-weight: 900;\n    position: relative;\n    overflow: hidden;\n    white-space: nowrap;\n    text-overflow: ellipsis; }\n.pricing .plan-price-symbol {\n    font-size: 1rem;\n    vertical-align: super; }\n.pricing .plan-price-period {\n    font-size: 0.8125rem;\n    display: inline-block;\n    padding: 0;\n    opacity: .7; }\n.pricing .plan-title {\n    font-size: 0.8125rem;\n    font-weight: 500;\n    overflow: hidden;\n    margin-bottom: 1rem;\n    white-space: nowrap;\n    text-transform: uppercase;\n    text-overflow: ellipsis; }\n.pricing .plan-features {\n    line-height: 2.5;\n    margin: 0;\n    padding: 1rem;\n    list-style: none; }\n.pricing .plan-features li {\n      overflow: hidden;\n      white-space: nowrap;\n      text-overflow: ellipsis; }\n.switch {\n  display: inline-block;\n  height: 24px;\n  line-height: 24px;\n  white-space: nowrap;\n  -webkit-user-select: none;\n     -moz-user-select: none;\n      -ms-user-select: none;\n          user-select: none; }\n.switch .slide-toggle-bar {\n    display: inline-block;\n    margin-right: 8px;\n    margin-left: 0;\n    position: relative;\n    top: 5px;\n    width: 36px;\n    height: 14px;\n    border-radius: 8px; }\n.switch input {\n    position: absolute;\n    left: -99999px; }\n.switch input + span {\n    position: relative;\n    display: block;\n    width: 36px;\n    height: 14px;\n    border-radius: 8px;\n    border: none;\n    background-color: #B2B2B2;\n    background-color: rgba(0, 0, 0, 0.26);\n    -webkit-transition: background-color linear .08s;\n    transition: background-color linear .08s; }\n.switch input + span:before {\n    content: '';\n    display: inline-block; }\n.switch input + span:after {\n    content: '';\n    position: absolute;\n    top: -3px;\n    height: 20px;\n    width: 20px;\n    border-radius: 50%;\n    -webkit-box-shadow: 0 1px 5px 0 rgba(0, 0, 0, 0.4), 0px 0px 0px -1px rgba(0, 0, 0, 0.5);\n            box-shadow: 0 1px 5px 0 rgba(0, 0, 0, 0.4), 0px 0px 0px -1px rgba(0, 0, 0, 0.5);\n    background-color: #FAFAFA;\n    border: none;\n    -webkit-transition: background-color linear .08s, -webkit-transform linear .08s;\n    transition: background-color linear .08s, -webkit-transform linear .08s;\n    transition: transform linear .08s, background-color linear .08s;\n    transition: transform linear .08s, background-color linear .08s, -webkit-transform linear .08s; }\n.switch input:checked + span {\n    background-color: rgba(255, 193, 7, 0.5); }\n.switch input:checked + span:after {\n    background-color: #ffc107;\n    -webkit-transform: translate3d(18px, 0, 0);\n            transform: translate3d(18px, 0, 0);\n    -webkit-box-shadow: 0 1px 5px 0 rgba(0, 0, 0, 0.4), 0px 0px 0px -1px rgba(0, 147, 133, 0.5);\n            box-shadow: 0 1px 5px 0 rgba(0, 0, 0, 0.4), 0px 0px 0px -1px rgba(0, 147, 133, 0.5); }\n.switch {\n  display: table;\n  margin: 0 auto; }\n"

/***/ }),

/***/ "./src/app/pages/pricing/pricing.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PricingComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var PricingComponent = (function () {
    function PricingComponent() {
    }
    PricingComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-pricing',
            template: __webpack_require__("./src/app/pages/pricing/pricing.component.html"),
            styles: [__webpack_require__("./src/app/pages/pricing/pricing.component.scss")]
        })
    ], PricingComponent);
    return PricingComponent;
}());

//# sourceMappingURL=pricing.component.js.map

/***/ }),

/***/ "./src/app/pages/timeline/timeline.component.html":
/***/ (function(module, exports) {

module.exports = "<ul class=\"timeline\" [ngClass]=\"{'stacked': stacked}\">\r\n  <li class=\"timeline-card timeline-button\">\r\n    <button class=\"btn btn-secondary\" [ngClass]=\"{'active': !stacked}\" (click)=\"stacked = !stacked\">Toggle</button>\r\n  </li>\r\n  <li class=\"timeline-card\">\r\n    <div class=\"timeline-icon bg-grey\"></div>\r\n    <section class=\"timeline-content\">\r\n      <div class=\"card\">\r\n        <div class=\"card-body\">\r\n          <p class=\"mb-0\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nullam id dolor id nibh ultricies vehicula ut id elit. Maecenas faucibus mollis interdum.</p>\r\n        </div>\r\n      </div>\r\n      <div class=\"timeline-date\">13:12 am</div>\r\n    </section>\r\n  </li>\r\n  <li class=\"timeline-card\">\r\n    <div class=\"timeline-icon bg-purple\"></div>\r\n    <section class=\"timeline-content\">\r\n      <div class=\"card\">\r\n        <div class=\"card-body\">\r\n          <p class=\"mb-0\">Aenean lacinia bibendum nulla sed consectetur. Nullam id dolor id nibh ultricies vehicula ut id elit. Integer posuere erat a ante venenatis dapibus posuere velit aliquet.</p>\r\n        </div>\r\n      </div>\r\n      <div class=\"timeline-date\">Yesterday</div>\r\n    </section>\r\n  </li>\r\n  <li class=\"timeline-card timeline-button\">\r\n    <button class=\"btn btn-danger\">2016</button>\r\n  </li>\r\n  <li class=\"timeline-card timeline-button\"></li>\r\n  <li class=\"timeline-card\">\r\n    <div class=\"timeline-icon bg-green\"></div>\r\n    <section class=\"timeline-content\">\r\n      <div class=\"card\">\r\n        <div class=\"card-body\">\r\n          <p class=\"mb-0\"> Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>\r\n        </div>\r\n      </div>\r\n      <div class=\"timeline-date\">6 Days Ago</div>\r\n    </section>\r\n  </li>\r\n  <li class=\"timeline-card\">\r\n    <div class=\"timeline-icon bg-lime\"></div>\r\n    <section class=\"timeline-content\">\r\n      <div class=\"card\">\r\n        <div class=\"card-body\">\r\n          <p class=\"mb-0\">3 more people joined your campaign.</p>\r\n        </div>\r\n      </div>\r\n      <div class=\"timeline-date\">2 Months Ago</div>\r\n    </section>\r\n  </li>\r\n  <li class=\"timeline-card\">\r\n    <div class=\"timeline-icon bg-brown\"></div>\r\n    <section class=\"timeline-content\">\r\n      <div class=\"card\">\r\n        <div class=\"card-body\">\r\n          <p class=\"mb-0\">Six new friend requests</p>\r\n        </div>\r\n      </div>\r\n      <div class=\"timeline-date\">23 September 2013</div>\r\n    </section>\r\n  </li>\r\n  <li class=\"timeline-card timeline-button\">\r\n    <button class=\"btn btn-secondary\">2015</button>\r\n  </li>\r\n</ul>\r\n"

/***/ }),

/***/ "./src/app/pages/timeline/timeline.component.scss":
/***/ (function(module, exports) {

module.exports = "/* $colors\r\n ------------------------------------------*/\n.timeline {\n  position: relative;\n  margin: 0.33333333rem;\n  padding: 0;\n  list-style: none; }\n.timeline::before {\n    position: absolute;\n    top: 6px;\n    width: 4px;\n    height: 100%;\n    content: '';\n    background: rgba(0, 0, 0, 0.06);\n    left: 4px; }\n.timeline .timeline-icon {\n    position: absolute;\n    top: 15px;\n    width: 12px;\n    height: 12px;\n    text-align: center;\n    left: 0;\n    border-radius: 50%; }\n.timeline .timeline-icon::after {\n      content: '';\n      position: absolute;\n      top: 2px;\n      left: 2px;\n      width: 8px;\n      height: 8px;\n      border-radius: 50%;\n      background: white; }\n.timeline .timeline-card {\n    position: relative;\n    margin: 40px 0; }\n.timeline .timeline-card::after {\n      display: block;\n      clear: both;\n      content: \"\"; }\n.timeline .timeline-card:first-child .timeline-icon {\n      top: 0; }\n.timeline .timeline-card:last-child .timeline-icon {\n      top: 0; }\n.timeline .timeline-card:first-child {\n    margin-top: 0; }\n.timeline .timeline-content {\n    position: relative;\n    margin-left: 30px; }\n.timeline .timeline-content::after {\n      display: block;\n      clear: both;\n      content: \"\"; }\n.timeline .timeline-content > .card {\n      float: left;\n      display: inline-block;\n      margin: 0; }\n.timeline .timeline-content .timeline-date {\n    display: inline-block;\n    padding: 4px 0 10px; }\n@media (min-width: 992px) {\n      .timeline .timeline-content .timeline-date {\n        position: absolute;\n        top: 7px;\n        width: 100%;\n        left: 112%; } }\n@media (max-width: 991px) {\n      .timeline .timeline-content .timeline-date {\n        position: relative;\n        top: 0;\n        width: 100%;\n        left: auto; } }\n.timeline .timeline-content > .card::before {\n    position: absolute;\n    top: 11px;\n    width: 0;\n    height: 0;\n    content: ' ';\n    pointer-events: none;\n    border-width: 10px;\n    border-style: solid;\n    right: 100%;\n    border-color: transparent white transparent transparent; }\n.timeline .timeline-content > .card::after {\n    position: absolute;\n    top: 12px;\n    width: 0;\n    height: 0;\n    content: ' ';\n    pointer-events: none;\n    border-width: 9px;\n    border-style: solid;\n    right: 100%;\n    border-color: transparent white transparent transparent; }\n@media (min-width: 992px) {\n    .timeline:not(.stacked)::before {\n      left: 50%;\n      margin-left: -2px; }\n    .timeline:not(.stacked) .timeline-card:nth-child(odd) .timeline-content {\n      float: right; }\n    .timeline:not(.stacked) .timeline-card:nth-child(even) .timeline-content {\n      float: left; }\n    .timeline:not(.stacked) .timeline-card:nth-child(even) .timeline-content > .card {\n      float: right; }\n    .timeline:not(.stacked) .timeline-card:nth-child(even) .timeline-content > .card::before {\n      position: absolute;\n      right: auto;\n      left: 100%;\n      border-color: transparent transparent transparent white; }\n    .timeline:not(.stacked) .timeline-card:nth-child(even) .timeline-content > .card::after {\n      position: absolute;\n      right: auto;\n      left: 100%;\n      border-color: transparent transparent transparent white; }\n    .timeline:not(.stacked) .timeline-card:nth-child(odd) .timeline-content .timeline-date {\n      right: 112%;\n      left: auto;\n      text-align: right; }\n    .timeline:not(.stacked) .timeline-card:first-child {\n      margin-top: 0; }\n    .timeline:not(.stacked) .timeline-card.timeline-button {\n      text-align: center; }\n    .timeline:not(.stacked) .timeline-icon {\n      left: 50%;\n      margin-left: -6px; }\n    .timeline:not(.stacked) .timeline-content {\n      width: 47%;\n      margin-left: 0; } }\n@media (min-width: 992px) {\n    .timeline.stacked .timeline-content > .card {\n      margin-bottom: 0; }\n    .timeline.stacked .timeline-date {\n      position: relative;\n      top: 0;\n      width: 100%;\n      left: auto; } }\n"

/***/ }),

/***/ "./src/app/pages/timeline/timeline.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TimelineComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var TimelineComponent = (function () {
    function TimelineComponent() {
        this.stacked = false;
    }
    TimelineComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-timeline',
            template: __webpack_require__("./src/app/pages/timeline/timeline.component.html"),
            styles: [__webpack_require__("./src/app/pages/timeline/timeline.component.scss")]
        })
    ], TimelineComponent);
    return TimelineComponent;
}());

//# sourceMappingURL=timeline.component.js.map

/***/ })

});
//# sourceMappingURL=pages.module.chunk.js.map