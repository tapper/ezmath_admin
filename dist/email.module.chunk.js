webpackJsonp(["email.module"],{

/***/ "./src/app/email/email.component.html":
/***/ (function(module, exports) {

module.exports = "<ng-sidebar-container class=\"page-height\">\r\n  <ng-sidebar\r\n    [(opened)]=\"isOpened\"\r\n    [mode]=\"isOver() ? 'over' : 'push'\"\r\n    [position]=\"'left'\"\r\n    [dockedSize]=\"'50px'\"\r\n    [autoCollapseWidth]=\"'991'\"\r\n    [closeOnClickOutside]=\"isOver() ? true : false\"\r\n    [showBackdrop]=\"isOver() ? true : false\"\r\n    [sidebarClass]=\"'email-panel'\">\r\n    <div class=\"scroll-y\">\r\n      <div (click)=\"onSelect(message)\" *ngFor=\"let message of messages; let i = index\" [class.selected]=\"message === selectedMessage\" class=\"list-group-item list-group-item-action border-left-0 border-right-0 flex-column align-items-start\">\r\n        <a class=\"d-flex b-b w-100\" href=\"javascript:;\">\r\n          <div class=\"mr-auto\">\r\n            <div *ngIf=\"message.type\" class=\"avatar-status bg-{{message.type}}\">\r\n              <img alt=\"\" class=\"avatar-sm rounded-circle\" src=\"{{message.avatar}}\"/>\r\n            </div>\r\n            <img *ngIf=\"!message.type\" alt=\"\" class=\"avatar-sm rounded-circle\" src=\"{{message.avatar}}\"/>\r\n          </div>\r\n          <div class=\"pl-3\">\r\n            <div class=\"d-flex w-100 justify-content-between align-items-center\">\r\n              <small class=\"bold ff-headers\">\r\n                {{message.from}}\r\n              </small>\r\n              <small class=\"bold text-muted time\">\r\n                {{ message.date | date: 'MMMM d, y' }}\r\n              </small>\r\n            </div>\r\n            <p class=\"mb-0\">\r\n              {{message.subject}}\r\n            </p>\r\n          </div>\r\n        </a>\r\n      </div>\r\n    </div>\r\n  </ng-sidebar>\r\n  <div ng-sidebar-content class=\"scroll-y\">\r\n    <nav class=\"navbar custom-navbar\">\r\n      <div class=\"nav navbar-nav\">\r\n        <a (click)=\"toogleSidebar()\" class=\"nav-link d-lg-none d-sm-inline-block\" href=\"javascript:;\">\r\n          <i class=\"hamburger-icon\">\r\n            <span></span>\r\n          </i>\r\n        </a>\r\n        <a class=\"nav-item nav-link active\" href=\"javascript:;\">\r\n          Reply\r\n        </a>\r\n        <a class=\"nav-item nav-link\" href=\"javascript:;\">\r\n          Forward\r\n        </a>\r\n        <a class=\"nav-item nav-link\" href=\"javascript:;\">\r\n          Flag\r\n        </a>\r\n        <a class=\"nav-item nav-link disabled\" href=\"javascript:;\">\r\n          Delete\r\n        </a>\r\n      </div>\r\n    </nav>\r\n    <div class=\"pl-5 pr-5 pt-3 pb-3\">\r\n      <div *ngIf=\"selectedMessage\">\r\n        <div class=\"d-flex align-items-center mb-3\">\r\n          <div class=\"mr-auto\">\r\n            <img alt=\"\" class=\"avatar-lg rounded-circle\" src=\"{{selectedMessage.avatar}}\"/>\r\n          </div>\r\n          <div class=\"pl-3\">\r\n            <div class=\"date\">\r\n              {{selectedMessage.date | date: 'fullDate'}}\r\n            </div>\r\n            <h4 class=\"lead mt-0\">\r\n              {{selectedMessage.subject}}\r\n            </h4>\r\n            <p>\r\n              <b>\r\n                {{selectedMessage.from}}\r\n              </b>\r\n              to Jeff &amp; Suzzane\r\n            </p>\r\n          </div>\r\n        </div>\r\n        <div [innerHtml]=\"selectedMessage.body\">\r\n        </div>\r\n      </div>\r\n      <div class=\"pt-3 pb-3\">\r\n        <div class=\"toolbar\" id=\"toolbar-toolbar\">\r\n          <span class=\"ql-formats\">\r\n            <select class=\"ql-font\">\r\n              <option selected=\"\"></option>\r\n              <option value=\"serif\"></option>\r\n              <option value=\"monospace\"></option>\r\n            </select>\r\n            <select class=\"ql-size\">\r\n              <option value=\"small\"></option>\r\n              <option selected=\"\"></option>\r\n              <option value=\"large\"></option>\r\n              <option value=\"huge\"></option>\r\n            </select>\r\n          </span>\r\n          <span class=\"ql-formats\">\r\n            <button class=\"ql-bold\"></button>\r\n            <button class=\"ql-italic\"></button>\r\n            <button class=\"ql-underline\"></button>\r\n            <button class=\"ql-strike\"></button>\r\n          </span>\r\n          <span class=\"ql-formats\">\r\n            <select class=\"ql-color\"></select>\r\n            <select class=\"ql-background\"></select>\r\n          </span>\r\n          <span class=\"ql-formats\">\r\n            <button class=\"ql-list\" value=\"ordered\"></button>\r\n            <button class=\"ql-list\" value=\"bullet\"></button>\r\n            <select class=\"ql-align\">\r\n              <option selected=\"\"></option>\r\n              <option value=\"center\"></option>\r\n              <option value=\"right\"></option>\r\n              <option value=\"justify\"></option>\r\n            </select>\r\n          </span>\r\n          <span class=\"ql-formats\">\r\n            <button class=\"ql-link\"></button>\r\n            <button class=\"ql-image\"></button>\r\n          </span>\r\n        </div>\r\n        <div id=\"editor-container\">\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</ng-sidebar-container>"

/***/ }),

/***/ "./src/app/email/email.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EmailComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__email_service__ = __webpack_require__("./src/app/email/email.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_quill__ = __webpack_require__("./node_modules/quill/dist/quill.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_quill___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_quill__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var EmailComponent = (function () {
    function EmailComponent(mailService) {
        this.mailService = mailService;
        this.messageOpen = false;
        this.isOpened = true;
        this._autoCollapseWidth = 991;
    }
    EmailComponent.prototype.ngOnInit = function () {
        if (this.isOver()) {
            this.isOpened = false;
        }
        this.getMessages();
    };
    EmailComponent.prototype.ngAfterContentInit = function () {
        var quill = new __WEBPACK_IMPORTED_MODULE_2_quill__('#editor-container', {
            modules: {
                toolbar: {
                    container: '#toolbar-toolbar'
                }
            },
            placeholder: 'Compose an epic...',
            theme: 'snow'
        });
    };
    EmailComponent.prototype.toogleSidebar = function () {
        this.isOpened = !this.isOpened;
    };
    EmailComponent.prototype.isOver = function () {
        return window.matchMedia("(max-width: 991px)").matches;
    };
    EmailComponent.prototype.getMessages = function () {
        var _this = this;
        this.mailService.getMessages().then(function (messages) {
            _this.messages = messages;
            _this.selectedMessage = _this.messages[1];
        });
    };
    EmailComponent.prototype.onSelect = function (message) {
        this.selectedMessage = message;
        if (this.isOver()) {
            this.isOpened = false;
        }
    };
    EmailComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-email',
            template: __webpack_require__("./src/app/email/email.component.html"),
            styles: [__webpack_require__("./src/app/email/email.component.scss")],
            providers: [__WEBPACK_IMPORTED_MODULE_1__email_service__["a" /* MailService */]]
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__email_service__["a" /* MailService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__email_service__["a" /* MailService */]) === "function" && _a || Object])
    ], EmailComponent);
    return EmailComponent;
    var _a;
}());

//# sourceMappingURL=email.component.js.map

/***/ }),

/***/ "./src/app/email/email.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EmailModule", function() { return EmailModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common__ = __webpack_require__("./node_modules/@angular/common/@angular/common.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ng_sidebar__ = __webpack_require__("./node_modules/ng-sidebar/lib/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ng_sidebar___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_ng_sidebar__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__email_component__ = __webpack_require__("./src/app/email/email.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__email_routing__ = __webpack_require__("./src/app/email/email.routing.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var EmailModule = (function () {
    function EmailModule() {
    }
    EmailModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [__WEBPACK_IMPORTED_MODULE_2__angular_common__["CommonModule"], __WEBPACK_IMPORTED_MODULE_1__angular_router__["d" /* RouterModule */].forChild(__WEBPACK_IMPORTED_MODULE_5__email_routing__["a" /* EmailRoutes */]), __WEBPACK_IMPORTED_MODULE_3_ng_sidebar__["SidebarModule"]],
            declarations: [__WEBPACK_IMPORTED_MODULE_4__email_component__["a" /* EmailComponent */]]
        })
    ], EmailModule);
    return EmailModule;
}());

//# sourceMappingURL=email.module.js.map

/***/ }),

/***/ "./src/app/email/email.routing.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EmailRoutes; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__email_component__ = __webpack_require__("./src/app/email/email.component.ts");

var EmailRoutes = [{
        path: '',
        component: __WEBPACK_IMPORTED_MODULE_0__email_component__["a" /* EmailComponent */],
        data: {
            heading: 'Email',
            removeFooter: true
        }
    }];
//# sourceMappingURL=email.routing.js.map

/***/ }),

/***/ "./src/app/email/email.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MailService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__mock_messages__ = __webpack_require__("./src/app/email/mock-messages.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


var MailService = (function () {
    function MailService() {
    }
    MailService.prototype.getMessages = function () {
        return Promise.resolve(__WEBPACK_IMPORTED_MODULE_1__mock_messages__["a" /* MESSAGES */]);
    };
    MailService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])()
    ], MailService);
    return MailService;
}());

//# sourceMappingURL=email.service.js.map

/***/ })

});
//# sourceMappingURL=email.module.chunk.js.map