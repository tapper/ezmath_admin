webpackJsonp(["charts.module"],{

/***/ "./src/app/charts/bar/bar.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\r\n  <div class=\"col-sm-12 col-md-6\">\r\n    <div class=\"card\">\r\n      <div class=\"card-header\">Vertical bar chart</div>\r\n      <div class=\"card-body\">\r\n        <div>\r\n          <ngx-charts-bar-vertical\r\n            class=\"chart-container\"\r\n            [scheme]=\"colorScheme\"\r\n            [schemeType]=\"schemeType\"\r\n            [results]=\"single\"\r\n            [gradient]=\"gradient\"\r\n            [xAxis]=\"showXAxis\"\r\n            [yAxis]=\"showYAxis\"\r\n            [legend]=\"showLegend\"\r\n            [showXAxisLabel]=\"showXAxisLabel\"\r\n            [showYAxisLabel]=\"showYAxisLabel\"\r\n            [tooltipDisabled]=\"tooltipDisabled\"\r\n            [xAxisLabel]=\"xAxisLabel\"\r\n            [yAxisLabel]=\"yAxisLabel\"\r\n            [showGridLines]=\"showGridLines\"\r\n            [barPadding]=\"barPadding\"\r\n            [roundDomains]=\"roundDomains\"\r\n            (select)=\"select($event)\"\r\n            (legendLabelClick)=\"onLegendLabelClick($event)\">\r\n          </ngx-charts-bar-vertical>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <div class=\"col-sm-12 col-md-6\">\r\n    <div class=\"card\">\r\n      <div class=\"card-header\">Horizontal bar chart</div>\r\n      <div class=\"card-body\">\r\n        <div>\r\n          <ngx-charts-bar-horizontal\r\n            class=\"chart-container\"\r\n            [scheme]=\"colorScheme\"\r\n            [schemeType]=\"schemeType\"\r\n            [results]=\"single\"\r\n            [gradient]=\"gradient\"\r\n            [xAxis]=\"showXAxis\"\r\n            [yAxis]=\"showYAxis\"\r\n            [legend]=\"showLegend\"\r\n            [showXAxisLabel]=\"showXAxisLabel\"\r\n            [showYAxisLabel]=\"showYAxisLabel\"\r\n            [tooltipDisabled]=\"tooltipDisabled\"\r\n            [xAxisLabel]=\"xAxisLabel\"\r\n            [yAxisLabel]=\"yAxisLabel\"\r\n            [showGridLines]=\"showGridLines\"\r\n            [barPadding]=\"barPadding\"\r\n            [roundDomains]=\"roundDomains\"\r\n            (legendLabelClick)=\"onLegendLabelClick($event)\"\r\n            (select)=\"select($event)\">\r\n          </ngx-charts-bar-horizontal>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n\r\n<div class=\"row\">\r\n  <div class=\"col-sm-12 col-md-6\">\r\n    <div class=\"card\">\r\n      <div class=\"card-header\">Grouped vertical bar chart</div>\r\n      <div class=\"card-body\">\r\n        <div>\r\n          <ngx-charts-bar-vertical-2d\r\n            class=\"chart-container\"\r\n            [scheme]=\"colorScheme\"\r\n            [schemeType]=\"schemeType\"\r\n            [results]=\"multi\"\r\n            [gradient]=\"gradient\"\r\n            [xAxis]=\"showXAxis\"\r\n            [yAxis]=\"showYAxis\"\r\n            [legend]=\"showLegend\"\r\n            [showXAxisLabel]=\"showXAxisLabel\"\r\n            [showYAxisLabel]=\"showYAxisLabel\"\r\n            [tooltipDisabled]=\"tooltipDisabled\"\r\n            [xAxisLabel]=\"xAxisLabel\"\r\n            [yAxisLabel]=\"yAxisLabel\"\r\n            (legendLabelClick)=\"onLegendLabelClick($event)\"\r\n            [showGridLines]=\"showGridLines\"\r\n            [barPadding]=\"barPadding\"\r\n            [groupPadding]=\"groupPadding\"\r\n            [roundDomains]=\"roundDomains\"\r\n            (select)=\"select($event)\">\r\n          </ngx-charts-bar-vertical-2d>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <div class=\"col-sm-12 col-md-6\">\r\n    <div class=\"card\">\r\n      <div class=\"card-header\">Grouped horizontal bar chart</div>\r\n      <div class=\"card-body\">\r\n        <div>\r\n          <ngx-charts-bar-horizontal-2d\r\n            class=\"chart-container\"\r\n            [scheme]=\"colorScheme\"\r\n            [schemeType]=\"schemeType\"\r\n            [results]=\"multi\"\r\n            [gradient]=\"gradient\"\r\n            [tooltipDisabled]=\"tooltipDisabled\"\r\n            [xAxis]=\"showXAxis\"\r\n            [yAxis]=\"showYAxis\"\r\n            [legend]=\"showLegend\"\r\n            (legendLabelClick)=\"onLegendLabelClick($event)\"\r\n            [showXAxisLabel]=\"showXAxisLabel\"\r\n            [showYAxisLabel]=\"showYAxisLabel\"\r\n            [xAxisLabel]=\"xAxisLabel\"\r\n            [yAxisLabel]=\"yAxisLabel\"\r\n            [showGridLines]=\"showGridLines\"\r\n            [barPadding]=\"barPadding\"\r\n            [groupPadding]=\"groupPadding\"\r\n            [roundDomains]=\"roundDomains\"\r\n            (select)=\"select($event)\">\r\n          </ngx-charts-bar-horizontal-2d>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n\r\n<div class=\"row\">\r\n  <div class=\"col-sm-12 col-md-6\">\r\n    <div class=\"card\">\r\n      <div class=\"card-header\">Stacked vertical bar chart</div>\r\n      <div class=\"card-body\">\r\n        <div>\r\n          <ngx-charts-bar-vertical-stacked\r\n            class=\"chart-container\"\r\n            [scheme]=\"colorScheme\"\r\n            [schemeType]=\"schemeType\"\r\n            [results]=\"multi\"\r\n            [gradient]=\"gradient\"\r\n            [tooltipDisabled]=\"tooltipDisabled\"\r\n            [xAxis]=\"showXAxis\"\r\n            [yAxis]=\"showYAxis\"\r\n            [legend]=\"showLegend\"\r\n            (legendLabelClick)=\"onLegendLabelClick($event)\"\r\n            [showXAxisLabel]=\"showXAxisLabel\"\r\n            [showYAxisLabel]=\"showYAxisLabel\"\r\n            [xAxisLabel]=\"xAxisLabel\"\r\n            [yAxisLabel]=\"yAxisLabel\"\r\n            [showGridLines]=\"showGridLines\"\r\n            [barPadding]=\"barPadding\"\r\n            [roundDomains]=\"roundDomains\"\r\n            (select)=\"select($event)\">\r\n          </ngx-charts-bar-vertical-stacked>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <div class=\"col-sm-12 col-md-6\">\r\n    <div class=\"card\">\r\n      <div class=\"card-header\">Stacked horizontal bar chart</div>\r\n      <div class=\"card-body\">\r\n        <div>\r\n          <ngx-charts-bar-horizontal-stacked\r\n            class=\"chart-container\"\r\n            [scheme]=\"colorScheme\"\r\n            [schemeType]=\"schemeType\"\r\n            [results]=\"multi\"\r\n            [gradient]=\"gradient\"\r\n            [tooltipDisabled]=\"tooltipDisabled\"\r\n            [xAxis]=\"showXAxis\"\r\n            [yAxis]=\"showYAxis\"\r\n            [legend]=\"showLegend\"\r\n            [showXAxisLabel]=\"showXAxisLabel\"\r\n            (legendLabelClick)=\"onLegendLabelClick($event)\"\r\n            [showYAxisLabel]=\"showYAxisLabel\"\r\n            [xAxisLabel]=\"xAxisLabel\"\r\n            [yAxisLabel]=\"yAxisLabel\"\r\n            [showGridLines]=\"showGridLines\"\r\n            [barPadding]=\"barPadding\"\r\n            [roundDomains]=\"roundDomains\"\r\n            (select)=\"select($event)\">\r\n          </ngx-charts-bar-horizontal-stacked>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n\r\n<div class=\"row\">\r\n  <div class=\"col-sm-12 col-md-6\">\r\n    <div class=\"card\">\r\n      <div class=\"card-header\">Normalized vertical bar chart</div>\r\n      <div class=\"card-body\">\r\n        <div>\r\n          <ngx-charts-bar-vertical-normalized\r\n            class=\"chart-container\"\r\n            [scheme]=\"colorScheme\"\r\n            [schemeType]=\"schemeType\"\r\n            [results]=\"multi\"\r\n            [gradient]=\"gradient\"\r\n            [tooltipDisabled]=\"tooltipDisabled\"\r\n            [xAxis]=\"showXAxis\"\r\n            [yAxis]=\"showYAxis\"\r\n            [legend]=\"showLegend\"\r\n            [showXAxisLabel]=\"showXAxisLabel\"\r\n            (legendLabelClick)=\"onLegendLabelClick($event)\"\r\n            [showYAxisLabel]=\"showYAxisLabel\"\r\n            [xAxisLabel]=\"xAxisLabel\"\r\n            [yAxisLabel]=\"yAxisLabel\"\r\n            [showGridLines]=\"showGridLines\"\r\n            [barPadding]=\"barPadding\"\r\n            [roundDomains]=\"roundDomains\"\r\n            (select)=\"select($event)\">\r\n          </ngx-charts-bar-vertical-normalized>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <div class=\"col-sm-12 col-md-6\">\r\n    <div class=\"card\">\r\n      <div class=\"card-header\">Normalized horizontal bar chart</div>\r\n      <div class=\"card-body\">\r\n        <div>\r\n          <ngx-charts-bar-horizontal-normalized\r\n            class=\"chart-container\"\r\n            [scheme]=\"colorScheme\"\r\n            [schemeType]=\"schemeType\"\r\n            [results]=\"multi\"\r\n            [gradient]=\"gradient\"\r\n            [tooltipDisabled]=\"tooltipDisabled\"\r\n            [xAxis]=\"showXAxis\"\r\n            [yAxis]=\"showYAxis\"\r\n            [legend]=\"showLegend\"\r\n            [showXAxisLabel]=\"showXAxisLabel\"\r\n            [showYAxisLabel]=\"showYAxisLabel\"\r\n            [xAxisLabel]=\"xAxisLabel\"\r\n            [yAxisLabel]=\"yAxisLabel\"\r\n            (legendLabelClick)=\"onLegendLabelClick($event)\"\r\n            [showGridLines]=\"showGridLines\"\r\n            [barPadding]=\"barPadding\"\r\n            [roundDomains]=\"roundDomains\"\r\n            (select)=\"select($event)\">\r\n          </ngx-charts-bar-horizontal-normalized>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./src/app/charts/bar/bar.component.scss":
/***/ (function(module, exports) {

module.exports = ".chart-container {\n  height: 300px;\n  width: 100%;\n  display: block; }\n"

/***/ }),

/***/ "./src/app/charts/bar/bar.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BarComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__shared_chartData__ = __webpack_require__("./src/app/shared/chartData.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var BarComponent = (function () {
    function BarComponent() {
        // options
        this.showXAxis = true;
        this.showYAxis = true;
        this.gradient = false;
        this.showLegend = false;
        this.showXAxisLabel = true;
        this.tooltipDisabled = false;
        this.xAxisLabel = 'Country';
        this.showYAxisLabel = true;
        this.yAxisLabel = 'GDP Per Capita';
        this.showGridLines = true;
        this.innerPadding = 0;
        this.barPadding = 8;
        this.groupPadding = 16;
        this.roundDomains = false;
        this.maxRadius = 10;
        this.minRadius = 3;
        this.colorScheme = {
            domain: [
                '#0099cc', '#2ECC71', '#4cc3d9'
            ]
        };
        this.schemeType = 'ordinal';
        Object.assign(this, {
            single: __WEBPACK_IMPORTED_MODULE_1__shared_chartData__["e" /* single */],
            multi: __WEBPACK_IMPORTED_MODULE_1__shared_chartData__["d" /* multi */]
        });
    }
    BarComponent.prototype.select = function (data) {
        console.log('Item clicked', data);
    };
    BarComponent.prototype.onLegendLabelClick = function (entry) {
        console.log('Legend clicked', entry);
    };
    BarComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-bar',
            template: __webpack_require__("./src/app/charts/bar/bar.component.html"),
            styles: [__webpack_require__("./src/app/charts/bar/bar.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], BarComponent);
    return BarComponent;
}());

//# sourceMappingURL=bar.component.js.map

/***/ }),

/***/ "./src/app/charts/charts.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChartsModule", function() { return ChartsModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common__ = __webpack_require__("./node_modules/@angular/common/@angular/common.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__swimlane_ngx_charts__ = __webpack_require__("./node_modules/@swimlane/ngx-charts/release/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__swimlane_ngx_charts___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3__swimlane_ngx_charts__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__charts_routing__ = __webpack_require__("./src/app/charts/charts.routing.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__bar_bar_component__ = __webpack_require__("./src/app/charts/bar/bar.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pie_pie_component__ = __webpack_require__("./src/app/charts/pie/pie.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__line_line_component__ = __webpack_require__("./src/app/charts/line/line.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__misc_misc_component__ = __webpack_require__("./src/app/charts/misc/misc.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};









var ChartsModule = (function () {
    function ChartsModule() {
    }
    ChartsModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_2__angular_common__["CommonModule"],
                __WEBPACK_IMPORTED_MODULE_1__angular_router__["d" /* RouterModule */].forChild(__WEBPACK_IMPORTED_MODULE_4__charts_routing__["a" /* ChartsRoutes */]),
                __WEBPACK_IMPORTED_MODULE_3__swimlane_ngx_charts__["NgxChartsModule"]
            ],
            declarations: [
                __WEBPACK_IMPORTED_MODULE_5__bar_bar_component__["a" /* BarComponent */],
                __WEBPACK_IMPORTED_MODULE_6__pie_pie_component__["a" /* PieComponent */],
                __WEBPACK_IMPORTED_MODULE_7__line_line_component__["a" /* LineComponent */],
                __WEBPACK_IMPORTED_MODULE_8__misc_misc_component__["a" /* MiscComponent */]
            ]
        })
    ], ChartsModule);
    return ChartsModule;
}());

//# sourceMappingURL=charts.module.js.map

/***/ }),

/***/ "./src/app/charts/charts.routing.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ChartsRoutes; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__bar_bar_component__ = __webpack_require__("./src/app/charts/bar/bar.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__pie_pie_component__ = __webpack_require__("./src/app/charts/pie/pie.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__line_line_component__ = __webpack_require__("./src/app/charts/line/line.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__misc_misc_component__ = __webpack_require__("./src/app/charts/misc/misc.component.ts");




var ChartsRoutes = [{
        path: '',
        children: [{
                path: 'bar',
                component: __WEBPACK_IMPORTED_MODULE_0__bar_bar_component__["a" /* BarComponent */],
                data: {
                    heading: 'Bar'
                }
            }, {
                path: 'pie',
                component: __WEBPACK_IMPORTED_MODULE_1__pie_pie_component__["a" /* PieComponent */],
                data: {
                    heading: 'Pie'
                }
            }, {
                path: 'line',
                component: __WEBPACK_IMPORTED_MODULE_2__line_line_component__["a" /* LineComponent */],
                data: {
                    heading: 'Line'
                }
            }, {
                path: 'misc',
                component: __WEBPACK_IMPORTED_MODULE_3__misc_misc_component__["a" /* MiscComponent */],
                data: {
                    heading: 'Misc'
                }
            }]
    }];
//# sourceMappingURL=charts.routing.js.map

/***/ }),

/***/ "./src/app/charts/line/line.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\r\n  <div class=\"col-sm-12 col-md-6\">\r\n    <div class=\"card\">\r\n      <div class=\"card-header\">Line chart</div>\r\n      <div class=\"card-body\">\r\n        <div>\r\n          <ngx-charts-line-chart\r\n            class=\"chart-container\"\r\n            [scheme]=\"colorScheme\"\r\n            [schemeType]=\"schemeType\"\r\n            [results]=\"dateDataWithOrWithoutRange\"\r\n            [legend]=\"showLegend\"\r\n            (legendLabelClick)=\"onLegendLabelClick($event)\"\r\n            [gradient]=\"gradient\"\r\n            [xAxis]=\"showXAxis\"\r\n            [yAxis]=\"showYAxis\"\r\n            [showXAxisLabel]=\"showXAxisLabel\"\r\n            [showYAxisLabel]=\"showYAxisLabel\"\r\n            [xAxisLabel]=\"xAxisLabel\"\r\n            [yAxisLabel]=\"yAxisLabel\"\r\n            [autoScale]=\"autoScale\"\r\n            [timeline]=\"timeline\"\r\n            [showGridLines]=\"showGridLines\"\r\n            [curve]=\"curve\"\r\n            [rangeFillOpacity]=\"rangeFillOpacity\"\r\n            [roundDomains]=\"roundDomains\"\r\n            [tooltipDisabled]=\"true\"\r\n            (select)=\"select($event)\">\r\n          </ngx-charts-line-chart>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <div class=\"col-sm-12 col-md-6\">\r\n    <div class=\"card\">\r\n      <div class=\"card-header\">Area chart</div>\r\n      <div class=\"card-body\">\r\n        <div>\r\n          <ngx-charts-area-chart\r\n            class=\"chart-container\"\r\n            [scheme]=\"colorScheme\"\r\n            [schemeType]=\"schemeType\"\r\n            [results]=\"dateData\"\r\n            [legend]=\"showLegend\"\r\n            (legendLabelClick)=\"onLegendLabelClick($event)\"\r\n            [gradient]=\"gradient\"\r\n            [xAxis]=\"showXAxis\"\r\n            [yAxis]=\"showYAxis\"\r\n            [showXAxisLabel]=\"showXAxisLabel\"\r\n            [showYAxisLabel]=\"showYAxisLabel\"\r\n            [xAxisLabel]=\"xAxisLabel\"\r\n            [yAxisLabel]=\"yAxisLabel\"\r\n            [autoScale]=\"autoScale\"\r\n            [timeline]=\"timeline\"\r\n            [showGridLines]=\"showGridLines\"\r\n            [roundDomains]=\"roundDomains\"\r\n            [curve]=\"curve\"\r\n            [tooltipDisabled]=\"tooltipDisabled\"\r\n            (select)=\"select($event)\">\r\n          </ngx-charts-area-chart>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n\r\n<div class=\"row\">\r\n  <div class=\"col-sm-12 col-md-6\">\r\n    <div class=\"card\">\r\n      <div class=\"card-header\">Stacked area chart</div>\r\n      <div class=\"card-body\">\r\n        <div>\r\n          <ngx-charts-area-chart-stacked\r\n            class=\"chart-container\"\r\n            [scheme]=\"colorScheme\"\r\n            [schemeType]=\"schemeType\"\r\n            [results]=\"dateData\"\r\n            [legend]=\"showLegend\"\r\n            [gradient]=\"gradient\"\r\n            [xAxis]=\"showXAxis\"\r\n            [yAxis]=\"showYAxis\"\r\n            (legendLabelClick)=\"onLegendLabelClick($event)\"\r\n            [showXAxisLabel]=\"showXAxisLabel\"\r\n            [showYAxisLabel]=\"showYAxisLabel\"\r\n            [xAxisLabel]=\"xAxisLabel\"\r\n            [yAxisLabel]=\"yAxisLabel\"\r\n            [timeline]=\"timeline\"\r\n            [showGridLines]=\"showGridLines\"\r\n            [roundDomains]=\"roundDomains\"\r\n            [tooltipDisabled]=\"tooltipDisabled\"\r\n            [curve]=\"curve\"\r\n            (select)=\"select($event)\">\r\n          </ngx-charts-area-chart-stacked>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <div class=\"col-sm-12 col-md-6\">\r\n    <div class=\"card\">\r\n      <div class=\"card-header\">Normalized area chart</div>\r\n      <div class=\"card-body\">\r\n        <div>\r\n          <ngx-charts-area-chart-normalized\r\n            class=\"chart-container\"\r\n            [scheme]=\"colorScheme\"\r\n            [schemeType]=\"schemeType\"\r\n            [results]=\"dateData\"\r\n            [legend]=\"showLegend\"\r\n            [gradient]=\"gradient\"\r\n            [xAxis]=\"showXAxis\"\r\n            (legendLabelClick)=\"onLegendLabelClick($event)\"\r\n            [yAxis]=\"showYAxis\"\r\n            [showXAxisLabel]=\"showXAxisLabel\"\r\n            [showYAxisLabel]=\"showYAxisLabel\"\r\n            [xAxisLabel]=\"xAxisLabel\"\r\n            [yAxisLabel]=\"yAxisLabel\"\r\n            [timeline]=\"timeline\"\r\n            [showGridLines]=\"showGridLines\"\r\n            [roundDomains]=\"roundDomains\"\r\n            [tooltipDisabled]=\"tooltipDisabled\"\r\n            [curve]=\"curve\"\r\n            (select)=\"select($event)\">\r\n          </ngx-charts-area-chart-normalized>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./src/app/charts/line/line.component.scss":
/***/ (function(module, exports) {

module.exports = ".chart-container {\n  height: 300px;\n  width: 100%;\n  display: block; }\n"

/***/ }),

/***/ "./src/app/charts/line/line.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LineComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_d3_shape__ = __webpack_require__("./node_modules/d3-shape/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__shared_chartData__ = __webpack_require__("./src/app/shared/chartData.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var LineComponent = (function () {
    function LineComponent() {
        this.range = false;
        // options
        this.showXAxis = true;
        this.showYAxis = true;
        this.gradient = false;
        this.showLegend = false;
        this.showXAxisLabel = true;
        this.tooltipDisabled = false;
        this.xAxisLabel = 'Country';
        this.showYAxisLabel = true;
        this.yAxisLabel = 'GDP Per Capita';
        this.showGridLines = true;
        this.innerPadding = 0;
        this.barPadding = 8;
        this.groupPadding = 16;
        this.roundDomains = false;
        this.maxRadius = 10;
        this.minRadius = 3;
        // line interpolation
        this.curve = __WEBPACK_IMPORTED_MODULE_1_d3_shape__["curveLinear"];
        this.colorScheme = {
            domain: [
                '#0099cc', '#2ECC71', '#4cc3d9', '#ffc65d', '#d96557', '#ba68c8'
            ]
        };
        this.schemeType = 'ordinal';
        this.rangeFillOpacity = 0.15;
        // line, area
        this.autoScale = true;
        this.timeline = false;
        Object.assign(this, {
            single: __WEBPACK_IMPORTED_MODULE_2__shared_chartData__["e" /* single */],
            multi: __WEBPACK_IMPORTED_MODULE_2__shared_chartData__["d" /* multi */]
        });
        this.dateData = Object(__WEBPACK_IMPORTED_MODULE_2__shared_chartData__["b" /* generateData */])(5, false);
        this.dateDataWithRange = Object(__WEBPACK_IMPORTED_MODULE_2__shared_chartData__["b" /* generateData */])(2, true);
    }
    Object.defineProperty(LineComponent.prototype, "dateDataWithOrWithoutRange", {
        get: function () {
            if (this.range) {
                return this.dateDataWithRange;
            }
            else {
                return this.dateData;
            }
        },
        enumerable: true,
        configurable: true
    });
    LineComponent.prototype.select = function (data) {
        console.log('Item clicked', data);
    };
    LineComponent.prototype.onLegendLabelClick = function (entry) {
        console.log('Legend clicked', entry);
    };
    LineComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-line',
            template: __webpack_require__("./src/app/charts/line/line.component.html"),
            styles: [__webpack_require__("./src/app/charts/line/line.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], LineComponent);
    return LineComponent;
}());

//# sourceMappingURL=line.component.js.map

/***/ }),

/***/ "./src/app/charts/misc/misc.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\r\n  <div class=\"col-sm-12 col-md-12\">\r\n    <div class=\"card\">\r\n      <div class=\"card-header\">Bubble chart</div>\r\n      <div class=\"card-body\">\r\n        <div>\r\n          <ngx-charts-bubble-chart\r\n            class=\"chart-container\"\r\n            [view]=\"view\"\r\n            [results]=\"bubble\"\r\n            [showGridLines]=\"showGridLines\"\r\n            [legend]=\"false\"\r\n            [xAxis]=\"showXAxis\"\r\n            [yAxis]=\"showYAxis\"\r\n            [showXAxisLabel]=\"showXAxisLabel\"\r\n            [showYAxisLabel]=\"false\"\r\n            [xAxisLabel]=\"xAxisLabel\"\r\n            [yAxisLabel]=\"yAxisLabel\"\r\n            [autoScale]=\"autoScale\"\r\n            [scheme]=\"colorScheme\"\r\n            [schemeType]=\"schemeType\"\r\n            [roundDomains]=\"roundDomains\"\r\n            [tooltipDisabled]=\"tooltipDisabled\"\r\n            [minRadius]=\"minRadius\"\r\n            [maxRadius]=\"maxRadius\">\r\n          </ngx-charts-bubble-chart>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n\r\n<div class=\"row\">\r\n  <div class=\"col-sm-12 col-md-6\">\r\n    <div class=\"card\">\r\n      <div class=\"card-header\">Heat map</div>\r\n      <div class=\"card-body\">\r\n        <div>\r\n          <ngx-charts-heat-map\r\n            class=\"chart-container\"\r\n            [scheme]=\"colorScheme\"\r\n            [results]=\"multi\"\r\n            [legend]=\"showLegend\"\r\n            [gradient]=\"gradient\"\r\n            (legendLabelClick)=\"onLegendLabelClick($event)\"\r\n            [xAxis]=\"showXAxis\"\r\n            [yAxis]=\"showYAxis\"\r\n            [showXAxisLabel]=\"showXAxisLabel\"\r\n            [showYAxisLabel]=\"showYAxisLabel\"\r\n            [xAxisLabel]=\"xAxisLabel\"\r\n            [yAxisLabel]=\"yAxisLabel\"\r\n            [innerPadding]=\"innerPadding\"\r\n            [tooltipDisabled]=\"tooltipDisabled\"\r\n            (select)=\"select($event)\">\r\n          </ngx-charts-heat-map>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <div class=\"col-sm-12 col-md-6\">\r\n    <div class=\"card\">\r\n      <div class=\"card-header\">Forced directed graph</div>\r\n      <div class=\"card-body\">\r\n        <div>\r\n          <ngx-charts-force-directed-graph\r\n            class=\"chart-container\"\r\n            [legend]=\"showLegend\"\r\n            [links]=\"graph.links\"\r\n            (legendLabelClick)=\"onLegendLabelClick($event)\"\r\n            [nodes]=\"graph.nodes\"\r\n            [scheme]=\"colorScheme\"\r\n            [tooltipDisabled]=\"tooltipDisabled\"\r\n            (select)=\"select($event)\">\r\n          </ngx-charts-force-directed-graph>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n\r\n<div class=\"row\">\r\n  <div class=\"col-sm-12 col-md-12\">\r\n    <div class=\"card\">\r\n      <div class=\"card-header\">Heat map - Calendar</div>\r\n      <div class=\"card-body\">\r\n        <div>\r\n          <ngx-charts-heat-map\r\n            class=\"chart-container\"\r\n            [scheme]=\"colorScheme\"\r\n            [results]=\"calendarData\"\r\n            [legend]=\"showLegend\"\r\n            [gradient]=\"gradient\"\r\n            (legendLabelClick)=\"onLegendLabelClick($event)\"\r\n            [xAxis]=\"showXAxis\"\r\n            [yAxis]=\"showYAxis\"\r\n            [showXAxisLabel]=\"false\"\r\n            [showYAxisLabel]=\"false\"\r\n            [xAxisTickFormatting]=\"calendarAxisTickFormatting\"\r\n            [tooltipText]=\"calendarTooltipText\"\r\n            [innerPadding]=\"innerPadding\"\r\n            [tooltipDisabled]=\"tooltipDisabled\"\r\n            (select)=\"select($event)\">\r\n          </ngx-charts-heat-map>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n\r\n<div class=\"row\">\r\n  <div class=\"col-sm-12 col-md-6\">\r\n    <div class=\"card\">\r\n      <div class=\"card-header\">Tree map</div>\r\n      <div class=\"card-body\">\r\n        <div>\r\n          <ngx-charts-tree-map\r\n            class=\"chart-container\"\r\n            (legendLabelClick)=\"onLegendLabelClick($event)\"\r\n            [scheme]=\"colorScheme\"\r\n            [results]=\"single\"\r\n            [tooltipDisabled]=\"tooltipDisabled\"\r\n            (select)=\"select($event)\">\r\n          </ngx-charts-tree-map>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <div class=\"col-sm-12 col-md-6\">\r\n    <div class=\"card\">\r\n      <div class=\"card-header\">Number cards</div>\r\n      <div class=\"card-body\">\r\n        <div>\r\n          <ngx-charts-number-card\r\n            class=\"chart-container\"\r\n            (legendLabelClick)=\"onLegendLabelClick($event)\"\r\n            [scheme]=\"colorScheme\"\r\n            [results]=\"single\"\r\n            (select)=\"select($event)\">\r\n          </ngx-charts-number-card>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n\r\n<div class=\"row\">\r\n  <div class=\"col-sm-12 col-md-6\">\r\n    <div class=\"card\">\r\n      <div class=\"card-header\">Gauge</div>\r\n      <div class=\"card-body\">\r\n        <div>\r\n          <ngx-charts-gauge\r\n            class=\"chart-container\"\r\n            [legend]=\"showLegend\"\r\n            [results]=\"single\"\r\n            [textValue]=\"gaugeTextValue\"\r\n            [scheme]=\"colorScheme\"\r\n            [min]=\"gaugeMin\"\r\n            [max]=\"gaugeMax\"\r\n            [units]=\"gaugeUnits\"\r\n            [angleSpan]=\"gaugeAngleSpan\"\r\n            [startAngle]=\"gaugeStartAngle\"\r\n            [showAxis]=\"gaugeShowAxis\"\r\n            [bigSegments]=\"gaugeLargeSegments\"\r\n            [smallSegments]=\"gaugeSmallSegments\"\r\n            [margin]=\"margin ? [marginTop, marginRight, marginBottom, marginLeft] : null\"\r\n            [tooltipDisabled]=\"tooltipDisabled\"\r\n            (select)=\"select($event)\"\r\n            (legendLabelClick)=\"onLegendLabelClick($event)\">\r\n          </ngx-charts-gauge>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <div class=\"col-sm-12 col-md-6\">\r\n    <div class=\"card\">\r\n      <div class=\"card-header\">Linear gauge</div>\r\n      <div class=\"card-body\">\r\n        <div>\r\n          <ngx-charts-linear-gauge\r\n            class=\"chart-container\"\r\n            [scheme]=\"colorScheme\"\r\n            [min]=\"gaugeMin\"\r\n            [max]=\"gaugeMax\"\r\n            [value]=\"gaugeValue\"\r\n            [previousValue]=\"gaugePreviousValue\"\r\n            [units]=\"gaugeUnits\"\r\n            (select)=\"select($event)\">\r\n          </ngx-charts-linear-gauge>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./src/app/charts/misc/misc.component.scss":
/***/ (function(module, exports) {

module.exports = ".chart-container {\n  height: 300px;\n  width: 100%;\n  display: block; }\n"

/***/ }),

/***/ "./src/app/charts/misc/misc.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MiscComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__shared_chartData__ = __webpack_require__("./src/app/shared/chartData.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var monthName = new Intl.DateTimeFormat('en-us', { month: 'short' });
var weekdayName = new Intl.DateTimeFormat('en-us', { weekday: 'short' });
var MiscComponent = (function () {
    function MiscComponent() {
        this.showXAxis = true;
        this.showYAxis = true;
        this.gradient = false;
        this.showLegend = true;
        this.showXAxisLabel = true;
        this.tooltipDisabled = false;
        this.xAxisLabel = 'Country';
        this.showYAxisLabel = true;
        this.yAxisLabel = 'GDP Per Capita';
        this.showGridLines = true;
        this.innerPadding = 0;
        this.barPadding = 8;
        this.groupPadding = 16;
        this.roundDomains = false;
        this.maxRadius = 10;
        this.minRadius = 3;
        this.colorScheme = {
            domain: [
                '#0099cc', '#2ECC71', '#4cc3d9', '#ffc65d', '#d96557', '#ba68c8'
            ]
        };
        this.schemeType = 'ordinal';
        // line, area
        this.autoScale = true;
        this.timeline = false;
        // margin
        this.margin = false;
        this.marginTop = 40;
        this.marginRight = 40;
        this.marginBottom = 40;
        this.marginLeft = 40;
        // gauge
        this.gaugeMin = 0;
        this.gaugeMax = 100;
        this.gaugeLargeSegments = 10;
        this.gaugeSmallSegments = 5;
        this.gaugeTextValue = '';
        this.gaugeUnits = 'alerts';
        this.gaugeAngleSpan = 240;
        this.gaugeStartAngle = -120;
        this.gaugeShowAxis = true;
        this.gaugeValue = 50; // linear gauge value
        this.gaugePreviousValue = 70;
        Object.assign(this, {
            single: __WEBPACK_IMPORTED_MODULE_1__shared_chartData__["e" /* single */],
            multi: __WEBPACK_IMPORTED_MODULE_1__shared_chartData__["d" /* multi */],
            graph: Object(__WEBPACK_IMPORTED_MODULE_1__shared_chartData__["c" /* generateGraph */])(50),
            bubble: __WEBPACK_IMPORTED_MODULE_1__shared_chartData__["a" /* bubble */]
        });
        this.view = undefined;
        this.calendarData = this.getCalendarData();
    }
    MiscComponent.prototype.select = function (data) {
        console.log('Item clicked', data);
    };
    MiscComponent.prototype.onLegendLabelClick = function (entry) {
        console.log('Legend clicked', entry);
    };
    MiscComponent.prototype.getCalendarData = function () {
        // today
        var now = new Date();
        var todaysDay = now.getDate();
        var thisDay = new Date(now.getFullYear(), now.getMonth(), todaysDay);
        // Monday
        var thisMonday = new Date(thisDay.getFullYear(), thisDay.getMonth(), todaysDay - thisDay.getDay() + 1);
        var thisMondayDay = thisMonday.getDate();
        var thisMondayYear = thisMonday.getFullYear();
        var thisMondayMonth = thisMonday.getMonth();
        // 52 weeks before monday
        var calendarData = [];
        var getDate = function (d) { return new Date(thisMondayYear, thisMondayMonth, d); };
        for (var week = -52; week <= 0; week++) {
            var mondayDay = thisMondayDay + (week * 7);
            var monday = getDate(mondayDay);
            // one week
            var series = [];
            for (var dayOfWeek = 7; dayOfWeek > 0; dayOfWeek--) {
                var date = getDate(mondayDay - 1 + dayOfWeek);
                // skip future dates
                if (date > now) {
                    continue;
                }
                // value
                var value = (dayOfWeek < 6) ? (date.getMonth() + 1) : 0;
                series.push({
                    date: date,
                    name: weekdayName.format(date),
                    value: value
                });
            }
            calendarData.push({
                name: monday.toString(),
                series: series
            });
        }
        return calendarData;
    };
    MiscComponent.prototype.calendarAxisTickFormatting = function (mondayString) {
        var monday = new Date(mondayString);
        var month = monday.getMonth();
        var day = monday.getDate();
        var year = monday.getFullYear();
        var lastSunday = new Date(year, month, day - 1);
        var nextSunday = new Date(year, month, day + 6);
        return (lastSunday.getMonth() !== nextSunday.getMonth()) ? monthName.format(nextSunday) : '';
    };
    MiscComponent.prototype.calendarTooltipText = function (c) {
        return "\n      <span class=\"tooltip-label\">" + c.label + " \u2022 " + c.cell.date.toLocaleDateString() + "</span>\n      <span class=\"tooltip-val\">" + c.data.toLocaleString() + "</span>\n    ";
    };
    MiscComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-misc',
            template: __webpack_require__("./src/app/charts/misc/misc.component.html"),
            styles: [__webpack_require__("./src/app/charts/misc/misc.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], MiscComponent);
    return MiscComponent;
}());

//# sourceMappingURL=misc.component.js.map

/***/ }),

/***/ "./src/app/charts/pie/pie.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\r\n  <div class=\"col-12\">\r\n    <div class=\"card\">\r\n      <div class=\"card-header\">Pie chart</div>\r\n      <div class=\"card-body\">\r\n        <div>\r\n          <ngx-charts-pie-chart\r\n            class=\"chart-container\"\r\n            [scheme]=\"colorScheme\"\r\n            [results]=\"single\"\r\n            [legend]=\"showLegend\"\r\n            [explodeSlices]=\"explodeSlices\"\r\n            [labels]=\"showLabels\"\r\n            [doughnut]=\"doughnut\"\r\n            [arcWidth]=\"arcWidth\"\r\n            (legendLabelClick)=\"onLegendLabelClick($event)\"\r\n            [gradient]=\"gradient\"\r\n            [tooltipDisabled]=\"tooltipDisabled\"\r\n            (select)=\"select($event)\">\r\n          </ngx-charts-pie-chart>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <div class=\"col-12\">\r\n    <div class=\"card\">\r\n      <div class=\"card-header\">Advanced pie chart</div>\r\n      <div class=\"card-body\">\r\n        <div>\r\n          <ngx-charts-advanced-pie-chart\r\n            class=\"chart-container\"\r\n            [scheme]=\"colorScheme\"\r\n            [results]=\"single\"\r\n            (legendLabelClick)=\"onLegendLabelClick($event)\"\r\n            [gradient]=\"gradient\"\r\n            [tooltipDisabled]=\"tooltipDisabled\"\r\n            (select)=\"select($event)\">\r\n          </ngx-charts-advanced-pie-chart>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <div class=\"col-12\">\r\n    <div class=\"card\">\r\n      <div class=\"card-header\">Pie grid</div>\r\n      <div class=\"card-body\">\r\n        <div>\r\n          <ngx-charts-pie-grid\r\n            class=\"chart-container\"\r\n            [scheme]=\"colorScheme\"\r\n            (legendLabelClick)=\"onLegendLabelClick($event)\"\r\n            [results]=\"single\"\r\n            [tooltipDisabled]=\"tooltipDisabled\"\r\n            (select)=\"select($event)\">\r\n          </ngx-charts-pie-grid>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./src/app/charts/pie/pie.component.scss":
/***/ (function(module, exports) {

module.exports = ".chart-container {\n  height: 300px;\n  width: 100%;\n  display: block; }\n"

/***/ }),

/***/ "./src/app/charts/pie/pie.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PieComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__shared_chartData__ = __webpack_require__("./src/app/shared/chartData.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var PieComponent = (function () {
    function PieComponent() {
        // options
        this.showXAxis = true;
        this.showYAxis = true;
        this.gradient = false;
        this.showLegend = true;
        this.showXAxisLabel = true;
        this.tooltipDisabled = false;
        this.xAxisLabel = 'Country';
        this.showYAxisLabel = true;
        this.yAxisLabel = 'GDP Per Capita';
        this.showGridLines = true;
        this.innerPadding = 0;
        this.barPadding = 8;
        this.groupPadding = 16;
        this.roundDomains = false;
        this.maxRadius = 10;
        this.minRadius = 3;
        this.colorScheme = {
            domain: [
                '#0099cc', '#2ECC71', '#4cc3d9', '#ffc65d', '#d96557', '#ba68c8'
            ]
        };
        this.schemeType = 'ordinal';
        // pie
        this.showLabels = true;
        this.explodeSlices = false;
        this.doughnut = false;
        this.arcWidth = 0.25;
        Object.assign(this, {
            single: __WEBPACK_IMPORTED_MODULE_1__shared_chartData__["e" /* single */]
        });
    }
    PieComponent.prototype.select = function (data) {
        console.log('Item clicked', data);
    };
    PieComponent.prototype.onLegendLabelClick = function (entry) {
        console.log('Legend clicked', entry);
    };
    PieComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-pie',
            template: __webpack_require__("./src/app/charts/pie/pie.component.html"),
            styles: [__webpack_require__("./src/app/charts/pie/pie.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], PieComponent);
    return PieComponent;
}());

//# sourceMappingURL=pie.component.js.map

/***/ })

});
//# sourceMappingURL=charts.module.chunk.js.map