webpackJsonp(["Main.module.11"],{

/***/ "./src/app/productImages/Main.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MainModule", function() { return MainModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common__ = __webpack_require__("./node_modules/@angular/common/@angular/common.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__Main_routing__ = __webpack_require__("./src/app/productImages/Main.routing.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__swimlane_ngx_datatable__ = __webpack_require__("./node_modules/@swimlane/ngx-datatable/release/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__swimlane_ngx_datatable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4__swimlane_ngx_datatable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__MainService_service__ = __webpack_require__("./src/app/productImages/MainService.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_http__ = __webpack_require__("./node_modules/@angular/http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__edit_edit_component__ = __webpack_require__("./src/app/productImages/edit/edit.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__index_index_component__ = __webpack_require__("./src/app/productImages/index/index.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__add_add_component__ = __webpack_require__("./src/app/productImages/add/add.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ng_bootstrap_ng_bootstrap__ = __webpack_require__("./node_modules/@ng-bootstrap/ng-bootstrap/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_ng2_file_upload__ = __webpack_require__("./node_modules/ng2-file-upload/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_ng2_file_upload___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_11_ng2_file_upload__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__angular_forms__ = __webpack_require__("./node_modules/@angular/forms/@angular/forms.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};













var MainModule = (function () {
    function MainModule() {
    }
    MainModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_2__angular_common__["CommonModule"],
                __WEBPACK_IMPORTED_MODULE_1__angular_router__["d" /* RouterModule */].forChild(__WEBPACK_IMPORTED_MODULE_3__Main_routing__["a" /* MaindRoutes */]),
                __WEBPACK_IMPORTED_MODULE_4__swimlane_ngx_datatable__["NgxDatatableModule"],
                __WEBPACK_IMPORTED_MODULE_6__angular_http__["c" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_10__ng_bootstrap_ng_bootstrap__["g" /* NgbModule */],
                __WEBPACK_IMPORTED_MODULE_11_ng2_file_upload__["FileUploadModule"],
                __WEBPACK_IMPORTED_MODULE_12__angular_forms__["FormsModule"]
            ],
            declarations: [
                __WEBPACK_IMPORTED_MODULE_7__edit_edit_component__["a" /* EditComponent */],
                __WEBPACK_IMPORTED_MODULE_8__index_index_component__["a" /* IndexComponent */],
                __WEBPACK_IMPORTED_MODULE_9__add_add_component__["a" /* AddComponent */]
            ],
            providers: [__WEBPACK_IMPORTED_MODULE_5__MainService_service__["a" /* MainService */]]
        })
    ], MainModule);
    return MainModule;
}());

//# sourceMappingURL=Main.module.js.map

/***/ }),

/***/ "./src/app/productImages/Main.routing.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MaindRoutes; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__edit_edit_component__ = __webpack_require__("./src/app/productImages/edit/edit.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__add_add_component__ = __webpack_require__("./src/app/productImages/add/add.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__index_index_component__ = __webpack_require__("./src/app/productImages/index/index.component.ts");



var MaindRoutes = [{
        path: '',
        children: [{
                path: 'index',
                component: __WEBPACK_IMPORTED_MODULE_2__index_index_component__["a" /* IndexComponent */],
                data: {
                    heading: 'Company'
                }
            }, {
                path: 'edit',
                component: __WEBPACK_IMPORTED_MODULE_0__edit_edit_component__["a" /* EditComponent */],
                data: {
                    heading: 'Edit Company'
                }
            }, {
                path: 'add',
                component: __WEBPACK_IMPORTED_MODULE_1__add_add_component__["a" /* AddComponent */],
                data: {
                    heading: 'Edit Company'
                }
            }]
    }];
//# sourceMappingURL=Main.routing.js.map

/***/ }),

/***/ "./src/app/productImages/MainService.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MainService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_http__ = __webpack_require__("./node_modules/@angular/http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__ = __webpack_require__("./node_modules/rxjs/Rx.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__settings_settings_service__ = __webpack_require__("./src/settings/settings.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




//public this.ServerUrl = "";//http://www.tapper.co.il/salecar/laravel/public/api/";
var MainService = (function () {
    function MainService(http, settings) {
        this.http = http;
        this.ServerUrl = "";
        this.headers = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/x-www-form-urlencoded' });
        this.options = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["f" /* RequestOptions */]({ headers: this.headers });
        this.Items = [];
        this.ServerUrl = settings.ServerUrl;
    }
    ;
    MainService.prototype.GetItems = function (url, Id) {
        var _this = this;
        var body = new FormData();
        body.append('id', Id);
        return this.http.post(this.ServerUrl + '' + url, body).map(function (res) { return res.json(); }).do(function (data) { _this.Items = data; }).toPromise();
    };
    MainService.prototype.AddItem = function (url, Id, File) {
        var body = new FormData();
        body.append("file", File);
        body.append('id', Id);
        return this.http.post(this.ServerUrl + '' + url, body).map(function (res) { return res.json(); }).do(function (data) { }).toPromise();
    };
    MainService.prototype.EditItem = function (url, Item, File) {
        console.log("IT : ", File, Item);
        this.Items = Item;
        var body = new FormData();
        body.append("file", File);
        body.append("category", JSON.stringify(this.Items));
        //let body = 'category=' + JSON.stringify(this.Items);
        return this.http.post(this.ServerUrl + '' + url, body).map(function (res) { return res.json(); }).do(function (data) { }).toPromise();
    };
    MainService.prototype.DeleteItem = function (url, Id, Pid) {
        var body = new FormData();
        body.append("id", Id);
        body.append("pid", Pid);
        return this.http.post(this.ServerUrl + '' + url, body).map(function (res) { return res.json(); }).do(function (data) { }).toPromise();
    };
    MainService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__angular_http__["b" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_http__["b" /* Http */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_3__settings_settings_service__["a" /* SettingsService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__settings_settings_service__["a" /* SettingsService */]) === "function" && _b || Object])
    ], MainService);
    return MainService;
    var _a, _b;
}());

;
//# sourceMappingURL=MainService.service.js.map

/***/ }),

/***/ "./src/app/productImages/add/add.component.css":
/***/ (function(module, exports) {

module.exports = ".FormClass\r\n{\r\n    text-align: right;\r\n    direction: rtl;\r\n}\r\n\r\n.row\r\n{\r\n    margin-top: 20px;\r\n}\r\n\r\n.textWhite\r\n{\r\n    background-color: white;\r\n}\r\n\r\ninput.ng-invalid.ng-touched\r\n{\r\n    border:1px solid red;\r\n}"

/***/ }),

/***/ "./src/app/productImages/add/add.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"row FormClass\">\r\n  <div class=\"col-lg-12\">\r\n    <div class=\"card\">\r\n      <div class=\"card-header\">\r\n        ערוך חברה\r\n      </div>\r\n      <div class=\"card-body\">\r\n        <form (ngSubmit)=\"onSubmit(f)\" #f=\"ngForm\" >\r\n          <div class=\"row\" >\r\n            <div class=\"form-group\" class=\"col-lg-6\" *ngFor=\"let row of rows let i=index\" style=\"margin-top: 15px;\">\r\n              <label for=\"formGroupExampleInput\">הכנס {{rowsNames[i]}} </label>\r\n              <input type=\"text\" class=\"form-control textWhite\" id=\"formGroupExampleInput\" name={{row}} [(ngModel)]=\"Item[row]\"  ngModel required>\r\n            </div>\r\n          </div>\r\n          <div class=\"row\">\r\n            <div class=\"form-group\" class=\"col-lg-6\">\r\n              <label for=\"formGroupExampleInput\">תיאור בעיברית</label>\r\n              <textarea rows=\"4\" cols=\"50\" class=\"form-control textWhite\" id=\"formGroupExampleInput\" [(ngModel)]=\"Item.description\" name=\"description\" ngModel required> </textarea>\r\n            </div>\r\n            <div class=\"form-group\" class=\"col-lg-6\">\r\n              <label for=\"formGroupExampleInput\">תיאור באנגלית</label>\r\n              <textarea rows=\"4\" cols=\"50\" class=\"form-control textWhite\" id=\"formGroupExampleInput\" [(ngModel)]=\"Item.description_english\" name=\"description_english\" ngModel required> </textarea>\r\n            </div>\r\n          </div>\r\n          <div class=\"row\">\r\n            <div class=\"form-group\" class=\"col-lg-6\">\r\n              <label for=\"formGroupExampleInput\">תת קטגורייה </label>\r\n              <select class=\"form-control textWhite\" [(ngModel)]=\"Item.sub_category_id\" name=\"sub_category_id\">\r\n                <option *ngFor=\"let item of SubCategories let i=index\" [value]=\"item.id\">{{item.title}}</option>\r\n              </select><br/>\r\n            </div>\r\n          </div>\r\n          <!--<div class=\"row\">-->\r\n          <!--<img src=\"{{Image}}\" style=\"width:50px; height: 50px\" />-->\r\n          <!--<input  #fileInput   type=\"file\"(change)=\"onChange($event)\" style=\"margin-right: 20px; margin-top: 10px\"/>-->\r\n          <!--</div>-->\r\n          <div class=\"row\">\r\n            <!--<div class=\"form-group\" class=\"col-lg-6\">-->\r\n              <!--<div class=\"row\">-->\r\n                <!--<label class=\"uploader\">-->\r\n\r\n                  <!--<img *ngIf=\"changeImage\" src=\"{{changeImage}}\" [class.loaded]=\"imageLoaded\" style=\"width: 100px;\"/>-->\r\n                  <!--<img *ngIf=\"!changeImage\" src=\"{{host}}{{Item.image}}\" [class.loaded]=\"imageLoaded\" style=\"width: 100px;\"/>-->\r\n                  <!--<input #fileInput type=\"file\" name=\"file\" accept=\"image/*\" (change)=\"onChange($event)\">-->\r\n                <!--</label>-->\r\n\r\n\r\n              <!--</div>-->\r\n            <!--</div>-->\r\n            <div class=\"form-group\" class=\"col-lg-6\">\r\n              <div class=\"row\">\r\n                <input #fileInput type=\"file\"/>\r\n              </div>\r\n            </div>\r\n          </div>\r\n\r\n          <div class=\"row\" align=\"left\" style=\"float: left; margin-left: 5px\">\r\n            <button [disabled]=\"!f.valid\" type=\"submit\"\r\n                    class=\"btn btn-primary btn-icon loading-demo mr-1 mb-1\"\r\n                    style=\"padding:0 20px !important; cursor: pointer; width: 205px; text-align: center; font-weight: bold; font-size: 16px\">\r\n              <span style=\"text-align: center; padding: 10px;  width: 100% !important;\">שלח טופס</span>\r\n            </button>\r\n          </div>\r\n        </form>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./src/app/productImages/add/add.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__("./node_modules/@angular/http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__MainService_service__ = __webpack_require__("./src/app/productImages/MainService.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AddComponent = (function () {
    function AddComponent(route, http, service, router) {
        var _this = this;
        this.route = route;
        this.http = http;
        this.service = service;
        this.router = router;
        this.navigateTo = '/category/index';
        this.imageSrc = '';
        this.folderName = 'products';
        this.rowsNames = ['שם בעיברית', 'שם באנגלית', 'מחיר נמוך', 'מחיר גבוה'];
        this.rows = ['title', 'title_english', 'low_price', 'high_price'];
        this.Item = {
            'title': '',
            'title_english': '',
            'low_price': '',
            'high_price': '',
            'description': '',
            'description_english': '',
            'image': '',
        };
        console.log("Row : ", this.rows);
        this.service.GetItems('GetSubCategoriesById', -1).then(function (data) {
            _this.SubCategories = data;
            _this.isReady = true;
            console.log("Sub : ", data);
        });
    }
    AddComponent.prototype.onSubmit = function (form) {
        var _this = this;
        console.log(form.value);
        var fi = this.fileInput.nativeElement;
        var fileToUpload;
        if (fi.files && fi.files[0]) {
            fileToUpload = fi.files[0];
        }
        this.service.AddItem('AddProduct', form.value, fileToUpload).then(function (data) {
            console.log("AddCompany : ", data);
            _this.router.navigate([_this.navigateTo]);
        });
    };
    AddComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.paramsSub = this.route.params.subscribe(function (params) { return _this.id = params['id']; });
    };
    AddComponent.prototype.ngOnDestroy = function () {
        this.paramsSub.unsubscribe();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])("fileInput"),
        __metadata("design:type", Object)
    ], AddComponent.prototype, "fileInput", void 0);
    AddComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-add',
            template: __webpack_require__("./src/app/productImages/add/add.component.html"),
            styles: [__webpack_require__("./src/app/productImages/add/add.component.css")]
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__MainService_service__["a" /* MainService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__MainService_service__["a" /* MainService */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */]) === "function" && _d || Object])
    ], AddComponent);
    return AddComponent;
    var _a, _b, _c, _d;
}());

//# sourceMappingURL=add.component.js.map

/***/ }),

/***/ "./src/app/productImages/edit/edit.component.css":
/***/ (function(module, exports) {

module.exports = ".FormClass\r\n{\r\n    text-align: right;\r\n    direction: rtl;\r\n}\r\n\r\n.row\r\n{\r\n    margin-top: 20px;\r\n}\r\n\r\n.textWhite\r\n{\r\n    background-color: white;\r\n}\r\n\r\ninput.ng-invalid.ng-touched\r\n{\r\n    border:1px solid red;\r\n}\r\n\r\n.SearchInput\r\n{\r\n    background-color: white;\r\n    text-align: right;\r\n}\r\n\r\n.p-3\r\n{\r\n    padding: 0px;\r\n    background-color: red;\r\n}\r\n\r\n.KitchensForm\r\n{\r\n    direction: rtl;\r\n    text-align: right;\r\n}\r\n\r\n.formCheck\r\n{\r\n    position: relative;\r\n    left:20px;\r\n    text-align: right;\r\n    width: 50px;\r\n    background-color: red;\r\n}"

/***/ }),

/***/ "./src/app/productImages/edit/edit.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"row FormClass\">\r\n    <div class=\"col-lg-12\">\r\n        <div class=\"card\">\r\n            <div class=\"card-header\">\r\n                ערוך חברה\r\n            </div>\r\n            <div class=\"card-body\">\r\n                <form (ngSubmit)=\"onSubmit(f)\" #f=\"ngForm\" >\r\n                    <div class=\"row\" >\r\n                        <div class=\"form-group\" class=\"col-lg-6\" *ngFor=\"let row of rows let i=index\" style=\"margin-top: 15px;\">\r\n                            <label for=\"formGroupExampleInput\">הכנס {{rowsNames[i]}} </label>\r\n                            <input type=\"text\" class=\"form-control textWhite\" id=\"formGroupExampleInput\" name={{row}} [(ngModel)]=\"Item[row]\"  ngModel required>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"row\">\r\n                        <div class=\"form-group\" class=\"col-lg-6\">\r\n                            <label for=\"formGroupExampleInput\">תיאור בעיברית</label>\r\n                            <textarea rows=\"4\" cols=\"50\" class=\"form-control textWhite\" id=\"formGroupExampleInput\" [(ngModel)]=\"Item.description\" name=\"description\" ngModel required> </textarea>\r\n                        </div>\r\n                        <div class=\"form-group\" class=\"col-lg-6\">\r\n                            <label for=\"formGroupExampleInput\">תיאור באנגלית</label>\r\n                            <textarea rows=\"4\" cols=\"50\" class=\"form-control textWhite\" id=\"formGroupExampleInput\" [(ngModel)]=\"Item.description_english\" name=\"description\" ngModel required> </textarea>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"row\">\r\n                        <div class=\"form-group\" class=\"col-lg-6\">\r\n                            <label for=\"formGroupExampleInput\">תת קטגורייה </label>\r\n                            <select class=\"form-control textWhite\" [(ngModel)]=\"Item.sub_category_id\" name=\"category\">\r\n                                <option *ngFor=\"let item of SubCategories let i=index\" [value]=\"item.id\">{{item.title}}</option>\r\n                            </select><br/>\r\n                        </div>\r\n                    </div>\r\n                    <!--<div class=\"row\">-->\r\n                        <!--<img src=\"{{Image}}\" style=\"width:50px; height: 50px\" />-->\r\n                        <!--<input  #fileInput   type=\"file\"(change)=\"onChange($event)\" style=\"margin-right: 20px; margin-top: 10px\"/>-->\r\n                    <!--</div>-->\r\n                    <div class=\"row\">\r\n                    <div class=\"form-group\" class=\"col-lg-6\">\r\n                        <div class=\"row\">\r\n                            <label class=\"uploader\">\r\n\r\n                                <img *ngIf=\"changeImage\" src=\"{{changeImage}}\" [class.loaded]=\"imageLoaded\" style=\"width: 100px;\"/>\r\n                                <img *ngIf=\"!changeImage\" src=\"{{host}}{{Item.image}}\" [class.loaded]=\"imageLoaded\" style=\"width: 100px;\"/>\r\n                                <input #fileInput type=\"file\" name=\"file\" accept=\"image/*\" (change)=\"onChange($event)\">\r\n                            </label>\r\n\r\n\r\n                        </div>\r\n                    </div></div>\r\n\r\n                    <div class=\"row\" align=\"left\" style=\"float: left; margin-left: 5px\">\r\n                        <button [disabled]=\"!f.valid\" type=\"submit\"\r\n                                class=\"btn btn-primary btn-icon loading-demo mr-1 mb-1\"\r\n                                style=\"padding:0 20px !important; cursor: pointer; width: 205px; text-align: center; font-weight: bold; font-size: 16px\">\r\n                            <span style=\"text-align: center; padding: 10px;  width: 100% !important;\">שלח טופס</span>\r\n                        </button>\r\n                    </div>\r\n                </form>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>"

/***/ }),

/***/ "./src/app/productImages/edit/edit.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EditComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__("./node_modules/@angular/http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__MainService_service__ = __webpack_require__("./src/app/productImages/MainService.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__settings_settings_service__ = __webpack_require__("./src/settings/settings.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var URL = 'https://evening-anchorage-3159.herokuapp.com/api/';
var EditComponent = (function () {
    function EditComponent(route, http, service, router, settings) {
        var _this = this;
        this.route = route;
        this.http = http;
        this.service = service;
        this.router = router;
        this.settings = settings;
        this.navigateTo = '/products/index';
        this.imageSrc = '';
        this.Items = [];
        this.rowsNames = ['שם בעיברית', 'שם באנגלית', 'מחיר נמוך', 'מחיר גבוה'];
        this.rows = ['title', 'title_english', 'low_price', 'high_price'];
        this.Change = false;
        this.route.params.subscribe(function (params) {
            _this.Id = params['id'];
            _this.Item = _this.service.Items[_this.Id];
            _this.host = settings.host;
            _this.service.GetItems('GetSubCategoriesById', -1).then(function (data) {
                _this.SubCategories = data;
                _this.isReady = true;
                console.log("Sub : ", data);
            });
            console.log("Product", _this.Item);
            console.log("Item : ", _this.Item);
        });
    }
    EditComponent.prototype.onSubmit = function (form) {
        var _this = this;
        console.log("Edit1 : ", this.Item);
        var fi = this.fileInput.nativeElement;
        var fileToUpload;
        if (fi.files && fi.files[0]) {
            fileToUpload = fi.files[0];
            console.log("fff : ", fileToUpload);
        }
        this.Item.change = this.Change.toString();
        //this.Item.change = this.Change;
        this.service.EditItem('EditProduct', this.Item, fileToUpload).then(function (data) {
            console.log("AddCompany : ", data);
            _this.router.navigate([_this.navigateTo]);
        });
    };
    EditComponent.prototype.onChange = function (event) {
        var _this = this;
        this.Change = true;
        var files = event.srcElement.files;
        this.changeImage = event.target.files[0];
        var reader = new FileReader();
        reader.onload = function (e) {
            _this.changeImage = e.target.result;
        };
        reader.readAsDataURL(event.target.files[0]);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])("fileInput"),
        __metadata("design:type", Object)
    ], EditComponent.prototype, "fileInput", void 0);
    EditComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-edit',
            template: __webpack_require__("./src/app/productImages/edit/edit.component.html"),
            styles: [__webpack_require__("./src/app/icons/fontawesome/fontawesome.component.scss"), __webpack_require__("./src/app/components/buttons/buttons.component.scss"), __webpack_require__("./src/app/productImages/edit/edit.component.css")]
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__MainService_service__["a" /* MainService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__MainService_service__["a" /* MainService */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_4__settings_settings_service__["a" /* SettingsService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__settings_settings_service__["a" /* SettingsService */]) === "function" && _e || Object])
    ], EditComponent);
    return EditComponent;
    var _a, _b, _c, _d, _e;
}());

//# sourceMappingURL=edit.component.js.map

/***/ }),

/***/ "./src/app/productImages/index/index.component.css":
/***/ (function(module, exports) {

module.exports = ".card\r\n{\r\n    text-align: right;\r\n    direction: rtl;\r\n}\r\n\r\n.card-body\r\n{\r\n    border-bottom: 1px solid #f2f1f2;\r\n}\r\n\r\n.mr-auto\r\n{\r\n    text-align: right;\r\n    direction: rtl;\r\n    background-color: red;\r\n    float: right;\r\n}\r\n\r\n.mr-3\r\n{\r\n    background-color: green;\r\n    float: right;\r\n}\r\n\r\n.IconClass\r\n{\r\n    margin-top: 6px;\r\n    text-align: center;\r\n    padding-left: -13px !important;\r\n    background-color: red;\r\n}\r\n\r\n.d-icon{\r\n    margin-top: -20px;\r\n}\r\n\r\n.titleImage\r\n{\r\n    width: 80px;\r\n    border-radius: 70%;\r\n    height:80px;\r\n    margin-top:3px;\r\n    border: 1px solid #f1f1f1;\r\n}\r\n\r\n.textHeader\r\n{\r\n    color: #337ab7;;\r\n    font-size: 15px;\r\n    font-weight: bold;\r\n}\r\n\r\n.SearchInput{\r\n    background-color: white;\r\n    text-align: right;\r\n    paddding:5px;\r\n    margin-bottom: -15px;\r\n    margin-top: -15px;\r\n}\r\n\r\n.p-3{\r\n    margin-top: 2px;\r\n    margin-bottom: 2px;\r\n}\r\n\r\n.sideButton\r\n{\r\n    width:90%;\r\n    cursor: pointer;\r\n    background-color: #3b5998;\r\n    color: white;\r\n    text-align: right;\r\n    padding: 3px;\r\n    overflow: hidden;\r\n}\r\n\r\n.sideButtonText\r\n{\r\n    margin-right: 10px;\r\n    font-size: 14px;\r\n    font-weight: bold;\r\n    top: 7px !important;\r\n    position: relative;\r\n}\r\n\r\n.sideButtonTextEmpty{\r\n    margin-right: 10px;\r\n    font-size: 14px;\r\n    font-weight: bold;\r\n    top: 0px !important;\r\n    position: relative;\r\n}\r\n\r\n.sideButtonBadge\r\n{\r\n    background-color: red;\r\n    border-radius:50%;\r\n    font-size: 12px;\r\n    margin-top: 5px;\r\n    padding: 3px;\r\n    width: 25px;\r\n    height: 25px;\r\n}\r\n\r\n.buttonDivBadge\r\n{\r\n    float: right;\r\n    width: 12%;\r\n}\r\n\r\n.buttonDivText\r\n{\r\n    float: right;\r\n    width: 90%;\r\n    text-align: right !important;\r\n}\r\n\r\n.SearchInput{\r\n    background-color: white;\r\n    text-align: right;\r\n    paddding:5px;\r\n    margin-bottom: -15px;\r\n    margin-top: -15px;\r\n}\r\n\r\n.buttonDivIcon\r\n{\r\n    float: left;\r\n    width: 20%;\r\n}\r\n\r\n.badgeText\r\n{\r\n    top: 4px;\r\n    position: relative;\r\n}\r\n\r\nngx-datatable {\r\n    direction: rtl !important;\r\n    text-align: right !important;\r\n}\r\n\r\n.yellow-star {\r\n    color: #ffbd53;\r\n}\r\n\r\n.grey-star {\r\n    color: grey;\r\n}\r\n\r\n.sideButtonTextEmpty{\r\n    margin-right: 10px;\r\n    font-size: 14px;\r\n    font-weight: bold;\r\n    top: 0px !important;\r\n    position: relative;\r\n    text-align: right !important;\r\n}\r\n"

/***/ }),

/***/ "./src/app/productImages/index/index.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"row nopadding\" style=\"padding: 0px; background-color: white; margin-top: -15px; margin-left: -17px\">\r\n            <div class=\"col-lg-12 nopadding\" style=\"margin-top: 10px;\">\r\n                        <div class=\"row\" style=\"direction: rtl; margin-left:0%;\">\r\n                            <div class=\"col-lg-9 nopadding\" style=\"margin-top: 10px;\">\r\n                                <form (ngSubmit)=\"onSubmit(f)\" #f=\"ngForm\" >\r\n                                    <div class=\"row\">\r\n                                        <label class=\"uploader\">\r\n                                            <!--<img *ngIf=\"changeImage\" src=\"{{changeImage}}\" [class.loaded]=\"imageLoaded\" style=\"width: 100px;\"/>-->\r\n                                            <!--<img *ngIf=\"!changeImage\" src=\"{{host}}{{Item.image}}\" [class.loaded]=\"imageLoaded\" style=\"width: 100px;\"/>-->\r\n                                            <input #fileInput type=\"file\" name=\"file\" accept=\"image/*\" (change)=\"onChange($event)\">\r\n                                        </label>\r\n                                    </div>\r\n                                </form>\r\n                            </div>\r\n\r\n                            <div class=\"col-lg-1 nopadding\" style=\"margin-top: 5px;\">\r\n                                <button [routerLink]=\"['/', 'products' , 'index' , { id:subCat}]\"  class=\"btn btn-icon btn-facebook mb-1 mr-1 sideButton\" >\r\n                                    <div class=\"buttonDivText\">\r\n                                        <span class=\"sideButtonTextEmpty\">חזרה</span>\r\n                                    </div>\r\n                                    <div class=\"buttonDivIcon\">\r\n                                        <i class=\"fa fa-chevron-left\"></i>\r\n                                    </div>\r\n                                </button>\r\n                            </div>\r\n\r\n                        </div>\r\n            </div>\r\n\r\n</div>\r\n\r\n<div class=\"row\">\r\n    <div class=\"media-grid col-lg-3 col-sm-6\" *ngFor=\"let Item of ItemsArray\">\r\n        <figure>\r\n            <img src=\"{{host}}{{Item.url}}\" alt=\"Card image cap\" class=\"img-fluid\" />\r\n            <figcaption>\r\n                <!--<h5 class=\"media-title fw-300\">Media <strong>{{ id }}</strong></h5>-->\r\n                <p>\r\n                    <a href=\"javascript:;\" placement=\"left\" ngbTooltip=\"Delete\" (click)=\"deleteItem(Item.id)\"><i class=\"icon-arrows-remove\"></i></a>\r\n                    <!--<a href=\"javascript:;\" placement=\"left\" ngbTooltip=\"Like\"><i class=\"icon icon-basic-heart\"></i></a>-->\r\n                    <!--<a href=\"javascript:;\" placement=\"left\" ngbTooltip=\"Share\"><i class=\"icon icon-basic-share\"></i></a>-->\r\n                    <!--<a href=\"javascript:;\" placement=\"left\" ngbTooltip=\"Bookmark\"><i class=\"icon icon-basic-bookmark\"></i></a>-->\r\n                </p>\r\n            </figcaption>\r\n        </figure>\r\n    </div>\r\n</div>\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n"

/***/ }),

/***/ "./src/app/productImages/index/index.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return IndexComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__MainService_service__ = __webpack_require__("./src/app/productImages/MainService.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__settings_settings_service__ = __webpack_require__("./src/settings/settings.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ng_bootstrap_ng_bootstrap__ = __webpack_require__("./node_modules/@ng-bootstrap/ng-bootstrap/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_router__ = __webpack_require__("./node_modules/@angular/router/@angular/router.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var IndexComponent = (function () {
    function IndexComponent(MainService, service, settings, modalService, route) {
        var _this = this;
        this.MainService = MainService;
        this.service = service;
        this.modalService = modalService;
        this.route = route;
        this.ItemsArray = [];
        this.ItemsArray1 = [];
        this.host = '';
        this.route.params.subscribe(function (params) {
            _this.ProductId = params['id'];
            if (!_this.ProductId)
                _this.ProductId = "-1";
            _this.subCat = params['subCat'];
            console.log("11 : ", _this.ProductId);
            _this.MainService.GetItems('getImages', _this.ProductId).then(function (data) {
                console.log("GetCategories12 : ", data);
                _this.ItemsArray = data;
                _this.ItemsArray1 = data;
                _this.host = settings.host;
            });
        });
    }
    IndexComponent.prototype.ngOnInit = function () {
    };
    IndexComponent.prototype.onChange = function (event) {
        var _this = this;
        console.log(event);
        var files = event.target.files[0]; //event.srcElement.files;
        // let reader = new FileReader();
        // reader.onload = (e: any) => {
        //     this.changeImage = e.target.result;
        // }
        //
        // reader.readAsDataURL(event.target.files[0]);
        var fi = this.fileInput.nativeElement;
        var fileToUpload;
        if (fi.files && fi.files[0]) {
            fileToUpload = fi.files[0];
            console.log("fff : ", fileToUpload);
        }
        //this.Item.change = this.Change;
        this.service.AddItem('AddImage', this.ProductId, fileToUpload).then(function (data) {
            console.log("GetCategories11 : ", data);
            _this.ItemsArray = data;
        });
    };
    IndexComponent.prototype.deleteItem = function (i) {
        var _this = this;
        console.log("I : ", i, this.ProductId);
        this.service.DeleteItem('DeleteImage', i, this.ProductId).then(function (data) {
            console.log("GetCategories11 : ", data);
            _this.ItemsArray = data;
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])("fileInput"),
        __metadata("design:type", Object)
    ], IndexComponent.prototype, "fileInput", void 0);
    IndexComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-index',
            template: __webpack_require__("./src/app/productImages/index/index.component.html"),
            styles: [__webpack_require__("./src/app/icons/fontawesome/fontawesome.component.scss"), __webpack_require__("./src/app/media/list/list.component.scss"), __webpack_require__("./src/app/productImages/index/index.component.css")]
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__MainService_service__["a" /* MainService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__MainService_service__["a" /* MainService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__MainService_service__["a" /* MainService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__MainService_service__["a" /* MainService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__settings_settings_service__["a" /* SettingsService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__settings_settings_service__["a" /* SettingsService */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_3__ng_bootstrap_ng_bootstrap__["f" /* NgbModal */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__ng_bootstrap_ng_bootstrap__["f" /* NgbModal */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_4__angular_router__["a" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__angular_router__["a" /* ActivatedRoute */]) === "function" && _e || Object])
    ], IndexComponent);
    return IndexComponent;
    var _a, _b, _c, _d, _e;
}());

//# sourceMappingURL=index.component.js.map

/***/ })

});
//# sourceMappingURL=Main.module.11.chunk.js.map