webpackJsonp(["main"],{

/***/ "./src/$$_gendir lazy recursive":
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./authentication/authentication.module": [
		"./src/app/authentication/authentication.module.ts",
		"common",
		"authentication.module"
	],
	"./branches/Main.module": [
		"./src/app/branches/Main.module.ts",
		"common",
		"Main.module.13"
	],
	"./cards/cards.module": [
		"./src/app/cards/cards.module.ts",
		"common",
		"cards.module"
	],
	"./cars/car.module": [
		"./src/app/cars/car.module.ts",
		"common",
		"car.module"
	],
	"./category/Main.module": [
		"./src/app/category/Main.module.ts",
		"common",
		"Main.module.12"
	],
	"./charts/charts.module": [
		"./src/app/charts/charts.module.ts",
		"common",
		"charts.module"
	],
	"./class_contact_leads/Main.module": [
		"./src/app/class_contact_leads/Main.module.ts",
		"common",
		"Main.module.17"
	],
	"./classes/Main.module": [
		"./src/app/classes/Main.module.ts",
		"common",
		"Main.module.0"
	],
	"./classes2/Main.module": [
		"./src/app/classes2/Main.module.ts",
		"common",
		"Main.module"
	],
	"./company/company.module": [
		"./src/app/company/company.module.ts",
		"common",
		"company.module"
	],
	"./components/components.module": [
		"./src/app/components/components.module.ts",
		"common",
		"components.module"
	],
	"./dashboard/dashboard.module": [
		"./src/app/dashboard/dashboard.module.ts",
		"common",
		"dashboard.module"
	],
	"./datatable/datatable.module": [
		"./src/app/datatable/datatable.module.ts",
		"datatable.module"
	],
	"./docs/docs.module": [
		"./src/app/docs/docs.module.ts",
		"docs.module"
	],
	"./email/email.module": [
		"./src/app/email/email.module.ts",
		"common",
		"email.module"
	],
	"./employee/employee.module": [
		"./src/app/employee/employee.module.ts",
		"common",
		"employee.module"
	],
	"./error/error.module": [
		"./src/app/error/error.module.ts",
		"error.module"
	],
	"./form/form.module": [
		"./src/app/form/form.module.ts",
		"common",
		"form.module"
	],
	"./fullcalendar/fullcalendar.module": [
		"./src/app/fullcalendar/fullcalendar.module.ts",
		"common",
		"fullcalendar.module"
	],
	"./icons/icons.module": [
		"./src/app/icons/icons.module.ts",
		"icons.module"
	],
	"./kitchens/kitchen.module": [
		"./src/app/kitchens/kitchen.module.ts",
		"common",
		"kitchen.module"
	],
	"./landing/landing.module": [
		"./src/app/landing/landing.module.ts",
		"landing.module"
	],
	"./maps/maps.module": [
		"./src/app/maps/maps.module.ts",
		"maps.module"
	],
	"./media/media.module": [
		"./src/app/media/media.module.ts",
		"common",
		"media.module"
	],
	"./orders/order.module": [
		"./src/app/orders/order.module.ts",
		"common",
		"order.module"
	],
	"./pages/pages.module": [
		"./src/app/pages/pages.module.ts",
		"pages.module"
	],
	"./productImages/Main.module": [
		"./src/app/productImages/Main.module.ts",
		"common",
		"Main.module.11"
	],
	"./products/Main.module": [
		"./src/app/products/Main.module.ts",
		"common",
		"Main.module.10"
	],
	"./professions/Main.module": [
		"./src/app/professions/Main.module.ts",
		"common",
		"Main.module.9"
	],
	"./push/Main.module": [
		"./src/app/push/Main.module.ts",
		"common",
		"Main.module.8"
	],
	"./schoolgrade/Main.module": [
		"./src/app/schoolgrade/Main.module.ts",
		"common",
		"Main.module.7"
	],
	"./social/social.module": [
		"./src/app/social/social.module.ts",
		"social.module"
	],
	"./subCategory/Main.module": [
		"./src/app/subCategory/Main.module.ts",
		"common",
		"Main.module.6"
	],
	"./tables/tables.module": [
		"./src/app/tables/tables.module.ts",
		"tables.module"
	],
	"./taskboard/taskboard.module": [
		"./src/app/taskboard/taskboard.module.ts",
		"common",
		"taskboard.module"
	],
	"./teachers_classes/Main.module": [
		"./src/app/teachers_classes/Main.module.ts",
		"common",
		"Main.module.16"
	],
	"./teachinglevel/Main.module": [
		"./src/app/teachinglevel/Main.module.ts",
		"common",
		"Main.module.5"
	],
	"./ticket_groups/Main.module": [
		"./src/app/ticket_groups/Main.module.ts",
		"common",
		"Main.module.4"
	],
	"./tickets/Main.module": [
		"./src/app/tickets/Main.module.ts",
		"common",
		"Main.module.3"
	],
	"./user_classes/Main.module": [
		"./src/app/user_classes/Main.module.ts",
		"common",
		"Main.module.15"
	],
	"./user_tickets/Main.module": [
		"./src/app/user_tickets/Main.module.ts",
		"common",
		"Main.module.2"
	],
	"./users/Main.module": [
		"./src/app/users/Main.module.ts",
		"common",
		"Main.module.1"
	],
	"./users_debt/Main.module": [
		"./src/app/users_debt/Main.module.ts",
		"common",
		"Main.module.14"
	],
	"./widgets/widgets.module": [
		"./src/app/widgets/widgets.module.ts",
		"widgets.module"
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return Promise.all(ids.slice(1).map(__webpack_require__.e)).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = "./src/$$_gendir lazy recursive";
module.exports = webpackAsyncContext;

/***/ }),

/***/ "./src/app/_guards/auth.guard.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthGuard; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/@angular/router.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AuthGuard = (function () {
    function AuthGuard(router) {
        this.router = router;
    }
    AuthGuard.prototype.canActivate = function (route, state) {
        if (localStorage.getItem('id')) {
            return true;
        }
        // not logged in so redirect to login page with the return url
        this.router.navigate(['/authentication']);
        return false;
    };
    AuthGuard = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */]) === "function" && _a || Object])
    ], AuthGuard);
    return AuthGuard;
    var _a;
}());

//# sourceMappingURL=auth.guard.js.map

/***/ }),

/***/ "./src/app/app.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ngx_translate_core__ = __webpack_require__("./node_modules/@ngx-translate/core/index.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AppComponent = (function () {
    function AppComponent(translate) {
        translate.addLangs(['en', 'fr']);
        translate.setDefaultLang('en');
        var browserLang = translate.getBrowserLang();
        translate.use(browserLang.match(/en|fr/) ? browserLang : 'en');
    }
    AppComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-root',
            template: '<router-outlet></router-outlet>'
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__ngx_translate_core__["c" /* TranslateService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__ngx_translate_core__["c" /* TranslateService */]) === "function" && _a || Object])
    ], AppComponent);
    return AppComponent;
    var _a;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ "./src/app/app.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export createTranslateLoader */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__("./node_modules/@angular/platform-browser/@angular/platform-browser.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_animations__ = __webpack_require__("./node_modules/@angular/platform-browser/@angular/platform-browser/animations.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("./node_modules/@angular/router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_forms__ = __webpack_require__("./node_modules/@angular/forms/@angular/forms.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_common_http__ = __webpack_require__("./node_modules/@angular/common/@angular/common/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ngx_translate_core__ = __webpack_require__("./node_modules/@ngx-translate/core/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ngx_translate_http_loader__ = __webpack_require__("./node_modules/@ngx-translate/http-loader/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ng_bootstrap_ng_bootstrap__ = __webpack_require__("./node_modules/@ng-bootstrap/ng-bootstrap/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_ng_sidebar__ = __webpack_require__("./node_modules/ng-sidebar/lib/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_ng_sidebar___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_9_ng_sidebar__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__app_routing__ = __webpack_require__("./src/app/app.routing.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__app_component__ = __webpack_require__("./src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__layouts_admin_admin_layout_component__ = __webpack_require__("./src/app/layouts/admin/admin-layout.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__layouts_auth_auth_layout_component__ = __webpack_require__("./src/app/layouts/auth/auth-layout.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__shared_shared_module__ = __webpack_require__("./src/app/shared/shared.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__settings_settings_service__ = __webpack_require__("./src/settings/settings.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__angular_http__ = __webpack_require__("./node_modules/@angular/http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__icons_fontawesome_fontawesome_component__ = __webpack_require__("./src/app/icons/fontawesome/fontawesome.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__icons_linea_linea_component__ = __webpack_require__("./src/app/icons/linea/linea.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__icons_sli_sli_component__ = __webpack_require__("./src/app/icons/sli/sli.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__components_button_icons_button_icons_component__ = __webpack_require__("./src/app/components/button-icons/button-icons.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__datatable_data_table_data_table_component__ = __webpack_require__("./src/app/datatable/data-table/data-table.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__swimlane_ngx_datatable__ = __webpack_require__("./node_modules/@swimlane/ngx-datatable/release/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__swimlane_ngx_datatable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_22__swimlane_ngx_datatable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__datatable_table_sorting_table_sorting_component__ = __webpack_require__("./src/app/datatable/table-sorting/table-sorting.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__datatable_table_selection_table_selection_component__ = __webpack_require__("./src/app/datatable/table-selection/table-selection.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__datatable_table_pinning_table_pinning_component__ = __webpack_require__("./src/app/datatable/table-pinning/table-pinning.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__datatable_table_paging_table_paging_component__ = __webpack_require__("./src/app/datatable/table-paging/table-paging.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__datatable_table_filter_table_filter_component__ = __webpack_require__("./src/app/datatable/table-filter/table-filter.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__datatable_table_editing_table_editing_component__ = __webpack_require__("./src/app/datatable/table-editing/table-editing.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__guards_auth_guard__ = __webpack_require__("./src/app/_guards/auth.guard.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






























function createTranslateLoader(http) {
    return new __WEBPACK_IMPORTED_MODULE_7__ngx_translate_http_loader__["a" /* TranslateHttpLoader */](http, './assets/i18n/', '.json');
}
var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_3__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_11__app_component__["a" /* AppComponent */],
                __WEBPACK_IMPORTED_MODULE_12__layouts_admin_admin_layout_component__["a" /* AdminLayoutComponent */],
                __WEBPACK_IMPORTED_MODULE_13__layouts_auth_auth_layout_component__["a" /* AuthLayoutComponent */],
                __WEBPACK_IMPORTED_MODULE_17__icons_fontawesome_fontawesome_component__["a" /* FontawesomeComponent */],
                __WEBPACK_IMPORTED_MODULE_19__icons_sli_sli_component__["a" /* SliComponent */],
                __WEBPACK_IMPORTED_MODULE_20__components_button_icons_button_icons_component__["a" /* ButtonIconsComponent */],
                __WEBPACK_IMPORTED_MODULE_18__icons_linea_linea_component__["a" /* LineaComponent */],
                __WEBPACK_IMPORTED_MODULE_21__datatable_data_table_data_table_component__["a" /* DataTableComponent */],
                __WEBPACK_IMPORTED_MODULE_28__datatable_table_editing_table_editing_component__["a" /* TableEditingComponent */],
                __WEBPACK_IMPORTED_MODULE_27__datatable_table_filter_table_filter_component__["a" /* TableFilterComponent */],
                __WEBPACK_IMPORTED_MODULE_26__datatable_table_paging_table_paging_component__["a" /* TablePagingComponent */],
                __WEBPACK_IMPORTED_MODULE_25__datatable_table_pinning_table_pinning_component__["a" /* TablePinningComponent */],
                __WEBPACK_IMPORTED_MODULE_24__datatable_table_selection_table_selection_component__["a" /* TableSelectionComponent */],
                __WEBPACK_IMPORTED_MODULE_23__datatable_table_sorting_table_sorting_component__["a" /* TableSortingComponent */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["BrowserModule"],
                __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_animations__["a" /* BrowserAnimationsModule */],
                __WEBPACK_IMPORTED_MODULE_14__shared_shared_module__["a" /* SharedModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_router__["d" /* RouterModule */].forRoot(__WEBPACK_IMPORTED_MODULE_10__app_routing__["a" /* AppRoutes */]),
                __WEBPACK_IMPORTED_MODULE_4__angular_forms__["FormsModule"],
                __WEBPACK_IMPORTED_MODULE_22__swimlane_ngx_datatable__["NgxDatatableModule"],
                __WEBPACK_IMPORTED_MODULE_5__angular_common_http__["b" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_16__angular_http__["c" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_6__ngx_translate_core__["b" /* TranslateModule */].forRoot({
                    loader: {
                        provide: __WEBPACK_IMPORTED_MODULE_6__ngx_translate_core__["a" /* TranslateLoader */],
                        useFactory: (createTranslateLoader),
                        deps: [__WEBPACK_IMPORTED_MODULE_5__angular_common_http__["a" /* HttpClient */]]
                    }
                }),
                __WEBPACK_IMPORTED_MODULE_8__ng_bootstrap_ng_bootstrap__["g" /* NgbModule */].forRoot(),
                __WEBPACK_IMPORTED_MODULE_9_ng_sidebar__["SidebarModule"].forRoot()
            ],
            providers: [__WEBPACK_IMPORTED_MODULE_15__settings_settings_service__["a" /* SettingsService */], __WEBPACK_IMPORTED_MODULE_29__guards_auth_guard__["a" /* AuthGuard */]],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_11__app_component__["a" /* AppComponent */]]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ "./src/app/app.routing.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppRoutes; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__layouts_admin_admin_layout_component__ = __webpack_require__("./src/app/layouts/admin/admin-layout.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__layouts_auth_auth_layout_component__ = __webpack_require__("./src/app/layouts/auth/auth-layout.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__guards_auth_guard__ = __webpack_require__("./src/app/_guards/auth.guard.ts");



var AppRoutes = [{
        path: '',
        component: __WEBPACK_IMPORTED_MODULE_0__layouts_admin_admin_layout_component__["a" /* AdminLayoutComponent */],
        children: [{
                path: '',
                loadChildren: './dashboard/dashboard.module#DashboardModule'
            }, {
                path: 'email',
                loadChildren: './email/email.module#EmailModule'
            }, {
                path: 'subCategory',
                loadChildren: './subCategory/Main.module#MainModule'
            }, {
                path: 'products',
                loadChildren: './products/Main.module#MainModule'
            }, {
                path: 'users',
                loadChildren: './users/Main.module#MainModule'
            }, {
                path: 'users_debt',
                loadChildren: './users_debt/Main.module#MainModule'
            }, {
                path: 'teachers_classes',
                loadChildren: './teachers_classes/Main.module#MainModule'
            }, {
                path: 'professions',
                loadChildren: './professions/Main.module#MainModule'
            }, {
                path: 'branches',
                loadChildren: './branches/Main.module#MainModule'
            },
            {
                path: 'classes2',
                loadChildren: './classes2/Main.module#MainModule'
            },
            {
                path: 'tickets',
                loadChildren: './tickets/Main.module#MainModule'
            },
            {
                path: 'ticket_groups',
                loadChildren: './ticket_groups/Main.module#MainModule'
            },
            {
                path: 'user_tickets',
                loadChildren: './user_tickets/Main.module#MainModule'
            },
            {
                path: 'schoolgrade',
                loadChildren: './schoolgrade/Main.module#MainModule'
            },
            {
                path: 'teachinglevel',
                loadChildren: './teachinglevel/Main.module#MainModule'
            },
            {
                path: 'user_classes',
                loadChildren: './user_classes/Main.module#MainModule'
            },
            {
                path: 'classes',
                loadChildren: './classes/Main.module#MainModule'
            },
            {
                path: 'class_contact_leads',
                loadChildren: './class_contact_leads/Main.module#MainModule'
            },
            {
                path: 'push',
                loadChildren: './push/Main.module#MainModule'
            },
            {
                path: 'productImages',
                loadChildren: './productImages/Main.module#MainModule'
            }, {
                path: 'car',
                loadChildren: './cars/car.module#CarModule'
            }, {
                path: 'employee',
                loadChildren: './employee/employee.module#EmployeeModule'
            }, {
                path: 'order',
                loadChildren: './orders/order.module#OrderModule'
            }, {
                path: 'kitchen',
                loadChildren: './kitchens/kitchen.module#KitchenModule'
            }, {
                path: 'company',
                loadChildren: './company/company.module#CompanyModule'
            }, {
                path: 'category',
                loadChildren: './category/Main.module#MainModule'
            }, {
                path: 'components',
                loadChildren: './components/components.module#ComponentsModule'
            }, {
                path: 'icons',
                loadChildren: './icons/icons.module#IconsModule'
            }, {
                path: 'cards',
                loadChildren: './cards/cards.module#CardsModule'
            }, {
                path: 'forms',
                loadChildren: './form/form.module#FormModule'
            }, {
                path: 'tables',
                loadChildren: './tables/tables.module#TablesModule'
            }, {
                path: 'datatable',
                loadChildren: './datatable/datatable.module#DatatableModule'
            }, {
                path: 'charts',
                loadChildren: './charts/charts.module#ChartsModule'
            }, {
                path: 'maps',
                loadChildren: './maps/maps.module#MapsModule'
            }, {
                path: 'pages',
                loadChildren: './pages/pages.module#PagesModule'
            }, {
                path: 'taskboard',
                loadChildren: './taskboard/taskboard.module#TaskboardModule'
            }, {
                path: 'calendar',
                loadChildren: './fullcalendar/fullcalendar.module#FullcalendarModule'
            }, {
                path: 'media',
                loadChildren: './media/media.module#MediaModule'
            }, {
                path: 'widgets',
                loadChildren: './widgets/widgets.module#WidgetsModule'
            }, {
                path: 'social',
                loadChildren: './social/social.module#SocialModule'
            }, {
                path: 'docs',
                loadChildren: './docs/docs.module#DocsModule'
            }], canActivate: [__WEBPACK_IMPORTED_MODULE_2__guards_auth_guard__["a" /* AuthGuard */]]
    }, {
        path: '',
        component: __WEBPACK_IMPORTED_MODULE_1__layouts_auth_auth_layout_component__["a" /* AuthLayoutComponent */],
        children: [{
                path: 'authentication',
                loadChildren: './authentication/authentication.module#AuthenticationModule'
            }, {
                path: 'error',
                loadChildren: './error/error.module#ErrorModule'
            }, {
                path: 'landing',
                loadChildren: './landing/landing.module#LandingModule'
            }]
    }, {
        path: '**',
        redirectTo: 'error/404'
    }];
//# sourceMappingURL=app.routing.js.map

/***/ }),

/***/ "./src/app/components/button-icons/button-icons.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"card\">\r\n  <div class=\"card-header\">Social buttons</div>\r\n  <div class=\"card-body\">\r\n    <div>        \r\n      <button class=\"btn btn-icon btn-adn mb-1 mr-1\">\r\n        <i class=\"fa fa-adn\"></i>\r\n        App.net\r\n      </button>\r\n\r\n      <button class=\"btn btn-icon btn-bitbucket mb-1 mr-1\">\r\n        <i class=\"fa fa-bitbucket\"></i>\r\n        Bitbucket\r\n      </button>\r\n\r\n      <button class=\"btn btn-icon btn-dropbox mb-1 mr-1\">\r\n        <i class=\"fa fa-dropbox\"></i>\r\n        Dropbox\r\n      </button>\r\n\r\n      <button class=\"btn btn-icon btn-facebook mb-1 mr-1\">\r\n        <i class=\"fa fa-facebook\"></i>\r\n        Facebook\r\n      </button>\r\n\r\n      <button class=\"btn btn-icon btn-flickr mb-1 mr-1\">\r\n        <i class=\"fa fa-flickr\"></i>\r\n        Flickr\r\n      </button>\r\n\r\n      <button class=\"btn btn-icon btn-foursquare mb-1 mr-1\">\r\n        <i class=\"fa fa-foursquare\"></i>\r\n        Foursquare\r\n      </button>\r\n\r\n      <button class=\"btn btn-icon btn-github mb-1 mr-1\">\r\n        <i class=\"fa fa-github\"></i>\r\n        GitHub\r\n      </button>\r\n\r\n      <button class=\"btn btn-icon btn-google mb-1 mr-1\">\r\n        <i class=\"fa fa-google-plus\"></i>\r\n        Google\r\n      </button>\r\n\r\n      <button class=\"btn btn-icon btn-instagram mb-1 mr-1\">\r\n        <i class=\"fa fa-instagram\"></i>\r\n        Instagram\r\n      </button>\r\n\r\n      <button class=\"btn btn-icon btn-linkedin mb-1 mr-1\">\r\n        <i class=\"fa fa-linkedin\"></i>\r\n        LinkedIn\r\n      </button>\r\n\r\n      <button class=\"btn btn-icon btn-microsoft mb-1 mr-1\">\r\n        <i class=\"fa fa-windows\"></i>\r\n        Microsoft\r\n      </button>\r\n\r\n      <button class=\"btn btn-icon btn-odnoklassniki mb-1 mr-1\">\r\n        <i class=\"fa fa-odnoklassniki\"></i>\r\n        Odnoklassniki\r\n      </button>\r\n\r\n      <button class=\"btn btn-icon btn-openid mb-1 mr-1\">\r\n        <i class=\"fa fa-openid\"></i>\r\n        OpenID\r\n      </button>\r\n\r\n      <button class=\"btn btn-icon btn-pinterest mb-1 mr-1\">\r\n        <i class=\"fa fa-pinterest\"></i>\r\n        Pinterest\r\n      </button>\r\n\r\n      <button class=\"btn btn-icon btn-reddit mb-1 mr-1\">\r\n        <i class=\"fa fa-reddit\"></i>\r\n        Reddit\r\n      </button>\r\n\r\n      <button class=\"btn btn-icon btn-soundcloud mb-1 mr-1\">\r\n        <i class=\"fa fa-soundcloud\"></i>\r\n        SoundCloud\r\n      </button>\r\n\r\n      <button class=\"btn btn-icon btn-tumblr mb-1 mr-1\">\r\n        <i class=\"fa fa-tumblr\"></i>\r\n        Tumblr\r\n      </button>\r\n\r\n      <button class=\"btn btn-icon btn-twitter mb-1 mr-1\">\r\n        <i class=\"fa fa-twitter\"></i>\r\n        Twitter\r\n      </button>\r\n\r\n      <button class=\"btn btn-icon btn-vimeo mb-1 mr-1\">\r\n        <i class=\"fa fa-vimeo-square\"></i>\r\n        Vimeo\r\n      </button>\r\n\r\n      <button class=\"btn btn-icon btn-vk mb-1 mr-1\">\r\n        <i class=\"fa fa-vk\"></i>\r\n        VK\r\n      </button>\r\n\r\n      <button class=\"btn btn-icon btn-yahoo mb-1 mr-1\">\r\n        <i class=\"fa fa-yahoo\"></i>\r\n        Yahoo!\r\n      </button>\r\n    </div>\r\n\r\n    <p class=\"pt-3\">Square</p>\r\n    <div>\r\n      <button class=\"btn btn-icon-icon btn-adn mb-1 mr-1\">\r\n        <i class=\"fa fa-adn\"></i>\r\n      </button>\r\n      <button class=\"btn btn-icon-icon btn-bitbucket mb-1 mr-1\">\r\n        <i class=\"fa fa-bitbucket\"></i>\r\n      </button>\r\n      <button class=\"btn btn-icon-icon btn-dropbox mb-1 mr-1\">\r\n        <i class=\"fa fa-dropbox\"></i>\r\n      </button>\r\n      <button class=\"btn btn-icon-icon btn-facebook mb-1 mr-1\">\r\n        <i class=\"fa fa-facebook\"></i>\r\n      </button>\r\n      <button class=\"btn btn-icon-icon btn-flickr mb-1 mr-1\">\r\n        <i class=\"fa fa-flickr\"></i>\r\n      </button>\r\n      <button class=\"btn btn-icon-icon btn-foursquare mb-1 mr-1\">\r\n        <i class=\"fa fa-foursquare\"></i>\r\n      </button>\r\n      <button class=\"btn btn-icon-icon btn-github mb-1 mr-1\">\r\n        <i class=\"fa fa-github\"></i>\r\n      </button>\r\n      <button class=\"btn btn-icon-icon btn-google mb-1 mr-1\">\r\n        <i class=\"fa fa-google-plus\"></i>\r\n      </button>\r\n      <button class=\"btn btn-icon-icon btn-instagram mb-1 mr-1\">\r\n        <i class=\"fa fa-instagram\"></i>\r\n      </button>\r\n      <button class=\"btn btn-icon-icon btn-linkedin mb-1 mr-1\">\r\n        <i class=\"fa fa-linkedin\"></i>\r\n      </button>\r\n      <button class=\"btn btn-icon-icon btn-microsoft mb-1 mr-1\">\r\n        <i class=\"fa fa-windows\"></i>\r\n      </button>\r\n      <button class=\"btn btn-icon-icon btn-odnoklassniki mb-1 mr-1\">\r\n        <i class=\"fa fa-odnoklassniki\"></i>\r\n      </button>\r\n      <button class=\"btn btn-icon-icon btn-openid mb-1 mr-1\">\r\n        <i class=\"fa fa-openid\"></i>\r\n      </button>\r\n      <button class=\"btn btn-icon-icon btn-pinterest mb-1 mr-1\">\r\n        <i class=\"fa fa-pinterest\"></i>\r\n      </button>\r\n      <button class=\"btn btn-icon-icon btn-reddit mb-1 mr-1\">\r\n        <i class=\"fa fa-reddit\"></i>\r\n      </button>\r\n      <button class=\"btn btn-icon-icon btn-soundcloud mb-1 mr-1\">\r\n        <i class=\"fa fa-soundcloud\"></i>\r\n      </button>\r\n      <button class=\"btn btn-icon-icon btn-tumblr mb-1 mr-1\">\r\n        <i class=\"fa fa-tumblr\"></i>\r\n      </button>\r\n      <button class=\"btn btn-icon-icon btn-twitter mb-1 mr-1\">\r\n        <i class=\"fa fa-twitter\"></i>\r\n      </button>\r\n      <button class=\"btn btn-icon-icon btn-vimeo mb-1 mr-1\">\r\n        <i class=\"fa fa-vimeo-square\"></i>\r\n      </button>\r\n      <button class=\"btn btn-icon-icon btn-vk mb-1 mr-1\">\r\n        <i class=\"fa fa-vk\"></i>\r\n      </button>\r\n      <button class=\"btn btn-icon-icon btn-yahoo mb-1 mr-1\">\r\n        <i class=\"fa fa-yahoo\"></i>\r\n      </button>\r\n    </div>\r\n    <p class=\"pt-3\">Circle</p>\r\n    <div>\r\n      <button class=\"btn btn-icon-icon btn-adn rounded-circle mb-1 mr-1\">\r\n        <i class=\"fa fa-adn\"></i>\r\n      </button>\r\n      <button class=\"btn btn-icon-icon btn-bitbucket rounded-circle mb-1 mr-1\">\r\n        <i class=\"fa fa-bitbucket\"></i>\r\n      </button>\r\n      <button class=\"btn btn-icon-icon btn-dropbox rounded-circle mb-1 mr-1\">\r\n        <i class=\"fa fa-dropbox\"></i>\r\n      </button>\r\n      <button class=\"btn btn-icon-icon btn-facebook rounded-circle mb-1 mr-1\">\r\n        <i class=\"fa fa-facebook\"></i>\r\n      </button>\r\n      <button class=\"btn btn-icon-icon btn-flickr rounded-circle mb-1 mr-1\">\r\n        <i class=\"fa fa-flickr\"></i>\r\n      </button>\r\n      <button class=\"btn btn-icon-icon btn-foursquare rounded-circle mb-1 mr-1\">\r\n        <i class=\"fa fa-foursquare\"></i>\r\n      </button>\r\n      <button class=\"btn btn-icon-icon btn-github rounded-circle mb-1 mr-1\">\r\n        <i class=\"fa fa-github\"></i>\r\n      </button>\r\n      <button class=\"btn btn-icon-icon btn-google rounded-circle mb-1 mr-1\">\r\n        <i class=\"fa fa-google-plus\"></i>\r\n      </button>\r\n      <button class=\"btn btn-icon-icon btn-instagram rounded-circle mb-1 mr-1\">\r\n        <i class=\"fa fa-instagram\"></i>\r\n      </button>\r\n      <button class=\"btn btn-icon-icon btn-linkedin rounded-circle mb-1 mr-1\">\r\n        <i class=\"fa fa-linkedin\"></i>\r\n      </button>\r\n      <button class=\"btn btn-icon-icon btn-microsoft rounded-circle mb-1 mr-1\">\r\n        <i class=\"fa fa-windows\"></i>\r\n      </button>\r\n      <button class=\"btn btn-icon-icon btn-odnoklassniki rounded-circle mb-1 mr-1\">\r\n        <i class=\"fa fa-odnoklassniki\"></i>\r\n      </button>\r\n      <button class=\"btn btn-icon-icon btn-openid rounded-circle mb-1 mr-1\">\r\n        <i class=\"fa fa-openid\"></i>\r\n      </button>\r\n      <button class=\"btn btn-icon-icon btn-pinterest rounded-circle mb-1 mr-1\">\r\n        <i class=\"fa fa-pinterest\"></i>\r\n      </button>\r\n      <button class=\"btn btn-icon-icon btn-reddit rounded-circle mb-1 mr-1\">\r\n        <i class=\"fa fa-reddit\"></i>\r\n      </button>\r\n      <button class=\"btn btn-icon-icon btn-soundcloud rounded-circle mb-1 mr-1\">\r\n        <i class=\"fa fa-soundcloud\"></i>\r\n      </button>\r\n      <button class=\"btn btn-icon-icon btn-tumblr rounded-circle mb-1 mr-1\">\r\n        <i class=\"fa fa-tumblr\"></i>\r\n      </button>\r\n      <button class=\"btn btn-icon-icon btn-twitter rounded-circle mb-1 mr-1\">\r\n        <i class=\"fa fa-twitter\"></i>\r\n      </button>\r\n      <button class=\"btn btn-icon-icon btn-vimeo rounded-circle mb-1 mr-1\">\r\n        <i class=\"fa fa-vimeo-square\"></i>\r\n      </button>\r\n      <button class=\"btn btn-icon-icon btn-vk rounded-circle mb-1 mr-1\">\r\n        <i class=\"fa fa-vk\"></i>\r\n      </button>\r\n      <button class=\"btn btn-icon-icon btn-yahoo rounded-circle mb-1 mr-1\">\r\n        <i class=\"fa fa-yahoo\"></i>\r\n      </button>\r\n    </div>\r\n    <p class=\"pt-3\">Large</p>\r\n    <div>\r\n      <button class=\"btn btn-icon-icon btn-adn btn-lg mb-1 mr-1\">\r\n        <i class=\"fa fa-adn\"></i>\r\n      </button>\r\n      <button class=\"btn btn-icon-icon btn-bitbucket btn-lg mb-1 mr-1\">\r\n        <i class=\"fa fa-bitbucket\"></i>\r\n      </button>\r\n      <button class=\"btn btn-icon-icon btn-dropbox btn-lg mb-1 mr-1\">\r\n        <i class=\"fa fa-dropbox\"></i>\r\n      </button>\r\n      <button class=\"btn btn-icon-icon btn-facebook btn-lg mb-1 mr-1\">\r\n        <i class=\"fa fa-facebook\"></i>\r\n      </button>\r\n      <button class=\"btn btn-icon-icon btn-flickr btn-lg mb-1 mr-1\">\r\n        <i class=\"fa fa-flickr\"></i>\r\n      </button>\r\n      <button class=\"btn btn-icon-icon btn-foursquare btn-lg mb-1 mr-1\">\r\n        <i class=\"fa fa-foursquare\"></i>\r\n      </button>\r\n      <button class=\"btn btn-icon-icon btn-github btn-lg mb-1 mr-1\">\r\n        <i class=\"fa fa-github\"></i>\r\n      </button>\r\n      <button class=\"btn btn-icon-icon btn-google btn-lg mb-1 mr-1\">\r\n        <i class=\"fa fa-google-plus\"></i>\r\n      </button>\r\n      <button class=\"btn btn-icon-icon btn-instagram btn-lg mb-1 mr-1\">\r\n        <i class=\"fa fa-instagram\"></i>\r\n      </button>\r\n      <button class=\"btn btn-icon-icon btn-linkedin btn-lg mb-1 mr-1\">\r\n        <i class=\"fa fa-linkedin\"></i>\r\n      </button>\r\n      <button class=\"btn btn-icon-icon btn-microsoft btn-lg mb-1 mr-1\">\r\n        <i class=\"fa fa-windows\"></i>\r\n      </button>\r\n      <button class=\"btn btn-icon-icon btn-odnoklassniki btn-lg mb-1 mr-1\">\r\n        <i class=\"fa fa-odnoklassniki\"></i>\r\n      </button>\r\n      <button class=\"btn btn-icon-icon btn-openid btn-lg mb-1 mr-1\">\r\n        <i class=\"fa fa-openid\"></i>\r\n      </button>\r\n      <button class=\"btn btn-icon-icon btn-pinterest btn-lg mb-1 mr-1\">\r\n        <i class=\"fa fa-pinterest\"></i>\r\n      </button>\r\n      <button class=\"btn btn-icon-icon btn-reddit btn-lg mb-1 mr-1\">\r\n        <i class=\"fa fa-reddit\"></i>\r\n      </button>\r\n      <button class=\"btn btn-icon-icon btn-soundcloud btn-lg mb-1 mr-1\">\r\n        <i class=\"fa fa-soundcloud\"></i>\r\n      </button>\r\n      <button class=\"btn btn-icon-icon btn-tumblr btn-lg mb-1 mr-1\">\r\n        <i class=\"fa fa-tumblr\"></i>\r\n      </button>\r\n      <button class=\"btn btn-icon-icon btn-twitter btn-lg mb-1 mr-1\">\r\n        <i class=\"fa fa-twitter\"></i>\r\n      </button>\r\n      <button class=\"btn btn-icon-icon btn-vimeo btn-lg mb-1 mr-1\">\r\n        <i class=\"fa fa-vimeo-square\"></i>\r\n      </button>\r\n      <button class=\"btn btn-icon-icon btn-vk btn-lg mb-1 mr-1\">\r\n        <i class=\"fa fa-vk\"></i>\r\n      </button>\r\n      <button class=\"btn btn-icon-icon btn-yahoo btn-lg mb-1 mr-1\">\r\n        <i class=\"fa fa-yahoo\"></i>\r\n      </button>\r\n    </div>\r\n    <p class=\"pt-3\">Small</p>\r\n    <div>\r\n      <button class=\"btn btn-icon-icon btn-adn btn-sm mb-1 mr-1\">\r\n        <i class=\"fa fa-adn\"></i>\r\n      </button>\r\n      <button class=\"btn btn-icon-icon btn-bitbucket btn-sm mb-1 mr-1\">\r\n        <i class=\"fa fa-bitbucket\"></i>\r\n      </button>\r\n      <button class=\"btn btn-icon-icon btn-dropbox btn-sm mb-1 mr-1\">\r\n        <i class=\"fa fa-dropbox\"></i>\r\n      </button>\r\n      <button class=\"btn btn-icon-icon btn-facebook btn-sm mb-1 mr-1\">\r\n        <i class=\"fa fa-facebook\"></i>\r\n      </button>\r\n      <button class=\"btn btn-icon-icon btn-flickr btn-sm mb-1 mr-1\">\r\n        <i class=\"fa fa-flickr\"></i>\r\n      </button>\r\n      <button class=\"btn btn-icon-icon btn-foursquare btn-sm mb-1 mr-1\">\r\n        <i class=\"fa fa-foursquare\"></i>\r\n      </button>\r\n      <button class=\"btn btn-icon-icon btn-github btn-sm mb-1 mr-1\">\r\n        <i class=\"fa fa-github\"></i>\r\n      </button>\r\n      <button class=\"btn btn-icon-icon btn-google btn-sm mb-1 mr-1\">\r\n        <i class=\"fa fa-google-plus\"></i>\r\n      </button>\r\n      <button class=\"btn btn-icon-icon btn-instagram btn-sm mb-1 mr-1\">\r\n        <i class=\"fa fa-instagram\"></i>\r\n      </button>\r\n      <button class=\"btn btn-icon-icon btn-linkedin btn-sm mb-1 mr-1\">\r\n        <i class=\"fa fa-linkedin\"></i>\r\n      </button>\r\n      <button class=\"btn btn-icon-icon btn-microsoft btn-sm mb-1 mr-1\">\r\n        <i class=\"fa fa-windows\"></i>\r\n      </button>\r\n      <button class=\"btn btn-icon-icon btn-odnoklassniki btn-sm mb-1 mr-1\">\r\n        <i class=\"fa fa-odnoklassniki\"></i>\r\n      </button>\r\n      <button class=\"btn btn-icon-icon btn-openid btn-sm mb-1 mr-1\">\r\n        <i class=\"fa fa-openid\"></i>\r\n      </button>\r\n      <button class=\"btn btn-icon-icon btn-pinterest btn-sm mb-1 mr-1\">\r\n        <i class=\"fa fa-pinterest\"></i>\r\n      </button>\r\n      <button class=\"btn btn-icon-icon btn-reddit btn-sm mb-1 mr-1\">\r\n        <i class=\"fa fa-reddit\"></i>\r\n      </button>\r\n      <button class=\"btn btn-icon-icon btn-soundcloud btn-sm mb-1 mr-1\">\r\n        <i class=\"fa fa-soundcloud\"></i>\r\n      </button>\r\n      <button class=\"btn btn-icon-icon btn-tumblr btn-sm mb-1 mr-1\">\r\n        <i class=\"fa fa-tumblr\"></i>\r\n      </button>\r\n      <button class=\"btn btn-icon-icon btn-twitter btn-sm mb-1 mr-1\">\r\n        <i class=\"fa fa-twitter\"></i>\r\n      </button>\r\n      <button class=\"btn btn-icon-icon btn-vimeo btn-sm mb-1 mr-1\">\r\n        <i class=\"fa fa-vimeo-square\"></i>\r\n      </button>\r\n      <button class=\"btn btn-icon-icon btn-vk btn-sm mb-1 mr-1\">\r\n        <i class=\"fa fa-vk\"></i>\r\n      </button>\r\n      <button class=\"btn btn-icon-icon btn-yahoo btn-sm mb-1 mr-1\">\r\n        <i class=\"fa fa-yahoo\"></i>\r\n      </button>\r\n    </div>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/components/button-icons/button-icons.component.scss":
/***/ (function(module, exports) {

module.exports = "/*!\r\n *  Font Awesome 4.7.0 by @davegandy - http://fontawesome.io - @fontawesome\r\n *  License - http://fontawesome.io/license (Font: SIL OFL 1.1, CSS: MIT License)\r\n */\r\n/* FONT PATH\r\n * -------------------------- */\r\n@font-face {\r\n  font-family: 'FontAwesome';\r\n  src: url('fontawesome-webfont.674f50d287a8c48dc19b.eot?v=4.7.0');\r\n  src: url('fontawesome-webfont.674f50d287a8c48dc19b.eot?#iefix&v=4.7.0') format('embedded-opentype'), url('fontawesome-webfont.af7ae505a9eed503f8b8.woff2?v=4.7.0') format('woff2'), url('fontawesome-webfont.fee66e712a8a08eef580.woff?v=4.7.0') format('woff'), url('fontawesome-webfont.b06871f281fee6b241d6.ttf?v=4.7.0') format('truetype'), url('fontawesome-webfont.acf3dcb7ff752b5296ca.svg?v=4.7.0#fontawesomeregular') format('svg');\r\n  font-weight: normal;\r\n  font-style: normal;\r\n}\r\n.fa {\r\n  display: inline-block;\r\n  font: normal normal normal 14px/1 FontAwesome;\r\n  font-size: inherit;\r\n  text-rendering: auto;\r\n  -webkit-font-smoothing: antialiased;\r\n  -moz-osx-font-smoothing: grayscale;\r\n}\r\n/* makes the font 33% larger relative to the icon container */\r\n.fa-lg {\r\n  font-size: 1.33333333em;\r\n  line-height: 0.75em;\r\n  vertical-align: -15%;\r\n}\r\n.fa-2x {\r\n  font-size: 2em;\r\n}\r\n.fa-3x {\r\n  font-size: 3em;\r\n}\r\n.fa-4x {\r\n  font-size: 4em;\r\n}\r\n.fa-5x {\r\n  font-size: 5em;\r\n}\r\n.fa-fw {\r\n  width: 1.28571429em;\r\n  text-align: center;\r\n}\r\n.fa-ul {\r\n  padding-left: 0;\r\n  margin-left: 2.14285714em;\r\n  list-style-type: none;\r\n}\r\n.fa-ul > li {\r\n  position: relative;\r\n}\r\n.fa-li {\r\n  position: absolute;\r\n  left: -2.14285714em;\r\n  width: 2.14285714em;\r\n  top: 0.14285714em;\r\n  text-align: center;\r\n}\r\n.fa-li.fa-lg {\r\n  left: -1.85714286em;\r\n}\r\n.fa-border {\r\n  padding: .2em .25em .15em;\r\n  border: solid 0.08em #eeeeee;\r\n  border-radius: .1em;\r\n}\r\n.fa-pull-left {\r\n  float: left;\r\n}\r\n.fa-pull-right {\r\n  float: right;\r\n}\r\n.fa.fa-pull-left {\r\n  margin-right: .3em;\r\n}\r\n.fa.fa-pull-right {\r\n  margin-left: .3em;\r\n}\r\n/* Deprecated as of 4.4.0 */\r\n.pull-right {\r\n  float: right;\r\n}\r\n.pull-left {\r\n  float: left;\r\n}\r\n.fa.pull-left {\r\n  margin-right: .3em;\r\n}\r\n.fa.pull-right {\r\n  margin-left: .3em;\r\n}\r\n.fa-spin {\r\n  -webkit-animation: fa-spin 2s infinite linear;\r\n  animation: fa-spin 2s infinite linear;\r\n}\r\n.fa-pulse {\r\n  -webkit-animation: fa-spin 1s infinite steps(8);\r\n  animation: fa-spin 1s infinite steps(8);\r\n}\r\n@-webkit-keyframes fa-spin {\r\n  0% {\r\n    -webkit-transform: rotate(0deg);\r\n    transform: rotate(0deg);\r\n  }\r\n  100% {\r\n    -webkit-transform: rotate(359deg);\r\n    transform: rotate(359deg);\r\n  }\r\n}\r\n@keyframes fa-spin {\r\n  0% {\r\n    -webkit-transform: rotate(0deg);\r\n    transform: rotate(0deg);\r\n  }\r\n  100% {\r\n    -webkit-transform: rotate(359deg);\r\n    transform: rotate(359deg);\r\n  }\r\n}\r\n.fa-rotate-90 {\r\n  -ms-filter: \"progid:DXImageTransform.Microsoft.BasicImage(rotation=1)\";\r\n  -webkit-transform: rotate(90deg);\r\n  transform: rotate(90deg);\r\n}\r\n.fa-rotate-180 {\r\n  -ms-filter: \"progid:DXImageTransform.Microsoft.BasicImage(rotation=2)\";\r\n  -webkit-transform: rotate(180deg);\r\n  transform: rotate(180deg);\r\n}\r\n.fa-rotate-270 {\r\n  -ms-filter: \"progid:DXImageTransform.Microsoft.BasicImage(rotation=3)\";\r\n  -webkit-transform: rotate(270deg);\r\n  transform: rotate(270deg);\r\n}\r\n.fa-flip-horizontal {\r\n  -ms-filter: \"progid:DXImageTransform.Microsoft.BasicImage(rotation=0, mirror=1)\";\r\n  -webkit-transform: scale(-1, 1);\r\n  transform: scale(-1, 1);\r\n}\r\n.fa-flip-vertical {\r\n  -ms-filter: \"progid:DXImageTransform.Microsoft.BasicImage(rotation=2, mirror=1)\";\r\n  -webkit-transform: scale(1, -1);\r\n  transform: scale(1, -1);\r\n}\r\n:root .fa-rotate-90,\r\n:root .fa-rotate-180,\r\n:root .fa-rotate-270,\r\n:root .fa-flip-horizontal,\r\n:root .fa-flip-vertical {\r\n  -webkit-filter: none;\r\n          filter: none;\r\n}\r\n.fa-stack {\r\n  position: relative;\r\n  display: inline-block;\r\n  width: 2em;\r\n  height: 2em;\r\n  line-height: 2em;\r\n  vertical-align: middle;\r\n}\r\n.fa-stack-1x,\r\n.fa-stack-2x {\r\n  position: absolute;\r\n  left: 0;\r\n  width: 100%;\r\n  text-align: center;\r\n}\r\n.fa-stack-1x {\r\n  line-height: inherit;\r\n}\r\n.fa-stack-2x {\r\n  font-size: 2em;\r\n}\r\n.fa-inverse {\r\n  color: #ffffff;\r\n}\r\n/* Font Awesome uses the Unicode Private Use Area (PUA) to ensure screen\r\n   readers do not read off random characters that represent icons */\r\n.fa-glass:before {\r\n  content: \"\\f000\";\r\n}\r\n.fa-music:before {\r\n  content: \"\\f001\";\r\n}\r\n.fa-search:before {\r\n  content: \"\\f002\";\r\n}\r\n.fa-envelope-o:before {\r\n  content: \"\\f003\";\r\n}\r\n.fa-heart:before {\r\n  content: \"\\f004\";\r\n}\r\n.fa-star:before {\r\n  content: \"\\f005\";\r\n}\r\n.fa-star-o:before {\r\n  content: \"\\f006\";\r\n}\r\n.fa-user:before {\r\n  content: \"\\f007\";\r\n}\r\n.fa-film:before {\r\n  content: \"\\f008\";\r\n}\r\n.fa-th-large:before {\r\n  content: \"\\f009\";\r\n}\r\n.fa-th:before {\r\n  content: \"\\f00a\";\r\n}\r\n.fa-th-list:before {\r\n  content: \"\\f00b\";\r\n}\r\n.fa-check:before {\r\n  content: \"\\f00c\";\r\n}\r\n.fa-remove:before,\r\n.fa-close:before,\r\n.fa-times:before {\r\n  content: \"\\f00d\";\r\n}\r\n.fa-search-plus:before {\r\n  content: \"\\f00e\";\r\n}\r\n.fa-search-minus:before {\r\n  content: \"\\f010\";\r\n}\r\n.fa-power-off:before {\r\n  content: \"\\f011\";\r\n}\r\n.fa-signal:before {\r\n  content: \"\\f012\";\r\n}\r\n.fa-gear:before,\r\n.fa-cog:before {\r\n  content: \"\\f013\";\r\n}\r\n.fa-trash-o:before {\r\n  content: \"\\f014\";\r\n}\r\n.fa-home:before {\r\n  content: \"\\f015\";\r\n}\r\n.fa-file-o:before {\r\n  content: \"\\f016\";\r\n}\r\n.fa-clock-o:before {\r\n  content: \"\\f017\";\r\n}\r\n.fa-road:before {\r\n  content: \"\\f018\";\r\n}\r\n.fa-download:before {\r\n  content: \"\\f019\";\r\n}\r\n.fa-arrow-circle-o-down:before {\r\n  content: \"\\f01a\";\r\n}\r\n.fa-arrow-circle-o-up:before {\r\n  content: \"\\f01b\";\r\n}\r\n.fa-inbox:before {\r\n  content: \"\\f01c\";\r\n}\r\n.fa-play-circle-o:before {\r\n  content: \"\\f01d\";\r\n}\r\n.fa-rotate-right:before,\r\n.fa-repeat:before {\r\n  content: \"\\f01e\";\r\n}\r\n.fa-refresh:before {\r\n  content: \"\\f021\";\r\n}\r\n.fa-list-alt:before {\r\n  content: \"\\f022\";\r\n}\r\n.fa-lock:before {\r\n  content: \"\\f023\";\r\n}\r\n.fa-flag:before {\r\n  content: \"\\f024\";\r\n}\r\n.fa-headphones:before {\r\n  content: \"\\f025\";\r\n}\r\n.fa-volume-off:before {\r\n  content: \"\\f026\";\r\n}\r\n.fa-volume-down:before {\r\n  content: \"\\f027\";\r\n}\r\n.fa-volume-up:before {\r\n  content: \"\\f028\";\r\n}\r\n.fa-qrcode:before {\r\n  content: \"\\f029\";\r\n}\r\n.fa-barcode:before {\r\n  content: \"\\f02a\";\r\n}\r\n.fa-tag:before {\r\n  content: \"\\f02b\";\r\n}\r\n.fa-tags:before {\r\n  content: \"\\f02c\";\r\n}\r\n.fa-book:before {\r\n  content: \"\\f02d\";\r\n}\r\n.fa-bookmark:before {\r\n  content: \"\\f02e\";\r\n}\r\n.fa-print:before {\r\n  content: \"\\f02f\";\r\n}\r\n.fa-camera:before {\r\n  content: \"\\f030\";\r\n}\r\n.fa-font:before {\r\n  content: \"\\f031\";\r\n}\r\n.fa-bold:before {\r\n  content: \"\\f032\";\r\n}\r\n.fa-italic:before {\r\n  content: \"\\f033\";\r\n}\r\n.fa-text-height:before {\r\n  content: \"\\f034\";\r\n}\r\n.fa-text-width:before {\r\n  content: \"\\f035\";\r\n}\r\n.fa-align-left:before {\r\n  content: \"\\f036\";\r\n}\r\n.fa-align-center:before {\r\n  content: \"\\f037\";\r\n}\r\n.fa-align-right:before {\r\n  content: \"\\f038\";\r\n}\r\n.fa-align-justify:before {\r\n  content: \"\\f039\";\r\n}\r\n.fa-list:before {\r\n  content: \"\\f03a\";\r\n}\r\n.fa-dedent:before,\r\n.fa-outdent:before {\r\n  content: \"\\f03b\";\r\n}\r\n.fa-indent:before {\r\n  content: \"\\f03c\";\r\n}\r\n.fa-video-camera:before {\r\n  content: \"\\f03d\";\r\n}\r\n.fa-photo:before,\r\n.fa-image:before,\r\n.fa-picture-o:before {\r\n  content: \"\\f03e\";\r\n}\r\n.fa-pencil:before {\r\n  content: \"\\f040\";\r\n}\r\n.fa-map-marker:before {\r\n  content: \"\\f041\";\r\n}\r\n.fa-adjust:before {\r\n  content: \"\\f042\";\r\n}\r\n.fa-tint:before {\r\n  content: \"\\f043\";\r\n}\r\n.fa-edit:before,\r\n.fa-pencil-square-o:before {\r\n  content: \"\\f044\";\r\n}\r\n.fa-share-square-o:before {\r\n  content: \"\\f045\";\r\n}\r\n.fa-check-square-o:before {\r\n  content: \"\\f046\";\r\n}\r\n.fa-arrows:before {\r\n  content: \"\\f047\";\r\n}\r\n.fa-step-backward:before {\r\n  content: \"\\f048\";\r\n}\r\n.fa-fast-backward:before {\r\n  content: \"\\f049\";\r\n}\r\n.fa-backward:before {\r\n  content: \"\\f04a\";\r\n}\r\n.fa-play:before {\r\n  content: \"\\f04b\";\r\n}\r\n.fa-pause:before {\r\n  content: \"\\f04c\";\r\n}\r\n.fa-stop:before {\r\n  content: \"\\f04d\";\r\n}\r\n.fa-forward:before {\r\n  content: \"\\f04e\";\r\n}\r\n.fa-fast-forward:before {\r\n  content: \"\\f050\";\r\n}\r\n.fa-step-forward:before {\r\n  content: \"\\f051\";\r\n}\r\n.fa-eject:before {\r\n  content: \"\\f052\";\r\n}\r\n.fa-chevron-left:before {\r\n  content: \"\\f053\";\r\n}\r\n.fa-chevron-right:before {\r\n  content: \"\\f054\";\r\n}\r\n.fa-plus-circle:before {\r\n  content: \"\\f055\";\r\n}\r\n.fa-minus-circle:before {\r\n  content: \"\\f056\";\r\n}\r\n.fa-times-circle:before {\r\n  content: \"\\f057\";\r\n}\r\n.fa-check-circle:before {\r\n  content: \"\\f058\";\r\n}\r\n.fa-question-circle:before {\r\n  content: \"\\f059\";\r\n}\r\n.fa-info-circle:before {\r\n  content: \"\\f05a\";\r\n}\r\n.fa-crosshairs:before {\r\n  content: \"\\f05b\";\r\n}\r\n.fa-times-circle-o:before {\r\n  content: \"\\f05c\";\r\n}\r\n.fa-check-circle-o:before {\r\n  content: \"\\f05d\";\r\n}\r\n.fa-ban:before {\r\n  content: \"\\f05e\";\r\n}\r\n.fa-arrow-left:before {\r\n  content: \"\\f060\";\r\n}\r\n.fa-arrow-right:before {\r\n  content: \"\\f061\";\r\n}\r\n.fa-arrow-up:before {\r\n  content: \"\\f062\";\r\n}\r\n.fa-arrow-down:before {\r\n  content: \"\\f063\";\r\n}\r\n.fa-mail-forward:before,\r\n.fa-share:before {\r\n  content: \"\\f064\";\r\n}\r\n.fa-expand:before {\r\n  content: \"\\f065\";\r\n}\r\n.fa-compress:before {\r\n  content: \"\\f066\";\r\n}\r\n.fa-plus:before {\r\n  content: \"\\f067\";\r\n}\r\n.fa-minus:before {\r\n  content: \"\\f068\";\r\n}\r\n.fa-asterisk:before {\r\n  content: \"\\f069\";\r\n}\r\n.fa-exclamation-circle:before {\r\n  content: \"\\f06a\";\r\n}\r\n.fa-gift:before {\r\n  content: \"\\f06b\";\r\n}\r\n.fa-leaf:before {\r\n  content: \"\\f06c\";\r\n}\r\n.fa-fire:before {\r\n  content: \"\\f06d\";\r\n}\r\n.fa-eye:before {\r\n  content: \"\\f06e\";\r\n}\r\n.fa-eye-slash:before {\r\n  content: \"\\f070\";\r\n}\r\n.fa-warning:before,\r\n.fa-exclamation-triangle:before {\r\n  content: \"\\f071\";\r\n}\r\n.fa-plane:before {\r\n  content: \"\\f072\";\r\n}\r\n.fa-calendar:before {\r\n  content: \"\\f073\";\r\n}\r\n.fa-random:before {\r\n  content: \"\\f074\";\r\n}\r\n.fa-comment:before {\r\n  content: \"\\f075\";\r\n}\r\n.fa-magnet:before {\r\n  content: \"\\f076\";\r\n}\r\n.fa-chevron-up:before {\r\n  content: \"\\f077\";\r\n}\r\n.fa-chevron-down:before {\r\n  content: \"\\f078\";\r\n}\r\n.fa-retweet:before {\r\n  content: \"\\f079\";\r\n}\r\n.fa-shopping-cart:before {\r\n  content: \"\\f07a\";\r\n}\r\n.fa-folder:before {\r\n  content: \"\\f07b\";\r\n}\r\n.fa-folder-open:before {\r\n  content: \"\\f07c\";\r\n}\r\n.fa-arrows-v:before {\r\n  content: \"\\f07d\";\r\n}\r\n.fa-arrows-h:before {\r\n  content: \"\\f07e\";\r\n}\r\n.fa-bar-chart-o:before,\r\n.fa-bar-chart:before {\r\n  content: \"\\f080\";\r\n}\r\n.fa-twitter-square:before {\r\n  content: \"\\f081\";\r\n}\r\n.fa-facebook-square:before {\r\n  content: \"\\f082\";\r\n}\r\n.fa-camera-retro:before {\r\n  content: \"\\f083\";\r\n}\r\n.fa-key:before {\r\n  content: \"\\f084\";\r\n}\r\n.fa-gears:before,\r\n.fa-cogs:before {\r\n  content: \"\\f085\";\r\n}\r\n.fa-comments:before {\r\n  content: \"\\f086\";\r\n}\r\n.fa-thumbs-o-up:before {\r\n  content: \"\\f087\";\r\n}\r\n.fa-thumbs-o-down:before {\r\n  content: \"\\f088\";\r\n}\r\n.fa-star-half:before {\r\n  content: \"\\f089\";\r\n}\r\n.fa-heart-o:before {\r\n  content: \"\\f08a\";\r\n}\r\n.fa-sign-out:before {\r\n  content: \"\\f08b\";\r\n}\r\n.fa-linkedin-square:before {\r\n  content: \"\\f08c\";\r\n}\r\n.fa-thumb-tack:before {\r\n  content: \"\\f08d\";\r\n}\r\n.fa-external-link:before {\r\n  content: \"\\f08e\";\r\n}\r\n.fa-sign-in:before {\r\n  content: \"\\f090\";\r\n}\r\n.fa-trophy:before {\r\n  content: \"\\f091\";\r\n}\r\n.fa-github-square:before {\r\n  content: \"\\f092\";\r\n}\r\n.fa-upload:before {\r\n  content: \"\\f093\";\r\n}\r\n.fa-lemon-o:before {\r\n  content: \"\\f094\";\r\n}\r\n.fa-phone:before {\r\n  content: \"\\f095\";\r\n}\r\n.fa-square-o:before {\r\n  content: \"\\f096\";\r\n}\r\n.fa-bookmark-o:before {\r\n  content: \"\\f097\";\r\n}\r\n.fa-phone-square:before {\r\n  content: \"\\f098\";\r\n}\r\n.fa-twitter:before {\r\n  content: \"\\f099\";\r\n}\r\n.fa-facebook-f:before,\r\n.fa-facebook:before {\r\n  content: \"\\f09a\";\r\n}\r\n.fa-github:before {\r\n  content: \"\\f09b\";\r\n}\r\n.fa-unlock:before {\r\n  content: \"\\f09c\";\r\n}\r\n.fa-credit-card:before {\r\n  content: \"\\f09d\";\r\n}\r\n.fa-feed:before,\r\n.fa-rss:before {\r\n  content: \"\\f09e\";\r\n}\r\n.fa-hdd-o:before {\r\n  content: \"\\f0a0\";\r\n}\r\n.fa-bullhorn:before {\r\n  content: \"\\f0a1\";\r\n}\r\n.fa-bell:before {\r\n  content: \"\\f0f3\";\r\n}\r\n.fa-certificate:before {\r\n  content: \"\\f0a3\";\r\n}\r\n.fa-hand-o-right:before {\r\n  content: \"\\f0a4\";\r\n}\r\n.fa-hand-o-left:before {\r\n  content: \"\\f0a5\";\r\n}\r\n.fa-hand-o-up:before {\r\n  content: \"\\f0a6\";\r\n}\r\n.fa-hand-o-down:before {\r\n  content: \"\\f0a7\";\r\n}\r\n.fa-arrow-circle-left:before {\r\n  content: \"\\f0a8\";\r\n}\r\n.fa-arrow-circle-right:before {\r\n  content: \"\\f0a9\";\r\n}\r\n.fa-arrow-circle-up:before {\r\n  content: \"\\f0aa\";\r\n}\r\n.fa-arrow-circle-down:before {\r\n  content: \"\\f0ab\";\r\n}\r\n.fa-globe:before {\r\n  content: \"\\f0ac\";\r\n}\r\n.fa-wrench:before {\r\n  content: \"\\f0ad\";\r\n}\r\n.fa-tasks:before {\r\n  content: \"\\f0ae\";\r\n}\r\n.fa-filter:before {\r\n  content: \"\\f0b0\";\r\n}\r\n.fa-briefcase:before {\r\n  content: \"\\f0b1\";\r\n}\r\n.fa-arrows-alt:before {\r\n  content: \"\\f0b2\";\r\n}\r\n.fa-group:before,\r\n.fa-users:before {\r\n  content: \"\\f0c0\";\r\n}\r\n.fa-chain:before,\r\n.fa-link:before {\r\n  content: \"\\f0c1\";\r\n}\r\n.fa-cloud:before {\r\n  content: \"\\f0c2\";\r\n}\r\n.fa-flask:before {\r\n  content: \"\\f0c3\";\r\n}\r\n.fa-cut:before,\r\n.fa-scissors:before {\r\n  content: \"\\f0c4\";\r\n}\r\n.fa-copy:before,\r\n.fa-files-o:before {\r\n  content: \"\\f0c5\";\r\n}\r\n.fa-paperclip:before {\r\n  content: \"\\f0c6\";\r\n}\r\n.fa-save:before,\r\n.fa-floppy-o:before {\r\n  content: \"\\f0c7\";\r\n}\r\n.fa-square:before {\r\n  content: \"\\f0c8\";\r\n}\r\n.fa-navicon:before,\r\n.fa-reorder:before,\r\n.fa-bars:before {\r\n  content: \"\\f0c9\";\r\n}\r\n.fa-list-ul:before {\r\n  content: \"\\f0ca\";\r\n}\r\n.fa-list-ol:before {\r\n  content: \"\\f0cb\";\r\n}\r\n.fa-strikethrough:before {\r\n  content: \"\\f0cc\";\r\n}\r\n.fa-underline:before {\r\n  content: \"\\f0cd\";\r\n}\r\n.fa-table:before {\r\n  content: \"\\f0ce\";\r\n}\r\n.fa-magic:before {\r\n  content: \"\\f0d0\";\r\n}\r\n.fa-truck:before {\r\n  content: \"\\f0d1\";\r\n}\r\n.fa-pinterest:before {\r\n  content: \"\\f0d2\";\r\n}\r\n.fa-pinterest-square:before {\r\n  content: \"\\f0d3\";\r\n}\r\n.fa-google-plus-square:before {\r\n  content: \"\\f0d4\";\r\n}\r\n.fa-google-plus:before {\r\n  content: \"\\f0d5\";\r\n}\r\n.fa-money:before {\r\n  content: \"\\f0d6\";\r\n}\r\n.fa-caret-down:before {\r\n  content: \"\\f0d7\";\r\n}\r\n.fa-caret-up:before {\r\n  content: \"\\f0d8\";\r\n}\r\n.fa-caret-left:before {\r\n  content: \"\\f0d9\";\r\n}\r\n.fa-caret-right:before {\r\n  content: \"\\f0da\";\r\n}\r\n.fa-columns:before {\r\n  content: \"\\f0db\";\r\n}\r\n.fa-unsorted:before,\r\n.fa-sort:before {\r\n  content: \"\\f0dc\";\r\n}\r\n.fa-sort-down:before,\r\n.fa-sort-desc:before {\r\n  content: \"\\f0dd\";\r\n}\r\n.fa-sort-up:before,\r\n.fa-sort-asc:before {\r\n  content: \"\\f0de\";\r\n}\r\n.fa-envelope:before {\r\n  content: \"\\f0e0\";\r\n}\r\n.fa-linkedin:before {\r\n  content: \"\\f0e1\";\r\n}\r\n.fa-rotate-left:before,\r\n.fa-undo:before {\r\n  content: \"\\f0e2\";\r\n}\r\n.fa-legal:before,\r\n.fa-gavel:before {\r\n  content: \"\\f0e3\";\r\n}\r\n.fa-dashboard:before,\r\n.fa-tachometer:before {\r\n  content: \"\\f0e4\";\r\n}\r\n.fa-comment-o:before {\r\n  content: \"\\f0e5\";\r\n}\r\n.fa-comments-o:before {\r\n  content: \"\\f0e6\";\r\n}\r\n.fa-flash:before,\r\n.fa-bolt:before {\r\n  content: \"\\f0e7\";\r\n}\r\n.fa-sitemap:before {\r\n  content: \"\\f0e8\";\r\n}\r\n.fa-umbrella:before {\r\n  content: \"\\f0e9\";\r\n}\r\n.fa-paste:before,\r\n.fa-clipboard:before {\r\n  content: \"\\f0ea\";\r\n}\r\n.fa-lightbulb-o:before {\r\n  content: \"\\f0eb\";\r\n}\r\n.fa-exchange:before {\r\n  content: \"\\f0ec\";\r\n}\r\n.fa-cloud-download:before {\r\n  content: \"\\f0ed\";\r\n}\r\n.fa-cloud-upload:before {\r\n  content: \"\\f0ee\";\r\n}\r\n.fa-user-md:before {\r\n  content: \"\\f0f0\";\r\n}\r\n.fa-stethoscope:before {\r\n  content: \"\\f0f1\";\r\n}\r\n.fa-suitcase:before {\r\n  content: \"\\f0f2\";\r\n}\r\n.fa-bell-o:before {\r\n  content: \"\\f0a2\";\r\n}\r\n.fa-coffee:before {\r\n  content: \"\\f0f4\";\r\n}\r\n.fa-cutlery:before {\r\n  content: \"\\f0f5\";\r\n}\r\n.fa-file-text-o:before {\r\n  content: \"\\f0f6\";\r\n}\r\n.fa-building-o:before {\r\n  content: \"\\f0f7\";\r\n}\r\n.fa-hospital-o:before {\r\n  content: \"\\f0f8\";\r\n}\r\n.fa-ambulance:before {\r\n  content: \"\\f0f9\";\r\n}\r\n.fa-medkit:before {\r\n  content: \"\\f0fa\";\r\n}\r\n.fa-fighter-jet:before {\r\n  content: \"\\f0fb\";\r\n}\r\n.fa-beer:before {\r\n  content: \"\\f0fc\";\r\n}\r\n.fa-h-square:before {\r\n  content: \"\\f0fd\";\r\n}\r\n.fa-plus-square:before {\r\n  content: \"\\f0fe\";\r\n}\r\n.fa-angle-double-left:before {\r\n  content: \"\\f100\";\r\n}\r\n.fa-angle-double-right:before {\r\n  content: \"\\f101\";\r\n}\r\n.fa-angle-double-up:before {\r\n  content: \"\\f102\";\r\n}\r\n.fa-angle-double-down:before {\r\n  content: \"\\f103\";\r\n}\r\n.fa-angle-left:before {\r\n  content: \"\\f104\";\r\n}\r\n.fa-angle-right:before {\r\n  content: \"\\f105\";\r\n}\r\n.fa-angle-up:before {\r\n  content: \"\\f106\";\r\n}\r\n.fa-angle-down:before {\r\n  content: \"\\f107\";\r\n}\r\n.fa-desktop:before {\r\n  content: \"\\f108\";\r\n}\r\n.fa-laptop:before {\r\n  content: \"\\f109\";\r\n}\r\n.fa-tablet:before {\r\n  content: \"\\f10a\";\r\n}\r\n.fa-mobile-phone:before,\r\n.fa-mobile:before {\r\n  content: \"\\f10b\";\r\n}\r\n.fa-circle-o:before {\r\n  content: \"\\f10c\";\r\n}\r\n.fa-quote-left:before {\r\n  content: \"\\f10d\";\r\n}\r\n.fa-quote-right:before {\r\n  content: \"\\f10e\";\r\n}\r\n.fa-spinner:before {\r\n  content: \"\\f110\";\r\n}\r\n.fa-circle:before {\r\n  content: \"\\f111\";\r\n}\r\n.fa-mail-reply:before,\r\n.fa-reply:before {\r\n  content: \"\\f112\";\r\n}\r\n.fa-github-alt:before {\r\n  content: \"\\f113\";\r\n}\r\n.fa-folder-o:before {\r\n  content: \"\\f114\";\r\n}\r\n.fa-folder-open-o:before {\r\n  content: \"\\f115\";\r\n}\r\n.fa-smile-o:before {\r\n  content: \"\\f118\";\r\n}\r\n.fa-frown-o:before {\r\n  content: \"\\f119\";\r\n}\r\n.fa-meh-o:before {\r\n  content: \"\\f11a\";\r\n}\r\n.fa-gamepad:before {\r\n  content: \"\\f11b\";\r\n}\r\n.fa-keyboard-o:before {\r\n  content: \"\\f11c\";\r\n}\r\n.fa-flag-o:before {\r\n  content: \"\\f11d\";\r\n}\r\n.fa-flag-checkered:before {\r\n  content: \"\\f11e\";\r\n}\r\n.fa-terminal:before {\r\n  content: \"\\f120\";\r\n}\r\n.fa-code:before {\r\n  content: \"\\f121\";\r\n}\r\n.fa-mail-reply-all:before,\r\n.fa-reply-all:before {\r\n  content: \"\\f122\";\r\n}\r\n.fa-star-half-empty:before,\r\n.fa-star-half-full:before,\r\n.fa-star-half-o:before {\r\n  content: \"\\f123\";\r\n}\r\n.fa-location-arrow:before {\r\n  content: \"\\f124\";\r\n}\r\n.fa-crop:before {\r\n  content: \"\\f125\";\r\n}\r\n.fa-code-fork:before {\r\n  content: \"\\f126\";\r\n}\r\n.fa-unlink:before,\r\n.fa-chain-broken:before {\r\n  content: \"\\f127\";\r\n}\r\n.fa-question:before {\r\n  content: \"\\f128\";\r\n}\r\n.fa-info:before {\r\n  content: \"\\f129\";\r\n}\r\n.fa-exclamation:before {\r\n  content: \"\\f12a\";\r\n}\r\n.fa-superscript:before {\r\n  content: \"\\f12b\";\r\n}\r\n.fa-subscript:before {\r\n  content: \"\\f12c\";\r\n}\r\n.fa-eraser:before {\r\n  content: \"\\f12d\";\r\n}\r\n.fa-puzzle-piece:before {\r\n  content: \"\\f12e\";\r\n}\r\n.fa-microphone:before {\r\n  content: \"\\f130\";\r\n}\r\n.fa-microphone-slash:before {\r\n  content: \"\\f131\";\r\n}\r\n.fa-shield:before {\r\n  content: \"\\f132\";\r\n}\r\n.fa-calendar-o:before {\r\n  content: \"\\f133\";\r\n}\r\n.fa-fire-extinguisher:before {\r\n  content: \"\\f134\";\r\n}\r\n.fa-rocket:before {\r\n  content: \"\\f135\";\r\n}\r\n.fa-maxcdn:before {\r\n  content: \"\\f136\";\r\n}\r\n.fa-chevron-circle-left:before {\r\n  content: \"\\f137\";\r\n}\r\n.fa-chevron-circle-right:before {\r\n  content: \"\\f138\";\r\n}\r\n.fa-chevron-circle-up:before {\r\n  content: \"\\f139\";\r\n}\r\n.fa-chevron-circle-down:before {\r\n  content: \"\\f13a\";\r\n}\r\n.fa-html5:before {\r\n  content: \"\\f13b\";\r\n}\r\n.fa-css3:before {\r\n  content: \"\\f13c\";\r\n}\r\n.fa-anchor:before {\r\n  content: \"\\f13d\";\r\n}\r\n.fa-unlock-alt:before {\r\n  content: \"\\f13e\";\r\n}\r\n.fa-bullseye:before {\r\n  content: \"\\f140\";\r\n}\r\n.fa-ellipsis-h:before {\r\n  content: \"\\f141\";\r\n}\r\n.fa-ellipsis-v:before {\r\n  content: \"\\f142\";\r\n}\r\n.fa-rss-square:before {\r\n  content: \"\\f143\";\r\n}\r\n.fa-play-circle:before {\r\n  content: \"\\f144\";\r\n}\r\n.fa-ticket:before {\r\n  content: \"\\f145\";\r\n}\r\n.fa-minus-square:before {\r\n  content: \"\\f146\";\r\n}\r\n.fa-minus-square-o:before {\r\n  content: \"\\f147\";\r\n}\r\n.fa-level-up:before {\r\n  content: \"\\f148\";\r\n}\r\n.fa-level-down:before {\r\n  content: \"\\f149\";\r\n}\r\n.fa-check-square:before {\r\n  content: \"\\f14a\";\r\n}\r\n.fa-pencil-square:before {\r\n  content: \"\\f14b\";\r\n}\r\n.fa-external-link-square:before {\r\n  content: \"\\f14c\";\r\n}\r\n.fa-share-square:before {\r\n  content: \"\\f14d\";\r\n}\r\n.fa-compass:before {\r\n  content: \"\\f14e\";\r\n}\r\n.fa-toggle-down:before,\r\n.fa-caret-square-o-down:before {\r\n  content: \"\\f150\";\r\n}\r\n.fa-toggle-up:before,\r\n.fa-caret-square-o-up:before {\r\n  content: \"\\f151\";\r\n}\r\n.fa-toggle-right:before,\r\n.fa-caret-square-o-right:before {\r\n  content: \"\\f152\";\r\n}\r\n.fa-euro:before,\r\n.fa-eur:before {\r\n  content: \"\\f153\";\r\n}\r\n.fa-gbp:before {\r\n  content: \"\\f154\";\r\n}\r\n.fa-dollar:before,\r\n.fa-usd:before {\r\n  content: \"\\f155\";\r\n}\r\n.fa-rupee:before,\r\n.fa-inr:before {\r\n  content: \"\\f156\";\r\n}\r\n.fa-cny:before,\r\n.fa-rmb:before,\r\n.fa-yen:before,\r\n.fa-jpy:before {\r\n  content: \"\\f157\";\r\n}\r\n.fa-ruble:before,\r\n.fa-rouble:before,\r\n.fa-rub:before {\r\n  content: \"\\f158\";\r\n}\r\n.fa-won:before,\r\n.fa-krw:before {\r\n  content: \"\\f159\";\r\n}\r\n.fa-bitcoin:before,\r\n.fa-btc:before {\r\n  content: \"\\f15a\";\r\n}\r\n.fa-file:before {\r\n  content: \"\\f15b\";\r\n}\r\n.fa-file-text:before {\r\n  content: \"\\f15c\";\r\n}\r\n.fa-sort-alpha-asc:before {\r\n  content: \"\\f15d\";\r\n}\r\n.fa-sort-alpha-desc:before {\r\n  content: \"\\f15e\";\r\n}\r\n.fa-sort-amount-asc:before {\r\n  content: \"\\f160\";\r\n}\r\n.fa-sort-amount-desc:before {\r\n  content: \"\\f161\";\r\n}\r\n.fa-sort-numeric-asc:before {\r\n  content: \"\\f162\";\r\n}\r\n.fa-sort-numeric-desc:before {\r\n  content: \"\\f163\";\r\n}\r\n.fa-thumbs-up:before {\r\n  content: \"\\f164\";\r\n}\r\n.fa-thumbs-down:before {\r\n  content: \"\\f165\";\r\n}\r\n.fa-youtube-square:before {\r\n  content: \"\\f166\";\r\n}\r\n.fa-youtube:before {\r\n  content: \"\\f167\";\r\n}\r\n.fa-xing:before {\r\n  content: \"\\f168\";\r\n}\r\n.fa-xing-square:before {\r\n  content: \"\\f169\";\r\n}\r\n.fa-youtube-play:before {\r\n  content: \"\\f16a\";\r\n}\r\n.fa-dropbox:before {\r\n  content: \"\\f16b\";\r\n}\r\n.fa-stack-overflow:before {\r\n  content: \"\\f16c\";\r\n}\r\n.fa-instagram:before {\r\n  content: \"\\f16d\";\r\n}\r\n.fa-flickr:before {\r\n  content: \"\\f16e\";\r\n}\r\n.fa-adn:before {\r\n  content: \"\\f170\";\r\n}\r\n.fa-bitbucket:before {\r\n  content: \"\\f171\";\r\n}\r\n.fa-bitbucket-square:before {\r\n  content: \"\\f172\";\r\n}\r\n.fa-tumblr:before {\r\n  content: \"\\f173\";\r\n}\r\n.fa-tumblr-square:before {\r\n  content: \"\\f174\";\r\n}\r\n.fa-long-arrow-down:before {\r\n  content: \"\\f175\";\r\n}\r\n.fa-long-arrow-up:before {\r\n  content: \"\\f176\";\r\n}\r\n.fa-long-arrow-left:before {\r\n  content: \"\\f177\";\r\n}\r\n.fa-long-arrow-right:before {\r\n  content: \"\\f178\";\r\n}\r\n.fa-apple:before {\r\n  content: \"\\f179\";\r\n}\r\n.fa-windows:before {\r\n  content: \"\\f17a\";\r\n}\r\n.fa-android:before {\r\n  content: \"\\f17b\";\r\n}\r\n.fa-linux:before {\r\n  content: \"\\f17c\";\r\n}\r\n.fa-dribbble:before {\r\n  content: \"\\f17d\";\r\n}\r\n.fa-skype:before {\r\n  content: \"\\f17e\";\r\n}\r\n.fa-foursquare:before {\r\n  content: \"\\f180\";\r\n}\r\n.fa-trello:before {\r\n  content: \"\\f181\";\r\n}\r\n.fa-female:before {\r\n  content: \"\\f182\";\r\n}\r\n.fa-male:before {\r\n  content: \"\\f183\";\r\n}\r\n.fa-gittip:before,\r\n.fa-gratipay:before {\r\n  content: \"\\f184\";\r\n}\r\n.fa-sun-o:before {\r\n  content: \"\\f185\";\r\n}\r\n.fa-moon-o:before {\r\n  content: \"\\f186\";\r\n}\r\n.fa-archive:before {\r\n  content: \"\\f187\";\r\n}\r\n.fa-bug:before {\r\n  content: \"\\f188\";\r\n}\r\n.fa-vk:before {\r\n  content: \"\\f189\";\r\n}\r\n.fa-weibo:before {\r\n  content: \"\\f18a\";\r\n}\r\n.fa-renren:before {\r\n  content: \"\\f18b\";\r\n}\r\n.fa-pagelines:before {\r\n  content: \"\\f18c\";\r\n}\r\n.fa-stack-exchange:before {\r\n  content: \"\\f18d\";\r\n}\r\n.fa-arrow-circle-o-right:before {\r\n  content: \"\\f18e\";\r\n}\r\n.fa-arrow-circle-o-left:before {\r\n  content: \"\\f190\";\r\n}\r\n.fa-toggle-left:before,\r\n.fa-caret-square-o-left:before {\r\n  content: \"\\f191\";\r\n}\r\n.fa-dot-circle-o:before {\r\n  content: \"\\f192\";\r\n}\r\n.fa-wheelchair:before {\r\n  content: \"\\f193\";\r\n}\r\n.fa-vimeo-square:before {\r\n  content: \"\\f194\";\r\n}\r\n.fa-turkish-lira:before,\r\n.fa-try:before {\r\n  content: \"\\f195\";\r\n}\r\n.fa-plus-square-o:before {\r\n  content: \"\\f196\";\r\n}\r\n.fa-space-shuttle:before {\r\n  content: \"\\f197\";\r\n}\r\n.fa-slack:before {\r\n  content: \"\\f198\";\r\n}\r\n.fa-envelope-square:before {\r\n  content: \"\\f199\";\r\n}\r\n.fa-wordpress:before {\r\n  content: \"\\f19a\";\r\n}\r\n.fa-openid:before {\r\n  content: \"\\f19b\";\r\n}\r\n.fa-institution:before,\r\n.fa-bank:before,\r\n.fa-university:before {\r\n  content: \"\\f19c\";\r\n}\r\n.fa-mortar-board:before,\r\n.fa-graduation-cap:before {\r\n  content: \"\\f19d\";\r\n}\r\n.fa-yahoo:before {\r\n  content: \"\\f19e\";\r\n}\r\n.fa-google:before {\r\n  content: \"\\f1a0\";\r\n}\r\n.fa-reddit:before {\r\n  content: \"\\f1a1\";\r\n}\r\n.fa-reddit-square:before {\r\n  content: \"\\f1a2\";\r\n}\r\n.fa-stumbleupon-circle:before {\r\n  content: \"\\f1a3\";\r\n}\r\n.fa-stumbleupon:before {\r\n  content: \"\\f1a4\";\r\n}\r\n.fa-delicious:before {\r\n  content: \"\\f1a5\";\r\n}\r\n.fa-digg:before {\r\n  content: \"\\f1a6\";\r\n}\r\n.fa-pied-piper-pp:before {\r\n  content: \"\\f1a7\";\r\n}\r\n.fa-pied-piper-alt:before {\r\n  content: \"\\f1a8\";\r\n}\r\n.fa-drupal:before {\r\n  content: \"\\f1a9\";\r\n}\r\n.fa-joomla:before {\r\n  content: \"\\f1aa\";\r\n}\r\n.fa-language:before {\r\n  content: \"\\f1ab\";\r\n}\r\n.fa-fax:before {\r\n  content: \"\\f1ac\";\r\n}\r\n.fa-building:before {\r\n  content: \"\\f1ad\";\r\n}\r\n.fa-child:before {\r\n  content: \"\\f1ae\";\r\n}\r\n.fa-paw:before {\r\n  content: \"\\f1b0\";\r\n}\r\n.fa-spoon:before {\r\n  content: \"\\f1b1\";\r\n}\r\n.fa-cube:before {\r\n  content: \"\\f1b2\";\r\n}\r\n.fa-cubes:before {\r\n  content: \"\\f1b3\";\r\n}\r\n.fa-behance:before {\r\n  content: \"\\f1b4\";\r\n}\r\n.fa-behance-square:before {\r\n  content: \"\\f1b5\";\r\n}\r\n.fa-steam:before {\r\n  content: \"\\f1b6\";\r\n}\r\n.fa-steam-square:before {\r\n  content: \"\\f1b7\";\r\n}\r\n.fa-recycle:before {\r\n  content: \"\\f1b8\";\r\n}\r\n.fa-automobile:before,\r\n.fa-car:before {\r\n  content: \"\\f1b9\";\r\n}\r\n.fa-cab:before,\r\n.fa-taxi:before {\r\n  content: \"\\f1ba\";\r\n}\r\n.fa-tree:before {\r\n  content: \"\\f1bb\";\r\n}\r\n.fa-spotify:before {\r\n  content: \"\\f1bc\";\r\n}\r\n.fa-deviantart:before {\r\n  content: \"\\f1bd\";\r\n}\r\n.fa-soundcloud:before {\r\n  content: \"\\f1be\";\r\n}\r\n.fa-database:before {\r\n  content: \"\\f1c0\";\r\n}\r\n.fa-file-pdf-o:before {\r\n  content: \"\\f1c1\";\r\n}\r\n.fa-file-word-o:before {\r\n  content: \"\\f1c2\";\r\n}\r\n.fa-file-excel-o:before {\r\n  content: \"\\f1c3\";\r\n}\r\n.fa-file-powerpoint-o:before {\r\n  content: \"\\f1c4\";\r\n}\r\n.fa-file-photo-o:before,\r\n.fa-file-picture-o:before,\r\n.fa-file-image-o:before {\r\n  content: \"\\f1c5\";\r\n}\r\n.fa-file-zip-o:before,\r\n.fa-file-archive-o:before {\r\n  content: \"\\f1c6\";\r\n}\r\n.fa-file-sound-o:before,\r\n.fa-file-audio-o:before {\r\n  content: \"\\f1c7\";\r\n}\r\n.fa-file-movie-o:before,\r\n.fa-file-video-o:before {\r\n  content: \"\\f1c8\";\r\n}\r\n.fa-file-code-o:before {\r\n  content: \"\\f1c9\";\r\n}\r\n.fa-vine:before {\r\n  content: \"\\f1ca\";\r\n}\r\n.fa-codepen:before {\r\n  content: \"\\f1cb\";\r\n}\r\n.fa-jsfiddle:before {\r\n  content: \"\\f1cc\";\r\n}\r\n.fa-life-bouy:before,\r\n.fa-life-buoy:before,\r\n.fa-life-saver:before,\r\n.fa-support:before,\r\n.fa-life-ring:before {\r\n  content: \"\\f1cd\";\r\n}\r\n.fa-circle-o-notch:before {\r\n  content: \"\\f1ce\";\r\n}\r\n.fa-ra:before,\r\n.fa-resistance:before,\r\n.fa-rebel:before {\r\n  content: \"\\f1d0\";\r\n}\r\n.fa-ge:before,\r\n.fa-empire:before {\r\n  content: \"\\f1d1\";\r\n}\r\n.fa-git-square:before {\r\n  content: \"\\f1d2\";\r\n}\r\n.fa-git:before {\r\n  content: \"\\f1d3\";\r\n}\r\n.fa-y-combinator-square:before,\r\n.fa-yc-square:before,\r\n.fa-hacker-news:before {\r\n  content: \"\\f1d4\";\r\n}\r\n.fa-tencent-weibo:before {\r\n  content: \"\\f1d5\";\r\n}\r\n.fa-qq:before {\r\n  content: \"\\f1d6\";\r\n}\r\n.fa-wechat:before,\r\n.fa-weixin:before {\r\n  content: \"\\f1d7\";\r\n}\r\n.fa-send:before,\r\n.fa-paper-plane:before {\r\n  content: \"\\f1d8\";\r\n}\r\n.fa-send-o:before,\r\n.fa-paper-plane-o:before {\r\n  content: \"\\f1d9\";\r\n}\r\n.fa-history:before {\r\n  content: \"\\f1da\";\r\n}\r\n.fa-circle-thin:before {\r\n  content: \"\\f1db\";\r\n}\r\n.fa-header:before {\r\n  content: \"\\f1dc\";\r\n}\r\n.fa-paragraph:before {\r\n  content: \"\\f1dd\";\r\n}\r\n.fa-sliders:before {\r\n  content: \"\\f1de\";\r\n}\r\n.fa-share-alt:before {\r\n  content: \"\\f1e0\";\r\n}\r\n.fa-share-alt-square:before {\r\n  content: \"\\f1e1\";\r\n}\r\n.fa-bomb:before {\r\n  content: \"\\f1e2\";\r\n}\r\n.fa-soccer-ball-o:before,\r\n.fa-futbol-o:before {\r\n  content: \"\\f1e3\";\r\n}\r\n.fa-tty:before {\r\n  content: \"\\f1e4\";\r\n}\r\n.fa-binoculars:before {\r\n  content: \"\\f1e5\";\r\n}\r\n.fa-plug:before {\r\n  content: \"\\f1e6\";\r\n}\r\n.fa-slideshare:before {\r\n  content: \"\\f1e7\";\r\n}\r\n.fa-twitch:before {\r\n  content: \"\\f1e8\";\r\n}\r\n.fa-yelp:before {\r\n  content: \"\\f1e9\";\r\n}\r\n.fa-newspaper-o:before {\r\n  content: \"\\f1ea\";\r\n}\r\n.fa-wifi:before {\r\n  content: \"\\f1eb\";\r\n}\r\n.fa-calculator:before {\r\n  content: \"\\f1ec\";\r\n}\r\n.fa-paypal:before {\r\n  content: \"\\f1ed\";\r\n}\r\n.fa-google-wallet:before {\r\n  content: \"\\f1ee\";\r\n}\r\n.fa-cc-visa:before {\r\n  content: \"\\f1f0\";\r\n}\r\n.fa-cc-mastercard:before {\r\n  content: \"\\f1f1\";\r\n}\r\n.fa-cc-discover:before {\r\n  content: \"\\f1f2\";\r\n}\r\n.fa-cc-amex:before {\r\n  content: \"\\f1f3\";\r\n}\r\n.fa-cc-paypal:before {\r\n  content: \"\\f1f4\";\r\n}\r\n.fa-cc-stripe:before {\r\n  content: \"\\f1f5\";\r\n}\r\n.fa-bell-slash:before {\r\n  content: \"\\f1f6\";\r\n}\r\n.fa-bell-slash-o:before {\r\n  content: \"\\f1f7\";\r\n}\r\n.fa-trash:before {\r\n  content: \"\\f1f8\";\r\n}\r\n.fa-copyright:before {\r\n  content: \"\\f1f9\";\r\n}\r\n.fa-at:before {\r\n  content: \"\\f1fa\";\r\n}\r\n.fa-eyedropper:before {\r\n  content: \"\\f1fb\";\r\n}\r\n.fa-paint-brush:before {\r\n  content: \"\\f1fc\";\r\n}\r\n.fa-birthday-cake:before {\r\n  content: \"\\f1fd\";\r\n}\r\n.fa-area-chart:before {\r\n  content: \"\\f1fe\";\r\n}\r\n.fa-pie-chart:before {\r\n  content: \"\\f200\";\r\n}\r\n.fa-line-chart:before {\r\n  content: \"\\f201\";\r\n}\r\n.fa-lastfm:before {\r\n  content: \"\\f202\";\r\n}\r\n.fa-lastfm-square:before {\r\n  content: \"\\f203\";\r\n}\r\n.fa-toggle-off:before {\r\n  content: \"\\f204\";\r\n}\r\n.fa-toggle-on:before {\r\n  content: \"\\f205\";\r\n}\r\n.fa-bicycle:before {\r\n  content: \"\\f206\";\r\n}\r\n.fa-bus:before {\r\n  content: \"\\f207\";\r\n}\r\n.fa-ioxhost:before {\r\n  content: \"\\f208\";\r\n}\r\n.fa-angellist:before {\r\n  content: \"\\f209\";\r\n}\r\n.fa-cc:before {\r\n  content: \"\\f20a\";\r\n}\r\n.fa-shekel:before,\r\n.fa-sheqel:before,\r\n.fa-ils:before {\r\n  content: \"\\f20b\";\r\n}\r\n.fa-meanpath:before {\r\n  content: \"\\f20c\";\r\n}\r\n.fa-buysellads:before {\r\n  content: \"\\f20d\";\r\n}\r\n.fa-connectdevelop:before {\r\n  content: \"\\f20e\";\r\n}\r\n.fa-dashcube:before {\r\n  content: \"\\f210\";\r\n}\r\n.fa-forumbee:before {\r\n  content: \"\\f211\";\r\n}\r\n.fa-leanpub:before {\r\n  content: \"\\f212\";\r\n}\r\n.fa-sellsy:before {\r\n  content: \"\\f213\";\r\n}\r\n.fa-shirtsinbulk:before {\r\n  content: \"\\f214\";\r\n}\r\n.fa-simplybuilt:before {\r\n  content: \"\\f215\";\r\n}\r\n.fa-skyatlas:before {\r\n  content: \"\\f216\";\r\n}\r\n.fa-cart-plus:before {\r\n  content: \"\\f217\";\r\n}\r\n.fa-cart-arrow-down:before {\r\n  content: \"\\f218\";\r\n}\r\n.fa-diamond:before {\r\n  content: \"\\f219\";\r\n}\r\n.fa-ship:before {\r\n  content: \"\\f21a\";\r\n}\r\n.fa-user-secret:before {\r\n  content: \"\\f21b\";\r\n}\r\n.fa-motorcycle:before {\r\n  content: \"\\f21c\";\r\n}\r\n.fa-street-view:before {\r\n  content: \"\\f21d\";\r\n}\r\n.fa-heartbeat:before {\r\n  content: \"\\f21e\";\r\n}\r\n.fa-venus:before {\r\n  content: \"\\f221\";\r\n}\r\n.fa-mars:before {\r\n  content: \"\\f222\";\r\n}\r\n.fa-mercury:before {\r\n  content: \"\\f223\";\r\n}\r\n.fa-intersex:before,\r\n.fa-transgender:before {\r\n  content: \"\\f224\";\r\n}\r\n.fa-transgender-alt:before {\r\n  content: \"\\f225\";\r\n}\r\n.fa-venus-double:before {\r\n  content: \"\\f226\";\r\n}\r\n.fa-mars-double:before {\r\n  content: \"\\f227\";\r\n}\r\n.fa-venus-mars:before {\r\n  content: \"\\f228\";\r\n}\r\n.fa-mars-stroke:before {\r\n  content: \"\\f229\";\r\n}\r\n.fa-mars-stroke-v:before {\r\n  content: \"\\f22a\";\r\n}\r\n.fa-mars-stroke-h:before {\r\n  content: \"\\f22b\";\r\n}\r\n.fa-neuter:before {\r\n  content: \"\\f22c\";\r\n}\r\n.fa-genderless:before {\r\n  content: \"\\f22d\";\r\n}\r\n.fa-facebook-official:before {\r\n  content: \"\\f230\";\r\n}\r\n.fa-pinterest-p:before {\r\n  content: \"\\f231\";\r\n}\r\n.fa-whatsapp:before {\r\n  content: \"\\f232\";\r\n}\r\n.fa-server:before {\r\n  content: \"\\f233\";\r\n}\r\n.fa-user-plus:before {\r\n  content: \"\\f234\";\r\n}\r\n.fa-user-times:before {\r\n  content: \"\\f235\";\r\n}\r\n.fa-hotel:before,\r\n.fa-bed:before {\r\n  content: \"\\f236\";\r\n}\r\n.fa-viacoin:before {\r\n  content: \"\\f237\";\r\n}\r\n.fa-train:before {\r\n  content: \"\\f238\";\r\n}\r\n.fa-subway:before {\r\n  content: \"\\f239\";\r\n}\r\n.fa-medium:before {\r\n  content: \"\\f23a\";\r\n}\r\n.fa-yc:before,\r\n.fa-y-combinator:before {\r\n  content: \"\\f23b\";\r\n}\r\n.fa-optin-monster:before {\r\n  content: \"\\f23c\";\r\n}\r\n.fa-opencart:before {\r\n  content: \"\\f23d\";\r\n}\r\n.fa-expeditedssl:before {\r\n  content: \"\\f23e\";\r\n}\r\n.fa-battery-4:before,\r\n.fa-battery:before,\r\n.fa-battery-full:before {\r\n  content: \"\\f240\";\r\n}\r\n.fa-battery-3:before,\r\n.fa-battery-three-quarters:before {\r\n  content: \"\\f241\";\r\n}\r\n.fa-battery-2:before,\r\n.fa-battery-half:before {\r\n  content: \"\\f242\";\r\n}\r\n.fa-battery-1:before,\r\n.fa-battery-quarter:before {\r\n  content: \"\\f243\";\r\n}\r\n.fa-battery-0:before,\r\n.fa-battery-empty:before {\r\n  content: \"\\f244\";\r\n}\r\n.fa-mouse-pointer:before {\r\n  content: \"\\f245\";\r\n}\r\n.fa-i-cursor:before {\r\n  content: \"\\f246\";\r\n}\r\n.fa-object-group:before {\r\n  content: \"\\f247\";\r\n}\r\n.fa-object-ungroup:before {\r\n  content: \"\\f248\";\r\n}\r\n.fa-sticky-note:before {\r\n  content: \"\\f249\";\r\n}\r\n.fa-sticky-note-o:before {\r\n  content: \"\\f24a\";\r\n}\r\n.fa-cc-jcb:before {\r\n  content: \"\\f24b\";\r\n}\r\n.fa-cc-diners-club:before {\r\n  content: \"\\f24c\";\r\n}\r\n.fa-clone:before {\r\n  content: \"\\f24d\";\r\n}\r\n.fa-balance-scale:before {\r\n  content: \"\\f24e\";\r\n}\r\n.fa-hourglass-o:before {\r\n  content: \"\\f250\";\r\n}\r\n.fa-hourglass-1:before,\r\n.fa-hourglass-start:before {\r\n  content: \"\\f251\";\r\n}\r\n.fa-hourglass-2:before,\r\n.fa-hourglass-half:before {\r\n  content: \"\\f252\";\r\n}\r\n.fa-hourglass-3:before,\r\n.fa-hourglass-end:before {\r\n  content: \"\\f253\";\r\n}\r\n.fa-hourglass:before {\r\n  content: \"\\f254\";\r\n}\r\n.fa-hand-grab-o:before,\r\n.fa-hand-rock-o:before {\r\n  content: \"\\f255\";\r\n}\r\n.fa-hand-stop-o:before,\r\n.fa-hand-paper-o:before {\r\n  content: \"\\f256\";\r\n}\r\n.fa-hand-scissors-o:before {\r\n  content: \"\\f257\";\r\n}\r\n.fa-hand-lizard-o:before {\r\n  content: \"\\f258\";\r\n}\r\n.fa-hand-spock-o:before {\r\n  content: \"\\f259\";\r\n}\r\n.fa-hand-pointer-o:before {\r\n  content: \"\\f25a\";\r\n}\r\n.fa-hand-peace-o:before {\r\n  content: \"\\f25b\";\r\n}\r\n.fa-trademark:before {\r\n  content: \"\\f25c\";\r\n}\r\n.fa-registered:before {\r\n  content: \"\\f25d\";\r\n}\r\n.fa-creative-commons:before {\r\n  content: \"\\f25e\";\r\n}\r\n.fa-gg:before {\r\n  content: \"\\f260\";\r\n}\r\n.fa-gg-circle:before {\r\n  content: \"\\f261\";\r\n}\r\n.fa-tripadvisor:before {\r\n  content: \"\\f262\";\r\n}\r\n.fa-odnoklassniki:before {\r\n  content: \"\\f263\";\r\n}\r\n.fa-odnoklassniki-square:before {\r\n  content: \"\\f264\";\r\n}\r\n.fa-get-pocket:before {\r\n  content: \"\\f265\";\r\n}\r\n.fa-wikipedia-w:before {\r\n  content: \"\\f266\";\r\n}\r\n.fa-safari:before {\r\n  content: \"\\f267\";\r\n}\r\n.fa-chrome:before {\r\n  content: \"\\f268\";\r\n}\r\n.fa-firefox:before {\r\n  content: \"\\f269\";\r\n}\r\n.fa-opera:before {\r\n  content: \"\\f26a\";\r\n}\r\n.fa-internet-explorer:before {\r\n  content: \"\\f26b\";\r\n}\r\n.fa-tv:before,\r\n.fa-television:before {\r\n  content: \"\\f26c\";\r\n}\r\n.fa-contao:before {\r\n  content: \"\\f26d\";\r\n}\r\n.fa-500px:before {\r\n  content: \"\\f26e\";\r\n}\r\n.fa-amazon:before {\r\n  content: \"\\f270\";\r\n}\r\n.fa-calendar-plus-o:before {\r\n  content: \"\\f271\";\r\n}\r\n.fa-calendar-minus-o:before {\r\n  content: \"\\f272\";\r\n}\r\n.fa-calendar-times-o:before {\r\n  content: \"\\f273\";\r\n}\r\n.fa-calendar-check-o:before {\r\n  content: \"\\f274\";\r\n}\r\n.fa-industry:before {\r\n  content: \"\\f275\";\r\n}\r\n.fa-map-pin:before {\r\n  content: \"\\f276\";\r\n}\r\n.fa-map-signs:before {\r\n  content: \"\\f277\";\r\n}\r\n.fa-map-o:before {\r\n  content: \"\\f278\";\r\n}\r\n.fa-map:before {\r\n  content: \"\\f279\";\r\n}\r\n.fa-commenting:before {\r\n  content: \"\\f27a\";\r\n}\r\n.fa-commenting-o:before {\r\n  content: \"\\f27b\";\r\n}\r\n.fa-houzz:before {\r\n  content: \"\\f27c\";\r\n}\r\n.fa-vimeo:before {\r\n  content: \"\\f27d\";\r\n}\r\n.fa-black-tie:before {\r\n  content: \"\\f27e\";\r\n}\r\n.fa-fonticons:before {\r\n  content: \"\\f280\";\r\n}\r\n.fa-reddit-alien:before {\r\n  content: \"\\f281\";\r\n}\r\n.fa-edge:before {\r\n  content: \"\\f282\";\r\n}\r\n.fa-credit-card-alt:before {\r\n  content: \"\\f283\";\r\n}\r\n.fa-codiepie:before {\r\n  content: \"\\f284\";\r\n}\r\n.fa-modx:before {\r\n  content: \"\\f285\";\r\n}\r\n.fa-fort-awesome:before {\r\n  content: \"\\f286\";\r\n}\r\n.fa-usb:before {\r\n  content: \"\\f287\";\r\n}\r\n.fa-product-hunt:before {\r\n  content: \"\\f288\";\r\n}\r\n.fa-mixcloud:before {\r\n  content: \"\\f289\";\r\n}\r\n.fa-scribd:before {\r\n  content: \"\\f28a\";\r\n}\r\n.fa-pause-circle:before {\r\n  content: \"\\f28b\";\r\n}\r\n.fa-pause-circle-o:before {\r\n  content: \"\\f28c\";\r\n}\r\n.fa-stop-circle:before {\r\n  content: \"\\f28d\";\r\n}\r\n.fa-stop-circle-o:before {\r\n  content: \"\\f28e\";\r\n}\r\n.fa-shopping-bag:before {\r\n  content: \"\\f290\";\r\n}\r\n.fa-shopping-basket:before {\r\n  content: \"\\f291\";\r\n}\r\n.fa-hashtag:before {\r\n  content: \"\\f292\";\r\n}\r\n.fa-bluetooth:before {\r\n  content: \"\\f293\";\r\n}\r\n.fa-bluetooth-b:before {\r\n  content: \"\\f294\";\r\n}\r\n.fa-percent:before {\r\n  content: \"\\f295\";\r\n}\r\n.fa-gitlab:before {\r\n  content: \"\\f296\";\r\n}\r\n.fa-wpbeginner:before {\r\n  content: \"\\f297\";\r\n}\r\n.fa-wpforms:before {\r\n  content: \"\\f298\";\r\n}\r\n.fa-envira:before {\r\n  content: \"\\f299\";\r\n}\r\n.fa-universal-access:before {\r\n  content: \"\\f29a\";\r\n}\r\n.fa-wheelchair-alt:before {\r\n  content: \"\\f29b\";\r\n}\r\n.fa-question-circle-o:before {\r\n  content: \"\\f29c\";\r\n}\r\n.fa-blind:before {\r\n  content: \"\\f29d\";\r\n}\r\n.fa-audio-description:before {\r\n  content: \"\\f29e\";\r\n}\r\n.fa-volume-control-phone:before {\r\n  content: \"\\f2a0\";\r\n}\r\n.fa-braille:before {\r\n  content: \"\\f2a1\";\r\n}\r\n.fa-assistive-listening-systems:before {\r\n  content: \"\\f2a2\";\r\n}\r\n.fa-asl-interpreting:before,\r\n.fa-american-sign-language-interpreting:before {\r\n  content: \"\\f2a3\";\r\n}\r\n.fa-deafness:before,\r\n.fa-hard-of-hearing:before,\r\n.fa-deaf:before {\r\n  content: \"\\f2a4\";\r\n}\r\n.fa-glide:before {\r\n  content: \"\\f2a5\";\r\n}\r\n.fa-glide-g:before {\r\n  content: \"\\f2a6\";\r\n}\r\n.fa-signing:before,\r\n.fa-sign-language:before {\r\n  content: \"\\f2a7\";\r\n}\r\n.fa-low-vision:before {\r\n  content: \"\\f2a8\";\r\n}\r\n.fa-viadeo:before {\r\n  content: \"\\f2a9\";\r\n}\r\n.fa-viadeo-square:before {\r\n  content: \"\\f2aa\";\r\n}\r\n.fa-snapchat:before {\r\n  content: \"\\f2ab\";\r\n}\r\n.fa-snapchat-ghost:before {\r\n  content: \"\\f2ac\";\r\n}\r\n.fa-snapchat-square:before {\r\n  content: \"\\f2ad\";\r\n}\r\n.fa-pied-piper:before {\r\n  content: \"\\f2ae\";\r\n}\r\n.fa-first-order:before {\r\n  content: \"\\f2b0\";\r\n}\r\n.fa-yoast:before {\r\n  content: \"\\f2b1\";\r\n}\r\n.fa-themeisle:before {\r\n  content: \"\\f2b2\";\r\n}\r\n.fa-google-plus-circle:before,\r\n.fa-google-plus-official:before {\r\n  content: \"\\f2b3\";\r\n}\r\n.fa-fa:before,\r\n.fa-font-awesome:before {\r\n  content: \"\\f2b4\";\r\n}\r\n.fa-handshake-o:before {\r\n  content: \"\\f2b5\";\r\n}\r\n.fa-envelope-open:before {\r\n  content: \"\\f2b6\";\r\n}\r\n.fa-envelope-open-o:before {\r\n  content: \"\\f2b7\";\r\n}\r\n.fa-linode:before {\r\n  content: \"\\f2b8\";\r\n}\r\n.fa-address-book:before {\r\n  content: \"\\f2b9\";\r\n}\r\n.fa-address-book-o:before {\r\n  content: \"\\f2ba\";\r\n}\r\n.fa-vcard:before,\r\n.fa-address-card:before {\r\n  content: \"\\f2bb\";\r\n}\r\n.fa-vcard-o:before,\r\n.fa-address-card-o:before {\r\n  content: \"\\f2bc\";\r\n}\r\n.fa-user-circle:before {\r\n  content: \"\\f2bd\";\r\n}\r\n.fa-user-circle-o:before {\r\n  content: \"\\f2be\";\r\n}\r\n.fa-user-o:before {\r\n  content: \"\\f2c0\";\r\n}\r\n.fa-id-badge:before {\r\n  content: \"\\f2c1\";\r\n}\r\n.fa-drivers-license:before,\r\n.fa-id-card:before {\r\n  content: \"\\f2c2\";\r\n}\r\n.fa-drivers-license-o:before,\r\n.fa-id-card-o:before {\r\n  content: \"\\f2c3\";\r\n}\r\n.fa-quora:before {\r\n  content: \"\\f2c4\";\r\n}\r\n.fa-free-code-camp:before {\r\n  content: \"\\f2c5\";\r\n}\r\n.fa-telegram:before {\r\n  content: \"\\f2c6\";\r\n}\r\n.fa-thermometer-4:before,\r\n.fa-thermometer:before,\r\n.fa-thermometer-full:before {\r\n  content: \"\\f2c7\";\r\n}\r\n.fa-thermometer-3:before,\r\n.fa-thermometer-three-quarters:before {\r\n  content: \"\\f2c8\";\r\n}\r\n.fa-thermometer-2:before,\r\n.fa-thermometer-half:before {\r\n  content: \"\\f2c9\";\r\n}\r\n.fa-thermometer-1:before,\r\n.fa-thermometer-quarter:before {\r\n  content: \"\\f2ca\";\r\n}\r\n.fa-thermometer-0:before,\r\n.fa-thermometer-empty:before {\r\n  content: \"\\f2cb\";\r\n}\r\n.fa-shower:before {\r\n  content: \"\\f2cc\";\r\n}\r\n.fa-bathtub:before,\r\n.fa-s15:before,\r\n.fa-bath:before {\r\n  content: \"\\f2cd\";\r\n}\r\n.fa-podcast:before {\r\n  content: \"\\f2ce\";\r\n}\r\n.fa-window-maximize:before {\r\n  content: \"\\f2d0\";\r\n}\r\n.fa-window-minimize:before {\r\n  content: \"\\f2d1\";\r\n}\r\n.fa-window-restore:before {\r\n  content: \"\\f2d2\";\r\n}\r\n.fa-times-rectangle:before,\r\n.fa-window-close:before {\r\n  content: \"\\f2d3\";\r\n}\r\n.fa-times-rectangle-o:before,\r\n.fa-window-close-o:before {\r\n  content: \"\\f2d4\";\r\n}\r\n.fa-bandcamp:before {\r\n  content: \"\\f2d5\";\r\n}\r\n.fa-grav:before {\r\n  content: \"\\f2d6\";\r\n}\r\n.fa-etsy:before {\r\n  content: \"\\f2d7\";\r\n}\r\n.fa-imdb:before {\r\n  content: \"\\f2d8\";\r\n}\r\n.fa-ravelry:before {\r\n  content: \"\\f2d9\";\r\n}\r\n.fa-eercast:before {\r\n  content: \"\\f2da\";\r\n}\r\n.fa-microchip:before {\r\n  content: \"\\f2db\";\r\n}\r\n.fa-snowflake-o:before {\r\n  content: \"\\f2dc\";\r\n}\r\n.fa-superpowers:before {\r\n  content: \"\\f2dd\";\r\n}\r\n.fa-wpexplorer:before {\r\n  content: \"\\f2de\";\r\n}\r\n.fa-meetup:before {\r\n  content: \"\\f2e0\";\r\n}\r\n.sr-only {\r\n  position: absolute;\r\n  width: 1px;\r\n  height: 1px;\r\n  padding: 0;\r\n  margin: -1px;\r\n  overflow: hidden;\r\n  clip: rect(0, 0, 0, 0);\r\n  border: 0;\r\n}\r\n.sr-only-focusable:active,\r\n.sr-only-focusable:focus {\r\n  position: static;\r\n  width: auto;\r\n  height: auto;\r\n  margin: 0;\r\n  overflow: visible;\r\n  clip: auto;\r\n}\r\n/* $colors\r\n ------------------------------------------*/\r\n.btn-adn {\n  color: #fff;\n  background-color: #d87a68;\n  border-color: #d87a68; }\r\n.btn-adn:hover {\n    color: #fff;\n    background-color: #d05f4a;\n    border-color: #ce563f; }\r\n.btn-adn:focus, .btn-adn.focus {\n    -webkit-box-shadow: 0 0 0 3px rgba(216, 122, 104, 0.5);\n            box-shadow: 0 0 0 3px rgba(216, 122, 104, 0.5); }\r\n.btn-adn.disabled, .btn-adn:disabled {\n    background-color: #d87a68;\n    border-color: #d87a68; }\r\n.btn-adn:active, .btn-adn.active,\n  .show > .btn-adn.dropdown-toggle {\n    background-color: #d05f4a;\n    background-image: none;\n    border-color: #ce563f; }\r\n.btn-bitbucket {\n  color: #fff;\n  background-color: #205081;\n  border-color: #205081; }\r\n.btn-bitbucket:hover {\n    color: #fff;\n    background-color: #183d62;\n    border-color: #163758; }\r\n.btn-bitbucket:focus, .btn-bitbucket.focus {\n    -webkit-box-shadow: 0 0 0 3px rgba(32, 80, 129, 0.5);\n            box-shadow: 0 0 0 3px rgba(32, 80, 129, 0.5); }\r\n.btn-bitbucket.disabled, .btn-bitbucket:disabled {\n    background-color: #205081;\n    border-color: #205081; }\r\n.btn-bitbucket:active, .btn-bitbucket.active,\n  .show > .btn-bitbucket.dropdown-toggle {\n    background-color: #183d62;\n    background-image: none;\n    border-color: #163758; }\r\n.btn-dropbox {\n  color: #fff;\n  background-color: #1087dd;\n  border-color: #1087dd; }\r\n.btn-dropbox:hover {\n    color: #fff;\n    background-color: #0d71b9;\n    border-color: #0d6aad; }\r\n.btn-dropbox:focus, .btn-dropbox.focus {\n    -webkit-box-shadow: 0 0 0 3px rgba(16, 135, 221, 0.5);\n            box-shadow: 0 0 0 3px rgba(16, 135, 221, 0.5); }\r\n.btn-dropbox.disabled, .btn-dropbox:disabled {\n    background-color: #1087dd;\n    border-color: #1087dd; }\r\n.btn-dropbox:active, .btn-dropbox.active,\n  .show > .btn-dropbox.dropdown-toggle {\n    background-color: #0d71b9;\n    background-image: none;\n    border-color: #0d6aad; }\r\n.btn-facebook {\n  color: #fff;\n  background-color: #3b5998;\n  border-color: #3b5998; }\r\n.btn-facebook:hover {\n    color: #fff;\n    background-color: #30497c;\n    border-color: #2d4373; }\r\n.btn-facebook:focus, .btn-facebook.focus {\n    -webkit-box-shadow: 0 0 0 3px rgba(59, 89, 152, 0.5);\n            box-shadow: 0 0 0 3px rgba(59, 89, 152, 0.5); }\r\n.btn-facebook.disabled, .btn-facebook:disabled {\n    background-color: #3b5998;\n    border-color: #3b5998; }\r\n.btn-facebook:active, .btn-facebook.active,\n  .show > .btn-facebook.dropdown-toggle {\n    background-color: #30497c;\n    background-image: none;\n    border-color: #2d4373; }\r\n.btn-flickr {\n  color: #fff;\n  background-color: #ff0084;\n  border-color: #ff0084; }\r\n.btn-flickr:hover {\n    color: #fff;\n    background-color: #d90070;\n    border-color: #cc006a; }\r\n.btn-flickr:focus, .btn-flickr.focus {\n    -webkit-box-shadow: 0 0 0 3px rgba(255, 0, 132, 0.5);\n            box-shadow: 0 0 0 3px rgba(255, 0, 132, 0.5); }\r\n.btn-flickr.disabled, .btn-flickr:disabled {\n    background-color: #ff0084;\n    border-color: #ff0084; }\r\n.btn-flickr:active, .btn-flickr.active,\n  .show > .btn-flickr.dropdown-toggle {\n    background-color: #d90070;\n    background-image: none;\n    border-color: #cc006a; }\r\n.btn-foursquare {\n  color: #fff;\n  background-color: #f94877;\n  border-color: #f94877; }\r\n.btn-foursquare:hover {\n    color: #fff;\n    background-color: #f8235b;\n    border-color: #f71752; }\r\n.btn-foursquare:focus, .btn-foursquare.focus {\n    -webkit-box-shadow: 0 0 0 3px rgba(249, 72, 119, 0.5);\n            box-shadow: 0 0 0 3px rgba(249, 72, 119, 0.5); }\r\n.btn-foursquare.disabled, .btn-foursquare:disabled {\n    background-color: #f94877;\n    border-color: #f94877; }\r\n.btn-foursquare:active, .btn-foursquare.active,\n  .show > .btn-foursquare.dropdown-toggle {\n    background-color: #f8235b;\n    background-image: none;\n    border-color: #f71752; }\r\n.btn-github {\n  color: #fff;\n  background-color: #444;\n  border-color: #444; }\r\n.btn-github:hover {\n    color: #fff;\n    background-color: #313131;\n    border-color: #2b2a2a; }\r\n.btn-github:focus, .btn-github.focus {\n    -webkit-box-shadow: 0 0 0 3px rgba(68, 68, 68, 0.5);\n            box-shadow: 0 0 0 3px rgba(68, 68, 68, 0.5); }\r\n.btn-github.disabled, .btn-github:disabled {\n    background-color: #444;\n    border-color: #444; }\r\n.btn-github:active, .btn-github.active,\n  .show > .btn-github.dropdown-toggle {\n    background-color: #313131;\n    background-image: none;\n    border-color: #2b2a2a; }\r\n.btn-google {\n  color: #fff;\n  background-color: #dd4b39;\n  border-color: #dd4b39; }\r\n.btn-google:hover {\n    color: #fff;\n    background-color: #cd3623;\n    border-color: #c23321; }\r\n.btn-google:focus, .btn-google.focus {\n    -webkit-box-shadow: 0 0 0 3px rgba(221, 75, 57, 0.5);\n            box-shadow: 0 0 0 3px rgba(221, 75, 57, 0.5); }\r\n.btn-google.disabled, .btn-google:disabled {\n    background-color: #dd4b39;\n    border-color: #dd4b39; }\r\n.btn-google:active, .btn-google.active,\n  .show > .btn-google.dropdown-toggle {\n    background-color: #cd3623;\n    background-image: none;\n    border-color: #c23321; }\r\n.btn-instagram {\n  color: #fff;\n  background-color: #3f729b;\n  border-color: #3f729b; }\r\n.btn-instagram:hover {\n    color: #fff;\n    background-color: #345e80;\n    border-color: #305777; }\r\n.btn-instagram:focus, .btn-instagram.focus {\n    -webkit-box-shadow: 0 0 0 3px rgba(63, 114, 155, 0.5);\n            box-shadow: 0 0 0 3px rgba(63, 114, 155, 0.5); }\r\n.btn-instagram.disabled, .btn-instagram:disabled {\n    background-color: #3f729b;\n    border-color: #3f729b; }\r\n.btn-instagram:active, .btn-instagram.active,\n  .show > .btn-instagram.dropdown-toggle {\n    background-color: #345e80;\n    background-image: none;\n    border-color: #305777; }\r\n.btn-linkedin {\n  color: #fff;\n  background-color: #007bb6;\n  border-color: #007bb6; }\r\n.btn-linkedin:hover {\n    color: #fff;\n    background-color: #006190;\n    border-color: #005983; }\r\n.btn-linkedin:focus, .btn-linkedin.focus {\n    -webkit-box-shadow: 0 0 0 3px rgba(0, 123, 182, 0.5);\n            box-shadow: 0 0 0 3px rgba(0, 123, 182, 0.5); }\r\n.btn-linkedin.disabled, .btn-linkedin:disabled {\n    background-color: #007bb6;\n    border-color: #007bb6; }\r\n.btn-linkedin:active, .btn-linkedin.active,\n  .show > .btn-linkedin.dropdown-toggle {\n    background-color: #006190;\n    background-image: none;\n    border-color: #005983; }\r\n.btn-microsoft {\n  color: #fff;\n  background-color: #2672ec;\n  border-color: #2672ec; }\r\n.btn-microsoft:hover {\n    color: #fff;\n    background-color: #135fd9;\n    border-color: #125acd; }\r\n.btn-microsoft:focus, .btn-microsoft.focus {\n    -webkit-box-shadow: 0 0 0 3px rgba(38, 114, 236, 0.5);\n            box-shadow: 0 0 0 3px rgba(38, 114, 236, 0.5); }\r\n.btn-microsoft.disabled, .btn-microsoft:disabled {\n    background-color: #2672ec;\n    border-color: #2672ec; }\r\n.btn-microsoft:active, .btn-microsoft.active,\n  .show > .btn-microsoft.dropdown-toggle {\n    background-color: #135fd9;\n    background-image: none;\n    border-color: #125acd; }\r\n.btn-odnoklassniki {\n  color: #fff;\n  background-color: #f4731c;\n  border-color: #f4731c; }\r\n.btn-odnoklassniki:hover {\n    color: #fff;\n    background-color: #df600b;\n    border-color: #d35b0a; }\r\n.btn-odnoklassniki:focus, .btn-odnoklassniki.focus {\n    -webkit-box-shadow: 0 0 0 3px rgba(244, 115, 28, 0.5);\n            box-shadow: 0 0 0 3px rgba(244, 115, 28, 0.5); }\r\n.btn-odnoklassniki.disabled, .btn-odnoklassniki:disabled {\n    background-color: #f4731c;\n    border-color: #f4731c; }\r\n.btn-odnoklassniki:active, .btn-odnoklassniki.active,\n  .show > .btn-odnoklassniki.dropdown-toggle {\n    background-color: #df600b;\n    background-image: none;\n    border-color: #d35b0a; }\r\n.btn-openid {\n  color: #111;\n  background-color: #f7931e;\n  border-color: #f7931e; }\r\n.btn-openid:hover {\n    color: #111;\n    background-color: #e78008;\n    border-color: #da7908; }\r\n.btn-openid:focus, .btn-openid.focus {\n    -webkit-box-shadow: 0 0 0 3px rgba(247, 147, 30, 0.5);\n            box-shadow: 0 0 0 3px rgba(247, 147, 30, 0.5); }\r\n.btn-openid.disabled, .btn-openid:disabled {\n    background-color: #f7931e;\n    border-color: #f7931e; }\r\n.btn-openid:active, .btn-openid.active,\n  .show > .btn-openid.dropdown-toggle {\n    background-color: #e78008;\n    background-image: none;\n    border-color: #da7908; }\r\n.btn-pinterest {\n  color: #fff;\n  background-color: #cb2027;\n  border-color: #cb2027; }\r\n.btn-pinterest:hover {\n    color: #fff;\n    background-color: #aa1b21;\n    border-color: #9f191f; }\r\n.btn-pinterest:focus, .btn-pinterest.focus {\n    -webkit-box-shadow: 0 0 0 3px rgba(203, 32, 39, 0.5);\n            box-shadow: 0 0 0 3px rgba(203, 32, 39, 0.5); }\r\n.btn-pinterest.disabled, .btn-pinterest:disabled {\n    background-color: #cb2027;\n    border-color: #cb2027; }\r\n.btn-pinterest:active, .btn-pinterest.active,\n  .show > .btn-pinterest.dropdown-toggle {\n    background-color: #aa1b21;\n    background-image: none;\n    border-color: #9f191f; }\r\n.btn-reddit {\n  color: #111;\n  background-color: #eff7ff;\n  border-color: #eff7ff;\n  color: block; }\r\n.btn-reddit:hover {\n    color: #111;\n    background-color: #c9e4ff;\n    border-color: #bcdeff; }\r\n.btn-reddit:focus, .btn-reddit.focus {\n    -webkit-box-shadow: 0 0 0 3px rgba(239, 247, 255, 0.5);\n            box-shadow: 0 0 0 3px rgba(239, 247, 255, 0.5); }\r\n.btn-reddit.disabled, .btn-reddit:disabled {\n    background-color: #eff7ff;\n    border-color: #eff7ff; }\r\n.btn-reddit:active, .btn-reddit.active,\n  .show > .btn-reddit.dropdown-toggle {\n    background-color: #c9e4ff;\n    background-image: none;\n    border-color: #bcdeff; }\r\n.btn-soundcloud {\n  color: #fff;\n  background-color: #f50;\n  border-color: #f50; }\r\n.btn-soundcloud:hover {\n    color: #fff;\n    background-color: #d94800;\n    border-color: #cc4400; }\r\n.btn-soundcloud:focus, .btn-soundcloud.focus {\n    -webkit-box-shadow: 0 0 0 3px rgba(255, 85, 0, 0.5);\n            box-shadow: 0 0 0 3px rgba(255, 85, 0, 0.5); }\r\n.btn-soundcloud.disabled, .btn-soundcloud:disabled {\n    background-color: #f50;\n    border-color: #f50; }\r\n.btn-soundcloud:active, .btn-soundcloud.active,\n  .show > .btn-soundcloud.dropdown-toggle {\n    background-color: #d94800;\n    background-image: none;\n    border-color: #cc4400; }\r\n.btn-tumblr {\n  color: #fff;\n  background-color: #2c4762;\n  border-color: #2c4762; }\r\n.btn-tumblr:hover {\n    color: #fff;\n    background-color: #203448;\n    border-color: #1c2e3f; }\r\n.btn-tumblr:focus, .btn-tumblr.focus {\n    -webkit-box-shadow: 0 0 0 3px rgba(44, 71, 98, 0.5);\n            box-shadow: 0 0 0 3px rgba(44, 71, 98, 0.5); }\r\n.btn-tumblr.disabled, .btn-tumblr:disabled {\n    background-color: #2c4762;\n    border-color: #2c4762; }\r\n.btn-tumblr:active, .btn-tumblr.active,\n  .show > .btn-tumblr.dropdown-toggle {\n    background-color: #203448;\n    background-image: none;\n    border-color: #1c2e3f; }\r\n.btn-twitter {\n  color: #111;\n  background-color: #55acee;\n  border-color: #55acee; }\r\n.btn-twitter:hover {\n    color: #111;\n    background-color: #329beb;\n    border-color: #2795e9; }\r\n.btn-twitter:focus, .btn-twitter.focus {\n    -webkit-box-shadow: 0 0 0 3px rgba(85, 172, 238, 0.5);\n            box-shadow: 0 0 0 3px rgba(85, 172, 238, 0.5); }\r\n.btn-twitter.disabled, .btn-twitter:disabled {\n    background-color: #55acee;\n    border-color: #55acee; }\r\n.btn-twitter:active, .btn-twitter.active,\n  .show > .btn-twitter.dropdown-toggle {\n    background-color: #329beb;\n    background-image: none;\n    border-color: #2795e9; }\r\n.btn-vimeo {\n  color: #fff;\n  background-color: #1ab7ea;\n  border-color: #1ab7ea; }\r\n.btn-vimeo:hover {\n    color: #fff;\n    background-color: #139ecb;\n    border-color: #1295bf; }\r\n.btn-vimeo:focus, .btn-vimeo.focus {\n    -webkit-box-shadow: 0 0 0 3px rgba(26, 183, 234, 0.5);\n            box-shadow: 0 0 0 3px rgba(26, 183, 234, 0.5); }\r\n.btn-vimeo.disabled, .btn-vimeo:disabled {\n    background-color: #1ab7ea;\n    border-color: #1ab7ea; }\r\n.btn-vimeo:active, .btn-vimeo.active,\n  .show > .btn-vimeo.dropdown-toggle {\n    background-color: #139ecb;\n    background-image: none;\n    border-color: #1295bf; }\r\n.btn-vk {\n  color: #fff;\n  background-color: #587ea3;\n  border-color: #587ea3; }\r\n.btn-vk:hover {\n    color: #fff;\n    background-color: #4b6b8a;\n    border-color: #466482; }\r\n.btn-vk:focus, .btn-vk.focus {\n    -webkit-box-shadow: 0 0 0 3px rgba(88, 126, 163, 0.5);\n            box-shadow: 0 0 0 3px rgba(88, 126, 163, 0.5); }\r\n.btn-vk.disabled, .btn-vk:disabled {\n    background-color: #587ea3;\n    border-color: #587ea3; }\r\n.btn-vk:active, .btn-vk.active,\n  .show > .btn-vk.dropdown-toggle {\n    background-color: #4b6b8a;\n    background-image: none;\n    border-color: #466482; }\r\n.btn-yahoo {\n  color: #fff;\n  background-color: #720e9e;\n  border-color: #720e9e; }\r\n.btn-yahoo:hover {\n    color: #fff;\n    background-color: #590b7b;\n    border-color: #500a6f; }\r\n.btn-yahoo:focus, .btn-yahoo.focus {\n    -webkit-box-shadow: 0 0 0 3px rgba(114, 14, 158, 0.5);\n            box-shadow: 0 0 0 3px rgba(114, 14, 158, 0.5); }\r\n.btn-yahoo.disabled, .btn-yahoo:disabled {\n    background-color: #720e9e;\n    border-color: #720e9e; }\r\n.btn-yahoo:active, .btn-yahoo.active,\n  .show > .btn-yahoo.dropdown-toggle {\n    background-color: #590b7b;\n    background-image: none;\n    border-color: #500a6f; }\r\n.btn-icon {\n  min-width: 160px; }\n"

/***/ }),

/***/ "./src/app/components/button-icons/button-icons.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ButtonIconsComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var ButtonIconsComponent = (function () {
    function ButtonIconsComponent() {
    }
    ButtonIconsComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-button-icons',
            template: __webpack_require__("./src/app/components/button-icons/button-icons.component.html"),
            styles: [__webpack_require__("./src/app/components/button-icons/button-icons.component.scss")]
        })
    ], ButtonIconsComponent);
    return ButtonIconsComponent;
}());

//# sourceMappingURL=button-icons.component.js.map

/***/ }),

/***/ "./src/app/datatable/data-table/data-table.component.html":
/***/ (function(module, exports) {

module.exports = "<ngx-datatable\r\n  class=\"fullscreen\"\r\n  [columnMode]=\"'force'\"\r\n  [headerHeight]=\"40\"\r\n  [footerHeight]=\"0\"\r\n  [rowHeight]=\"40\"\r\n  [scrollbarV]=\"true\"\r\n  [scrollbarH]=\"true\"\r\n  [rows]=\"rows\">\r\n  <ngx-datatable-column name=\"Id\" [width]=\"80\"></ngx-datatable-column>\r\n  <ngx-datatable-column name=\"Name\" [width]=\"300\"></ngx-datatable-column>\r\n  <ngx-datatable-column name=\"Gender\"></ngx-datatable-column>\r\n  <ngx-datatable-column name=\"Age\"></ngx-datatable-column>\r\n  <ngx-datatable-column name=\"City\" [width]=\"300\" prop=\"address.city\"></ngx-datatable-column>\r\n  <ngx-datatable-column name=\"State\" [width]=\"300\" prop=\"address.state\"></ngx-datatable-column>\r\n</ngx-datatable>\r\n"

/***/ }),

/***/ "./src/app/datatable/data-table/data-table.component.scss":
/***/ (function(module, exports) {

module.exports = ".datatable,\n.datatable > div,\n.datatable.fixed-header .datatable-header .datatable-header-inner {\n  height: 100%;\n  direction: rtl;\n  text-align: right;\n  background-color: red; }\n\ndiv.dataTables_wrapper {\n  direction: rtl;\n  background-color: red; }\n\n/* Ensure that the demo table scrolls */\n\nth, td {\n  white-space: nowrap; }\n\ndiv.dataTables_wrapper {\n  width: 800px;\n  margin: 0 auto; }\n\n.datatable-header-inner {\n  direction: rtl;\n  text-align: right; }\n"

/***/ }),

/***/ "./src/app/datatable/data-table/data-table.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DataTableComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var DataTableComponent = (function () {
    function DataTableComponent() {
        var _this = this;
        this.rows = [];
        this.fetch(function (data) {
            _this.rows = data;
        });
    }
    DataTableComponent.prototype.fetch = function (cb) {
        var req = new XMLHttpRequest();
        req.open('GET', "assets/data/100k.json");
        req.onload = function () {
            cb(JSON.parse(req.response));
        };
        req.send();
    };
    DataTableComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-data-table',
            template: __webpack_require__("./src/app/datatable/data-table/data-table.component.html"),
            styles: [__webpack_require__("./src/app/datatable/data-table/data-table.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], DataTableComponent);
    return DataTableComponent;
}());

//# sourceMappingURL=data-table.component.js.map

/***/ }),

/***/ "./src/app/datatable/table-editing/table-editing.component.html":
/***/ (function(module, exports) {

module.exports = "<ngx-datatable\r\n  #mydatatable\r\n  [headerHeight]=\"40\"\r\n  [limit]=\"5\"\r\n  [columnMode]=\"'force'\"\r\n  [footerHeight]=\"50\"\r\n  [rowHeight]=\"'auto'\"\r\n  [rows]=\"rows\">\r\n  <ngx-datatable-column name=\"Name\">\r\n    <ng-template ngx-datatable-cell-template let-value=\"value\" let-row=\"row\">\r\n      <span\r\n        title=\"Double click to edit\"\r\n        (dblclick)=\"editing[row.$$index + '-name'] = true\"\r\n        *ngIf=\"!editing[row.$$index + '-name']\">\r\n        {{value}}\r\n      </span>\r\n      <input\r\n        autofocus\r\n        (blur)=\"updateValue($event, 'name', value, row)\"\r\n        *ngIf=\"editing[row.$$index + '-name']\"\r\n        type=\"text\"\r\n        [value]=\"value\"\r\n      />\r\n    </ng-template>\r\n  </ngx-datatable-column>\r\n  <ngx-datatable-column name=\"Gender\">\r\n    <ng-template ngx-datatable-cell-template let-row=\"row\" let-value=\"value\">\r\n       <span\r\n        title=\"Double click to edit\"\r\n        (dblclick)=\"editing[row.$$index + '-gender'] = true\"\r\n        *ngIf=\"!editing[row.$$index + '-gender']\">\r\n        {{value}}\r\n      </span>\r\n      <select\r\n        *ngIf=\"editing[row.$$index + '-gender']\"\r\n        (change)=\"updateValue($event, 'gender', value, row)\"\r\n        [value]=\"value\">\r\n        <option value=\"male\">Male</option>\r\n        <option value=\"female\">Female</option>\r\n      </select>\r\n    </ng-template>\r\n  </ngx-datatable-column>\r\n  <ngx-datatable-column name=\"Age\">\r\n    <ng-template ngx-datatable-cell-template let-value=\"value\">\r\n      {{value}}\r\n    </ng-template>\r\n  </ngx-datatable-column>\r\n</ngx-datatable>\r\n"

/***/ }),

/***/ "./src/app/datatable/table-editing/table-editing.component.scss":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/datatable/table-editing/table-editing.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TableEditingComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var TableEditingComponent = (function () {
    function TableEditingComponent() {
        var _this = this;
        this.editing = {};
        this.rows = [];
        this.fetch(function (data) {
            _this.rows = data;
        });
    }
    TableEditingComponent.prototype.fetch = function (cb) {
        var req = new XMLHttpRequest();
        req.open('GET', "assets/data/company.json");
        req.onload = function () {
            cb(JSON.parse(req.response));
        };
        req.send();
    };
    TableEditingComponent.prototype.updateValue = function (event, cell, cellValue, row) {
        this.editing[row.$$index + '-' + cell] = false;
        this.rows[row.$$index][cell] = event.target.value;
    };
    TableEditingComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-table-editing',
            template: __webpack_require__("./src/app/datatable/table-editing/table-editing.component.html"),
            styles: [__webpack_require__("./src/app/datatable/table-editing/table-editing.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], TableEditingComponent);
    return TableEditingComponent;
}());

//# sourceMappingURL=table-editing.component.js.map

/***/ }),

/***/ "./src/app/datatable/table-filter/table-filter.component.html":
/***/ (function(module, exports) {

module.exports = "<input type=\"email\" class=\"form-control mb-3\" placeholder=\"Type to filter the name column...\" required (keyup)='updateFilter($event)'>\r\n<ngx-datatable\r\n[columns]=\"columns\"\r\n[columnMode]=\"'force'\"\r\n[headerHeight]=\"40\"\r\n[footerHeight]=\"50\"\r\n[rowHeight]=\"'auto'\"\r\n[limit]=\"10\"\r\n[rows]='rows'>\r\n</ngx-datatable>\r\n"

/***/ }),

/***/ "./src/app/datatable/table-filter/table-filter.component.scss":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/datatable/table-filter/table-filter.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TableFilterComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var TableFilterComponent = (function () {
    function TableFilterComponent() {
        var _this = this;
        this.rows = [];
        this.temp = [];
        this.columns = [
            { prop: 'name' },
            { name: 'Company' },
            { name: 'Gender' }
        ];
        this.fetch(function (data) {
            // cache our list
            _this.temp = data.slice();
            // push our inital complete list
            _this.rows = data;
        });
    }
    TableFilterComponent.prototype.fetch = function (cb) {
        var req = new XMLHttpRequest();
        req.open('GET', "assets/data/company.json");
        req.onload = function () {
            cb(JSON.parse(req.response));
        };
        req.send();
    };
    TableFilterComponent.prototype.updateFilter = function (event) {
        var val = event.target.value;
        // filter our data
        var temp = this.temp.filter(function (d) {
            console.log();
            return d.CompanyName[0]['name'].toLowerCase().indexOf(val) !== -1 || !val;
        });
        // update the rows
        this.rows = temp;
    };
    TableFilterComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-table-filter',
            template: __webpack_require__("./src/app/datatable/table-filter/table-filter.component.html"),
            styles: [__webpack_require__("./src/app/datatable/table-filter/table-filter.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], TableFilterComponent);
    return TableFilterComponent;
}());

//# sourceMappingURL=table-filter.component.js.map

/***/ }),

/***/ "./src/app/datatable/table-paging/table-paging.component.html":
/***/ (function(module, exports) {

module.exports = "<ngx-datatable\r\n  [rows]=\"rows\"\r\n  [columns]=\"[{name:'Name'},{name:'Gender'},{name:'Company'}]\"\r\n  [columnMode]=\"'force'\"\r\n  [headerHeight]=\"40\"\r\n  [footerHeight]=\"50\"\r\n  [rowHeight]=\"'auto'\"\r\n  [externalPaging]=\"true\"\r\n  [count]=\"count\"\r\n  [offset]=\"offset\"\r\n  [limit]=\"limit\"\r\n  (page)='onPage($event)'>\r\n</ngx-datatable>\r\n"

/***/ }),

/***/ "./src/app/datatable/table-paging/table-paging.component.scss":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/datatable/table-paging/table-paging.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TablePagingComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var TablePagingComponent = (function () {
    function TablePagingComponent() {
        this.rows = [];
        this.count = 0;
        this.offset = 0;
        this.limit = 10;
    }
    TablePagingComponent.prototype.ngOnInit = function () {
        this.page(this.offset, this.limit);
    };
    TablePagingComponent.prototype.page = function (offset, limit) {
        var _this = this;
        this.fetch(function (results) {
            _this.count = results.length;
            var start = offset * limit;
            var end = start + limit;
            var rows = _this.rows.slice();
            for (var i = start; i < end; i++) {
                rows[i] = results[i];
            }
            _this.rows = rows;
            console.log('Page Results', start, end, rows);
        });
    };
    TablePagingComponent.prototype.fetch = function (cb) {
        var req = new XMLHttpRequest();
        req.open('GET', "assets/data/company.json");
        req.onload = function () {
            cb(JSON.parse(req.response));
        };
        req.send();
    };
    TablePagingComponent.prototype.onPage = function (event) {
        console.log('Page Event', event);
        this.page(event.offset, event.limit);
    };
    TablePagingComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-table-paging',
            template: __webpack_require__("./src/app/datatable/table-paging/table-paging.component.html"),
            styles: [__webpack_require__("./src/app/datatable/table-paging/table-paging.component.scss")]
        })
    ], TablePagingComponent);
    return TablePagingComponent;
}());

//# sourceMappingURL=table-paging.component.js.map

/***/ }),

/***/ "./src/app/datatable/table-pinning/table-pinning.component.html":
/***/ (function(module, exports) {

module.exports = "<ngx-datatable\r\n[columnMode]=\"'force'\"\r\n[headerHeight]=\"40\"\r\n[footerHeight]=\"50\"\r\n[rowHeight]=\"40\"\r\n[scrollbarV]=\"true\"\r\n[scrollbarH]=\"true\"\r\n[rows]=\"rows\">\r\n  <ngx-datatable-column\r\n    name=\"Name\"\r\n    [width]=\"300\"\r\n    [frozenLeft]=\"true\">\r\n  </ngx-datatable-column>\r\n  <ngx-datatable-column\r\n    name=\"Gender\">\r\n  </ngx-datatable-column>\r\n  <ngx-datatable-column\r\n    name=\"Age\">\r\n  </ngx-datatable-column>\r\n  <ngx-datatable-column\r\n    name=\"City\"\r\n    [width]=\"150\"\r\n    prop=\"address.city\">\r\n  </ngx-datatable-column>\r\n  <ngx-datatable-column\r\n    name=\"State\"\r\n    [width]=\"300\"\r\n    prop=\"address.state\"\r\n    [frozenRight]=\"true\">\r\n  </ngx-datatable-column>\r\n</ngx-datatable>\r\n"

/***/ }),

/***/ "./src/app/datatable/table-pinning/table-pinning.component.scss":
/***/ (function(module, exports) {

module.exports = ".datatable,\n.datatable > div,\n.datatable.fixed-header .datatable-header .datatable-header-inner {\n  height: 100%; }\n"

/***/ }),

/***/ "./src/app/datatable/table-pinning/table-pinning.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TablePinningComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var TablePinningComponent = (function () {
    function TablePinningComponent() {
        var _this = this;
        this.rows = [];
        this.fetch(function (data) {
            _this.rows = data;
        });
    }
    TablePinningComponent.prototype.fetch = function (cb) {
        var req = new XMLHttpRequest();
        req.open('GET', "assets/data/100k.json");
        req.onload = function () {
            cb(JSON.parse(req.response));
        };
        req.send();
    };
    TablePinningComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-table-pinning',
            template: __webpack_require__("./src/app/datatable/table-pinning/table-pinning.component.html"),
            styles: [__webpack_require__("./src/app/datatable/table-pinning/table-pinning.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], TablePinningComponent);
    return TablePinningComponent;
}());

//# sourceMappingURL=table-pinning.component.js.map

/***/ }),

/***/ "./src/app/datatable/table-selection/table-selection.component.html":
/***/ (function(module, exports) {

module.exports = "<ngx-datatable\r\n  class=\"selection-cell\"\r\n  [rows]=\"rows\"\r\n  [columnMode]=\"'force'\"\r\n  [columns]=\"columns\"\r\n  [headerHeight]=\"40\"\r\n  [footerHeight]=\"50\"\r\n  [rowHeight]=\"40\"\r\n  [selected]=\"selected\"\r\n  [selectionType]=\"'cell'\"\r\n  (select)=\"onSelect($event)\"\r\n  (activate)=\"onActivate($event)\">\r\n</ngx-datatable>\r\n"

/***/ }),

/***/ "./src/app/datatable/table-selection/table-selection.component.scss":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/datatable/table-selection/table-selection.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TableSelectionComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var TableSelectionComponent = (function () {
    function TableSelectionComponent() {
        var _this = this;
        this.rows = [];
        this.selected = [];
        this.columns = [
            { prop: 'name' },
            { name: 'Company' },
            { name: 'Gender' }
        ];
        this.fetch(function (data) {
            _this.rows = data;
        });
    }
    TableSelectionComponent.prototype.fetch = function (cb) {
        var req = new XMLHttpRequest();
        req.open('GET', "assets/data/company.json");
        req.onload = function () {
            cb(JSON.parse(req.response));
        };
        req.send();
    };
    TableSelectionComponent.prototype.onSelect = function (event) {
        console.log('Event: select', event, this.selected);
    };
    TableSelectionComponent.prototype.onActivate = function (event) {
        console.log('Event: activate', event);
    };
    TableSelectionComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-table-selection',
            template: __webpack_require__("./src/app/datatable/table-selection/table-selection.component.html"),
            styles: [__webpack_require__("./src/app/datatable/table-selection/table-selection.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], TableSelectionComponent);
    return TableSelectionComponent;
}());

//# sourceMappingURL=table-selection.component.js.map

/***/ }),

/***/ "./src/app/datatable/table-sorting/table-sorting.component.html":
/***/ (function(module, exports) {

module.exports = "<ngx-datatable\r\n  [rows]=\"rows\"\r\n  [columns]=\"columns\"\r\n  [sortType]=\"'multi'\"\r\n  [columnMode]=\"'force'\"\r\n  [headerHeight]=\"40\"\r\n  [footerHeight]=\"50\"\r\n  [rowHeight]=\"40\">\r\n</ngx-datatable>\r\n"

/***/ }),

/***/ "./src/app/datatable/table-sorting/table-sorting.component.scss":
/***/ (function(module, exports) {

module.exports = ".datatable,\n.datatable > div,\n.datatable.fixed-header .datatable-header .datatable-header-inner {\n  height: 100%; }\n"

/***/ }),

/***/ "./src/app/datatable/table-sorting/table-sorting.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TableSortingComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var TableSortingComponent = (function () {
    function TableSortingComponent() {
        var _this = this;
        this.rows = [];
        this.columns = [
            { name: 'Company' },
            { name: 'Name' },
            { name: 'Gender' }
        ];
        this.fetch(function (data) {
            _this.rows = data;
        });
    }
    TableSortingComponent.prototype.fetch = function (cb) {
        var req = new XMLHttpRequest();
        req.open('GET', "assets/data/company.json");
        req.onload = function () {
            var data = JSON.parse(req.response);
            cb(data);
        };
        req.send();
    };
    TableSortingComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-table-sorting',
            template: __webpack_require__("./src/app/datatable/table-sorting/table-sorting.component.html"),
            styles: [__webpack_require__("./src/app/datatable/table-sorting/table-sorting.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], TableSortingComponent);
    return TableSortingComponent;
}());

//# sourceMappingURL=table-sorting.component.js.map

/***/ }),

/***/ "./src/app/icons/fontawesome/fontawesome.component.html":
/***/ (function(module, exports) {

module.exports = "<input type=\"email\" class=\"form-control form-control-lg mb-3\" placeholder=\"Type to filter icons\" required (keyup)='updateFilter($event)'>\r\n\r\n<div class=\"icon-list\">\r\n  <div class=\"row\">\r\n    <div class=\"fa-hover col-md-3 col-sm-4\" *ngFor=\"let icon of icons\">\r\n      <a href=\"http://fontawesome.io/icon/{{icon}}\" class=\"text-color\" target=\"_blank\">\r\n        <i class=\"fa fa-{{icon}}\"></i>\r\n        <span>{{icon}}</span>\r\n      </a>\r\n    </div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./src/app/icons/fontawesome/fontawesome.component.scss":
/***/ (function(module, exports) {

module.exports = "/*!\r\n *  Font Awesome 4.7.0 by @davegandy - http://fontawesome.io - @fontawesome\r\n *  License - http://fontawesome.io/license (Font: SIL OFL 1.1, CSS: MIT License)\r\n */\r\n/* FONT PATH\r\n * -------------------------- */\r\n@font-face {\r\n  font-family: 'FontAwesome';\r\n  src: url('fontawesome-webfont.674f50d287a8c48dc19b.eot?v=4.7.0');\r\n  src: url('fontawesome-webfont.674f50d287a8c48dc19b.eot?#iefix&v=4.7.0') format('embedded-opentype'), url('fontawesome-webfont.af7ae505a9eed503f8b8.woff2?v=4.7.0') format('woff2'), url('fontawesome-webfont.fee66e712a8a08eef580.woff?v=4.7.0') format('woff'), url('fontawesome-webfont.b06871f281fee6b241d6.ttf?v=4.7.0') format('truetype'), url('fontawesome-webfont.acf3dcb7ff752b5296ca.svg?v=4.7.0#fontawesomeregular') format('svg');\r\n  font-weight: normal;\r\n  font-style: normal;\r\n}\r\n.fa {\r\n  display: inline-block;\r\n  font: normal normal normal 14px/1 FontAwesome;\r\n  font-size: inherit;\r\n  text-rendering: auto;\r\n  -webkit-font-smoothing: antialiased;\r\n  -moz-osx-font-smoothing: grayscale;\r\n}\r\n/* makes the font 33% larger relative to the icon container */\r\n.fa-lg {\r\n  font-size: 1.33333333em;\r\n  line-height: 0.75em;\r\n  vertical-align: -15%;\r\n}\r\n.fa-2x {\r\n  font-size: 2em;\r\n}\r\n.fa-3x {\r\n  font-size: 3em;\r\n}\r\n.fa-4x {\r\n  font-size: 4em;\r\n}\r\n.fa-5x {\r\n  font-size: 5em;\r\n}\r\n.fa-fw {\r\n  width: 1.28571429em;\r\n  text-align: center;\r\n}\r\n.fa-ul {\r\n  padding-left: 0;\r\n  margin-left: 2.14285714em;\r\n  list-style-type: none;\r\n}\r\n.fa-ul > li {\r\n  position: relative;\r\n}\r\n.fa-li {\r\n  position: absolute;\r\n  left: -2.14285714em;\r\n  width: 2.14285714em;\r\n  top: 0.14285714em;\r\n  text-align: center;\r\n}\r\n.fa-li.fa-lg {\r\n  left: -1.85714286em;\r\n}\r\n.fa-border {\r\n  padding: .2em .25em .15em;\r\n  border: solid 0.08em #eeeeee;\r\n  border-radius: .1em;\r\n}\r\n.fa-pull-left {\r\n  float: left;\r\n}\r\n.fa-pull-right {\r\n  float: right;\r\n}\r\n.fa.fa-pull-left {\r\n  margin-right: .3em;\r\n}\r\n.fa.fa-pull-right {\r\n  margin-left: .3em;\r\n}\r\n/* Deprecated as of 4.4.0 */\r\n.pull-right {\r\n  float: right;\r\n}\r\n.pull-left {\r\n  float: left;\r\n}\r\n.fa.pull-left {\r\n  margin-right: .3em;\r\n}\r\n.fa.pull-right {\r\n  margin-left: .3em;\r\n}\r\n.fa-spin {\r\n  -webkit-animation: fa-spin 2s infinite linear;\r\n  animation: fa-spin 2s infinite linear;\r\n}\r\n.fa-pulse {\r\n  -webkit-animation: fa-spin 1s infinite steps(8);\r\n  animation: fa-spin 1s infinite steps(8);\r\n}\r\n@-webkit-keyframes fa-spin {\r\n  0% {\r\n    -webkit-transform: rotate(0deg);\r\n    transform: rotate(0deg);\r\n  }\r\n  100% {\r\n    -webkit-transform: rotate(359deg);\r\n    transform: rotate(359deg);\r\n  }\r\n}\r\n@keyframes fa-spin {\r\n  0% {\r\n    -webkit-transform: rotate(0deg);\r\n    transform: rotate(0deg);\r\n  }\r\n  100% {\r\n    -webkit-transform: rotate(359deg);\r\n    transform: rotate(359deg);\r\n  }\r\n}\r\n.fa-rotate-90 {\r\n  -ms-filter: \"progid:DXImageTransform.Microsoft.BasicImage(rotation=1)\";\r\n  -webkit-transform: rotate(90deg);\r\n  transform: rotate(90deg);\r\n}\r\n.fa-rotate-180 {\r\n  -ms-filter: \"progid:DXImageTransform.Microsoft.BasicImage(rotation=2)\";\r\n  -webkit-transform: rotate(180deg);\r\n  transform: rotate(180deg);\r\n}\r\n.fa-rotate-270 {\r\n  -ms-filter: \"progid:DXImageTransform.Microsoft.BasicImage(rotation=3)\";\r\n  -webkit-transform: rotate(270deg);\r\n  transform: rotate(270deg);\r\n}\r\n.fa-flip-horizontal {\r\n  -ms-filter: \"progid:DXImageTransform.Microsoft.BasicImage(rotation=0, mirror=1)\";\r\n  -webkit-transform: scale(-1, 1);\r\n  transform: scale(-1, 1);\r\n}\r\n.fa-flip-vertical {\r\n  -ms-filter: \"progid:DXImageTransform.Microsoft.BasicImage(rotation=2, mirror=1)\";\r\n  -webkit-transform: scale(1, -1);\r\n  transform: scale(1, -1);\r\n}\r\n:root .fa-rotate-90,\r\n:root .fa-rotate-180,\r\n:root .fa-rotate-270,\r\n:root .fa-flip-horizontal,\r\n:root .fa-flip-vertical {\r\n  -webkit-filter: none;\r\n          filter: none;\r\n}\r\n.fa-stack {\r\n  position: relative;\r\n  display: inline-block;\r\n  width: 2em;\r\n  height: 2em;\r\n  line-height: 2em;\r\n  vertical-align: middle;\r\n}\r\n.fa-stack-1x,\r\n.fa-stack-2x {\r\n  position: absolute;\r\n  left: 0;\r\n  width: 100%;\r\n  text-align: center;\r\n}\r\n.fa-stack-1x {\r\n  line-height: inherit;\r\n}\r\n.fa-stack-2x {\r\n  font-size: 2em;\r\n}\r\n.fa-inverse {\r\n  color: #ffffff;\r\n}\r\n/* Font Awesome uses the Unicode Private Use Area (PUA) to ensure screen\r\n   readers do not read off random characters that represent icons */\r\n.fa-glass:before {\r\n  content: \"\\f000\";\r\n}\r\n.fa-music:before {\r\n  content: \"\\f001\";\r\n}\r\n.fa-search:before {\r\n  content: \"\\f002\";\r\n}\r\n.fa-envelope-o:before {\r\n  content: \"\\f003\";\r\n}\r\n.fa-heart:before {\r\n  content: \"\\f004\";\r\n}\r\n.fa-star:before {\r\n  content: \"\\f005\";\r\n}\r\n.fa-star-o:before {\r\n  content: \"\\f006\";\r\n}\r\n.fa-user:before {\r\n  content: \"\\f007\";\r\n}\r\n.fa-film:before {\r\n  content: \"\\f008\";\r\n}\r\n.fa-th-large:before {\r\n  content: \"\\f009\";\r\n}\r\n.fa-th:before {\r\n  content: \"\\f00a\";\r\n}\r\n.fa-th-list:before {\r\n  content: \"\\f00b\";\r\n}\r\n.fa-check:before {\r\n  content: \"\\f00c\";\r\n}\r\n.fa-remove:before,\r\n.fa-close:before,\r\n.fa-times:before {\r\n  content: \"\\f00d\";\r\n}\r\n.fa-search-plus:before {\r\n  content: \"\\f00e\";\r\n}\r\n.fa-search-minus:before {\r\n  content: \"\\f010\";\r\n}\r\n.fa-power-off:before {\r\n  content: \"\\f011\";\r\n}\r\n.fa-signal:before {\r\n  content: \"\\f012\";\r\n}\r\n.fa-gear:before,\r\n.fa-cog:before {\r\n  content: \"\\f013\";\r\n}\r\n.fa-trash-o:before {\r\n  content: \"\\f014\";\r\n}\r\n.fa-home:before {\r\n  content: \"\\f015\";\r\n}\r\n.fa-file-o:before {\r\n  content: \"\\f016\";\r\n}\r\n.fa-clock-o:before {\r\n  content: \"\\f017\";\r\n}\r\n.fa-road:before {\r\n  content: \"\\f018\";\r\n}\r\n.fa-download:before {\r\n  content: \"\\f019\";\r\n}\r\n.fa-arrow-circle-o-down:before {\r\n  content: \"\\f01a\";\r\n}\r\n.fa-arrow-circle-o-up:before {\r\n  content: \"\\f01b\";\r\n}\r\n.fa-inbox:before {\r\n  content: \"\\f01c\";\r\n}\r\n.fa-play-circle-o:before {\r\n  content: \"\\f01d\";\r\n}\r\n.fa-rotate-right:before,\r\n.fa-repeat:before {\r\n  content: \"\\f01e\";\r\n}\r\n.fa-refresh:before {\r\n  content: \"\\f021\";\r\n}\r\n.fa-list-alt:before {\r\n  content: \"\\f022\";\r\n}\r\n.fa-lock:before {\r\n  content: \"\\f023\";\r\n}\r\n.fa-flag:before {\r\n  content: \"\\f024\";\r\n}\r\n.fa-headphones:before {\r\n  content: \"\\f025\";\r\n}\r\n.fa-volume-off:before {\r\n  content: \"\\f026\";\r\n}\r\n.fa-volume-down:before {\r\n  content: \"\\f027\";\r\n}\r\n.fa-volume-up:before {\r\n  content: \"\\f028\";\r\n}\r\n.fa-qrcode:before {\r\n  content: \"\\f029\";\r\n}\r\n.fa-barcode:before {\r\n  content: \"\\f02a\";\r\n}\r\n.fa-tag:before {\r\n  content: \"\\f02b\";\r\n}\r\n.fa-tags:before {\r\n  content: \"\\f02c\";\r\n}\r\n.fa-book:before {\r\n  content: \"\\f02d\";\r\n}\r\n.fa-bookmark:before {\r\n  content: \"\\f02e\";\r\n}\r\n.fa-print:before {\r\n  content: \"\\f02f\";\r\n}\r\n.fa-camera:before {\r\n  content: \"\\f030\";\r\n}\r\n.fa-font:before {\r\n  content: \"\\f031\";\r\n}\r\n.fa-bold:before {\r\n  content: \"\\f032\";\r\n}\r\n.fa-italic:before {\r\n  content: \"\\f033\";\r\n}\r\n.fa-text-height:before {\r\n  content: \"\\f034\";\r\n}\r\n.fa-text-width:before {\r\n  content: \"\\f035\";\r\n}\r\n.fa-align-left:before {\r\n  content: \"\\f036\";\r\n}\r\n.fa-align-center:before {\r\n  content: \"\\f037\";\r\n}\r\n.fa-align-right:before {\r\n  content: \"\\f038\";\r\n}\r\n.fa-align-justify:before {\r\n  content: \"\\f039\";\r\n}\r\n.fa-list:before {\r\n  content: \"\\f03a\";\r\n}\r\n.fa-dedent:before,\r\n.fa-outdent:before {\r\n  content: \"\\f03b\";\r\n}\r\n.fa-indent:before {\r\n  content: \"\\f03c\";\r\n}\r\n.fa-video-camera:before {\r\n  content: \"\\f03d\";\r\n}\r\n.fa-photo:before,\r\n.fa-image:before,\r\n.fa-picture-o:before {\r\n  content: \"\\f03e\";\r\n}\r\n.fa-pencil:before {\r\n  content: \"\\f040\";\r\n}\r\n.fa-map-marker:before {\r\n  content: \"\\f041\";\r\n}\r\n.fa-adjust:before {\r\n  content: \"\\f042\";\r\n}\r\n.fa-tint:before {\r\n  content: \"\\f043\";\r\n}\r\n.fa-edit:before,\r\n.fa-pencil-square-o:before {\r\n  content: \"\\f044\";\r\n}\r\n.fa-share-square-o:before {\r\n  content: \"\\f045\";\r\n}\r\n.fa-check-square-o:before {\r\n  content: \"\\f046\";\r\n}\r\n.fa-arrows:before {\r\n  content: \"\\f047\";\r\n}\r\n.fa-step-backward:before {\r\n  content: \"\\f048\";\r\n}\r\n.fa-fast-backward:before {\r\n  content: \"\\f049\";\r\n}\r\n.fa-backward:before {\r\n  content: \"\\f04a\";\r\n}\r\n.fa-play:before {\r\n  content: \"\\f04b\";\r\n}\r\n.fa-pause:before {\r\n  content: \"\\f04c\";\r\n}\r\n.fa-stop:before {\r\n  content: \"\\f04d\";\r\n}\r\n.fa-forward:before {\r\n  content: \"\\f04e\";\r\n}\r\n.fa-fast-forward:before {\r\n  content: \"\\f050\";\r\n}\r\n.fa-step-forward:before {\r\n  content: \"\\f051\";\r\n}\r\n.fa-eject:before {\r\n  content: \"\\f052\";\r\n}\r\n.fa-chevron-left:before {\r\n  content: \"\\f053\";\r\n}\r\n.fa-chevron-right:before {\r\n  content: \"\\f054\";\r\n}\r\n.fa-plus-circle:before {\r\n  content: \"\\f055\";\r\n}\r\n.fa-minus-circle:before {\r\n  content: \"\\f056\";\r\n}\r\n.fa-times-circle:before {\r\n  content: \"\\f057\";\r\n}\r\n.fa-check-circle:before {\r\n  content: \"\\f058\";\r\n}\r\n.fa-question-circle:before {\r\n  content: \"\\f059\";\r\n}\r\n.fa-info-circle:before {\r\n  content: \"\\f05a\";\r\n}\r\n.fa-crosshairs:before {\r\n  content: \"\\f05b\";\r\n}\r\n.fa-times-circle-o:before {\r\n  content: \"\\f05c\";\r\n}\r\n.fa-check-circle-o:before {\r\n  content: \"\\f05d\";\r\n}\r\n.fa-ban:before {\r\n  content: \"\\f05e\";\r\n}\r\n.fa-arrow-left:before {\r\n  content: \"\\f060\";\r\n}\r\n.fa-arrow-right:before {\r\n  content: \"\\f061\";\r\n}\r\n.fa-arrow-up:before {\r\n  content: \"\\f062\";\r\n}\r\n.fa-arrow-down:before {\r\n  content: \"\\f063\";\r\n}\r\n.fa-mail-forward:before,\r\n.fa-share:before {\r\n  content: \"\\f064\";\r\n}\r\n.fa-expand:before {\r\n  content: \"\\f065\";\r\n}\r\n.fa-compress:before {\r\n  content: \"\\f066\";\r\n}\r\n.fa-plus:before {\r\n  content: \"\\f067\";\r\n}\r\n.fa-minus:before {\r\n  content: \"\\f068\";\r\n}\r\n.fa-asterisk:before {\r\n  content: \"\\f069\";\r\n}\r\n.fa-exclamation-circle:before {\r\n  content: \"\\f06a\";\r\n}\r\n.fa-gift:before {\r\n  content: \"\\f06b\";\r\n}\r\n.fa-leaf:before {\r\n  content: \"\\f06c\";\r\n}\r\n.fa-fire:before {\r\n  content: \"\\f06d\";\r\n}\r\n.fa-eye:before {\r\n  content: \"\\f06e\";\r\n}\r\n.fa-eye-slash:before {\r\n  content: \"\\f070\";\r\n}\r\n.fa-warning:before,\r\n.fa-exclamation-triangle:before {\r\n  content: \"\\f071\";\r\n}\r\n.fa-plane:before {\r\n  content: \"\\f072\";\r\n}\r\n.fa-calendar:before {\r\n  content: \"\\f073\";\r\n}\r\n.fa-random:before {\r\n  content: \"\\f074\";\r\n}\r\n.fa-comment:before {\r\n  content: \"\\f075\";\r\n}\r\n.fa-magnet:before {\r\n  content: \"\\f076\";\r\n}\r\n.fa-chevron-up:before {\r\n  content: \"\\f077\";\r\n}\r\n.fa-chevron-down:before {\r\n  content: \"\\f078\";\r\n}\r\n.fa-retweet:before {\r\n  content: \"\\f079\";\r\n}\r\n.fa-shopping-cart:before {\r\n  content: \"\\f07a\";\r\n}\r\n.fa-folder:before {\r\n  content: \"\\f07b\";\r\n}\r\n.fa-folder-open:before {\r\n  content: \"\\f07c\";\r\n}\r\n.fa-arrows-v:before {\r\n  content: \"\\f07d\";\r\n}\r\n.fa-arrows-h:before {\r\n  content: \"\\f07e\";\r\n}\r\n.fa-bar-chart-o:before,\r\n.fa-bar-chart:before {\r\n  content: \"\\f080\";\r\n}\r\n.fa-twitter-square:before {\r\n  content: \"\\f081\";\r\n}\r\n.fa-facebook-square:before {\r\n  content: \"\\f082\";\r\n}\r\n.fa-camera-retro:before {\r\n  content: \"\\f083\";\r\n}\r\n.fa-key:before {\r\n  content: \"\\f084\";\r\n}\r\n.fa-gears:before,\r\n.fa-cogs:before {\r\n  content: \"\\f085\";\r\n}\r\n.fa-comments:before {\r\n  content: \"\\f086\";\r\n}\r\n.fa-thumbs-o-up:before {\r\n  content: \"\\f087\";\r\n}\r\n.fa-thumbs-o-down:before {\r\n  content: \"\\f088\";\r\n}\r\n.fa-star-half:before {\r\n  content: \"\\f089\";\r\n}\r\n.fa-heart-o:before {\r\n  content: \"\\f08a\";\r\n}\r\n.fa-sign-out:before {\r\n  content: \"\\f08b\";\r\n}\r\n.fa-linkedin-square:before {\r\n  content: \"\\f08c\";\r\n}\r\n.fa-thumb-tack:before {\r\n  content: \"\\f08d\";\r\n}\r\n.fa-external-link:before {\r\n  content: \"\\f08e\";\r\n}\r\n.fa-sign-in:before {\r\n  content: \"\\f090\";\r\n}\r\n.fa-trophy:before {\r\n  content: \"\\f091\";\r\n}\r\n.fa-github-square:before {\r\n  content: \"\\f092\";\r\n}\r\n.fa-upload:before {\r\n  content: \"\\f093\";\r\n}\r\n.fa-lemon-o:before {\r\n  content: \"\\f094\";\r\n}\r\n.fa-phone:before {\r\n  content: \"\\f095\";\r\n}\r\n.fa-square-o:before {\r\n  content: \"\\f096\";\r\n}\r\n.fa-bookmark-o:before {\r\n  content: \"\\f097\";\r\n}\r\n.fa-phone-square:before {\r\n  content: \"\\f098\";\r\n}\r\n.fa-twitter:before {\r\n  content: \"\\f099\";\r\n}\r\n.fa-facebook-f:before,\r\n.fa-facebook:before {\r\n  content: \"\\f09a\";\r\n}\r\n.fa-github:before {\r\n  content: \"\\f09b\";\r\n}\r\n.fa-unlock:before {\r\n  content: \"\\f09c\";\r\n}\r\n.fa-credit-card:before {\r\n  content: \"\\f09d\";\r\n}\r\n.fa-feed:before,\r\n.fa-rss:before {\r\n  content: \"\\f09e\";\r\n}\r\n.fa-hdd-o:before {\r\n  content: \"\\f0a0\";\r\n}\r\n.fa-bullhorn:before {\r\n  content: \"\\f0a1\";\r\n}\r\n.fa-bell:before {\r\n  content: \"\\f0f3\";\r\n}\r\n.fa-certificate:before {\r\n  content: \"\\f0a3\";\r\n}\r\n.fa-hand-o-right:before {\r\n  content: \"\\f0a4\";\r\n}\r\n.fa-hand-o-left:before {\r\n  content: \"\\f0a5\";\r\n}\r\n.fa-hand-o-up:before {\r\n  content: \"\\f0a6\";\r\n}\r\n.fa-hand-o-down:before {\r\n  content: \"\\f0a7\";\r\n}\r\n.fa-arrow-circle-left:before {\r\n  content: \"\\f0a8\";\r\n}\r\n.fa-arrow-circle-right:before {\r\n  content: \"\\f0a9\";\r\n}\r\n.fa-arrow-circle-up:before {\r\n  content: \"\\f0aa\";\r\n}\r\n.fa-arrow-circle-down:before {\r\n  content: \"\\f0ab\";\r\n}\r\n.fa-globe:before {\r\n  content: \"\\f0ac\";\r\n}\r\n.fa-wrench:before {\r\n  content: \"\\f0ad\";\r\n}\r\n.fa-tasks:before {\r\n  content: \"\\f0ae\";\r\n}\r\n.fa-filter:before {\r\n  content: \"\\f0b0\";\r\n}\r\n.fa-briefcase:before {\r\n  content: \"\\f0b1\";\r\n}\r\n.fa-arrows-alt:before {\r\n  content: \"\\f0b2\";\r\n}\r\n.fa-group:before,\r\n.fa-users:before {\r\n  content: \"\\f0c0\";\r\n}\r\n.fa-chain:before,\r\n.fa-link:before {\r\n  content: \"\\f0c1\";\r\n}\r\n.fa-cloud:before {\r\n  content: \"\\f0c2\";\r\n}\r\n.fa-flask:before {\r\n  content: \"\\f0c3\";\r\n}\r\n.fa-cut:before,\r\n.fa-scissors:before {\r\n  content: \"\\f0c4\";\r\n}\r\n.fa-copy:before,\r\n.fa-files-o:before {\r\n  content: \"\\f0c5\";\r\n}\r\n.fa-paperclip:before {\r\n  content: \"\\f0c6\";\r\n}\r\n.fa-save:before,\r\n.fa-floppy-o:before {\r\n  content: \"\\f0c7\";\r\n}\r\n.fa-square:before {\r\n  content: \"\\f0c8\";\r\n}\r\n.fa-navicon:before,\r\n.fa-reorder:before,\r\n.fa-bars:before {\r\n  content: \"\\f0c9\";\r\n}\r\n.fa-list-ul:before {\r\n  content: \"\\f0ca\";\r\n}\r\n.fa-list-ol:before {\r\n  content: \"\\f0cb\";\r\n}\r\n.fa-strikethrough:before {\r\n  content: \"\\f0cc\";\r\n}\r\n.fa-underline:before {\r\n  content: \"\\f0cd\";\r\n}\r\n.fa-table:before {\r\n  content: \"\\f0ce\";\r\n}\r\n.fa-magic:before {\r\n  content: \"\\f0d0\";\r\n}\r\n.fa-truck:before {\r\n  content: \"\\f0d1\";\r\n}\r\n.fa-pinterest:before {\r\n  content: \"\\f0d2\";\r\n}\r\n.fa-pinterest-square:before {\r\n  content: \"\\f0d3\";\r\n}\r\n.fa-google-plus-square:before {\r\n  content: \"\\f0d4\";\r\n}\r\n.fa-google-plus:before {\r\n  content: \"\\f0d5\";\r\n}\r\n.fa-money:before {\r\n  content: \"\\f0d6\";\r\n}\r\n.fa-caret-down:before {\r\n  content: \"\\f0d7\";\r\n}\r\n.fa-caret-up:before {\r\n  content: \"\\f0d8\";\r\n}\r\n.fa-caret-left:before {\r\n  content: \"\\f0d9\";\r\n}\r\n.fa-caret-right:before {\r\n  content: \"\\f0da\";\r\n}\r\n.fa-columns:before {\r\n  content: \"\\f0db\";\r\n}\r\n.fa-unsorted:before,\r\n.fa-sort:before {\r\n  content: \"\\f0dc\";\r\n}\r\n.fa-sort-down:before,\r\n.fa-sort-desc:before {\r\n  content: \"\\f0dd\";\r\n}\r\n.fa-sort-up:before,\r\n.fa-sort-asc:before {\r\n  content: \"\\f0de\";\r\n}\r\n.fa-envelope:before {\r\n  content: \"\\f0e0\";\r\n}\r\n.fa-linkedin:before {\r\n  content: \"\\f0e1\";\r\n}\r\n.fa-rotate-left:before,\r\n.fa-undo:before {\r\n  content: \"\\f0e2\";\r\n}\r\n.fa-legal:before,\r\n.fa-gavel:before {\r\n  content: \"\\f0e3\";\r\n}\r\n.fa-dashboard:before,\r\n.fa-tachometer:before {\r\n  content: \"\\f0e4\";\r\n}\r\n.fa-comment-o:before {\r\n  content: \"\\f0e5\";\r\n}\r\n.fa-comments-o:before {\r\n  content: \"\\f0e6\";\r\n}\r\n.fa-flash:before,\r\n.fa-bolt:before {\r\n  content: \"\\f0e7\";\r\n}\r\n.fa-sitemap:before {\r\n  content: \"\\f0e8\";\r\n}\r\n.fa-umbrella:before {\r\n  content: \"\\f0e9\";\r\n}\r\n.fa-paste:before,\r\n.fa-clipboard:before {\r\n  content: \"\\f0ea\";\r\n}\r\n.fa-lightbulb-o:before {\r\n  content: \"\\f0eb\";\r\n}\r\n.fa-exchange:before {\r\n  content: \"\\f0ec\";\r\n}\r\n.fa-cloud-download:before {\r\n  content: \"\\f0ed\";\r\n}\r\n.fa-cloud-upload:before {\r\n  content: \"\\f0ee\";\r\n}\r\n.fa-user-md:before {\r\n  content: \"\\f0f0\";\r\n}\r\n.fa-stethoscope:before {\r\n  content: \"\\f0f1\";\r\n}\r\n.fa-suitcase:before {\r\n  content: \"\\f0f2\";\r\n}\r\n.fa-bell-o:before {\r\n  content: \"\\f0a2\";\r\n}\r\n.fa-coffee:before {\r\n  content: \"\\f0f4\";\r\n}\r\n.fa-cutlery:before {\r\n  content: \"\\f0f5\";\r\n}\r\n.fa-file-text-o:before {\r\n  content: \"\\f0f6\";\r\n}\r\n.fa-building-o:before {\r\n  content: \"\\f0f7\";\r\n}\r\n.fa-hospital-o:before {\r\n  content: \"\\f0f8\";\r\n}\r\n.fa-ambulance:before {\r\n  content: \"\\f0f9\";\r\n}\r\n.fa-medkit:before {\r\n  content: \"\\f0fa\";\r\n}\r\n.fa-fighter-jet:before {\r\n  content: \"\\f0fb\";\r\n}\r\n.fa-beer:before {\r\n  content: \"\\f0fc\";\r\n}\r\n.fa-h-square:before {\r\n  content: \"\\f0fd\";\r\n}\r\n.fa-plus-square:before {\r\n  content: \"\\f0fe\";\r\n}\r\n.fa-angle-double-left:before {\r\n  content: \"\\f100\";\r\n}\r\n.fa-angle-double-right:before {\r\n  content: \"\\f101\";\r\n}\r\n.fa-angle-double-up:before {\r\n  content: \"\\f102\";\r\n}\r\n.fa-angle-double-down:before {\r\n  content: \"\\f103\";\r\n}\r\n.fa-angle-left:before {\r\n  content: \"\\f104\";\r\n}\r\n.fa-angle-right:before {\r\n  content: \"\\f105\";\r\n}\r\n.fa-angle-up:before {\r\n  content: \"\\f106\";\r\n}\r\n.fa-angle-down:before {\r\n  content: \"\\f107\";\r\n}\r\n.fa-desktop:before {\r\n  content: \"\\f108\";\r\n}\r\n.fa-laptop:before {\r\n  content: \"\\f109\";\r\n}\r\n.fa-tablet:before {\r\n  content: \"\\f10a\";\r\n}\r\n.fa-mobile-phone:before,\r\n.fa-mobile:before {\r\n  content: \"\\f10b\";\r\n}\r\n.fa-circle-o:before {\r\n  content: \"\\f10c\";\r\n}\r\n.fa-quote-left:before {\r\n  content: \"\\f10d\";\r\n}\r\n.fa-quote-right:before {\r\n  content: \"\\f10e\";\r\n}\r\n.fa-spinner:before {\r\n  content: \"\\f110\";\r\n}\r\n.fa-circle:before {\r\n  content: \"\\f111\";\r\n}\r\n.fa-mail-reply:before,\r\n.fa-reply:before {\r\n  content: \"\\f112\";\r\n}\r\n.fa-github-alt:before {\r\n  content: \"\\f113\";\r\n}\r\n.fa-folder-o:before {\r\n  content: \"\\f114\";\r\n}\r\n.fa-folder-open-o:before {\r\n  content: \"\\f115\";\r\n}\r\n.fa-smile-o:before {\r\n  content: \"\\f118\";\r\n}\r\n.fa-frown-o:before {\r\n  content: \"\\f119\";\r\n}\r\n.fa-meh-o:before {\r\n  content: \"\\f11a\";\r\n}\r\n.fa-gamepad:before {\r\n  content: \"\\f11b\";\r\n}\r\n.fa-keyboard-o:before {\r\n  content: \"\\f11c\";\r\n}\r\n.fa-flag-o:before {\r\n  content: \"\\f11d\";\r\n}\r\n.fa-flag-checkered:before {\r\n  content: \"\\f11e\";\r\n}\r\n.fa-terminal:before {\r\n  content: \"\\f120\";\r\n}\r\n.fa-code:before {\r\n  content: \"\\f121\";\r\n}\r\n.fa-mail-reply-all:before,\r\n.fa-reply-all:before {\r\n  content: \"\\f122\";\r\n}\r\n.fa-star-half-empty:before,\r\n.fa-star-half-full:before,\r\n.fa-star-half-o:before {\r\n  content: \"\\f123\";\r\n}\r\n.fa-location-arrow:before {\r\n  content: \"\\f124\";\r\n}\r\n.fa-crop:before {\r\n  content: \"\\f125\";\r\n}\r\n.fa-code-fork:before {\r\n  content: \"\\f126\";\r\n}\r\n.fa-unlink:before,\r\n.fa-chain-broken:before {\r\n  content: \"\\f127\";\r\n}\r\n.fa-question:before {\r\n  content: \"\\f128\";\r\n}\r\n.fa-info:before {\r\n  content: \"\\f129\";\r\n}\r\n.fa-exclamation:before {\r\n  content: \"\\f12a\";\r\n}\r\n.fa-superscript:before {\r\n  content: \"\\f12b\";\r\n}\r\n.fa-subscript:before {\r\n  content: \"\\f12c\";\r\n}\r\n.fa-eraser:before {\r\n  content: \"\\f12d\";\r\n}\r\n.fa-puzzle-piece:before {\r\n  content: \"\\f12e\";\r\n}\r\n.fa-microphone:before {\r\n  content: \"\\f130\";\r\n}\r\n.fa-microphone-slash:before {\r\n  content: \"\\f131\";\r\n}\r\n.fa-shield:before {\r\n  content: \"\\f132\";\r\n}\r\n.fa-calendar-o:before {\r\n  content: \"\\f133\";\r\n}\r\n.fa-fire-extinguisher:before {\r\n  content: \"\\f134\";\r\n}\r\n.fa-rocket:before {\r\n  content: \"\\f135\";\r\n}\r\n.fa-maxcdn:before {\r\n  content: \"\\f136\";\r\n}\r\n.fa-chevron-circle-left:before {\r\n  content: \"\\f137\";\r\n}\r\n.fa-chevron-circle-right:before {\r\n  content: \"\\f138\";\r\n}\r\n.fa-chevron-circle-up:before {\r\n  content: \"\\f139\";\r\n}\r\n.fa-chevron-circle-down:before {\r\n  content: \"\\f13a\";\r\n}\r\n.fa-html5:before {\r\n  content: \"\\f13b\";\r\n}\r\n.fa-css3:before {\r\n  content: \"\\f13c\";\r\n}\r\n.fa-anchor:before {\r\n  content: \"\\f13d\";\r\n}\r\n.fa-unlock-alt:before {\r\n  content: \"\\f13e\";\r\n}\r\n.fa-bullseye:before {\r\n  content: \"\\f140\";\r\n}\r\n.fa-ellipsis-h:before {\r\n  content: \"\\f141\";\r\n}\r\n.fa-ellipsis-v:before {\r\n  content: \"\\f142\";\r\n}\r\n.fa-rss-square:before {\r\n  content: \"\\f143\";\r\n}\r\n.fa-play-circle:before {\r\n  content: \"\\f144\";\r\n}\r\n.fa-ticket:before {\r\n  content: \"\\f145\";\r\n}\r\n.fa-minus-square:before {\r\n  content: \"\\f146\";\r\n}\r\n.fa-minus-square-o:before {\r\n  content: \"\\f147\";\r\n}\r\n.fa-level-up:before {\r\n  content: \"\\f148\";\r\n}\r\n.fa-level-down:before {\r\n  content: \"\\f149\";\r\n}\r\n.fa-check-square:before {\r\n  content: \"\\f14a\";\r\n}\r\n.fa-pencil-square:before {\r\n  content: \"\\f14b\";\r\n}\r\n.fa-external-link-square:before {\r\n  content: \"\\f14c\";\r\n}\r\n.fa-share-square:before {\r\n  content: \"\\f14d\";\r\n}\r\n.fa-compass:before {\r\n  content: \"\\f14e\";\r\n}\r\n.fa-toggle-down:before,\r\n.fa-caret-square-o-down:before {\r\n  content: \"\\f150\";\r\n}\r\n.fa-toggle-up:before,\r\n.fa-caret-square-o-up:before {\r\n  content: \"\\f151\";\r\n}\r\n.fa-toggle-right:before,\r\n.fa-caret-square-o-right:before {\r\n  content: \"\\f152\";\r\n}\r\n.fa-euro:before,\r\n.fa-eur:before {\r\n  content: \"\\f153\";\r\n}\r\n.fa-gbp:before {\r\n  content: \"\\f154\";\r\n}\r\n.fa-dollar:before,\r\n.fa-usd:before {\r\n  content: \"\\f155\";\r\n}\r\n.fa-rupee:before,\r\n.fa-inr:before {\r\n  content: \"\\f156\";\r\n}\r\n.fa-cny:before,\r\n.fa-rmb:before,\r\n.fa-yen:before,\r\n.fa-jpy:before {\r\n  content: \"\\f157\";\r\n}\r\n.fa-ruble:before,\r\n.fa-rouble:before,\r\n.fa-rub:before {\r\n  content: \"\\f158\";\r\n}\r\n.fa-won:before,\r\n.fa-krw:before {\r\n  content: \"\\f159\";\r\n}\r\n.fa-bitcoin:before,\r\n.fa-btc:before {\r\n  content: \"\\f15a\";\r\n}\r\n.fa-file:before {\r\n  content: \"\\f15b\";\r\n}\r\n.fa-file-text:before {\r\n  content: \"\\f15c\";\r\n}\r\n.fa-sort-alpha-asc:before {\r\n  content: \"\\f15d\";\r\n}\r\n.fa-sort-alpha-desc:before {\r\n  content: \"\\f15e\";\r\n}\r\n.fa-sort-amount-asc:before {\r\n  content: \"\\f160\";\r\n}\r\n.fa-sort-amount-desc:before {\r\n  content: \"\\f161\";\r\n}\r\n.fa-sort-numeric-asc:before {\r\n  content: \"\\f162\";\r\n}\r\n.fa-sort-numeric-desc:before {\r\n  content: \"\\f163\";\r\n}\r\n.fa-thumbs-up:before {\r\n  content: \"\\f164\";\r\n}\r\n.fa-thumbs-down:before {\r\n  content: \"\\f165\";\r\n}\r\n.fa-youtube-square:before {\r\n  content: \"\\f166\";\r\n}\r\n.fa-youtube:before {\r\n  content: \"\\f167\";\r\n}\r\n.fa-xing:before {\r\n  content: \"\\f168\";\r\n}\r\n.fa-xing-square:before {\r\n  content: \"\\f169\";\r\n}\r\n.fa-youtube-play:before {\r\n  content: \"\\f16a\";\r\n}\r\n.fa-dropbox:before {\r\n  content: \"\\f16b\";\r\n}\r\n.fa-stack-overflow:before {\r\n  content: \"\\f16c\";\r\n}\r\n.fa-instagram:before {\r\n  content: \"\\f16d\";\r\n}\r\n.fa-flickr:before {\r\n  content: \"\\f16e\";\r\n}\r\n.fa-adn:before {\r\n  content: \"\\f170\";\r\n}\r\n.fa-bitbucket:before {\r\n  content: \"\\f171\";\r\n}\r\n.fa-bitbucket-square:before {\r\n  content: \"\\f172\";\r\n}\r\n.fa-tumblr:before {\r\n  content: \"\\f173\";\r\n}\r\n.fa-tumblr-square:before {\r\n  content: \"\\f174\";\r\n}\r\n.fa-long-arrow-down:before {\r\n  content: \"\\f175\";\r\n}\r\n.fa-long-arrow-up:before {\r\n  content: \"\\f176\";\r\n}\r\n.fa-long-arrow-left:before {\r\n  content: \"\\f177\";\r\n}\r\n.fa-long-arrow-right:before {\r\n  content: \"\\f178\";\r\n}\r\n.fa-apple:before {\r\n  content: \"\\f179\";\r\n}\r\n.fa-windows:before {\r\n  content: \"\\f17a\";\r\n}\r\n.fa-android:before {\r\n  content: \"\\f17b\";\r\n}\r\n.fa-linux:before {\r\n  content: \"\\f17c\";\r\n}\r\n.fa-dribbble:before {\r\n  content: \"\\f17d\";\r\n}\r\n.fa-skype:before {\r\n  content: \"\\f17e\";\r\n}\r\n.fa-foursquare:before {\r\n  content: \"\\f180\";\r\n}\r\n.fa-trello:before {\r\n  content: \"\\f181\";\r\n}\r\n.fa-female:before {\r\n  content: \"\\f182\";\r\n}\r\n.fa-male:before {\r\n  content: \"\\f183\";\r\n}\r\n.fa-gittip:before,\r\n.fa-gratipay:before {\r\n  content: \"\\f184\";\r\n}\r\n.fa-sun-o:before {\r\n  content: \"\\f185\";\r\n}\r\n.fa-moon-o:before {\r\n  content: \"\\f186\";\r\n}\r\n.fa-archive:before {\r\n  content: \"\\f187\";\r\n}\r\n.fa-bug:before {\r\n  content: \"\\f188\";\r\n}\r\n.fa-vk:before {\r\n  content: \"\\f189\";\r\n}\r\n.fa-weibo:before {\r\n  content: \"\\f18a\";\r\n}\r\n.fa-renren:before {\r\n  content: \"\\f18b\";\r\n}\r\n.fa-pagelines:before {\r\n  content: \"\\f18c\";\r\n}\r\n.fa-stack-exchange:before {\r\n  content: \"\\f18d\";\r\n}\r\n.fa-arrow-circle-o-right:before {\r\n  content: \"\\f18e\";\r\n}\r\n.fa-arrow-circle-o-left:before {\r\n  content: \"\\f190\";\r\n}\r\n.fa-toggle-left:before,\r\n.fa-caret-square-o-left:before {\r\n  content: \"\\f191\";\r\n}\r\n.fa-dot-circle-o:before {\r\n  content: \"\\f192\";\r\n}\r\n.fa-wheelchair:before {\r\n  content: \"\\f193\";\r\n}\r\n.fa-vimeo-square:before {\r\n  content: \"\\f194\";\r\n}\r\n.fa-turkish-lira:before,\r\n.fa-try:before {\r\n  content: \"\\f195\";\r\n}\r\n.fa-plus-square-o:before {\r\n  content: \"\\f196\";\r\n}\r\n.fa-space-shuttle:before {\r\n  content: \"\\f197\";\r\n}\r\n.fa-slack:before {\r\n  content: \"\\f198\";\r\n}\r\n.fa-envelope-square:before {\r\n  content: \"\\f199\";\r\n}\r\n.fa-wordpress:before {\r\n  content: \"\\f19a\";\r\n}\r\n.fa-openid:before {\r\n  content: \"\\f19b\";\r\n}\r\n.fa-institution:before,\r\n.fa-bank:before,\r\n.fa-university:before {\r\n  content: \"\\f19c\";\r\n}\r\n.fa-mortar-board:before,\r\n.fa-graduation-cap:before {\r\n  content: \"\\f19d\";\r\n}\r\n.fa-yahoo:before {\r\n  content: \"\\f19e\";\r\n}\r\n.fa-google:before {\r\n  content: \"\\f1a0\";\r\n}\r\n.fa-reddit:before {\r\n  content: \"\\f1a1\";\r\n}\r\n.fa-reddit-square:before {\r\n  content: \"\\f1a2\";\r\n}\r\n.fa-stumbleupon-circle:before {\r\n  content: \"\\f1a3\";\r\n}\r\n.fa-stumbleupon:before {\r\n  content: \"\\f1a4\";\r\n}\r\n.fa-delicious:before {\r\n  content: \"\\f1a5\";\r\n}\r\n.fa-digg:before {\r\n  content: \"\\f1a6\";\r\n}\r\n.fa-pied-piper-pp:before {\r\n  content: \"\\f1a7\";\r\n}\r\n.fa-pied-piper-alt:before {\r\n  content: \"\\f1a8\";\r\n}\r\n.fa-drupal:before {\r\n  content: \"\\f1a9\";\r\n}\r\n.fa-joomla:before {\r\n  content: \"\\f1aa\";\r\n}\r\n.fa-language:before {\r\n  content: \"\\f1ab\";\r\n}\r\n.fa-fax:before {\r\n  content: \"\\f1ac\";\r\n}\r\n.fa-building:before {\r\n  content: \"\\f1ad\";\r\n}\r\n.fa-child:before {\r\n  content: \"\\f1ae\";\r\n}\r\n.fa-paw:before {\r\n  content: \"\\f1b0\";\r\n}\r\n.fa-spoon:before {\r\n  content: \"\\f1b1\";\r\n}\r\n.fa-cube:before {\r\n  content: \"\\f1b2\";\r\n}\r\n.fa-cubes:before {\r\n  content: \"\\f1b3\";\r\n}\r\n.fa-behance:before {\r\n  content: \"\\f1b4\";\r\n}\r\n.fa-behance-square:before {\r\n  content: \"\\f1b5\";\r\n}\r\n.fa-steam:before {\r\n  content: \"\\f1b6\";\r\n}\r\n.fa-steam-square:before {\r\n  content: \"\\f1b7\";\r\n}\r\n.fa-recycle:before {\r\n  content: \"\\f1b8\";\r\n}\r\n.fa-automobile:before,\r\n.fa-car:before {\r\n  content: \"\\f1b9\";\r\n}\r\n.fa-cab:before,\r\n.fa-taxi:before {\r\n  content: \"\\f1ba\";\r\n}\r\n.fa-tree:before {\r\n  content: \"\\f1bb\";\r\n}\r\n.fa-spotify:before {\r\n  content: \"\\f1bc\";\r\n}\r\n.fa-deviantart:before {\r\n  content: \"\\f1bd\";\r\n}\r\n.fa-soundcloud:before {\r\n  content: \"\\f1be\";\r\n}\r\n.fa-database:before {\r\n  content: \"\\f1c0\";\r\n}\r\n.fa-file-pdf-o:before {\r\n  content: \"\\f1c1\";\r\n}\r\n.fa-file-word-o:before {\r\n  content: \"\\f1c2\";\r\n}\r\n.fa-file-excel-o:before {\r\n  content: \"\\f1c3\";\r\n}\r\n.fa-file-powerpoint-o:before {\r\n  content: \"\\f1c4\";\r\n}\r\n.fa-file-photo-o:before,\r\n.fa-file-picture-o:before,\r\n.fa-file-image-o:before {\r\n  content: \"\\f1c5\";\r\n}\r\n.fa-file-zip-o:before,\r\n.fa-file-archive-o:before {\r\n  content: \"\\f1c6\";\r\n}\r\n.fa-file-sound-o:before,\r\n.fa-file-audio-o:before {\r\n  content: \"\\f1c7\";\r\n}\r\n.fa-file-movie-o:before,\r\n.fa-file-video-o:before {\r\n  content: \"\\f1c8\";\r\n}\r\n.fa-file-code-o:before {\r\n  content: \"\\f1c9\";\r\n}\r\n.fa-vine:before {\r\n  content: \"\\f1ca\";\r\n}\r\n.fa-codepen:before {\r\n  content: \"\\f1cb\";\r\n}\r\n.fa-jsfiddle:before {\r\n  content: \"\\f1cc\";\r\n}\r\n.fa-life-bouy:before,\r\n.fa-life-buoy:before,\r\n.fa-life-saver:before,\r\n.fa-support:before,\r\n.fa-life-ring:before {\r\n  content: \"\\f1cd\";\r\n}\r\n.fa-circle-o-notch:before {\r\n  content: \"\\f1ce\";\r\n}\r\n.fa-ra:before,\r\n.fa-resistance:before,\r\n.fa-rebel:before {\r\n  content: \"\\f1d0\";\r\n}\r\n.fa-ge:before,\r\n.fa-empire:before {\r\n  content: \"\\f1d1\";\r\n}\r\n.fa-git-square:before {\r\n  content: \"\\f1d2\";\r\n}\r\n.fa-git:before {\r\n  content: \"\\f1d3\";\r\n}\r\n.fa-y-combinator-square:before,\r\n.fa-yc-square:before,\r\n.fa-hacker-news:before {\r\n  content: \"\\f1d4\";\r\n}\r\n.fa-tencent-weibo:before {\r\n  content: \"\\f1d5\";\r\n}\r\n.fa-qq:before {\r\n  content: \"\\f1d6\";\r\n}\r\n.fa-wechat:before,\r\n.fa-weixin:before {\r\n  content: \"\\f1d7\";\r\n}\r\n.fa-send:before,\r\n.fa-paper-plane:before {\r\n  content: \"\\f1d8\";\r\n}\r\n.fa-send-o:before,\r\n.fa-paper-plane-o:before {\r\n  content: \"\\f1d9\";\r\n}\r\n.fa-history:before {\r\n  content: \"\\f1da\";\r\n}\r\n.fa-circle-thin:before {\r\n  content: \"\\f1db\";\r\n}\r\n.fa-header:before {\r\n  content: \"\\f1dc\";\r\n}\r\n.fa-paragraph:before {\r\n  content: \"\\f1dd\";\r\n}\r\n.fa-sliders:before {\r\n  content: \"\\f1de\";\r\n}\r\n.fa-share-alt:before {\r\n  content: \"\\f1e0\";\r\n}\r\n.fa-share-alt-square:before {\r\n  content: \"\\f1e1\";\r\n}\r\n.fa-bomb:before {\r\n  content: \"\\f1e2\";\r\n}\r\n.fa-soccer-ball-o:before,\r\n.fa-futbol-o:before {\r\n  content: \"\\f1e3\";\r\n}\r\n.fa-tty:before {\r\n  content: \"\\f1e4\";\r\n}\r\n.fa-binoculars:before {\r\n  content: \"\\f1e5\";\r\n}\r\n.fa-plug:before {\r\n  content: \"\\f1e6\";\r\n}\r\n.fa-slideshare:before {\r\n  content: \"\\f1e7\";\r\n}\r\n.fa-twitch:before {\r\n  content: \"\\f1e8\";\r\n}\r\n.fa-yelp:before {\r\n  content: \"\\f1e9\";\r\n}\r\n.fa-newspaper-o:before {\r\n  content: \"\\f1ea\";\r\n}\r\n.fa-wifi:before {\r\n  content: \"\\f1eb\";\r\n}\r\n.fa-calculator:before {\r\n  content: \"\\f1ec\";\r\n}\r\n.fa-paypal:before {\r\n  content: \"\\f1ed\";\r\n}\r\n.fa-google-wallet:before {\r\n  content: \"\\f1ee\";\r\n}\r\n.fa-cc-visa:before {\r\n  content: \"\\f1f0\";\r\n}\r\n.fa-cc-mastercard:before {\r\n  content: \"\\f1f1\";\r\n}\r\n.fa-cc-discover:before {\r\n  content: \"\\f1f2\";\r\n}\r\n.fa-cc-amex:before {\r\n  content: \"\\f1f3\";\r\n}\r\n.fa-cc-paypal:before {\r\n  content: \"\\f1f4\";\r\n}\r\n.fa-cc-stripe:before {\r\n  content: \"\\f1f5\";\r\n}\r\n.fa-bell-slash:before {\r\n  content: \"\\f1f6\";\r\n}\r\n.fa-bell-slash-o:before {\r\n  content: \"\\f1f7\";\r\n}\r\n.fa-trash:before {\r\n  content: \"\\f1f8\";\r\n}\r\n.fa-copyright:before {\r\n  content: \"\\f1f9\";\r\n}\r\n.fa-at:before {\r\n  content: \"\\f1fa\";\r\n}\r\n.fa-eyedropper:before {\r\n  content: \"\\f1fb\";\r\n}\r\n.fa-paint-brush:before {\r\n  content: \"\\f1fc\";\r\n}\r\n.fa-birthday-cake:before {\r\n  content: \"\\f1fd\";\r\n}\r\n.fa-area-chart:before {\r\n  content: \"\\f1fe\";\r\n}\r\n.fa-pie-chart:before {\r\n  content: \"\\f200\";\r\n}\r\n.fa-line-chart:before {\r\n  content: \"\\f201\";\r\n}\r\n.fa-lastfm:before {\r\n  content: \"\\f202\";\r\n}\r\n.fa-lastfm-square:before {\r\n  content: \"\\f203\";\r\n}\r\n.fa-toggle-off:before {\r\n  content: \"\\f204\";\r\n}\r\n.fa-toggle-on:before {\r\n  content: \"\\f205\";\r\n}\r\n.fa-bicycle:before {\r\n  content: \"\\f206\";\r\n}\r\n.fa-bus:before {\r\n  content: \"\\f207\";\r\n}\r\n.fa-ioxhost:before {\r\n  content: \"\\f208\";\r\n}\r\n.fa-angellist:before {\r\n  content: \"\\f209\";\r\n}\r\n.fa-cc:before {\r\n  content: \"\\f20a\";\r\n}\r\n.fa-shekel:before,\r\n.fa-sheqel:before,\r\n.fa-ils:before {\r\n  content: \"\\f20b\";\r\n}\r\n.fa-meanpath:before {\r\n  content: \"\\f20c\";\r\n}\r\n.fa-buysellads:before {\r\n  content: \"\\f20d\";\r\n}\r\n.fa-connectdevelop:before {\r\n  content: \"\\f20e\";\r\n}\r\n.fa-dashcube:before {\r\n  content: \"\\f210\";\r\n}\r\n.fa-forumbee:before {\r\n  content: \"\\f211\";\r\n}\r\n.fa-leanpub:before {\r\n  content: \"\\f212\";\r\n}\r\n.fa-sellsy:before {\r\n  content: \"\\f213\";\r\n}\r\n.fa-shirtsinbulk:before {\r\n  content: \"\\f214\";\r\n}\r\n.fa-simplybuilt:before {\r\n  content: \"\\f215\";\r\n}\r\n.fa-skyatlas:before {\r\n  content: \"\\f216\";\r\n}\r\n.fa-cart-plus:before {\r\n  content: \"\\f217\";\r\n}\r\n.fa-cart-arrow-down:before {\r\n  content: \"\\f218\";\r\n}\r\n.fa-diamond:before {\r\n  content: \"\\f219\";\r\n}\r\n.fa-ship:before {\r\n  content: \"\\f21a\";\r\n}\r\n.fa-user-secret:before {\r\n  content: \"\\f21b\";\r\n}\r\n.fa-motorcycle:before {\r\n  content: \"\\f21c\";\r\n}\r\n.fa-street-view:before {\r\n  content: \"\\f21d\";\r\n}\r\n.fa-heartbeat:before {\r\n  content: \"\\f21e\";\r\n}\r\n.fa-venus:before {\r\n  content: \"\\f221\";\r\n}\r\n.fa-mars:before {\r\n  content: \"\\f222\";\r\n}\r\n.fa-mercury:before {\r\n  content: \"\\f223\";\r\n}\r\n.fa-intersex:before,\r\n.fa-transgender:before {\r\n  content: \"\\f224\";\r\n}\r\n.fa-transgender-alt:before {\r\n  content: \"\\f225\";\r\n}\r\n.fa-venus-double:before {\r\n  content: \"\\f226\";\r\n}\r\n.fa-mars-double:before {\r\n  content: \"\\f227\";\r\n}\r\n.fa-venus-mars:before {\r\n  content: \"\\f228\";\r\n}\r\n.fa-mars-stroke:before {\r\n  content: \"\\f229\";\r\n}\r\n.fa-mars-stroke-v:before {\r\n  content: \"\\f22a\";\r\n}\r\n.fa-mars-stroke-h:before {\r\n  content: \"\\f22b\";\r\n}\r\n.fa-neuter:before {\r\n  content: \"\\f22c\";\r\n}\r\n.fa-genderless:before {\r\n  content: \"\\f22d\";\r\n}\r\n.fa-facebook-official:before {\r\n  content: \"\\f230\";\r\n}\r\n.fa-pinterest-p:before {\r\n  content: \"\\f231\";\r\n}\r\n.fa-whatsapp:before {\r\n  content: \"\\f232\";\r\n}\r\n.fa-server:before {\r\n  content: \"\\f233\";\r\n}\r\n.fa-user-plus:before {\r\n  content: \"\\f234\";\r\n}\r\n.fa-user-times:before {\r\n  content: \"\\f235\";\r\n}\r\n.fa-hotel:before,\r\n.fa-bed:before {\r\n  content: \"\\f236\";\r\n}\r\n.fa-viacoin:before {\r\n  content: \"\\f237\";\r\n}\r\n.fa-train:before {\r\n  content: \"\\f238\";\r\n}\r\n.fa-subway:before {\r\n  content: \"\\f239\";\r\n}\r\n.fa-medium:before {\r\n  content: \"\\f23a\";\r\n}\r\n.fa-yc:before,\r\n.fa-y-combinator:before {\r\n  content: \"\\f23b\";\r\n}\r\n.fa-optin-monster:before {\r\n  content: \"\\f23c\";\r\n}\r\n.fa-opencart:before {\r\n  content: \"\\f23d\";\r\n}\r\n.fa-expeditedssl:before {\r\n  content: \"\\f23e\";\r\n}\r\n.fa-battery-4:before,\r\n.fa-battery:before,\r\n.fa-battery-full:before {\r\n  content: \"\\f240\";\r\n}\r\n.fa-battery-3:before,\r\n.fa-battery-three-quarters:before {\r\n  content: \"\\f241\";\r\n}\r\n.fa-battery-2:before,\r\n.fa-battery-half:before {\r\n  content: \"\\f242\";\r\n}\r\n.fa-battery-1:before,\r\n.fa-battery-quarter:before {\r\n  content: \"\\f243\";\r\n}\r\n.fa-battery-0:before,\r\n.fa-battery-empty:before {\r\n  content: \"\\f244\";\r\n}\r\n.fa-mouse-pointer:before {\r\n  content: \"\\f245\";\r\n}\r\n.fa-i-cursor:before {\r\n  content: \"\\f246\";\r\n}\r\n.fa-object-group:before {\r\n  content: \"\\f247\";\r\n}\r\n.fa-object-ungroup:before {\r\n  content: \"\\f248\";\r\n}\r\n.fa-sticky-note:before {\r\n  content: \"\\f249\";\r\n}\r\n.fa-sticky-note-o:before {\r\n  content: \"\\f24a\";\r\n}\r\n.fa-cc-jcb:before {\r\n  content: \"\\f24b\";\r\n}\r\n.fa-cc-diners-club:before {\r\n  content: \"\\f24c\";\r\n}\r\n.fa-clone:before {\r\n  content: \"\\f24d\";\r\n}\r\n.fa-balance-scale:before {\r\n  content: \"\\f24e\";\r\n}\r\n.fa-hourglass-o:before {\r\n  content: \"\\f250\";\r\n}\r\n.fa-hourglass-1:before,\r\n.fa-hourglass-start:before {\r\n  content: \"\\f251\";\r\n}\r\n.fa-hourglass-2:before,\r\n.fa-hourglass-half:before {\r\n  content: \"\\f252\";\r\n}\r\n.fa-hourglass-3:before,\r\n.fa-hourglass-end:before {\r\n  content: \"\\f253\";\r\n}\r\n.fa-hourglass:before {\r\n  content: \"\\f254\";\r\n}\r\n.fa-hand-grab-o:before,\r\n.fa-hand-rock-o:before {\r\n  content: \"\\f255\";\r\n}\r\n.fa-hand-stop-o:before,\r\n.fa-hand-paper-o:before {\r\n  content: \"\\f256\";\r\n}\r\n.fa-hand-scissors-o:before {\r\n  content: \"\\f257\";\r\n}\r\n.fa-hand-lizard-o:before {\r\n  content: \"\\f258\";\r\n}\r\n.fa-hand-spock-o:before {\r\n  content: \"\\f259\";\r\n}\r\n.fa-hand-pointer-o:before {\r\n  content: \"\\f25a\";\r\n}\r\n.fa-hand-peace-o:before {\r\n  content: \"\\f25b\";\r\n}\r\n.fa-trademark:before {\r\n  content: \"\\f25c\";\r\n}\r\n.fa-registered:before {\r\n  content: \"\\f25d\";\r\n}\r\n.fa-creative-commons:before {\r\n  content: \"\\f25e\";\r\n}\r\n.fa-gg:before {\r\n  content: \"\\f260\";\r\n}\r\n.fa-gg-circle:before {\r\n  content: \"\\f261\";\r\n}\r\n.fa-tripadvisor:before {\r\n  content: \"\\f262\";\r\n}\r\n.fa-odnoklassniki:before {\r\n  content: \"\\f263\";\r\n}\r\n.fa-odnoklassniki-square:before {\r\n  content: \"\\f264\";\r\n}\r\n.fa-get-pocket:before {\r\n  content: \"\\f265\";\r\n}\r\n.fa-wikipedia-w:before {\r\n  content: \"\\f266\";\r\n}\r\n.fa-safari:before {\r\n  content: \"\\f267\";\r\n}\r\n.fa-chrome:before {\r\n  content: \"\\f268\";\r\n}\r\n.fa-firefox:before {\r\n  content: \"\\f269\";\r\n}\r\n.fa-opera:before {\r\n  content: \"\\f26a\";\r\n}\r\n.fa-internet-explorer:before {\r\n  content: \"\\f26b\";\r\n}\r\n.fa-tv:before,\r\n.fa-television:before {\r\n  content: \"\\f26c\";\r\n}\r\n.fa-contao:before {\r\n  content: \"\\f26d\";\r\n}\r\n.fa-500px:before {\r\n  content: \"\\f26e\";\r\n}\r\n.fa-amazon:before {\r\n  content: \"\\f270\";\r\n}\r\n.fa-calendar-plus-o:before {\r\n  content: \"\\f271\";\r\n}\r\n.fa-calendar-minus-o:before {\r\n  content: \"\\f272\";\r\n}\r\n.fa-calendar-times-o:before {\r\n  content: \"\\f273\";\r\n}\r\n.fa-calendar-check-o:before {\r\n  content: \"\\f274\";\r\n}\r\n.fa-industry:before {\r\n  content: \"\\f275\";\r\n}\r\n.fa-map-pin:before {\r\n  content: \"\\f276\";\r\n}\r\n.fa-map-signs:before {\r\n  content: \"\\f277\";\r\n}\r\n.fa-map-o:before {\r\n  content: \"\\f278\";\r\n}\r\n.fa-map:before {\r\n  content: \"\\f279\";\r\n}\r\n.fa-commenting:before {\r\n  content: \"\\f27a\";\r\n}\r\n.fa-commenting-o:before {\r\n  content: \"\\f27b\";\r\n}\r\n.fa-houzz:before {\r\n  content: \"\\f27c\";\r\n}\r\n.fa-vimeo:before {\r\n  content: \"\\f27d\";\r\n}\r\n.fa-black-tie:before {\r\n  content: \"\\f27e\";\r\n}\r\n.fa-fonticons:before {\r\n  content: \"\\f280\";\r\n}\r\n.fa-reddit-alien:before {\r\n  content: \"\\f281\";\r\n}\r\n.fa-edge:before {\r\n  content: \"\\f282\";\r\n}\r\n.fa-credit-card-alt:before {\r\n  content: \"\\f283\";\r\n}\r\n.fa-codiepie:before {\r\n  content: \"\\f284\";\r\n}\r\n.fa-modx:before {\r\n  content: \"\\f285\";\r\n}\r\n.fa-fort-awesome:before {\r\n  content: \"\\f286\";\r\n}\r\n.fa-usb:before {\r\n  content: \"\\f287\";\r\n}\r\n.fa-product-hunt:before {\r\n  content: \"\\f288\";\r\n}\r\n.fa-mixcloud:before {\r\n  content: \"\\f289\";\r\n}\r\n.fa-scribd:before {\r\n  content: \"\\f28a\";\r\n}\r\n.fa-pause-circle:before {\r\n  content: \"\\f28b\";\r\n}\r\n.fa-pause-circle-o:before {\r\n  content: \"\\f28c\";\r\n}\r\n.fa-stop-circle:before {\r\n  content: \"\\f28d\";\r\n}\r\n.fa-stop-circle-o:before {\r\n  content: \"\\f28e\";\r\n}\r\n.fa-shopping-bag:before {\r\n  content: \"\\f290\";\r\n}\r\n.fa-shopping-basket:before {\r\n  content: \"\\f291\";\r\n}\r\n.fa-hashtag:before {\r\n  content: \"\\f292\";\r\n}\r\n.fa-bluetooth:before {\r\n  content: \"\\f293\";\r\n}\r\n.fa-bluetooth-b:before {\r\n  content: \"\\f294\";\r\n}\r\n.fa-percent:before {\r\n  content: \"\\f295\";\r\n}\r\n.fa-gitlab:before {\r\n  content: \"\\f296\";\r\n}\r\n.fa-wpbeginner:before {\r\n  content: \"\\f297\";\r\n}\r\n.fa-wpforms:before {\r\n  content: \"\\f298\";\r\n}\r\n.fa-envira:before {\r\n  content: \"\\f299\";\r\n}\r\n.fa-universal-access:before {\r\n  content: \"\\f29a\";\r\n}\r\n.fa-wheelchair-alt:before {\r\n  content: \"\\f29b\";\r\n}\r\n.fa-question-circle-o:before {\r\n  content: \"\\f29c\";\r\n}\r\n.fa-blind:before {\r\n  content: \"\\f29d\";\r\n}\r\n.fa-audio-description:before {\r\n  content: \"\\f29e\";\r\n}\r\n.fa-volume-control-phone:before {\r\n  content: \"\\f2a0\";\r\n}\r\n.fa-braille:before {\r\n  content: \"\\f2a1\";\r\n}\r\n.fa-assistive-listening-systems:before {\r\n  content: \"\\f2a2\";\r\n}\r\n.fa-asl-interpreting:before,\r\n.fa-american-sign-language-interpreting:before {\r\n  content: \"\\f2a3\";\r\n}\r\n.fa-deafness:before,\r\n.fa-hard-of-hearing:before,\r\n.fa-deaf:before {\r\n  content: \"\\f2a4\";\r\n}\r\n.fa-glide:before {\r\n  content: \"\\f2a5\";\r\n}\r\n.fa-glide-g:before {\r\n  content: \"\\f2a6\";\r\n}\r\n.fa-signing:before,\r\n.fa-sign-language:before {\r\n  content: \"\\f2a7\";\r\n}\r\n.fa-low-vision:before {\r\n  content: \"\\f2a8\";\r\n}\r\n.fa-viadeo:before {\r\n  content: \"\\f2a9\";\r\n}\r\n.fa-viadeo-square:before {\r\n  content: \"\\f2aa\";\r\n}\r\n.fa-snapchat:before {\r\n  content: \"\\f2ab\";\r\n}\r\n.fa-snapchat-ghost:before {\r\n  content: \"\\f2ac\";\r\n}\r\n.fa-snapchat-square:before {\r\n  content: \"\\f2ad\";\r\n}\r\n.fa-pied-piper:before {\r\n  content: \"\\f2ae\";\r\n}\r\n.fa-first-order:before {\r\n  content: \"\\f2b0\";\r\n}\r\n.fa-yoast:before {\r\n  content: \"\\f2b1\";\r\n}\r\n.fa-themeisle:before {\r\n  content: \"\\f2b2\";\r\n}\r\n.fa-google-plus-circle:before,\r\n.fa-google-plus-official:before {\r\n  content: \"\\f2b3\";\r\n}\r\n.fa-fa:before,\r\n.fa-font-awesome:before {\r\n  content: \"\\f2b4\";\r\n}\r\n.fa-handshake-o:before {\r\n  content: \"\\f2b5\";\r\n}\r\n.fa-envelope-open:before {\r\n  content: \"\\f2b6\";\r\n}\r\n.fa-envelope-open-o:before {\r\n  content: \"\\f2b7\";\r\n}\r\n.fa-linode:before {\r\n  content: \"\\f2b8\";\r\n}\r\n.fa-address-book:before {\r\n  content: \"\\f2b9\";\r\n}\r\n.fa-address-book-o:before {\r\n  content: \"\\f2ba\";\r\n}\r\n.fa-vcard:before,\r\n.fa-address-card:before {\r\n  content: \"\\f2bb\";\r\n}\r\n.fa-vcard-o:before,\r\n.fa-address-card-o:before {\r\n  content: \"\\f2bc\";\r\n}\r\n.fa-user-circle:before {\r\n  content: \"\\f2bd\";\r\n}\r\n.fa-user-circle-o:before {\r\n  content: \"\\f2be\";\r\n}\r\n.fa-user-o:before {\r\n  content: \"\\f2c0\";\r\n}\r\n.fa-id-badge:before {\r\n  content: \"\\f2c1\";\r\n}\r\n.fa-drivers-license:before,\r\n.fa-id-card:before {\r\n  content: \"\\f2c2\";\r\n}\r\n.fa-drivers-license-o:before,\r\n.fa-id-card-o:before {\r\n  content: \"\\f2c3\";\r\n}\r\n.fa-quora:before {\r\n  content: \"\\f2c4\";\r\n}\r\n.fa-free-code-camp:before {\r\n  content: \"\\f2c5\";\r\n}\r\n.fa-telegram:before {\r\n  content: \"\\f2c6\";\r\n}\r\n.fa-thermometer-4:before,\r\n.fa-thermometer:before,\r\n.fa-thermometer-full:before {\r\n  content: \"\\f2c7\";\r\n}\r\n.fa-thermometer-3:before,\r\n.fa-thermometer-three-quarters:before {\r\n  content: \"\\f2c8\";\r\n}\r\n.fa-thermometer-2:before,\r\n.fa-thermometer-half:before {\r\n  content: \"\\f2c9\";\r\n}\r\n.fa-thermometer-1:before,\r\n.fa-thermometer-quarter:before {\r\n  content: \"\\f2ca\";\r\n}\r\n.fa-thermometer-0:before,\r\n.fa-thermometer-empty:before {\r\n  content: \"\\f2cb\";\r\n}\r\n.fa-shower:before {\r\n  content: \"\\f2cc\";\r\n}\r\n.fa-bathtub:before,\r\n.fa-s15:before,\r\n.fa-bath:before {\r\n  content: \"\\f2cd\";\r\n}\r\n.fa-podcast:before {\r\n  content: \"\\f2ce\";\r\n}\r\n.fa-window-maximize:before {\r\n  content: \"\\f2d0\";\r\n}\r\n.fa-window-minimize:before {\r\n  content: \"\\f2d1\";\r\n}\r\n.fa-window-restore:before {\r\n  content: \"\\f2d2\";\r\n}\r\n.fa-times-rectangle:before,\r\n.fa-window-close:before {\r\n  content: \"\\f2d3\";\r\n}\r\n.fa-times-rectangle-o:before,\r\n.fa-window-close-o:before {\r\n  content: \"\\f2d4\";\r\n}\r\n.fa-bandcamp:before {\r\n  content: \"\\f2d5\";\r\n}\r\n.fa-grav:before {\r\n  content: \"\\f2d6\";\r\n}\r\n.fa-etsy:before {\r\n  content: \"\\f2d7\";\r\n}\r\n.fa-imdb:before {\r\n  content: \"\\f2d8\";\r\n}\r\n.fa-ravelry:before {\r\n  content: \"\\f2d9\";\r\n}\r\n.fa-eercast:before {\r\n  content: \"\\f2da\";\r\n}\r\n.fa-microchip:before {\r\n  content: \"\\f2db\";\r\n}\r\n.fa-snowflake-o:before {\r\n  content: \"\\f2dc\";\r\n}\r\n.fa-superpowers:before {\r\n  content: \"\\f2dd\";\r\n}\r\n.fa-wpexplorer:before {\r\n  content: \"\\f2de\";\r\n}\r\n.fa-meetup:before {\r\n  content: \"\\f2e0\";\r\n}\r\n.sr-only {\r\n  position: absolute;\r\n  width: 1px;\r\n  height: 1px;\r\n  padding: 0;\r\n  margin: -1px;\r\n  overflow: hidden;\r\n  clip: rect(0, 0, 0, 0);\r\n  border: 0;\r\n}\r\n.sr-only-focusable:active,\r\n.sr-only-focusable:focus {\r\n  position: static;\r\n  width: auto;\r\n  height: auto;\r\n  margin: 0;\r\n  overflow: visible;\r\n  clip: auto;\r\n}\r\n.icon-list a {\n  display: block;\n  padding-left: 40px;\n  height: 32px;\n  line-height: 32px;\n  margin-bottom: 5px; }\r\n.icon-list a .icon, .icon-list a .fa,\n  .icon-list a [class^=\"icon-\"],\n  .icon-list a [class*=\" icon-\"] {\n    position: absolute;\n    top: 0;\n    left: 10px;\n    min-width: 30px;\n    text-align: center;\n    line-height: 32px; }\r\n.icon-list a:hover .fa,\n  .icon-list a:hover .icon,\n  .icon-list a:hover [class^=\"icon-\"],\n  .icon-list a:hover [class*=\" icon-\"] {\n    top: 5px;\n    font-size: 1.5em; }\r\n.IconClass {\n  font-size: 30px;\n  height: 40px;\n  top: -12px;\n  position: relative; }\r\n.IconClassMt3 {\n  font-size: 33px;\n  position: relative;\n  top: -10px;\n  margin-top: 10px; }\n"

/***/ }),

/***/ "./src/app/icons/fontawesome/fontawesome.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FontawesomeComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var FontawesomeComponent = (function () {
    function FontawesomeComponent() {
        this.temp = [];
        this.icons = [];
        var icons = [
            '500px',
            'address-book',
            'address-book-o',
            'address-card',
            'address-card-o',
            'adjust',
            'adn',
            'align-center',
            'align-justify',
            'align-left',
            'align-right',
            'amazon',
            'ambulance',
            'american-sign-language-interpreting',
            'anchor',
            'android',
            'angellist',
            'angle-double-down',
            'angle-double-left',
            'angle-double-right',
            'angle-double-up',
            'angle-down',
            'angle-left',
            'angle-right',
            'angle-up',
            'apple',
            'archive',
            'area-chart',
            'arrow-circle-down',
            'arrow-circle-left',
            'arrow-circle-o-down',
            'arrow-circle-o-left',
            'arrow-circle-o-right',
            'arrow-circle-o-up',
            'arrow-circle-right',
            'arrow-circle-up',
            'arrow-down',
            'arrow-left',
            'arrow-right',
            'arrow-up',
            'arrows',
            'arrows-alt',
            'arrows-h',
            'arrows-v',
            'asl-interpreting',
            'assistive-listening-systems',
            'asterisk',
            'at',
            'audio-description',
            'automobile',
            'backward',
            'balance-scale',
            'ban',
            'bandcamp',
            'bank',
            'bar-chart',
            'bar-chart-o',
            'barcode',
            'bars',
            'bath',
            'bathtub',
            'battery',
            'battery-0',
            'battery-1',
            'battery-2',
            'battery-3',
            'battery-4',
            'battery-empty',
            'battery-full',
            'battery-half',
            'battery-quarter',
            'battery-three-quarters',
            'bed',
            'beer',
            'behance',
            'behance-square',
            'bell',
            'bell-o',
            'bell-slash',
            'bell-slash-o',
            'bicycle',
            'binoculars',
            'birthday-cake',
            'bitbucket',
            'bitbucket-square',
            'bitcoin',
            'black-tie',
            'blind',
            'bluetooth',
            'bluetooth-b',
            'bold',
            'bolt',
            'bomb',
            'book',
            'bookmark',
            'bookmark-o',
            'braille',
            'briefcase',
            'btc',
            'bug',
            'building',
            'building-o',
            'bullhorn',
            'bullseye',
            'bus',
            'buysellads',
            'cab',
            'calculator',
            'calendar',
            'calendar-check-o',
            'calendar-minus-o',
            'calendar-o',
            'calendar-plus-o',
            'calendar-times-o',
            'camera',
            'camera-retro',
            'car',
            'caret-down',
            'caret-left',
            'caret-right',
            'caret-square-o-down',
            'caret-square-o-left',
            'caret-square-o-right',
            'caret-square-o-up',
            'caret-up',
            'cart-arrow-down',
            'cart-plus',
            'cc',
            'cc-amex',
            'cc-diners-club',
            'cc-discover',
            'cc-jcb',
            'cc-mastercard',
            'cc-paypal',
            'cc-stripe',
            'cc-visa',
            'certificate',
            'chain',
            'chain-broken',
            'check',
            'check-circle',
            'check-circle-o',
            'check-square',
            'check-square-o',
            'chevron-circle-down',
            'chevron-circle-left',
            'chevron-circle-right',
            'chevron-circle-up',
            'chevron-down',
            'chevron-left',
            'chevron-right',
            'chevron-up',
            'child',
            'chrome',
            'circle',
            'circle-o',
            'circle-o-notch',
            'circle-thin',
            'clipboard',
            'clock-o',
            'clone',
            'close',
            'cloud',
            'cloud-download',
            'cloud-upload',
            'cny',
            'code',
            'code-fork',
            'codepen',
            'codiepie',
            'coffee',
            'cog',
            'cogs',
            'columns',
            'comment',
            'comment-o',
            'commenting',
            'commenting-o',
            'comments',
            'comments-o',
            'compass',
            'compress',
            'connectdevelop',
            'contao',
            'copy',
            'copyright',
            'creative-commons',
            'credit-card',
            'credit-card-alt',
            'crop',
            'crosshairs',
            'css3',
            'cube',
            'cubes',
            'cut',
            'cutlery',
            'dashboard',
            'dashcube',
            'database',
            'deaf',
            'deafness',
            'dedent',
            'delicious',
            'desktop',
            'deviantart',
            'diamond',
            'digg',
            'dollar',
            'dot-circle-o',
            'download',
            'dribbble',
            'drivers-license',
            'drivers-license-o',
            'dropbox',
            'drupal',
            'edge',
            'edit',
            'eercast',
            'eject',
            'ellipsis-h',
            'ellipsis-v',
            'empire',
            'envelope',
            'envelope-o',
            'envelope-open',
            'envelope-open-o',
            'envelope-square',
            'envira',
            'eraser',
            'etsy',
            'eur',
            'euro',
            'exchange',
            'exclamation',
            'exclamation-circle',
            'exclamation-triangle',
            'expand',
            'expeditedssl',
            'external-link',
            'external-link-square',
            'eye',
            'eye-slash',
            'eyedropper',
            'fa',
            'facebook',
            'facebook-f',
            'facebook-official',
            'facebook-square',
            'fast-backward',
            'fast-forward',
            'fax',
            'feed',
            'female',
            'fighter-jet',
            'file',
            'file-archive-o',
            'file-audio-o',
            'file-code-o',
            'file-excel-o',
            'file-image-o',
            'file-movie-o',
            'file-o',
            'file-pdf-o',
            'file-photo-o',
            'file-picture-o',
            'file-powerpoint-o',
            'file-sound-o',
            'file-text',
            'file-text-o',
            'file-video-o',
            'file-word-o',
            'file-zip-o',
            'files-o',
            'film',
            'filter',
            'fire',
            'fire-extinguisher',
            'firefox',
            'first-order',
            'flag',
            'flag-checkered',
            'flag-o',
            'flash',
            'flask',
            'flickr',
            'floppy-o',
            'folder',
            'folder-o',
            'folder-open',
            'folder-open-o',
            'font',
            'font-awesome',
            'fonticons',
            'fort-awesome',
            'forumbee',
            'forward',
            'foursquare',
            'free-code-camp',
            'frown-o',
            'futbol-o',
            'gamepad',
            'gavel',
            'gbp',
            'ge',
            'gear',
            'gears',
            'genderless',
            'get-pocket',
            'gg',
            'gg-circle',
            'gift',
            'git',
            'git-square',
            'github',
            'github-alt',
            'github-square',
            'gitlab',
            'gittip',
            'glass',
            'glide',
            'glide-g',
            'globe',
            'google',
            'google-plus',
            'google-plus-circle',
            'google-plus-official',
            'google-plus-square',
            'google-wallet',
            'graduation-cap',
            'gratipay',
            'grav',
            'group',
            'h-square',
            'hacker-news',
            'hand-grab-o',
            'hand-lizard-o',
            'hand-o-down',
            'hand-o-left',
            'hand-o-right',
            'hand-o-up',
            'hand-paper-o',
            'hand-peace-o',
            'hand-pointer-o',
            'hand-rock-o',
            'hand-scissors-o',
            'hand-spock-o',
            'hand-stop-o',
            'handshake-o',
            'hard-of-hearing',
            'hashtag',
            'hdd-o',
            'header',
            'headphones',
            'heart',
            'heart-o',
            'heartbeat',
            'history',
            'home',
            'hospital-o',
            'hotel',
            'hourglass',
            'hourglass-1',
            'hourglass-2',
            'hourglass-3',
            'hourglass-end',
            'hourglass-half',
            'hourglass-o',
            'hourglass-start',
            'houzz',
            'html5',
            'i-cursor',
            'id-badge',
            'id-card',
            'id-card-o',
            'ils',
            'image',
            'imdb',
            'inbox',
            'indent',
            'industry',
            'info',
            'info-circle',
            'inr',
            'instagram',
            'institution',
            'internet-explorer',
            'intersex',
            'ioxhost',
            'italic',
            'joomla',
            'jpy',
            'jsfiddle',
            'key',
            'keyboard-o',
            'krw',
            'language',
            'laptop',
            'lastfm',
            'lastfm-square',
            'leaf',
            'leanpub',
            'legal',
            'lemon-o',
            'level-down',
            'level-up',
            'life-bouy',
            'life-buoy',
            'life-ring',
            'life-saver',
            'lightbulb-o',
            'line-chart',
            'link',
            'linkedin',
            'linkedin-square',
            'linode',
            'linux',
            'list',
            'list-alt',
            'list-ol',
            'list-ul',
            'location-arrow',
            'lock',
            'long-arrow-down',
            'long-arrow-left',
            'long-arrow-right',
            'long-arrow-up',
            'low-vision',
            'magic',
            'magnet',
            'mail-forward',
            'mail-reply',
            'mail-reply-all',
            'male',
            'map',
            'map-marker',
            'map-o',
            'map-pin',
            'map-signs',
            'mars',
            'mars-double',
            'mars-stroke',
            'mars-stroke-h',
            'mars-stroke-v',
            'maxcdn',
            'meanpath',
            'medium',
            'medkit',
            'meetup',
            'meh-o',
            'mercury',
            'microchip',
            'microphone',
            'microphone-slash',
            'minus',
            'minus-circle',
            'minus-square',
            'minus-square-o',
            'mixcloud',
            'mobile',
            'mobile-phone',
            'modx',
            'money',
            'moon-o',
            'mortar-board',
            'motorcycle',
            'mouse-pointer',
            'music',
            'navicon',
            'neuter',
            'newspaper-o',
            'object-group',
            'object-ungroup',
            'odnoklassniki',
            'odnoklassniki-square',
            'opencart',
            'openid',
            'opera',
            'optin-monster',
            'outdent',
            'pagelines',
            'paint-brush',
            'paper-plane',
            'paper-plane-o',
            'paperclip',
            'paragraph',
            'paste',
            'pause',
            'pause-circle',
            'pause-circle-o',
            'paw',
            'paypal',
            'pencil',
            'pencil-square',
            'pencil-square-o',
            'percent',
            'phone',
            'phone-square',
            'photo',
            'picture-o',
            'pie-chart',
            'pied-piper',
            'pied-piper-alt',
            'pied-piper-pp',
            'pinterest',
            'pinterest-p',
            'pinterest-square',
            'plane',
            'play',
            'play-circle',
            'play-circle-o',
            'plug',
            'plus',
            'plus-circle',
            'plus-square',
            'plus-square-o',
            'podcast',
            'power-off',
            'print',
            'product-hunt',
            'puzzle-piece',
            'qq',
            'qrcode',
            'question',
            'question-circle',
            'question-circle-o',
            'quora',
            'quote-left',
            'quote-right',
            'ra',
            'random',
            'ravelry',
            'rebel',
            'recycle',
            'reddit',
            'reddit-alien',
            'reddit-square',
            'refresh',
            'registered',
            'remove',
            'renren',
            'reorder',
            'repeat',
            'reply',
            'reply-all',
            'resistance',
            'retweet',
            'rmb',
            'road',
            'rocket',
            'rotate-left',
            'rotate-right',
            'rouble',
            'rss',
            'rss-square',
            'rub',
            'ruble',
            'rupee',
            's15',
            'safari',
            'save',
            'scissors',
            'scribd',
            'search',
            'search-minus',
            'search-plus',
            'sellsy',
            'send',
            'send-o',
            'server',
            'share',
            'share-alt',
            'share-alt-square',
            'share-square',
            'share-square-o',
            'shekel',
            'sheqel',
            'shield',
            'ship',
            'shirtsinbulk',
            'shopping-bag',
            'shopping-basket',
            'shopping-cart',
            'shower',
            'sign-in',
            'sign-language',
            'sign-out',
            'signal',
            'signing',
            'simplybuilt',
            'sitemap',
            'skyatlas',
            'skype',
            'slack',
            'sliders',
            'slideshare',
            'smile-o',
            'snapchat',
            'snapchat-ghost',
            'snapchat-square',
            'snowflake-o',
            'soccer-ball-o',
            'sort',
            'sort-alpha-asc',
            'sort-alpha-desc',
            'sort-amount-asc',
            'sort-amount-desc',
            'sort-asc',
            'sort-desc',
            'sort-down',
            'sort-numeric-asc',
            'sort-numeric-desc',
            'sort-up',
            'soundcloud',
            'space-shuttle',
            'spinner',
            'spoon',
            'spotify',
            'square',
            'square-o',
            'stack-exchange',
            'stack-overflow',
            'star',
            'star-half',
            'star-half-empty',
            'star-half-full',
            'star-half-o',
            'star-o',
            'steam',
            'steam-square',
            'step-backward',
            'step-forward',
            'stethoscope',
            'sticky-note',
            'sticky-note-o',
            'stop',
            'stop-circle',
            'stop-circle-o',
            'street-view',
            'strikethrough',
            'stumbleupon',
            'stumbleupon-circle',
            'subscript',
            'subway',
            'suitcase',
            'sun-o',
            'superpowers',
            'superscript',
            'support',
            'table',
            'tablet',
            'tachometer',
            'tag',
            'tags',
            'tasks',
            'taxi',
            'telegram',
            'television',
            'tencent-weibo',
            'terminal',
            'text-height',
            'text-width',
            'th',
            'th-large',
            'th-list',
            'themeisle',
            'thermometer',
            'thermometer-0',
            'thermometer-1',
            'thermometer-2',
            'thermometer-3',
            'thermometer-4',
            'thermometer-empty',
            'thermometer-full',
            'thermometer-half',
            'thermometer-quarter',
            'thermometer-three-quarters',
            'thumb-tack',
            'thumbs-down',
            'thumbs-o-down',
            'thumbs-o-up',
            'thumbs-up',
            'ticket',
            'times',
            'times-circle',
            'times-circle-o',
            'times-rectangle',
            'times-rectangle-o',
            'tint',
            'toggle-down',
            'toggle-left',
            'toggle-off',
            'toggle-on',
            'toggle-right',
            'toggle-up',
            'trademark',
            'train',
            'transgender',
            'transgender-alt',
            'trash',
            'trash-o',
            'tree',
            'trello',
            'tripadvisor',
            'trophy',
            'truck',
            'try',
            'tty',
            'tumblr',
            'tumblr-square',
            'turkish-lira',
            'tv',
            'twitch',
            'twitter',
            'twitter-square',
            'umbrella',
            'underline',
            'undo',
            'universal-access',
            'university',
            'unlink',
            'unlock',
            'unlock-alt',
            'unsorted',
            'upload',
            'usb',
            'usd',
            'user',
            'user-circle',
            'user-circle-o',
            'user-md',
            'user-o',
            'user-plus',
            'user-secret',
            'user-times',
            'users',
            'vcard',
            'vcard-o',
            'venus',
            'venus-double',
            'venus-mars',
            'viacoin',
            'viadeo',
            'viadeo-square',
            'video-camera',
            'vimeo',
            'vimeo-square',
            'vine',
            'vk',
            'volume-control-phone',
            'volume-down',
            'volume-off',
            'volume-up',
            'warning',
            'wechat',
            'weibo',
            'weixin',
            'whatsapp',
            'wheelchair',
            'wheelchair-alt',
            'wifi',
            'wikipedia-w',
            'window-close',
            'window-close-o',
            'window-maximize',
            'window-minimize',
            'window-restore',
            'windows',
            'won',
            'wordpress',
            'wpbeginner',
            'wpexplorer',
            'wpforms',
            'wrench',
            'xing',
            'xing-square',
            'y-combinator',
            'y-combinator-square',
            'yahoo',
            'yc',
            'yc-square',
            'yelp',
            'yen',
            'yoast',
            'youtube',
            'youtube-play',
            'youtube-square'
        ];
        this.temp = icons;
        this.icons = icons;
    }
    FontawesomeComponent.prototype.updateFilter = function (event) {
        var query = event.target.value;
        var filtered = this.temp.filter(function (el) {
            return el.toLowerCase().indexOf(query.toLowerCase()) > -1;
        });
        this.icons = filtered;
    };
    FontawesomeComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-fontawesome',
            template: __webpack_require__("./src/app/icons/fontawesome/fontawesome.component.html"),
            styles: [__webpack_require__("./src/app/icons/fontawesome/fontawesome.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], FontawesomeComponent);
    return FontawesomeComponent;
}());

//# sourceMappingURL=fontawesome.component.js.map

/***/ }),

/***/ "./src/app/icons/linea/linea.component.html":
/***/ (function(module, exports) {

module.exports = "<input type=\"email\" class=\"form-control form-control-lg mb-3\" placeholder=\"Type to filter icons\" required (keyup)='updateFilter($event)'>\r\n\r\n<div class=\"icon-list\">\r\n  <div class=\"mb-3\" *ngFor=\"let group of collection\">\r\n    <p class=\"ff-headers text-uppercase mb-3 fw-600\">{{group.name}}</p>\r\n    <div class=\"row\">\r\n      <div class=\"fa-hover col-md-3 col-sm-4\" *ngFor=\"let icon of group.icons\">\r\n        <a href=\"javascript:;\" class=\"text-color\" target=\"_blank\">\r\n          <i class=\"icon {{icon}}\"></i>\r\n          <span>{{icon}}</span>\r\n        </a>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./src/app/icons/linea/linea.component.scss":
/***/ (function(module, exports) {

module.exports = ".icon-list a {\n  display: block;\n  padding-left: 40px;\n  height: 32px;\n  line-height: 32px;\n  margin-bottom: 5px; }\n  .icon-list a .icon, .icon-list a .fa,\n  .icon-list a [class^=\"icon-\"],\n  .icon-list a [class*=\" icon-\"] {\n    position: absolute;\n    top: 0;\n    left: 10px;\n    min-width: 30px;\n    text-align: center;\n    line-height: 32px; }\n  .icon-list a:hover .fa,\n  .icon-list a:hover .icon,\n  .icon-list a:hover [class^=\"icon-\"],\n  .icon-list a:hover [class*=\" icon-\"] {\n    top: 5px;\n    font-size: 1.5em; }\n"

/***/ }),

/***/ "./src/app/icons/linea/linea.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LineaComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var LineaComponent = (function () {
    function LineaComponent() {
        this.temp = [];
        this.collection = [];
        var linea = [{
                name: 'arrows',
                icons: [
                    'icon-arrows-anticlockwise',
                    'icon-arrows-anticlockwise-dashed',
                    'icon-arrows-button-down',
                    'icon-arrows-button-off',
                    'icon-arrows-button-on',
                    'icon-arrows-button-up',
                    'icon-arrows-check',
                    'icon-arrows-circle-check',
                    'icon-arrows-circle-down',
                    'icon-arrows-circle-downleft',
                    'icon-arrows-circle-downright',
                    'icon-arrows-circle-left',
                    'icon-arrows-circle-minus',
                    'icon-arrows-circle-plus',
                    'icon-arrows-circle-remove',
                    'icon-arrows-circle-right',
                    'icon-arrows-circle-up',
                    'icon-arrows-circle-upleft',
                    'icon-arrows-circle-upright',
                    'icon-arrows-clockwise',
                    'icon-arrows-clockwise-dashed',
                    'icon-arrows-compress',
                    'icon-arrows-deny',
                    'icon-arrows-diagonal',
                    'icon-arrows-diagonal2',
                    'icon-arrows-down',
                    'icon-arrows-down-double',
                    'icon-arrows-downleft',
                    'icon-arrows-downright',
                    'icon-arrows-drag-down',
                    'icon-arrows-drag-down-dashed',
                    'icon-arrows-drag-horiz',
                    'icon-arrows-drag-left',
                    'icon-arrows-drag-left-dashed',
                    'icon-arrows-drag-right',
                    'icon-arrows-drag-right-dashed',
                    'icon-arrows-drag-up',
                    'icon-arrows-drag-up-dashed',
                    'icon-arrows-drag-vert',
                    'icon-arrows-exclamation',
                    'icon-arrows-expand',
                    'icon-arrows-expand-diagonal1',
                    'icon-arrows-expand-horizontal1',
                    'icon-arrows-expand-vertical1',
                    'icon-arrows-fit-horizontal',
                    'icon-arrows-fit-vertical',
                    'icon-arrows-glide',
                    'icon-arrows-glide-horizontal',
                    'icon-arrows-glide-vertical',
                    'icon-arrows-hamburger1',
                    'icon-arrows-hamburger-2',
                    'icon-arrows-horizontal',
                    'icon-arrows-info',
                    'icon-arrows-keyboard-alt',
                    'icon-arrows-keyboard-cmd',
                    'icon-arrows-keyboard-delete',
                    'icon-arrows-keyboard-down',
                    'icon-arrows-keyboard-left',
                    'icon-arrows-keyboard-return',
                    'icon-arrows-keyboard-right',
                    'icon-arrows-keyboard-shift',
                    'icon-arrows-keyboard-tab',
                    'icon-arrows-keyboard-up',
                    'icon-arrows-left',
                    'icon-arrows-left-double-32',
                    'icon-arrows-minus',
                    'icon-arrows-move',
                    'icon-arrows-move2',
                    'icon-arrows-move-bottom',
                    'icon-arrows-move-left',
                    'icon-arrows-move-right',
                    'icon-arrows-move-top',
                    'icon-arrows-plus',
                    'icon-arrows-question',
                    'icon-arrows-remove',
                    'icon-arrows-right',
                    'icon-arrows-right-double',
                    'icon-arrows-rotate',
                    'icon-arrows-rotate-anti',
                    'icon-arrows-rotate-anti-dashed',
                    'icon-arrows-rotate-dashed',
                    'icon-arrows-shrink',
                    'icon-arrows-shrink-diagonal1',
                    'icon-arrows-shrink-diagonal2',
                    'icon-arrows-shrink-horizonal2',
                    'icon-arrows-shrink-horizontal1',
                    'icon-arrows-shrink-vertical1',
                    'icon-arrows-shrink-vertical2',
                    'icon-arrows-sign-down',
                    'icon-arrows-sign-left',
                    'icon-arrows-sign-right',
                    'icon-arrows-sign-up',
                    'icon-arrows-slide-down1',
                    'icon-arrows-slide-down2',
                    'icon-arrows-slide-left1',
                    'icon-arrows-slide-left2',
                    'icon-arrows-slide-right1',
                    'icon-arrows-slide-right2',
                    'icon-arrows-slide-up1',
                    'icon-arrows-slide-up2',
                    'icon-arrows-slim-down',
                    'icon-arrows-slim-down-dashed',
                    'icon-arrows-slim-left',
                    'icon-arrows-slim-left-dashed',
                    'icon-arrows-slim-right',
                    'icon-arrows-slim-right-dashed',
                    'icon-arrows-slim-up',
                    'icon-arrows-slim-up-dashed',
                    'icon-arrows-square-check',
                    'icon-arrows-square-down',
                    'icon-arrows-square-downleft',
                    'icon-arrows-square-downright',
                    'icon-arrows-square-left',
                    'icon-arrows-square-minus',
                    'icon-arrows-square-plus',
                    'icon-arrows-square-remove',
                    'icon-arrows-square-right',
                    'icon-arrows-square-up',
                    'icon-arrows-square-upleft',
                    'icon-arrows-square-upright',
                    'icon-arrows-squares',
                    'icon-arrows-stretch-diagonal1',
                    'icon-arrows-stretch-diagonal2',
                    'icon-arrows-stretch-diagonal3',
                    'icon-arrows-stretch-diagonal4',
                    'icon-arrows-stretch-horizontal1',
                    'icon-arrows-stretch-horizontal2',
                    'icon-arrows-stretch-vertical1',
                    'icon-arrows-stretch-vertical2',
                    'icon-arrows-switch-horizontal',
                    'icon-arrows-switch-vertical',
                    'icon-arrows-up',
                    'icon-arrows-up-double-33',
                    'icon-arrows-upleft',
                    'icon-arrows-upright',
                    'icon-arrows-vertical'
                ]
            },
            {
                name: 'basic',
                icons: [
                    'icon-basic-accelerator',
                    'icon-basic-alarm',
                    'icon-basic-anchor',
                    'icon-basic-anticlockwise',
                    'icon-basic-archive',
                    'icon-basic-archive-full',
                    'icon-basic-ban',
                    'icon-basic-battery-charge',
                    'icon-basic-battery-empty',
                    'icon-basic-battery-full',
                    'icon-basic-battery-half',
                    'icon-basic-bolt',
                    'icon-basic-book',
                    'icon-basic-book-pen',
                    'icon-basic-book-pencil',
                    'icon-basic-bookmark',
                    'icon-basic-calculator',
                    'icon-basic-calendar',
                    'icon-basic-cards-diamonds',
                    'icon-basic-cards-hearts',
                    'icon-basic-case',
                    'icon-basic-chronometer',
                    'icon-basic-clessidre',
                    'icon-basic-clock',
                    'icon-basic-clockwise',
                    'icon-basic-cloud',
                    'icon-basic-clubs',
                    'icon-basic-compass',
                    'icon-basic-cup',
                    'icon-basic-diamonds',
                    'icon-basic-display',
                    'icon-basic-download',
                    'icon-basic-exclamation',
                    'icon-basic-eye',
                    'icon-basic-eye-closed',
                    'icon-basic-female',
                    'icon-basic-flag1',
                    'icon-basic-flag2',
                    'icon-basic-floppydisk',
                    'icon-basic-folder',
                    'icon-basic-folder-multiple',
                    'icon-basic-gear',
                    'icon-basic-geolocalize-01',
                    'icon-basic-geolocalize-05',
                    'icon-basic-globe',
                    'icon-basic-gunsight',
                    'icon-basic-hammer',
                    'icon-basic-headset',
                    'icon-basic-heart',
                    'icon-basic-heart-broken',
                    'icon-basic-helm',
                    'icon-basic-home',
                    'icon-basic-info',
                    'icon-basic-ipod',
                    'icon-basic-joypad',
                    'icon-basic-key',
                    'icon-basic-keyboard',
                    'icon-basic-laptop',
                    'icon-basic-life-buoy',
                    'icon-basic-lightbulb',
                    'icon-basic-link',
                    'icon-basic-lock',
                    'icon-basic-lock-open',
                    'icon-basic-magic-mouse',
                    'icon-basic-magnifier',
                    'icon-basic-magnifier-minus',
                    'icon-basic-magnifier-plus',
                    'icon-basic-mail',
                    'icon-basic-mail-multiple',
                    'icon-basic-mail-open',
                    'icon-basic-mail-open-text',
                    'icon-basic-male',
                    'icon-basic-map',
                    'icon-basic-message',
                    'icon-basic-message-multiple',
                    'icon-basic-message-txt',
                    'icon-basic-mixer2',
                    'icon-basic-mouse',
                    'icon-basic-notebook',
                    'icon-basic-notebook-pen',
                    'icon-basic-notebook-pencil',
                    'icon-basic-paperplane',
                    'icon-basic-pencil-ruler',
                    'icon-basic-pencil-ruler-pen',
                    'icon-basic-photo',
                    'icon-basic-picture',
                    'icon-basic-picture-multiple',
                    'icon-basic-pin1',
                    'icon-basic-pin2',
                    'icon-basic-postcard',
                    'icon-basic-postcard-multiple',
                    'icon-basic-printer',
                    'icon-basic-question',
                    'icon-basic-rss',
                    'icon-basic-server',
                    'icon-basic-server2',
                    'icon-basic-server-cloud',
                    'icon-basic-server-download',
                    'icon-basic-server-upload',
                    'icon-basic-settings',
                    'icon-basic-share',
                    'icon-basic-sheet',
                    'icon-basic-sheet-multiple',
                    'icon-basic-sheet-pen',
                    'icon-basic-sheet-pencil',
                    'icon-basic-sheet-txt',
                    'icon-basic-signs',
                    'icon-basic-smartphone',
                    'icon-basic-spades',
                    'icon-basic-spread',
                    'icon-basic-spread-bookmark',
                    'icon-basic-spread-text',
                    'icon-basic-spread-text-bookmark',
                    'icon-basic-star',
                    'icon-basic-tablet',
                    'icon-basic-target',
                    'icon-basic-todo',
                    'icon-basic-todo-pen',
                    'icon-basic-todo-pencil',
                    'icon-basic-todo-txt',
                    'icon-basic-todolist-pen',
                    'icon-basic-todolist-pencil',
                    'icon-basic-trashcan',
                    'icon-basic-trashcan-full',
                    'icon-basic-trashcan-refresh',
                    'icon-basic-trashcan-remove',
                    'icon-basic-upload',
                    'icon-basic-usb',
                    'icon-basic-video',
                    'icon-basic-watch',
                    'icon-basic-webpage',
                    'icon-basic-webpage-img-txt',
                    'icon-basic-webpage-multiple',
                    'icon-basic-webpage-txt',
                    'icon-basic-world'
                ]
            },
            {
                name: 'basic-elaboration',
                icons: [
                    'icon-basic-elaboration-bookmark-checck',
                    'icon-basic-elaboration-bookmark-minus',
                    'icon-basic-elaboration-bookmark-plus',
                    'icon-basic-elaboration-bookmark-remove',
                    'icon-basic-elaboration-briefcase-check',
                    'icon-basic-elaboration-briefcase-download',
                    'icon-basic-elaboration-briefcase-flagged',
                    'icon-basic-elaboration-briefcase-minus',
                    'icon-basic-elaboration-briefcase-plus',
                    'icon-basic-elaboration-briefcase-refresh',
                    'icon-basic-elaboration-briefcase-remove',
                    'icon-basic-elaboration-briefcase-search',
                    'icon-basic-elaboration-briefcase-star',
                    'icon-basic-elaboration-briefcase-upload',
                    'icon-basic-elaboration-browser-check',
                    'icon-basic-elaboration-browser-download',
                    'icon-basic-elaboration-browser-minus',
                    'icon-basic-elaboration-browser-plus',
                    'icon-basic-elaboration-browser-refresh',
                    'icon-basic-elaboration-browser-remove',
                    'icon-basic-elaboration-browser-search',
                    'icon-basic-elaboration-browser-star',
                    'icon-basic-elaboration-browser-upload',
                    'icon-basic-elaboration-calendar-check',
                    'icon-basic-elaboration-calendar-cloud',
                    'icon-basic-elaboration-calendar-download',
                    'icon-basic-elaboration-calendar-empty',
                    'icon-basic-elaboration-calendar-flagged',
                    'icon-basic-elaboration-calendar-heart',
                    'icon-basic-elaboration-calendar-minus',
                    'icon-basic-elaboration-calendar-next',
                    'icon-basic-elaboration-calendar-noaccess',
                    'icon-basic-elaboration-calendar-pencil',
                    'icon-basic-elaboration-calendar-plus',
                    'icon-basic-elaboration-calendar-previous',
                    'icon-basic-elaboration-calendar-refresh',
                    'icon-basic-elaboration-calendar-remove',
                    'icon-basic-elaboration-calendar-search',
                    'icon-basic-elaboration-calendar-star',
                    'icon-basic-elaboration-calendar-upload',
                    'icon-basic-elaboration-cloud-check',
                    'icon-basic-elaboration-cloud-download',
                    'icon-basic-elaboration-cloud-minus',
                    'icon-basic-elaboration-cloud-noaccess',
                    'icon-basic-elaboration-cloud-plus',
                    'icon-basic-elaboration-cloud-refresh',
                    'icon-basic-elaboration-cloud-remove',
                    'icon-basic-elaboration-cloud-search',
                    'icon-basic-elaboration-cloud-upload',
                    'icon-basic-elaboration-document-check',
                    'icon-basic-elaboration-document-cloud',
                    'icon-basic-elaboration-document-download',
                    'icon-basic-elaboration-document-flagged',
                    'icon-basic-elaboration-document-graph',
                    'icon-basic-elaboration-document-heart',
                    'icon-basic-elaboration-document-minus',
                    'icon-basic-elaboration-document-next',
                    'icon-basic-elaboration-document-noaccess',
                    'icon-basic-elaboration-document-note',
                    'icon-basic-elaboration-document-pencil',
                    'icon-basic-elaboration-document-picture',
                    'icon-basic-elaboration-document-plus',
                    'icon-basic-elaboration-document-previous',
                    'icon-basic-elaboration-document-refresh',
                    'icon-basic-elaboration-document-remove',
                    'icon-basic-elaboration-document-search',
                    'icon-basic-elaboration-document-star',
                    'icon-basic-elaboration-document-upload',
                    'icon-basic-elaboration-folder-check',
                    'icon-basic-elaboration-folder-cloud',
                    'icon-basic-elaboration-folder-document',
                    'icon-basic-elaboration-folder-download',
                    'icon-basic-elaboration-folder-flagged',
                    'icon-basic-elaboration-folder-graph',
                    'icon-basic-elaboration-folder-heart',
                    'icon-basic-elaboration-folder-minus',
                    'icon-basic-elaboration-folder-next',
                    'icon-basic-elaboration-folder-noaccess',
                    'icon-basic-elaboration-folder-note',
                    'icon-basic-elaboration-folder-pencil',
                    'icon-basic-elaboration-folder-picture',
                    'icon-basic-elaboration-folder-plus',
                    'icon-basic-elaboration-folder-previous',
                    'icon-basic-elaboration-folder-refresh',
                    'icon-basic-elaboration-folder-remove',
                    'icon-basic-elaboration-folder-search',
                    'icon-basic-elaboration-folder-star',
                    'icon-basic-elaboration-folder-upload',
                    'icon-basic-elaboration-mail-check',
                    'icon-basic-elaboration-mail-cloud',
                    'icon-basic-elaboration-mail-document',
                    'icon-basic-elaboration-mail-download',
                    'icon-basic-elaboration-mail-flagged',
                    'icon-basic-elaboration-mail-heart',
                    'icon-basic-elaboration-mail-next',
                    'icon-basic-elaboration-mail-noaccess',
                    'icon-basic-elaboration-mail-note',
                    'icon-basic-elaboration-mail-pencil',
                    'icon-basic-elaboration-mail-picture',
                    'icon-basic-elaboration-mail-previous',
                    'icon-basic-elaboration-mail-refresh',
                    'icon-basic-elaboration-mail-remove',
                    'icon-basic-elaboration-mail-search',
                    'icon-basic-elaboration-mail-star',
                    'icon-basic-elaboration-mail-upload',
                    'icon-basic-elaboration-message-check',
                    'icon-basic-elaboration-message-dots',
                    'icon-basic-elaboration-message-happy',
                    'icon-basic-elaboration-message-heart',
                    'icon-basic-elaboration-message-minus',
                    'icon-basic-elaboration-message-note',
                    'icon-basic-elaboration-message-plus',
                    'icon-basic-elaboration-message-refresh',
                    'icon-basic-elaboration-message-remove',
                    'icon-basic-elaboration-message-sad',
                    'icon-basic-elaboration-smartphone-cloud',
                    'icon-basic-elaboration-smartphone-heart',
                    'icon-basic-elaboration-smartphone-noaccess',
                    'icon-basic-elaboration-smartphone-note',
                    'icon-basic-elaboration-smartphone-pencil',
                    'icon-basic-elaboration-smartphone-picture',
                    'icon-basic-elaboration-smartphone-refresh',
                    'icon-basic-elaboration-smartphone-search',
                    'icon-basic-elaboration-tablet-cloud',
                    'icon-basic-elaboration-tablet-heart',
                    'icon-basic-elaboration-tablet-noaccess',
                    'icon-basic-elaboration-tablet-note',
                    'icon-basic-elaboration-tablet-pencil',
                    'icon-basic-elaboration-tablet-picture',
                    'icon-basic-elaboration-tablet-refresh',
                    'icon-basic-elaboration-tablet-search',
                    'icon-basic-elaboration-todolist-2',
                    'icon-basic-elaboration-todolist-check',
                    'icon-basic-elaboration-todolist-cloud',
                    'icon-basic-elaboration-todolist-download',
                    'icon-basic-elaboration-todolist-flagged',
                    'icon-basic-elaboration-todolist-minus',
                    'icon-basic-elaboration-todolist-noaccess',
                    'icon-basic-elaboration-todolist-pencil',
                    'icon-basic-elaboration-todolist-plus',
                    'icon-basic-elaboration-todolist-refresh',
                    'icon-basic-elaboration-todolist-remove',
                    'icon-basic-elaboration-todolist-search',
                    'icon-basic-elaboration-todolist-star',
                    'icon-basic-elaboration-todolist-upload'
                ]
            },
            {
                name: 'ecommerce',
                icons: [
                    'icon-ecommerce-bag',
                    'icon-ecommerce-bag-check',
                    'icon-ecommerce-bag-cloud',
                    'icon-ecommerce-bag-download',
                    'icon-ecommerce-bag-minus',
                    'icon-ecommerce-bag-plus',
                    'icon-ecommerce-bag-refresh',
                    'icon-ecommerce-bag-remove',
                    'icon-ecommerce-bag-search',
                    'icon-ecommerce-bag-upload',
                    'icon-ecommerce-banknote',
                    'icon-ecommerce-banknotes',
                    'icon-ecommerce-basket',
                    'icon-ecommerce-basket-check',
                    'icon-ecommerce-basket-cloud',
                    'icon-ecommerce-basket-download',
                    'icon-ecommerce-basket-minus',
                    'icon-ecommerce-basket-plus',
                    'icon-ecommerce-basket-refresh',
                    'icon-ecommerce-basket-remove',
                    'icon-ecommerce-basket-search',
                    'icon-ecommerce-basket-upload',
                    'icon-ecommerce-bath',
                    'icon-ecommerce-cart',
                    'icon-ecommerce-cart-check',
                    'icon-ecommerce-cart-cloud',
                    'icon-ecommerce-cart-content',
                    'icon-ecommerce-cart-download',
                    'icon-ecommerce-cart-minus',
                    'icon-ecommerce-cart-plus',
                    'icon-ecommerce-cart-refresh',
                    'icon-ecommerce-cart-remove',
                    'icon-ecommerce-cart-search',
                    'icon-ecommerce-cart-upload',
                    'icon-ecommerce-cent',
                    'icon-ecommerce-colon',
                    'icon-ecommerce-creditcard',
                    'icon-ecommerce-diamond',
                    'icon-ecommerce-dollar',
                    'icon-ecommerce-euro',
                    'icon-ecommerce-franc',
                    'icon-ecommerce-gift',
                    'icon-ecommerce-graph1',
                    'icon-ecommerce-graph2',
                    'icon-ecommerce-graph3',
                    'icon-ecommerce-graph-decrease',
                    'icon-ecommerce-graph-increase',
                    'icon-ecommerce-guarani',
                    'icon-ecommerce-kips',
                    'icon-ecommerce-lira',
                    'icon-ecommerce-megaphone',
                    'icon-ecommerce-money',
                    'icon-ecommerce-naira',
                    'icon-ecommerce-pesos',
                    'icon-ecommerce-pound',
                    'icon-ecommerce-receipt',
                    'icon-ecommerce-receipt-bath',
                    'icon-ecommerce-receipt-cent',
                    'icon-ecommerce-receipt-dollar',
                    'icon-ecommerce-receipt-euro',
                    'icon-ecommerce-receipt-franc',
                    'icon-ecommerce-receipt-guarani',
                    'icon-ecommerce-receipt-kips',
                    'icon-ecommerce-receipt-lira',
                    'icon-ecommerce-receipt-naira',
                    'icon-ecommerce-receipt-pesos',
                    'icon-ecommerce-receipt-pound',
                    'icon-ecommerce-receipt-rublo',
                    'icon-ecommerce-receipt-rupee',
                    'icon-ecommerce-receipt-tugrik',
                    'icon-ecommerce-receipt-won',
                    'icon-ecommerce-receipt-yen',
                    'icon-ecommerce-receipt-yen2',
                    'icon-ecommerce-recept-colon',
                    'icon-ecommerce-rublo',
                    'icon-ecommerce-rupee',
                    'icon-ecommerce-safe',
                    'icon-ecommerce-sale',
                    'icon-ecommerce-sales',
                    'icon-ecommerce-ticket',
                    'icon-ecommerce-tugriks',
                    'icon-ecommerce-wallet',
                    'icon-ecommerce-won',
                    'icon-ecommerce-yen',
                    'icon-ecommerce-yen2'
                ]
            },
            {
                name: 'music',
                icons: [
                    'icon-music-beginning-button',
                    'icon-music-bell',
                    'icon-music-cd',
                    'icon-music-diapason',
                    'icon-music-eject-button',
                    'icon-music-end-button',
                    'icon-music-fastforward-button',
                    'icon-music-headphones',
                    'icon-music-ipod',
                    'icon-music-loudspeaker',
                    'icon-music-microphone',
                    'icon-music-microphone-old',
                    'icon-music-mixer',
                    'icon-music-mute',
                    'icon-music-note-multiple',
                    'icon-music-note-single',
                    'icon-music-pause-button',
                    'icon-music-play-button',
                    'icon-music-playlist',
                    'icon-music-radio-ghettoblaster',
                    'icon-music-radio-portable',
                    'icon-music-record',
                    'icon-music-recordplayer',
                    'icon-music-repeat-button',
                    'icon-music-rewind-button',
                    'icon-music-shuffle-button',
                    'icon-music-stop-button',
                    'icon-music-tape',
                    'icon-music-volume-down',
                    'icon-music-volume-up'
                ]
            },
            {
                name: 'software',
                icons: [
                    'icon-software-add-vectorpoint',
                    'icon-software-box-oval',
                    'icon-software-box-polygon',
                    'icon-software-box-rectangle',
                    'icon-software-box-roundedrectangle',
                    'icon-software-character',
                    'icon-software-crop',
                    'icon-software-eyedropper',
                    'icon-software-font-allcaps',
                    'icon-software-font-baseline-shift',
                    'icon-software-font-horizontal-scale',
                    'icon-software-font-kerning',
                    'icon-software-font-leading',
                    'icon-software-font-size',
                    'icon-software-font-smallcapital',
                    'icon-software-font-smallcaps',
                    'icon-software-font-strikethrough',
                    'icon-software-font-tracking',
                    'icon-software-font-underline',
                    'icon-software-font-vertical-scale',
                    'icon-software-horizontal-align-center',
                    'icon-software-horizontal-align-left',
                    'icon-software-horizontal-align-right',
                    'icon-software-horizontal-distribute-center',
                    'icon-software-horizontal-distribute-left',
                    'icon-software-horizontal-distribute-right',
                    'icon-software-indent-firstline',
                    'icon-software-indent-left',
                    'icon-software-indent-right',
                    'icon-software-lasso',
                    'icon-software-layers1',
                    'icon-software-layers2',
                    'icon-software-layout',
                    'icon-software-layout-2columns',
                    'icon-software-layout-3columns',
                    'icon-software-layout-4boxes',
                    'icon-software-layout-4columns',
                    'icon-software-layout-4lines',
                    'icon-software-layout-8boxes',
                    'icon-software-layout-header',
                    'icon-software-layout-header-2columns',
                    'icon-software-layout-header-3columns',
                    'icon-software-layout-header-4boxes',
                    'icon-software-layout-header-4columns',
                    'icon-software-layout-header-complex',
                    'icon-software-layout-header-complex2',
                    'icon-software-layout-header-complex3',
                    'icon-software-layout-header-complex4',
                    'icon-software-layout-header-sideleft',
                    'icon-software-layout-header-sideright',
                    'icon-software-layout-sidebar-left',
                    'icon-software-layout-sidebar-right',
                    'icon-software-magnete',
                    'icon-software-pages',
                    'icon-software-paintbrush',
                    'icon-software-paintbucket',
                    'icon-software-paintroller',
                    'icon-software-paragraph',
                    'icon-software-paragraph-align-left',
                    'icon-software-paragraph-align-right',
                    'icon-software-paragraph-center',
                    'icon-software-paragraph-justify-all',
                    'icon-software-paragraph-justify-center',
                    'icon-software-paragraph-justify-left',
                    'icon-software-paragraph-justify-right',
                    'icon-software-paragraph-space-after',
                    'icon-software-paragraph-space-before',
                    'icon-software-pathfinder-exclude',
                    'icon-software-pathfinder-intersect',
                    'icon-software-pathfinder-subtract',
                    'icon-software-pathfinder-unite',
                    'icon-software-pen',
                    'icon-software-pen-add',
                    'icon-software-pen-remove',
                    'icon-software-pencil',
                    'icon-software-polygonallasso',
                    'icon-software-reflect-horizontal',
                    'icon-software-reflect-vertical',
                    'icon-software-remove-vectorpoint',
                    'icon-software-scale-expand',
                    'icon-software-scale-reduce',
                    'icon-software-selection-oval',
                    'icon-software-selection-polygon',
                    'icon-software-selection-rectangle',
                    'icon-software-selection-roundedrectangle',
                    'icon-software-shape-oval',
                    'icon-software-shape-polygon',
                    'icon-software-shape-rectangle',
                    'icon-software-shape-roundedrectangle',
                    'icon-software-slice',
                    'icon-software-transform-bezier',
                    'icon-software-vector-box',
                    'icon-software-vector-composite',
                    'icon-software-vector-line',
                    'icon-software-vertical-align-bottom',
                    'icon-software-vertical-align-center',
                    'icon-software-vertical-align-top',
                    'icon-software-vertical-distribute-bottom',
                    'icon-software-vertical-distribute-center',
                    'icon-software-vertical-distribute-top'
                ]
            },
            {
                name: 'weather',
                icons: [
                    'icon-weather-aquarius',
                    'icon-weather-aries',
                    'icon-weather-cancer',
                    'icon-weather-capricorn',
                    'icon-weather-cloud',
                    'icon-weather-cloud-drop',
                    'icon-weather-cloud-lightning',
                    'icon-weather-cloud-snowflake',
                    'icon-weather-downpour-fullmoon',
                    'icon-weather-downpour-halfmoon',
                    'icon-weather-downpour-sun',
                    'icon-weather-drop',
                    'icon-weather-first-quarter',
                    'icon-weather-fog',
                    'icon-weather-fog-fullmoon',
                    'icon-weather-fog-halfmoon',
                    'icon-weather-fog-sun',
                    'icon-weather-fullmoon',
                    'icon-weather-gemini',
                    'icon-weather-hail',
                    'icon-weather-hail-fullmoon',
                    'icon-weather-hail-halfmoon',
                    'icon-weather-hail-sun',
                    'icon-weather-last-quarter',
                    'icon-weather-leo',
                    'icon-weather-libra',
                    'icon-weather-lightning',
                    'icon-weather-mistyrain',
                    'icon-weather-mistyrain-fullmoon',
                    'icon-weather-mistyrain-halfmoon',
                    'icon-weather-mistyrain-sun',
                    'icon-weather-moon',
                    'icon-weather-moondown-full',
                    'icon-weather-moondown-half',
                    'icon-weather-moonset-full',
                    'icon-weather-moonset-half',
                    'icon-weather-move2',
                    'icon-weather-newmoon',
                    'icon-weather-pisces',
                    'icon-weather-rain',
                    'icon-weather-rain-fullmoon',
                    'icon-weather-rain-halfmoon',
                    'icon-weather-rain-sun',
                    'icon-weather-sagittarius',
                    'icon-weather-scorpio',
                    'icon-weather-snow',
                    'icon-weather-snow-fullmoon',
                    'icon-weather-snow-halfmoon',
                    'icon-weather-snow-sun',
                    'icon-weather-snowflake',
                    'icon-weather-star',
                    'icon-weather-storm-11',
                    'icon-weather-storm-32',
                    'icon-weather-storm-fullmoon',
                    'icon-weather-storm-halfmoon',
                    'icon-weather-storm-sun',
                    'icon-weather-sun',
                    'icon-weather-sundown',
                    'icon-weather-sunset',
                    'icon-weather-taurus',
                    'icon-weather-tempest',
                    'icon-weather-tempest-fullmoon',
                    'icon-weather-tempest-halfmoon',
                    'icon-weather-tempest-sun',
                    'icon-weather-variable-fullmoon',
                    'icon-weather-variable-halfmoon',
                    'icon-weather-variable-sun',
                    'icon-weather-virgo',
                    'icon-weather-waning-cresent',
                    'icon-weather-waning-gibbous',
                    'icon-weather-waxing-cresent',
                    'icon-weather-waxing-gibbous',
                    'icon-weather-wind',
                    'icon-weather-wind-e',
                    'icon-weather-wind-fullmoon',
                    'icon-weather-wind-halfmoon',
                    'icon-weather-wind-n',
                    'icon-weather-wind-ne',
                    'icon-weather-wind-nw',
                    'icon-weather-wind-s',
                    'icon-weather-wind-se',
                    'icon-weather-wind-sun',
                    'icon-weather-wind-sw',
                    'icon-weather-wind-w',
                    'icon-weather-windgust'
                ]
            }];
        this.temp = linea;
        this.collection = linea;
    }
    LineaComponent.prototype.updateFilter = function (event) {
        var query = event.target.value;
        var updated = [];
        var filtered = this.temp.filter(function (el) {
            el.ico = el.icons.filter(function (elm) {
                return elm.toLowerCase().indexOf(query) !== -1 || !query;
            });
            updated.push({
                name: el.name,
                icons: el.ico
            });
        });
        this.collection = updated;
    };
    LineaComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-linea',
            template: __webpack_require__("./src/app/icons/linea/linea.component.html"),
            styles: [__webpack_require__("./src/app/icons/linea/linea.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], LineaComponent);
    return LineaComponent;
}());

//# sourceMappingURL=linea.component.js.map

/***/ }),

/***/ "./src/app/icons/sli/sli.component.html":
/***/ (function(module, exports) {

module.exports = "<input type=\"email\" class=\"form-control form-control-lg mb-3\" placeholder=\"Type to filter icons\" required (keyup)='updateFilter($event)'>\r\n\r\n<div class=\"icon-group\">\r\n  <div class=\"row\">\r\n    <div class=\"fa-hover col-md-3 col-sm-4\" *ngFor=\"let icon of icons\">\r\n      <a href=\"javascript:;\" class=\"text-color\" target=\"_blank\">\r\n        <i class=\"icon-{{icon}}\"></i>\r\n        <span>{{icon}}</span>\r\n      </a>\r\n    </div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./src/app/icons/sli/sli.component.scss":
/***/ (function(module, exports) {

module.exports = "@font-face {\r\n  font-family: 'simple-line-icons';\r\n  src: url('Simple-Line-Icons.f33df365d6d0255b586f.eot?v=2.4.0');\r\n  src: url('Simple-Line-Icons.f33df365d6d0255b586f.eot?v=2.4.0#iefix') format('embedded-opentype'), url('Simple-Line-Icons.0cb0b9c589c0624c9c78.woff2?v=2.4.0') format('woff2'), url('Simple-Line-Icons.d2285965fe34b0546504.ttf?v=2.4.0') format('truetype'), url('Simple-Line-Icons.78f07e2c2a535c26ef21.woff?v=2.4.0') format('woff'), url('Simple-Line-Icons.73a932562a1e31470346.svg?v=2.4.0#simple-line-icons') format('svg');\r\n  font-weight: normal;\r\n  font-style: normal;\r\n}\r\n/*\r\n Use the following CSS code if you want to have a class per icon.\r\n Instead of a list of all class selectors, you can use the generic [class*=\"icon-\"] selector, but it's slower:\r\n*/\r\n.icon-user,\r\n.icon-people,\r\n.icon-user-female,\r\n.icon-user-follow,\r\n.icon-user-following,\r\n.icon-user-unfollow,\r\n.icon-login,\r\n.icon-logout,\r\n.icon-emotsmile,\r\n.icon-phone,\r\n.icon-call-end,\r\n.icon-call-in,\r\n.icon-call-out,\r\n.icon-map,\r\n.icon-location-pin,\r\n.icon-direction,\r\n.icon-directions,\r\n.icon-compass,\r\n.icon-layers,\r\n.icon-menu,\r\n.icon-list,\r\n.icon-options-vertical,\r\n.icon-options,\r\n.icon-arrow-down,\r\n.icon-arrow-left,\r\n.icon-arrow-right,\r\n.icon-arrow-up,\r\n.icon-arrow-up-circle,\r\n.icon-arrow-left-circle,\r\n.icon-arrow-right-circle,\r\n.icon-arrow-down-circle,\r\n.icon-check,\r\n.icon-clock,\r\n.icon-plus,\r\n.icon-minus,\r\n.icon-close,\r\n.icon-event,\r\n.icon-exclamation,\r\n.icon-organization,\r\n.icon-trophy,\r\n.icon-screen-smartphone,\r\n.icon-screen-desktop,\r\n.icon-plane,\r\n.icon-notebook,\r\n.icon-mustache,\r\n.icon-mouse,\r\n.icon-magnet,\r\n.icon-energy,\r\n.icon-disc,\r\n.icon-cursor,\r\n.icon-cursor-move,\r\n.icon-crop,\r\n.icon-chemistry,\r\n.icon-speedometer,\r\n.icon-shield,\r\n.icon-screen-tablet,\r\n.icon-magic-wand,\r\n.icon-hourglass,\r\n.icon-graduation,\r\n.icon-ghost,\r\n.icon-game-controller,\r\n.icon-fire,\r\n.icon-eyeglass,\r\n.icon-envelope-open,\r\n.icon-envelope-letter,\r\n.icon-bell,\r\n.icon-badge,\r\n.icon-anchor,\r\n.icon-wallet,\r\n.icon-vector,\r\n.icon-speech,\r\n.icon-puzzle,\r\n.icon-printer,\r\n.icon-present,\r\n.icon-playlist,\r\n.icon-pin,\r\n.icon-picture,\r\n.icon-handbag,\r\n.icon-globe-alt,\r\n.icon-globe,\r\n.icon-folder-alt,\r\n.icon-folder,\r\n.icon-film,\r\n.icon-feed,\r\n.icon-drop,\r\n.icon-drawer,\r\n.icon-docs,\r\n.icon-doc,\r\n.icon-diamond,\r\n.icon-cup,\r\n.icon-calculator,\r\n.icon-bubbles,\r\n.icon-briefcase,\r\n.icon-book-open,\r\n.icon-basket-loaded,\r\n.icon-basket,\r\n.icon-bag,\r\n.icon-action-undo,\r\n.icon-action-redo,\r\n.icon-wrench,\r\n.icon-umbrella,\r\n.icon-trash,\r\n.icon-tag,\r\n.icon-support,\r\n.icon-frame,\r\n.icon-size-fullscreen,\r\n.icon-size-actual,\r\n.icon-shuffle,\r\n.icon-share-alt,\r\n.icon-share,\r\n.icon-rocket,\r\n.icon-question,\r\n.icon-pie-chart,\r\n.icon-pencil,\r\n.icon-note,\r\n.icon-loop,\r\n.icon-home,\r\n.icon-grid,\r\n.icon-graph,\r\n.icon-microphone,\r\n.icon-music-tone-alt,\r\n.icon-music-tone,\r\n.icon-earphones-alt,\r\n.icon-earphones,\r\n.icon-equalizer,\r\n.icon-like,\r\n.icon-dislike,\r\n.icon-control-start,\r\n.icon-control-rewind,\r\n.icon-control-play,\r\n.icon-control-pause,\r\n.icon-control-forward,\r\n.icon-control-end,\r\n.icon-volume-1,\r\n.icon-volume-2,\r\n.icon-volume-off,\r\n.icon-calendar,\r\n.icon-bulb,\r\n.icon-chart,\r\n.icon-ban,\r\n.icon-bubble,\r\n.icon-camrecorder,\r\n.icon-camera,\r\n.icon-cloud-download,\r\n.icon-cloud-upload,\r\n.icon-envelope,\r\n.icon-eye,\r\n.icon-flag,\r\n.icon-heart,\r\n.icon-info,\r\n.icon-key,\r\n.icon-link,\r\n.icon-lock,\r\n.icon-lock-open,\r\n.icon-magnifier,\r\n.icon-magnifier-add,\r\n.icon-magnifier-remove,\r\n.icon-paper-clip,\r\n.icon-paper-plane,\r\n.icon-power,\r\n.icon-refresh,\r\n.icon-reload,\r\n.icon-settings,\r\n.icon-star,\r\n.icon-symbol-female,\r\n.icon-symbol-male,\r\n.icon-target,\r\n.icon-credit-card,\r\n.icon-paypal,\r\n.icon-social-tumblr,\r\n.icon-social-twitter,\r\n.icon-social-facebook,\r\n.icon-social-instagram,\r\n.icon-social-linkedin,\r\n.icon-social-pinterest,\r\n.icon-social-github,\r\n.icon-social-google,\r\n.icon-social-reddit,\r\n.icon-social-skype,\r\n.icon-social-dribbble,\r\n.icon-social-behance,\r\n.icon-social-foursqare,\r\n.icon-social-soundcloud,\r\n.icon-social-spotify,\r\n.icon-social-stumbleupon,\r\n.icon-social-youtube,\r\n.icon-social-dropbox,\r\n.icon-social-vkontakte,\r\n.icon-social-steam {\r\n  font-family: 'simple-line-icons';\r\n  speak: none;\r\n  font-style: normal;\r\n  font-weight: normal;\r\n  font-variant: normal;\r\n  text-transform: none;\r\n  line-height: 1;\r\n  /* Better Font Rendering =========== */\r\n  -webkit-font-smoothing: antialiased;\r\n  -moz-osx-font-smoothing: grayscale;\r\n}\r\n.icon-user:before {\r\n  content: \"\\e005\";\r\n}\r\n.icon-people:before {\r\n  content: \"\\e001\";\r\n}\r\n.icon-user-female:before {\r\n  content: \"\\e000\";\r\n}\r\n.icon-user-follow:before {\r\n  content: \"\\e002\";\r\n}\r\n.icon-user-following:before {\r\n  content: \"\\e003\";\r\n}\r\n.icon-user-unfollow:before {\r\n  content: \"\\e004\";\r\n}\r\n.icon-login:before {\r\n  content: \"\\e066\";\r\n}\r\n.icon-logout:before {\r\n  content: \"\\e065\";\r\n}\r\n.icon-emotsmile:before {\r\n  content: \"\\e021\";\r\n}\r\n.icon-phone:before {\r\n  content: \"\\e600\";\r\n}\r\n.icon-call-end:before {\r\n  content: \"\\e048\";\r\n}\r\n.icon-call-in:before {\r\n  content: \"\\e047\";\r\n}\r\n.icon-call-out:before {\r\n  content: \"\\e046\";\r\n}\r\n.icon-map:before {\r\n  content: \"\\e033\";\r\n}\r\n.icon-location-pin:before {\r\n  content: \"\\e096\";\r\n}\r\n.icon-direction:before {\r\n  content: \"\\e042\";\r\n}\r\n.icon-directions:before {\r\n  content: \"\\e041\";\r\n}\r\n.icon-compass:before {\r\n  content: \"\\e045\";\r\n}\r\n.icon-layers:before {\r\n  content: \"\\e034\";\r\n}\r\n.icon-menu:before {\r\n  content: \"\\e601\";\r\n}\r\n.icon-list:before {\r\n  content: \"\\e067\";\r\n}\r\n.icon-options-vertical:before {\r\n  content: \"\\e602\";\r\n}\r\n.icon-options:before {\r\n  content: \"\\e603\";\r\n}\r\n.icon-arrow-down:before {\r\n  content: \"\\e604\";\r\n}\r\n.icon-arrow-left:before {\r\n  content: \"\\e605\";\r\n}\r\n.icon-arrow-right:before {\r\n  content: \"\\e606\";\r\n}\r\n.icon-arrow-up:before {\r\n  content: \"\\e607\";\r\n}\r\n.icon-arrow-up-circle:before {\r\n  content: \"\\e078\";\r\n}\r\n.icon-arrow-left-circle:before {\r\n  content: \"\\e07a\";\r\n}\r\n.icon-arrow-right-circle:before {\r\n  content: \"\\e079\";\r\n}\r\n.icon-arrow-down-circle:before {\r\n  content: \"\\e07b\";\r\n}\r\n.icon-check:before {\r\n  content: \"\\e080\";\r\n}\r\n.icon-clock:before {\r\n  content: \"\\e081\";\r\n}\r\n.icon-plus:before {\r\n  content: \"\\e095\";\r\n}\r\n.icon-minus:before {\r\n  content: \"\\e615\";\r\n}\r\n.icon-close:before {\r\n  content: \"\\e082\";\r\n}\r\n.icon-event:before {\r\n  content: \"\\e619\";\r\n}\r\n.icon-exclamation:before {\r\n  content: \"\\e617\";\r\n}\r\n.icon-organization:before {\r\n  content: \"\\e616\";\r\n}\r\n.icon-trophy:before {\r\n  content: \"\\e006\";\r\n}\r\n.icon-screen-smartphone:before {\r\n  content: \"\\e010\";\r\n}\r\n.icon-screen-desktop:before {\r\n  content: \"\\e011\";\r\n}\r\n.icon-plane:before {\r\n  content: \"\\e012\";\r\n}\r\n.icon-notebook:before {\r\n  content: \"\\e013\";\r\n}\r\n.icon-mustache:before {\r\n  content: \"\\e014\";\r\n}\r\n.icon-mouse:before {\r\n  content: \"\\e015\";\r\n}\r\n.icon-magnet:before {\r\n  content: \"\\e016\";\r\n}\r\n.icon-energy:before {\r\n  content: \"\\e020\";\r\n}\r\n.icon-disc:before {\r\n  content: \"\\e022\";\r\n}\r\n.icon-cursor:before {\r\n  content: \"\\e06e\";\r\n}\r\n.icon-cursor-move:before {\r\n  content: \"\\e023\";\r\n}\r\n.icon-crop:before {\r\n  content: \"\\e024\";\r\n}\r\n.icon-chemistry:before {\r\n  content: \"\\e026\";\r\n}\r\n.icon-speedometer:before {\r\n  content: \"\\e007\";\r\n}\r\n.icon-shield:before {\r\n  content: \"\\e00e\";\r\n}\r\n.icon-screen-tablet:before {\r\n  content: \"\\e00f\";\r\n}\r\n.icon-magic-wand:before {\r\n  content: \"\\e017\";\r\n}\r\n.icon-hourglass:before {\r\n  content: \"\\e018\";\r\n}\r\n.icon-graduation:before {\r\n  content: \"\\e019\";\r\n}\r\n.icon-ghost:before {\r\n  content: \"\\e01a\";\r\n}\r\n.icon-game-controller:before {\r\n  content: \"\\e01b\";\r\n}\r\n.icon-fire:before {\r\n  content: \"\\e01c\";\r\n}\r\n.icon-eyeglass:before {\r\n  content: \"\\e01d\";\r\n}\r\n.icon-envelope-open:before {\r\n  content: \"\\e01e\";\r\n}\r\n.icon-envelope-letter:before {\r\n  content: \"\\e01f\";\r\n}\r\n.icon-bell:before {\r\n  content: \"\\e027\";\r\n}\r\n.icon-badge:before {\r\n  content: \"\\e028\";\r\n}\r\n.icon-anchor:before {\r\n  content: \"\\e029\";\r\n}\r\n.icon-wallet:before {\r\n  content: \"\\e02a\";\r\n}\r\n.icon-vector:before {\r\n  content: \"\\e02b\";\r\n}\r\n.icon-speech:before {\r\n  content: \"\\e02c\";\r\n}\r\n.icon-puzzle:before {\r\n  content: \"\\e02d\";\r\n}\r\n.icon-printer:before {\r\n  content: \"\\e02e\";\r\n}\r\n.icon-present:before {\r\n  content: \"\\e02f\";\r\n}\r\n.icon-playlist:before {\r\n  content: \"\\e030\";\r\n}\r\n.icon-pin:before {\r\n  content: \"\\e031\";\r\n}\r\n.icon-picture:before {\r\n  content: \"\\e032\";\r\n}\r\n.icon-handbag:before {\r\n  content: \"\\e035\";\r\n}\r\n.icon-globe-alt:before {\r\n  content: \"\\e036\";\r\n}\r\n.icon-globe:before {\r\n  content: \"\\e037\";\r\n}\r\n.icon-folder-alt:before {\r\n  content: \"\\e039\";\r\n}\r\n.icon-folder:before {\r\n  content: \"\\e089\";\r\n}\r\n.icon-film:before {\r\n  content: \"\\e03a\";\r\n}\r\n.icon-feed:before {\r\n  content: \"\\e03b\";\r\n}\r\n.icon-drop:before {\r\n  content: \"\\e03e\";\r\n}\r\n.icon-drawer:before {\r\n  content: \"\\e03f\";\r\n}\r\n.icon-docs:before {\r\n  content: \"\\e040\";\r\n}\r\n.icon-doc:before {\r\n  content: \"\\e085\";\r\n}\r\n.icon-diamond:before {\r\n  content: \"\\e043\";\r\n}\r\n.icon-cup:before {\r\n  content: \"\\e044\";\r\n}\r\n.icon-calculator:before {\r\n  content: \"\\e049\";\r\n}\r\n.icon-bubbles:before {\r\n  content: \"\\e04a\";\r\n}\r\n.icon-briefcase:before {\r\n  content: \"\\e04b\";\r\n}\r\n.icon-book-open:before {\r\n  content: \"\\e04c\";\r\n}\r\n.icon-basket-loaded:before {\r\n  content: \"\\e04d\";\r\n}\r\n.icon-basket:before {\r\n  content: \"\\e04e\";\r\n}\r\n.icon-bag:before {\r\n  content: \"\\e04f\";\r\n}\r\n.icon-action-undo:before {\r\n  content: \"\\e050\";\r\n}\r\n.icon-action-redo:before {\r\n  content: \"\\e051\";\r\n}\r\n.icon-wrench:before {\r\n  content: \"\\e052\";\r\n}\r\n.icon-umbrella:before {\r\n  content: \"\\e053\";\r\n}\r\n.icon-trash:before {\r\n  content: \"\\e054\";\r\n}\r\n.icon-tag:before {\r\n  content: \"\\e055\";\r\n}\r\n.icon-support:before {\r\n  content: \"\\e056\";\r\n}\r\n.icon-frame:before {\r\n  content: \"\\e038\";\r\n}\r\n.icon-size-fullscreen:before {\r\n  content: \"\\e057\";\r\n}\r\n.icon-size-actual:before {\r\n  content: \"\\e058\";\r\n}\r\n.icon-shuffle:before {\r\n  content: \"\\e059\";\r\n}\r\n.icon-share-alt:before {\r\n  content: \"\\e05a\";\r\n}\r\n.icon-share:before {\r\n  content: \"\\e05b\";\r\n}\r\n.icon-rocket:before {\r\n  content: \"\\e05c\";\r\n}\r\n.icon-question:before {\r\n  content: \"\\e05d\";\r\n}\r\n.icon-pie-chart:before {\r\n  content: \"\\e05e\";\r\n}\r\n.icon-pencil:before {\r\n  content: \"\\e05f\";\r\n}\r\n.icon-note:before {\r\n  content: \"\\e060\";\r\n}\r\n.icon-loop:before {\r\n  content: \"\\e064\";\r\n}\r\n.icon-home:before {\r\n  content: \"\\e069\";\r\n}\r\n.icon-grid:before {\r\n  content: \"\\e06a\";\r\n}\r\n.icon-graph:before {\r\n  content: \"\\e06b\";\r\n}\r\n.icon-microphone:before {\r\n  content: \"\\e063\";\r\n}\r\n.icon-music-tone-alt:before {\r\n  content: \"\\e061\";\r\n}\r\n.icon-music-tone:before {\r\n  content: \"\\e062\";\r\n}\r\n.icon-earphones-alt:before {\r\n  content: \"\\e03c\";\r\n}\r\n.icon-earphones:before {\r\n  content: \"\\e03d\";\r\n}\r\n.icon-equalizer:before {\r\n  content: \"\\e06c\";\r\n}\r\n.icon-like:before {\r\n  content: \"\\e068\";\r\n}\r\n.icon-dislike:before {\r\n  content: \"\\e06d\";\r\n}\r\n.icon-control-start:before {\r\n  content: \"\\e06f\";\r\n}\r\n.icon-control-rewind:before {\r\n  content: \"\\e070\";\r\n}\r\n.icon-control-play:before {\r\n  content: \"\\e071\";\r\n}\r\n.icon-control-pause:before {\r\n  content: \"\\e072\";\r\n}\r\n.icon-control-forward:before {\r\n  content: \"\\e073\";\r\n}\r\n.icon-control-end:before {\r\n  content: \"\\e074\";\r\n}\r\n.icon-volume-1:before {\r\n  content: \"\\e09f\";\r\n}\r\n.icon-volume-2:before {\r\n  content: \"\\e0a0\";\r\n}\r\n.icon-volume-off:before {\r\n  content: \"\\e0a1\";\r\n}\r\n.icon-calendar:before {\r\n  content: \"\\e075\";\r\n}\r\n.icon-bulb:before {\r\n  content: \"\\e076\";\r\n}\r\n.icon-chart:before {\r\n  content: \"\\e077\";\r\n}\r\n.icon-ban:before {\r\n  content: \"\\e07c\";\r\n}\r\n.icon-bubble:before {\r\n  content: \"\\e07d\";\r\n}\r\n.icon-camrecorder:before {\r\n  content: \"\\e07e\";\r\n}\r\n.icon-camera:before {\r\n  content: \"\\e07f\";\r\n}\r\n.icon-cloud-download:before {\r\n  content: \"\\e083\";\r\n}\r\n.icon-cloud-upload:before {\r\n  content: \"\\e084\";\r\n}\r\n.icon-envelope:before {\r\n  content: \"\\e086\";\r\n}\r\n.icon-eye:before {\r\n  content: \"\\e087\";\r\n}\r\n.icon-flag:before {\r\n  content: \"\\e088\";\r\n}\r\n.icon-heart:before {\r\n  content: \"\\e08a\";\r\n}\r\n.icon-info:before {\r\n  content: \"\\e08b\";\r\n}\r\n.icon-key:before {\r\n  content: \"\\e08c\";\r\n}\r\n.icon-link:before {\r\n  content: \"\\e08d\";\r\n}\r\n.icon-lock:before {\r\n  content: \"\\e08e\";\r\n}\r\n.icon-lock-open:before {\r\n  content: \"\\e08f\";\r\n}\r\n.icon-magnifier:before {\r\n  content: \"\\e090\";\r\n}\r\n.icon-magnifier-add:before {\r\n  content: \"\\e091\";\r\n}\r\n.icon-magnifier-remove:before {\r\n  content: \"\\e092\";\r\n}\r\n.icon-paper-clip:before {\r\n  content: \"\\e093\";\r\n}\r\n.icon-paper-plane:before {\r\n  content: \"\\e094\";\r\n}\r\n.icon-power:before {\r\n  content: \"\\e097\";\r\n}\r\n.icon-refresh:before {\r\n  content: \"\\e098\";\r\n}\r\n.icon-reload:before {\r\n  content: \"\\e099\";\r\n}\r\n.icon-settings:before {\r\n  content: \"\\e09a\";\r\n}\r\n.icon-star:before {\r\n  content: \"\\e09b\";\r\n}\r\n.icon-symbol-female:before {\r\n  content: \"\\e09c\";\r\n}\r\n.icon-symbol-male:before {\r\n  content: \"\\e09d\";\r\n}\r\n.icon-target:before {\r\n  content: \"\\e09e\";\r\n}\r\n.icon-credit-card:before {\r\n  content: \"\\e025\";\r\n}\r\n.icon-paypal:before {\r\n  content: \"\\e608\";\r\n}\r\n.icon-social-tumblr:before {\r\n  content: \"\\e00a\";\r\n}\r\n.icon-social-twitter:before {\r\n  content: \"\\e009\";\r\n}\r\n.icon-social-facebook:before {\r\n  content: \"\\e00b\";\r\n}\r\n.icon-social-instagram:before {\r\n  content: \"\\e609\";\r\n}\r\n.icon-social-linkedin:before {\r\n  content: \"\\e60a\";\r\n}\r\n.icon-social-pinterest:before {\r\n  content: \"\\e60b\";\r\n}\r\n.icon-social-github:before {\r\n  content: \"\\e60c\";\r\n}\r\n.icon-social-google:before {\r\n  content: \"\\e60d\";\r\n}\r\n.icon-social-reddit:before {\r\n  content: \"\\e60e\";\r\n}\r\n.icon-social-skype:before {\r\n  content: \"\\e60f\";\r\n}\r\n.icon-social-dribbble:before {\r\n  content: \"\\e00d\";\r\n}\r\n.icon-social-behance:before {\r\n  content: \"\\e610\";\r\n}\r\n.icon-social-foursqare:before {\r\n  content: \"\\e611\";\r\n}\r\n.icon-social-soundcloud:before {\r\n  content: \"\\e612\";\r\n}\r\n.icon-social-spotify:before {\r\n  content: \"\\e613\";\r\n}\r\n.icon-social-stumbleupon:before {\r\n  content: \"\\e614\";\r\n}\r\n.icon-social-youtube:before {\r\n  content: \"\\e008\";\r\n}\r\n.icon-social-dropbox:before {\r\n  content: \"\\e00c\";\r\n}\r\n.icon-social-vkontakte:before {\r\n  content: \"\\e618\";\r\n}\r\n.icon-social-steam:before {\r\n  content: \"\\e620\";\r\n}\r\n.icon-group a {\n  display: block;\n  padding-left: 40px;\n  height: 32px;\n  line-height: 32px;\n  margin-bottom: 5px; }\r\n.icon-group a .icon, .icon-group a .fa,\n  .icon-group a [class^=\"icon-\"],\n  .icon-group a [class*=\" icon-\"] {\n    position: absolute;\n    top: 0;\n    left: 10px;\n    min-width: 30px;\n    text-align: center;\n    line-height: 32px; }\r\n.icon-group a:hover .fa,\n  .icon-group a:hover .icon,\n  .icon-group a:hover [class^=\"icon-\"],\n  .icon-group a:hover [class*=\" icon-\"] {\n    top: 5px;\n    font-size: 1.5em; }\n"

/***/ }),

/***/ "./src/app/icons/sli/sli.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SliComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var SliComponent = (function () {
    function SliComponent() {
        this.temp = [];
        this.icons = [];
        var icons = [
            'user',
            'people',
            'user-female',
            'user-follow',
            'user-following',
            'user-unfollow',
            'login',
            'logout',
            'emotsmile',
            'phone',
            'call-end',
            'call-in',
            'call-out',
            'map',
            'location-pin',
            'direction',
            'directions',
            'compass',
            'layers',
            'menu',
            'list',
            'options-vertical',
            'options',
            'arrow-down',
            'arrow-left',
            'arrow-right',
            'arrow-up',
            'arrow-up-circle',
            'arrow-left-circle',
            'arrow-right-circle',
            'arrow-down-circle',
            'check',
            'clock',
            'plus',
            'minus',
            'close',
            'event',
            'exclamation',
            'organization',
            'trophy',
            'screen-smartphone',
            'screen-desktop',
            'plane',
            'notebook',
            'mustache',
            'mouse',
            'magnet',
            'energy',
            'disc',
            'cursor',
            'cursor-move',
            'crop',
            'chemistry',
            'speedometer',
            'shield',
            'screen-tablet',
            'magic-wand',
            'hourglass',
            'graduation',
            'ghost',
            'game-controller',
            'fire',
            'eyeglass',
            'envelope-open',
            'envelope-letter',
            'bell',
            'badge',
            'anchor',
            'wallet',
            'vector',
            'speech',
            'puzzle',
            'printer',
            'present',
            'playlist',
            'pin',
            'picture',
            'handbag',
            'globe-alt',
            'globe',
            'folder-alt',
            'folder',
            'film',
            'feed',
            'drop',
            'drawer',
            'docs',
            'doc',
            'diamond',
            'cup',
            'calculator',
            'bubbles',
            'briefcase',
            'book-open',
            'basket-loaded',
            'basket',
            'bag',
            'action-undo',
            'action-redo',
            'wrench',
            'umbrella',
            'trash',
            'tag',
            'support',
            'frame',
            'size-fullscreen',
            'size-actual',
            'shuffle',
            'share-alt',
            'share',
            'rocket',
            'question',
            'pie-chart',
            'pencil',
            'note',
            'loop',
            'home',
            'grid',
            'graph',
            'microphone',
            'music-tone-alt',
            'music-tone',
            'earphones-alt',
            'earphones',
            'equalizer',
            'like',
            'dislike',
            'control-start',
            'control-rewind',
            'control-play',
            'control-pause',
            'control-forward',
            'control-end',
            'volume-1',
            'volume-2',
            'volume-off',
            'calendar',
            'bulb',
            'chart',
            'ban',
            'bubble',
            'camrecorder',
            'camera',
            'cloud-download',
            'cloud-upload',
            'envelope',
            'eye',
            'flag',
            'heart',
            'info',
            'key',
            'link',
            'lock',
            'lock-open',
            'magnifier',
            'magnifier-add',
            'magnifier-remove',
            'paper-clip',
            'paper-plane',
            'power',
            'refresh',
            'reload',
            'settings',
            'star',
            'symbol-female',
            'symbol-male',
            'target',
            'credit-card',
            'paypal',
            'social-tumblr',
            'social-twitter',
            'social-facebook',
            'social-instagram',
            'social-linkedin',
            'social-pinterest',
            'social-github',
            'social-google',
            'social-reddit',
            'social-skype',
            'social-dribbble',
            'social-behance',
            'social-foursqare',
            'social-soundcloud',
            'social-spotify',
            'social-stumbleupon',
            'social-youtube',
            'social-dropbox',
            'social-vkontakte',
            'social-steam'
        ];
        this.temp = icons;
        this.icons = icons;
    }
    SliComponent.prototype.updateFilter = function (event) {
        var query = event.target.value;
        var filtered = this.temp.filter(function (el) {
            return el.toLowerCase().indexOf(query.toLowerCase()) > -1;
        });
        this.icons = filtered;
    };
    SliComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-sli',
            template: __webpack_require__("./src/app/icons/sli/sli.component.html"),
            styles: [__webpack_require__("./src/app/icons/sli/sli.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], SliComponent);
    return SliComponent;
}());

//# sourceMappingURL=sli.component.js.map

/***/ }),

/***/ "./src/app/layouts/admin/admin-layout.component.html":
/***/ (function(module, exports) {

module.exports = "<ng-sidebar-container class=\"app\" [ngClass]=\"{'mode-boxed': isBoxed, 'sidebar-opened': isOpened, 'theme-light': theme == 'light', 'theme-dark': theme == 'dark', 'mode-push': _mode == 'push', 'mode-dock': _mode == 'dock', 'mode-over': _mode == 'over', 'mode-slide': _mode == 'slide', 'no-footer': options?.removeFooter, 'map-header': options?.mapHeader}\">\r\n  <ng-sidebar\r\n    [(opened)]=\"isOpened\"\r\n    [(mode)]=\"_mode\"\r\n    [position]=\"'left'\"\r\n    [dockedSize]=\"'80px'\"\r\n    [autoCollapseWidth]=\"'991'\"\r\n    [closeOnClickOutside]=\"isOver()\"\r\n    [showBackdrop]=\"isOver()\"\r\n    [sidebarClass]=\"'sidebar-panel'\" #sidebar>\r\n    <nav class=\"navbar custom-navbar main-brand\">\r\n      <a class=\"navbar-brand mr-auto\" [routerLink]=\"['/']\">\r\n        <img src=\"assets/images/logo.png\" class=\"navbar-brand-logo\" alt=\"\">\r\n        <span class=\"docked-hidden\">&nbsp;EZmath</span>\r\n      </a>\r\n      <ul class=\"navbar-nav\">\r\n        <li class=\"nav-item\">\r\n          <a href=\"javascript:;\" class=\"nav-link\" (click)=\"toogleSidebar()\">\r\n            <i class=\"hamburger-icon v2\" *ngIf=\"_mode === 'over' && !isOver()\">\r\n              <span></span>\r\n            </i>\r\n          </a>\r\n        </li>\r\n      </ul>\r\n    </nav>\r\n    <!-- main navigation -->\r\n    <nav class=\"menu\">\r\n      <ul class=\"navigation\" appAccordion>\r\n        <li class=\"navigation-item\" appAccordionLink *ngFor=\"let menuitem of menuItems.getAll()\" group=\"{{menuitem.state}}\">\r\n          <a class=\"navigation-link\" appAccordionToggle [routerLink]=\"['/', menuitem.state,menuitem.child]\" *ngIf=\"menuitem.type === 'new'\">\r\n            <i class=\"icon icon-{{ menuitem.icon }}\"></i>\r\n            <span>{{ menuitem.name | translate }}</span>\r\n            <span class=\"mr-auto\"></span>\r\n            <span class=\"badge badge-{{ badge.type }}\" *ngFor=\"let badge of menuitem.badge\">{{ badge.value }}</span>\r\n          </a>\r\n          <a class=\"navigation-link\" appAccordionToggle [routerLink]=\"['/', menuitem.state]\" *ngIf=\"menuitem.type === 'link'\">\r\n            <i class=\"icon icon-{{ menuitem.icon }}\"></i>\r\n            <span>{{ menuitem.name | translate }}</span>\r\n            <span class=\"mr-auto\"></span>\r\n            <span class=\"badge badge-{{ badge.type }}\" *ngFor=\"let badge of menuitem.badge\">{{ badge.value }}</span>\r\n          </a>\r\n          <a class=\"navigation-link\" appAccordionToggle href=\"{{menuitem.state}}\" *ngIf=\"menuitem.type === 'extLink'\">\r\n            <i class=\"icon icon-{{ menuitem.icon }}\"></i>\r\n            <span>{{ menuitem.name | translate }}</span>\r\n            <span class=\"mr-auto\"></span>\r\n            <span class=\"badge badge-{{ badge.type }}\" *ngFor=\"let badge of menuitem.badge\">{{ badge.value }}</span>\r\n          </a>\r\n          <a class=\"navigation-link\" appAccordionToggle href=\"{{menuitem.state}}\" target=\"_blank\" *ngIf=\"menuitem.type === 'extTabLink'\">\r\n            <i class=\"icon icon-{{ menuitem.icon }}\"></i>\r\n            <span>{{ menuitem.name | translate }}</span>\r\n            <span class=\"mr-auto\"></span>\r\n            <span class=\"badge badge-{{ badge.type }}\" *ngFor=\"let badge of menuitem.badge\">{{ badge.value }}</span>\r\n          </a>\r\n          <a class=\"navigation-link\" appAccordionToggle href=\"javascript:;\" *ngIf=\"menuitem.type === 'sub'\">\r\n            <i class=\"icon icon-{{ menuitem.icon }}\"></i>\r\n            <span>{{ menuitem.name | translate }}</span>\r\n            <span class=\"mr-auto\"></span>\r\n            <span class=\"badge badge-{{ badge.type }}\" *ngFor=\"let badge of menuitem.badge\">{{ badge.value }}</span>\r\n            <i class=\"menu-caret icon icon-arrows-right\"></i>\r\n          </a>\r\n          <ul class=\"navigation-submenu\" *ngIf=\"menuitem.type === 'sub'\">\r\n            <li class=\"navigation-item\" *ngFor=\"let childitem of menuitem.children\" routerLinkActive=\"open\">\r\n              <a [routerLink]=\"['/', menuitem.state, childitem.state ]\" class=\"navigation-link relative\">{{ childitem.name | translate }}</a>\r\n            </li>\r\n          </ul>\r\n        </li>\r\n        <li class=\"navigation-item\"><hr class=\"mt-0 mb-0\" /></li>\r\n        <!--<li class=\"navigation-item\">-->\r\n          <!--<a class=\"navigation-link\" (click)=\"addMenuItem()\">-->\r\n            <!--<i class=\"icon icon-basic-add\"></i>-->\r\n            <!--<span>Add</span>-->\r\n          <!--</a>-->\r\n        <!--</li>-->\r\n      </ul>\r\n    </nav>\r\n    <!-- /main navigation -->\r\n  </ng-sidebar>\r\n\r\n  <div ng-sidebar-content class=\"app-inner\">\r\n    <nav class=\"navbar custom-navbar bg-faded main-header\">\r\n      <ul class=\"navbar-nav\">\r\n        <li class=\"nav-item\">\r\n          <a href=\"javascript:;\" class=\"nav-link\" (click)=\"toogleSidebar()\">\r\n            <i class=\"hamburger-icon v2\" *ngIf=\"_mode !== 'dock'\">\r\n              <span></span>\r\n            </i>\r\n          </a>\r\n        </li>\r\n      </ul>\r\n      <span class=\"navbar-heading hidden-xs-down\">{{options?.heading}}</span>\r\n      <span class=\"mr-auto\"></span>\r\n      <ul class=\"navbar-nav\">\r\n        <li class=\"nav-item\" ngbDropdown placement=\"bottom-right\">\r\n          <a href=\"javascript:;\" class=\"nav-link\" ngbDropdownToggle>\r\n            <img src=\"assets/images/avatar.jpg\" class=\"navbar-avatar rounded-circle\" alt=\"user\" title=\"user\">\r\n          </a>\r\n          <div ngbDropdownMenu class=\"dropdown-menu dropdown-menu-right\">\r\n\r\n            <div class=\"dropdown-divider\"></div>\r\n            <a class=\"dropdown-item\" href=\"#\" (click)=\"logout()\">\r\n              <i class=\"icon icon-arrows-switch-vertical mr-3\"></i>\r\n              <span>התנתקות</span>\r\n            </a>\r\n          </div>\r\n        </li>\r\n        <!--\r\n       <li class=\"nav-item\" ngbDropdown placement=\"bottom-right\">\r\n\r\n         <a href=\"javascript:;\" class=\"nav-link\" ngbDropdownToggle>\r\n           <i class=\"fi flaticon-notification\"></i>\r\n           <span class=\"badge badge-danger\">4</span>\r\n         </a>\r\n         <div ngbDropdownMenu class=\"dropdown-menu dropdown-menu-right notifications\">\r\n           <div class=\"notifications-wrapper\">\r\n             <a href=\"javascript:;\" class=\"dropdown-item\">\r\n               <span class=\"badge badge-warning\">NEW</span>\r\n               &nbsp;Sean launched a new application\r\n               <span class=\"time\">2 seconds ago</span>\r\n             </a>\r\n             <a href=\"javascript:;\" class=\"dropdown-item\">\r\n               Removed calendar from app list\r\n               <span class=\"time\">4 hours ago</span>\r\n             </a>\r\n             <a href=\"javascript:;\" class=\"dropdown-item\">\r\n               Jack Hunt has joined mailing list\r\n               <span class=\"time\">9 days ago</span>\r\n             </a>\r\n             <a href=\"javascript:;\" class=\"dropdown-item\">\r\n               <span class=\"text-muted\">Conan Johns created a new list</span>\r\n               <span class=\"time\">9 days ago</span>\r\n             </a>\r\n           </div>\r\n           <div class=\"notification-footer\">Notifications</div>\r\n         </div>\r\n       </li>\r\n\r\n       -->\r\n        <li class=\"nav-item\">\r\n          <a href=\"javascript:;\" class=\"nav-link\" appToggleFullscreen>\r\n            <i class=\"fi flaticon-fullscreen\"></i>\r\n          </a>\r\n        </li>\r\n        \r\n        <li class=\"nav-item\">\r\n          <span class=\"nav-divider\"></span>\r\n        </li>\r\n        <!--\r\n        <li class=\"nav-item\">\r\n          <a href=\"javascript:;\" class=\"nav-link\" (click)=\"openSearch(search)\">\r\n            <i class=\"fi flaticon-search\"></i>\r\n          </a>\r\n        </li>\r\n         -->\r\n      </ul>\r\n    </nav>\r\n\r\n    <div class=\"main-content\">\r\n      <router-outlet></router-outlet>\r\n\r\n      <nav class=\"navbar custom-navbar navbar-light main-footer small\">\r\n        <ul class=\"navbar-nav mr-auto\">\r\n          <!--\r\n          <li class=\"nav-item\">\r\n            <a class=\"nav-link\" href=\"javascript:;\">\r\n              <span>Copyright &copy; 2017</span> <span class=\"ff-headers text-uppercase\">Decima</span>. All rights reserved\r\n            </a>\r\n          </li>\r\n          -->\r\n        </ul>\r\n        <ul class=\"navbar-nav hidden-xs-down\">\r\n          <!--\r\n          <li class=\"nav-item\">\r\n            <a class=\"nav-link\" href=\"javascript:;\">Made with love on Earth</a>\r\n          </li>\r\n           -->\r\n        </ul>\r\n      </nav>\r\n    </div>\r\n\r\n  </div>\r\n\r\n</ng-sidebar-container>\r\n\r\n<ng-template #search let-c=\"close\" let-d=\"dismiss\">\r\n  <form class=\"search__form\" action=\"\">\r\n    <input class=\"search-input\" name=\"search\" type=\"search\" placeholder=\"Search...\" autocomplete=\"off\" autocorrect=\"off\" autocapitalize=\"off\" spellcheck=\"false\" autofocus=\"true\" />\r\n    <p class=\"text-muted\"><small><strong>Hit enter to search or ESC to close</strong></small></p>\r\n  </form>\r\n  <div class=\"search-suggestions\">\r\n    <h6 class=\"text-uppercase\"><strong>Suggestions?</strong></h6>\r\n    <p class=\"text-primary\">#medical #analytics #fitness #transport #ui #dashboard #admin #bootstrap #angular #typescript</p>\r\n  </div>\r\n  <button type=\"button\" class=\"search-close\" aria-label=\"Close search form\" (click)=\"d('Cross click')\">\r\n    <i class=\"fi flaticon-close\"></i>\r\n  </button>\r\n</ng-template>\r\n\r\n\r\n<div class=\"configuration hidden-sm-down\" [ngClass]=\"{'active': showSettings}\" *ngIf=\"showSettings\">\r\n  <div class=\"configuration-cog\" (click)=\"showSettings = !showSettings\">\r\n    <i class=\"icon icon-basic-mixer2\"></i>\r\n  </div>\r\n  <div class=\"card\">\r\n    <div class=\"card-header\">\r\n      Template Options\r\n    </div>\r\n    <div class=\"card-body\">\r\n      <small class=\"ff-headers text-uppercase mb-3\"><strong>Explore Sidebar API</strong></small>\r\n      <div class=\"custom-controls-stacked mb-2\">\r\n        <label class=\"custom-control custom-checkbox\">\r\n          <input class=\"custom-control-input\" name=\"radio-stacked\" type=\"radio\" value=\"push\" [(ngModel)]=\"_mode\" (change)=\"isOpened = true; mode = _mode\">\r\n          <span class=\"custom-control-indicator\"></span>\r\n          <span class=\"custom-control-description\">Push mode</span>\r\n        </label>\r\n        <label class=\"custom-control custom-checkbox\">\r\n          <input class=\"custom-control-input\" name=\"radio-stacked\" type=\"radio\" value=\"dock\" [(ngModel)]=\"_mode\" (change)=\"isOpened = true; mode = _mode\">\r\n          <span class=\"custom-control-indicator\"></span>\r\n          <span class=\"custom-control-description\">Docked mode</span>\r\n        </label>\r\n        <label class=\"custom-control custom-checkbox\">\r\n          <input class=\"custom-control-input\" name=\"radio-stacked\" type=\"radio\" value=\"over\" [(ngModel)]=\"_mode\" (change)=\"isOpened = true; mode = _mode\">\r\n          <span class=\"custom-control-indicator\"></span>\r\n          <span class=\"custom-control-description\">Over content mode</span>\r\n        </label>\r\n        <label class=\"custom-control custom-checkbox\">\r\n          <input class=\"custom-control-input\" name=\"radio-stacked\" type=\"radio\" value=\"slide\" [(ngModel)]=\"_mode\" (change)=\"isOpened = true; mode = _mode\">\r\n          <span class=\"custom-control-indicator\"></span>\r\n          <span class=\"custom-control-description\">Slide mode</span>\r\n        </label>\r\n      </div>\r\n\r\n      <small class=\"ff-headers text-uppercase mb-3\"><strong>Select A Layout</strong></small>\r\n      <div class=\"d-flex align-items-center\">\r\n        <label class=\"custom-control custom-checkbox mb-2\">\r\n          <input type=\"checkbox\" class=\"custom-control-input\" [(ngModel)]=\"isBoxed\">\r\n          <span class=\"custom-control-indicator\"></span>\r\n          <span class=\"custom-control-description\">Boxed</span>\r\n        </label>\r\n      </div>\r\n      <div class=\"d-flex align-items-center mb-2\">\r\n        <label class=\"custom-control custom-checkbox mb-2\">\r\n          <input type=\"checkbox\" class=\"custom-control-input\" [ngModel]=\"options?.removeFooter\" (ngModelChange)=\"options.removeFooter=$event\">\r\n          <span class=\"custom-control-indicator\"></span>\r\n          <span class=\"custom-control-description\">Remove footer</span>\r\n        </label>\r\n      </div>\r\n\r\n      <small class=\"ff-headers text-uppercase mb-3\"><strong>Select A Theme</strong></small>\r\n      <div class=\"custom-controls-stacked mb-2\">\r\n        <label class=\"custom-control custom-checkbox\">\r\n          <input class=\"custom-control-input\" name=\"radio-stacked\" type=\"radio\" value=\"light\" [(ngModel)]=\"theme\">\r\n          <span class=\"custom-control-indicator\"></span>\r\n          <span class=\"custom-control-description\">Light theme</span>\r\n        </label>\r\n        <label class=\"custom-control custom-checkbox\">\r\n          <input class=\"custom-control-input\" name=\"radio-stacked\" type=\"radio\" value=\"dark\" [(ngModel)]=\"theme\">\r\n          <span class=\"custom-control-indicator\"></span>\r\n          <span class=\"custom-control-description\">Dark theme</span>\r\n        </label>\r\n      </div>\r\n\r\n      <small class=\"ff-headers text-uppercase mb-3\"><strong>Select A Language</strong></small>\r\n      <div class=\"d-flex align-items-center\">\r\n        <select class=\"custom-select\" [(ngModel)]=\"currentLang\" #langSelect=\"ngModel\" (ngModelChange)=\"translate.use(currentLang)\" placeholder=\"Select language\" style=\"min-width: 50%;\">\r\n          <option *ngFor=\"let lang of translate.getLangs()\" [value]=\"lang\">{{ lang }}</option>\r\n        </select>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/layouts/admin/admin-layout.component.scss":
/***/ (function(module, exports) {

module.exports = ".configuration {\n  width: 240px;\n  position: fixed;\n  right: 0;\n  top: 150px;\n  margin-left: 0;\n  z-index: 99999999;\n  -webkit-transition: -webkit-transform .3s ease-in-out;\n  transition: -webkit-transform .3s ease-in-out;\n  transition: transform .3s ease-in-out;\n  transition: transform .3s ease-in-out, -webkit-transform .3s ease-in-out;\n  -webkit-transform: translate(100%, 0);\n          transform: translate(100%, 0); }\n  .configuration .card {\n    -webkit-box-shadow: 0 0 5px rgba(0, 0, 0, 0.2);\n            box-shadow: 0 0 5px rgba(0, 0, 0, 0.2);\n    margin: 0;\n    border-radius: 0; }\n  .configuration.active {\n  -webkit-transform: translate(0, 0);\n          transform: translate(0, 0); }\n  .configuration-cog {\n  width: 50px;\n  height: 50px;\n  position: absolute;\n  left: -50px;\n  line-height: 50px;\n  font-size: 24px;\n  text-align: center;\n  background: #fff;\n  -webkit-box-shadow: 0 0 1px rgba(0, 0, 0, 0.2);\n          box-shadow: 0 0 1px rgba(0, 0, 0, 0.2);\n  border-top-left-radius: 2px;\n  border-bottom-left-radius: 2px;\n  cursor: pointer; }\n  .configuration-cog i:before {\n    line-height: 50px; }\n"

/***/ }),

/***/ "./src/app/layouts/admin/admin-layout.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AdminLayoutComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__ = __webpack_require__("./node_modules/@angular/platform-browser/@angular/platform-browser.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("./node_modules/@angular/router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ng_bootstrap_ng_bootstrap__ = __webpack_require__("./node_modules/@ng-bootstrap/ng-bootstrap/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__shared_menu_items_menu_items__ = __webpack_require__("./src/app/shared/menu-items/menu-items.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_filter__ = __webpack_require__("./node_modules/rxjs/add/operator/filter.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_filter___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_filter__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ngx_translate_core__ = __webpack_require__("./node_modules/@ngx-translate/core/index.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var SMALL_WIDTH_BREAKPOINT = 991;
var AdminLayoutComponent = (function () {
    function AdminLayoutComponent(menuItems, router, route, translate, modalService, titleService, zone) {
        var _this = this;
        this.menuItems = menuItems;
        this.router = router;
        this.route = route;
        this.translate = translate;
        this.modalService = modalService;
        this.titleService = titleService;
        this.zone = zone;
        this.mediaMatcher = matchMedia("(max-width: " + SMALL_WIDTH_BREAKPOINT + "px)");
        this.currentLang = 'en';
        this.theme = 'light';
        this.showSettings = false;
        this.isDocked = false;
        this.isBoxed = false;
        this.isOpened = true;
        this.mode = 'push';
        this._mode = this.mode;
        this._autoCollapseWidth = 991;
        this.width = window.innerWidth;
        var browserLang = translate.getBrowserLang();
        translate.use(browserLang.match(/en|fr/) ? browserLang : 'en');
        this.mediaMatcher.addListener(function (mql) { return zone.run(function () { return _this.mediaMatcher = mql; }); });
    }
    AdminLayoutComponent.prototype.ngOnInit = function () {
        var _this = this;
        if (this.isOver()) {
            this._mode = 'over';
            this.isOpened = false;
        }
        this._router = this.router.events.filter(function (event) { return event instanceof __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* NavigationEnd */]; }).subscribe(function (event) {
            // Scroll to top on view load
            document.querySelector('.main-content').scrollTop = 0;
            _this.runOnRouteChange();
        });
    };
    AdminLayoutComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        setTimeout(function (_) { return _this.runOnRouteChange(); });
    };
    AdminLayoutComponent.prototype.ngOnDestroy = function () {
        this._router.unsubscribe();
    };
    AdminLayoutComponent.prototype.runOnRouteChange = function () {
        var _this = this;
        if (this.isOver() || this.router.url === '/maps/fullscreen') {
            this.isOpened = false;
        }
        this.route.children.forEach(function (route) {
            var activeRoute = route;
            while (activeRoute.firstChild) {
                activeRoute = activeRoute.firstChild;
            }
            _this.options = activeRoute.snapshot.data;
        });
        if (this.options) {
            if (this.options.hasOwnProperty('heading')) {
                this.setTitle(this.options.heading);
            }
        }
    };
    AdminLayoutComponent.prototype.setTitle = function (newTitle) {
        this.titleService.setTitle('EZmath | ' + newTitle);
    };
    AdminLayoutComponent.prototype.toogleSidebar = function () {
        if (this._mode !== 'dock') {
            this.isOpened = !this.isOpened;
        }
    };
    AdminLayoutComponent.prototype.isOver = function () {
        return window.matchMedia("(max-width: 991px)").matches;
    };
    AdminLayoutComponent.prototype.openSearch = function (search) {
        this.modalService.open(search, { windowClass: 'search', backdrop: false });
    };
    AdminLayoutComponent.prototype.addMenuItem = function () {
        this.menuItems.add({
            state: 'menu',
            name: 'MENU',
            type: 'sub',
            icon: 'basic-webpage-txt',
            children: [
                { state: 'menu', name: 'MENU' },
                { state: 'menu', name: 'MENU' }
            ]
        });
    };
    AdminLayoutComponent.prototype.logout = function () {
        localStorage.clear();
        this.router.navigate(['/authentication']);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('sidebar'),
        __metadata("design:type", Object)
    ], AdminLayoutComponent.prototype, "sidebar", void 0);
    AdminLayoutComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-layout',
            template: __webpack_require__("./src/app/layouts/admin/admin-layout.component.html"),
            styles: [__webpack_require__("./src/app/layouts/admin/admin-layout.component.scss")]
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_4__shared_menu_items_menu_items__["a" /* MenuItems */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__shared_menu_items_menu_items__["a" /* MenuItems */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__angular_router__["c" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_router__["c" /* Router */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* ActivatedRoute */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_6__ngx_translate_core__["c" /* TranslateService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_6__ngx_translate_core__["c" /* TranslateService */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_3__ng_bootstrap_ng_bootstrap__["f" /* NgbModal */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__ng_bootstrap_ng_bootstrap__["f" /* NgbModal */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["Title"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["Title"]) === "function" && _f || Object, typeof (_g = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["NgZone"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["NgZone"]) === "function" && _g || Object])
    ], AdminLayoutComponent);
    return AdminLayoutComponent;
    var _a, _b, _c, _d, _e, _f, _g;
}());

//# sourceMappingURL=admin-layout.component.js.map

/***/ }),

/***/ "./src/app/layouts/auth/auth-layout.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthLayoutComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AuthLayoutComponent = (function () {
    function AuthLayoutComponent() {
    }
    AuthLayoutComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-layout',
            template: '<router-outlet></router-outlet>'
        })
    ], AuthLayoutComponent);
    return AuthLayoutComponent;
}());

//# sourceMappingURL=auth-layout.component.js.map

/***/ }),

/***/ "./src/app/shared/accordion/accordion.directive.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AccordionDirective; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/@angular/router.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AccordionDirective = (function () {
    function AccordionDirective(router) {
        var _this = this;
        this.router = router;
        this.navlinks = [];
        setTimeout(function () { return _this.checkOpenLinks(); });
    }
    AccordionDirective.prototype.closeOtherLinks = function (openLink) {
        this.navlinks.forEach(function (link) {
            if (link !== openLink) {
                link.open = false;
            }
        });
    };
    AccordionDirective.prototype.addLink = function (link) {
        this.navlinks.push(link);
    };
    AccordionDirective.prototype.removeGroup = function (link) {
        var index = this.navlinks.indexOf(link);
        if (index !== -1) {
            this.navlinks.splice(index, 1);
        }
    };
    AccordionDirective.prototype.checkOpenLinks = function () {
        var _this = this;
        this.navlinks.forEach(function (link) {
            if (link.group) {
                var routeUrl = _this.router.url;
                var currentUrl = routeUrl.split('/');
                if (currentUrl.indexOf(link.group) > 0) {
                    link.open = true;
                    _this.closeOtherLinks(link);
                }
            }
        });
    };
    AccordionDirective.prototype.ngAfterContentChecked = function () {
        var _this = this;
        this.router.events.filter(function (event) { return event instanceof __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* NavigationEnd */]; }).subscribe(function (e) { return _this.checkOpenLinks(); });
    };
    AccordionDirective = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Directive"])({
            selector: '[appAccordion]',
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */]) === "function" && _a || Object])
    ], AccordionDirective);
    return AccordionDirective;
    var _a;
}());

//# sourceMappingURL=accordion.directive.js.map

/***/ }),

/***/ "./src/app/shared/accordion/accordionanchor.directive.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AccordionAnchorDirective; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__accordionlink_directive__ = __webpack_require__("./src/app/shared/accordion/accordionlink.directive.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};


var AccordionAnchorDirective = (function () {
    function AccordionAnchorDirective(navlink) {
        this.navlink = navlink;
    }
    AccordionAnchorDirective.prototype.onClick = function (e) {
        this.navlink.toggle();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["HostListener"])('click', ['$event']),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Object]),
        __metadata("design:returntype", void 0)
    ], AccordionAnchorDirective.prototype, "onClick", null);
    AccordionAnchorDirective = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Directive"])({
            selector: '[appAccordionToggle]'
        }),
        __param(0, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Inject"])(__WEBPACK_IMPORTED_MODULE_1__accordionlink_directive__["a" /* AccordionLinkDirective */])),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__accordionlink_directive__["a" /* AccordionLinkDirective */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__accordionlink_directive__["a" /* AccordionLinkDirective */]) === "function" && _a || Object])
    ], AccordionAnchorDirective);
    return AccordionAnchorDirective;
    var _a;
}());

//# sourceMappingURL=accordionanchor.directive.js.map

/***/ }),

/***/ "./src/app/shared/accordion/accordionlink.directive.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AccordionLinkDirective; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__accordion_directive__ = __webpack_require__("./src/app/shared/accordion/accordion.directive.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};


var AccordionLinkDirective = (function () {
    function AccordionLinkDirective(nav) {
        this.nav = nav;
    }
    Object.defineProperty(AccordionLinkDirective.prototype, "open", {
        get: function () {
            return this._open;
        },
        set: function (value) {
            this._open = value;
            if (value) {
                this.nav.closeOtherLinks(this);
            }
        },
        enumerable: true,
        configurable: true
    });
    AccordionLinkDirective.prototype.ngOnInit = function () {
        this.nav.addLink(this);
    };
    AccordionLinkDirective.prototype.ngOnDestroy = function () {
        this.nav.removeGroup(this);
    };
    AccordionLinkDirective.prototype.toggle = function () {
        this.open = !this.open;
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], AccordionLinkDirective.prototype, "group", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["HostBinding"])('class.open'),
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Boolean),
        __metadata("design:paramtypes", [Boolean])
    ], AccordionLinkDirective.prototype, "open", null);
    AccordionLinkDirective = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Directive"])({
            selector: '[appAccordionLink]'
        }),
        __param(0, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Inject"])(__WEBPACK_IMPORTED_MODULE_1__accordion_directive__["a" /* AccordionDirective */])),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__accordion_directive__["a" /* AccordionDirective */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__accordion_directive__["a" /* AccordionDirective */]) === "function" && _a || Object])
    ], AccordionLinkDirective);
    return AccordionLinkDirective;
    var _a;
}());

//# sourceMappingURL=accordionlink.directive.js.map

/***/ }),

/***/ "./src/app/shared/accordion/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__accordionanchor_directive__ = __webpack_require__("./src/app/shared/accordion/accordionanchor.directive.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__accordionanchor_directive__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__accordionlink_directive__ = __webpack_require__("./src/app/shared/accordion/accordionlink.directive.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "c", function() { return __WEBPACK_IMPORTED_MODULE_1__accordionlink_directive__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__accordion_directive__ = __webpack_require__("./src/app/shared/accordion/accordion.directive.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "b", function() { return __WEBPACK_IMPORTED_MODULE_2__accordion_directive__["a"]; });



//# sourceMappingURL=index.js.map

/***/ }),

/***/ "./src/app/shared/fullscreen/toggle-fullscreen.directive.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ToggleFullscreenDirective; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_screenfull__ = __webpack_require__("./node_modules/screenfull/dist/screenfull.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_screenfull___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_screenfull__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ToggleFullscreenDirective = (function () {
    function ToggleFullscreenDirective() {
    }
    ToggleFullscreenDirective.prototype.onClick = function () {
        if (__WEBPACK_IMPORTED_MODULE_1_screenfull__["enabled"]) {
            __WEBPACK_IMPORTED_MODULE_1_screenfull__["toggle"]();
        }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["HostListener"])('click'),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", []),
        __metadata("design:returntype", void 0)
    ], ToggleFullscreenDirective.prototype, "onClick", null);
    ToggleFullscreenDirective = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Directive"])({
            selector: '[appToggleFullscreen]'
        })
    ], ToggleFullscreenDirective);
    return ToggleFullscreenDirective;
}());

//# sourceMappingURL=toggle-fullscreen.directive.js.map

/***/ }),

/***/ "./src/app/shared/menu-items/menu-items.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MenuItems; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var MENUITEMS = [
    {
        state: '/',
        name: 'ראשי',
        type: 'link',
        icon: 'basic-accelerator'
    }, {
        state: 'schoolgrade',
        child: 'index',
        name: 'כיתות',
        type: 'new',
        icon: 'basic-message-txt'
    }, {
        state: 'teachinglevel',
        child: 'index',
        name: 'רמת לימוד',
        type: 'new',
        icon: 'basic-message-txt'
    }, {
        state: 'users',
        child: 'index',
        name: 'משתמשים',
        type: 'new',
        icon: 'basic-message-txt'
    }, {
        state: 'users_debt',
        child: 'index',
        name: 'תלמידים בחוב',
        type: 'new',
        icon: 'basic-message-txt'
    }, {
        state: 'tickets',
        child: 'index',
        name: 'כרטיסיות',
        type: 'new',
        icon: 'basic-message-txt'
    },
    {
        state: 'professions',
        child: 'index',
        name: 'מקצועות',
        type: 'new',
        icon: 'basic-message-txt'
    },
    {
        state: 'branches',
        child: 'index',
        name: 'סניפים',
        type: 'new',
        icon: 'basic-message-txt'
    },
    {
        state: 'classes2',
        child: 'index',
        name: 'שיעורים',
        type: 'new',
        icon: 'basic-message-txt'
    }, {
        state: 'teachers_classes',
        child: 'index',
        name: 'ניהול יום לימודים',
        type: 'new',
        icon: 'basic-message-txt'
    },
    {
        state: 'user_classes',
        child: 'index',
        name: 'הרשמות לשיעורים',
        type: 'new',
        icon: 'basic-message-txt'
    },
    {
        state: 'ticket_groups',
        child: 'index',
        name: 'קבוצת כרטיסיות',
        type: 'new',
        icon: 'basic-message-txt'
    },
    {
        state: 'class_contact_leads',
        child: 'index',
        name: 'צור קשר שיעור',
        type: 'new',
        icon: 'basic-message-txt'
    },
    /*
        {
            state: 'classes',
            child: 'index',
            name: 'שיעורים',
            type: 'new',
            icon: 'basic-message-txt'
        },
     */
    {
        state: 'push',
        child: 'index',
        name: 'הודעות פוש',
        type: 'new',
        icon: 'basic-message-txt'
    }
];
var MenuItems = (function () {
    function MenuItems() {
    }
    MenuItems.prototype.getAll = function () {
        return MENUITEMS;
    };
    MenuItems.prototype.add = function (menu) {
        MENUITEMS.push(menu);
    };
    MenuItems = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])()
    ], MenuItems);
    return MenuItems;
}());

//# sourceMappingURL=menu-items.js.map

/***/ }),

/***/ "./src/app/shared/shared.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SharedModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__menu_items_menu_items__ = __webpack_require__("./src/app/shared/menu-items/menu-items.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__accordion__ = __webpack_require__("./src/app/shared/accordion/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__fullscreen_toggle_fullscreen_directive__ = __webpack_require__("./src/app/shared/fullscreen/toggle-fullscreen.directive.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var SharedModule = (function () {
    function SharedModule() {
    }
    SharedModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [__WEBPACK_IMPORTED_MODULE_2__accordion__["a" /* AccordionAnchorDirective */], __WEBPACK_IMPORTED_MODULE_2__accordion__["c" /* AccordionLinkDirective */], __WEBPACK_IMPORTED_MODULE_2__accordion__["b" /* AccordionDirective */], __WEBPACK_IMPORTED_MODULE_3__fullscreen_toggle_fullscreen_directive__["a" /* ToggleFullscreenDirective */]],
            exports: [__WEBPACK_IMPORTED_MODULE_2__accordion__["a" /* AccordionAnchorDirective */], __WEBPACK_IMPORTED_MODULE_2__accordion__["c" /* AccordionLinkDirective */], __WEBPACK_IMPORTED_MODULE_2__accordion__["b" /* AccordionDirective */], __WEBPACK_IMPORTED_MODULE_3__fullscreen_toggle_fullscreen_directive__["a" /* ToggleFullscreenDirective */]],
            providers: [__WEBPACK_IMPORTED_MODULE_1__menu_items_menu_items__["a" /* MenuItems */]]
        })
    ], SharedModule);
    return SharedModule;
}());

//# sourceMappingURL=shared.module.js.map

/***/ }),

/***/ "./src/environments/environment.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
var environment = {
    production: false
};
//# sourceMappingURL=environment.js.map

/***/ }),

/***/ "./src/main.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__("./node_modules/@angular/platform-browser-dynamic/@angular/platform-browser-dynamic.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__("./src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__("./src/environments/environment.ts");




if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["enableProdMode"])();
}
Object(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */])
    .catch(function (err) { return console.log(err); });
//# sourceMappingURL=main.js.map

/***/ }),

/***/ "./src/settings/settings.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SettingsService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_http__ = __webpack_require__("./node_modules/@angular/http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__ = __webpack_require__("./node_modules/rxjs/Rx.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var SettingsService = (function () {
    function SettingsService(http) {
        this.http = http;
        this.ServerUrl = "http://tapper.org.il/ezmath/laravel/public/api/";
        this.host = "http://tapper.org.il/ezmath/laravel/storage/app/public/";
        this.avatar = "http://tapper.org.il/ezmath/avatar.jpg";
    }
    ;
    SettingsService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__angular_http__["b" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_http__["b" /* Http */]) === "function" && _a || Object])
    ], SettingsService);
    return SettingsService;
    var _a;
}());

;
//# sourceMappingURL=settings.service.js.map

/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("./src/main.ts");


/***/ })

},[0]);
//# sourceMappingURL=main.bundle.js.map