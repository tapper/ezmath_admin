webpackJsonp(["order.module"],{

/***/ "./src/app/orders/add/add.component.css":
/***/ (function(module, exports) {

module.exports = ".FormClass\r\n{\r\n    text-align: right;\r\n    direction: rtl;\r\n}\r\n\r\n.row\r\n{\r\n    margin-top: 20px;\r\n}\r\n\r\n.textWhite\r\n{\r\n    background-color: white;\r\n}\r\n\r\ninput.ng-invalid.ng-touched\r\n{\r\n    border:1px solid red;\r\n}"

/***/ }),

/***/ "./src/app/orders/add/add.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"row FormClass\">\r\n  <div class=\"col-lg-12\">\r\n    <div class=\"card\">\r\n      <div class=\"card-header\">\r\n        הוסף חברה\r\n      </div>\r\n      <div class=\"card-body\">\r\n        <form (ngSubmit)=\"onSubmit(f)\" #f=\"ngForm\">\r\n          <div class=\"row\">\r\n            <div class=\"form-group\" class=\"col-lg-12\">\r\n              <label for=\"formGroupExampleInput\">הכנס שם חברה </label>\r\n              <input type=\"text\" class=\"form-control textWhite\" id=\"formGroupExampleInput\" name=\"name\" ngModel required>\r\n            </div>\r\n          </div>\r\n\r\n          <div class=\"row\">\r\n            <div class=\"form-group\" class=\"col-lg-6\">\r\n              <label for=\"formGroupExampleInput\">הכנס כתובת </label>\r\n              <input type=\"text\" class=\"form-control textWhite\" id=\"formGroupExampleInput\" name=\"address\"\r\n                     ngModel required>\r\n            </div>\r\n            <div class=\"form-group\" class=\"col-lg-6\">\r\n              <label for=\"formGroupExampleInput2\">הכנס טלפון</label>\r\n              <input type=\"text\" class=\"form-control textWhite\" id=\"formGroupExampleInput2\" name=\"phone\"\r\n                     ngModel required>\r\n            </div>\r\n          </div>\r\n\r\n          <div class=\"row\">\r\n            <div class=\"form-group\" class=\"col-lg-6\">\r\n              <label for=\"formGroupExampleInput\">הזן סיסמה</label>\r\n              <input type=\"text\" class=\"form-control textWhite\" id=\"formGroupExampleInput\" name=\"password\" ngModel required>\r\n            </div>\r\n            <div class=\"form-group\" class=\"col-lg-6\">\r\n              <label for=\"formGroupExampleInput2\">הכנס אימייל</label>\r\n              <input type=\"text\" class=\"form-control textWhite\" id=\"formGroupExampleInput2\" name=\"email\" ngModel required email>\r\n            </div>\r\n          </div>\r\n\r\n          <div class=\"row\">\r\n            <div class=\"form-group\" class=\"col-lg-6\">\r\n              <label for=\"formGroupExampleInput\">הכנס אתר אינטרנט</label>\r\n              <input type=\"text\" class=\"form-control textWhite\" id=\"formGroupExampleInput\" name=\"website\"\r\n                     ngModel>\r\n            </div>\r\n            <div class=\"form-group\" class=\"col-lg-6\">\r\n              <label for=\"formGroupExampleInput2\">הכנס WAZE</label>\r\n              <input type=\"text\" class=\"form-control textWhite\" id=\"formGroupExampleInput2\" name=\"waze\"\r\n                     ngModel>\r\n            </div>\r\n          </div>\r\n          <div class=\"row\">\r\n            <div class=\"form-group\" class=\"col-lg-6\">\r\n              <label for=\"formGroupExampleInput\">הכנס פייסבוק</label>\r\n              <input type=\"text\" class=\"form-control textWhite\" id=\"formGroupExampleInput\" name=\"facebook\"\r\n                     ngModel>\r\n            </div>\r\n            <div class=\"form-group\" class=\"col-lg-6\">\r\n              <label for=\"formGroupExampleInput2\">הכנס subDomain</label>\r\n              <input type=\"text\" class=\"form-control textWhite\" id=\"formGroupExampleInput2\" name=\"subdomain\"\r\n                     ngModel required>\r\n            </div>\r\n          </div>\r\n          <div class=\"row\">\r\n            <div class=\"form-group\" class=\"col-lg-12\">\r\n              <label for=\"formGroupExampleInput\">פרטים נוספים</label>\r\n              <textarea  rows=\"4\" cols=\"50\"  class=\"form-control textWhite\" id=\"formGroupExampleInput\" name=\"description\" ngModel required> </textarea>\r\n            </div>\r\n          </div>\r\n          <div class=\"row\">\r\n            <label class=\"uploader\">\r\n              <img [src]=\"imageSrc\"  [class.loaded]=\"imageLoaded\" style=\"width: 100px;\"/>\r\n              <input type=\"file\" name=\"file\" accept=\"image/*\" (change)=\"handleInputChange($event)\">\r\n            </label>\r\n          </div>\r\n          <div class=\"row\" align=\"left\" style=\"float: left; margin-left: 5px\">\r\n            <button [disabled]=\"!f.valid\" type=\"submit\" class=\"btn btn-primary btn-icon loading-demo mr-1 mb-1\"\r\n                    style=\"padding:0 20px !important; cursor: pointer; width: 205px; text-align: center; font-weight: bold; font-size: 16px\">\r\n              <span style=\"text-align: center; padding: 10px;  width: 100% !important;\">שלח טופס</span>\r\n            </button>\r\n          </div>\r\n        </form>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./src/app/orders/add/add.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__("./node_modules/@angular/http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__order_service__ = __webpack_require__("./src/app/orders/order.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AddComponent = (function () {
    function AddComponent(route, http, service, router) {
        this.route = route;
        this.http = http;
        this.service = service;
        this.router = router;
        this.navigateTo = '/company/index';
        this.imageSrc = '';
    }
    AddComponent.prototype.onSubmit = function (form) {
        var _this = this;
        console.log(form.value);
        this.service.AddCompany('AddCompany1', form.value).then(function (data) {
            console.log("AddCompany : ", data);
            _this.router.navigate([_this.navigateTo]);
        });
    };
    AddComponent.prototype.handleInputChange = function (e) {
        var file = e.dataTransfer ? e.dataTransfer.files[0] : e.target.files[0];
        var pattern = /image-*/;
        var reader = new FileReader();
        if (!file.type.match(pattern)) {
            alert('invalid format');
            return;
        }
        reader.onload = this._handleReaderLoaded.bind(this);
        reader.readAsDataURL(file);
        console.log("2 : ", reader);
    };
    AddComponent.prototype._handleReaderLoaded = function (e) {
        var reader = e.target;
        this.imageSrc = reader.result;
        console.log("3 : ", this.imageSrc);
        /* this.service.AddCompany('AddCompany1',form.value).then((data: any) => {
             console.log("AddCompany : " , data);
             this.router.navigate([this.navigateTo]);
         });*/
    };
    AddComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-add',
            template: __webpack_require__("./src/app/orders/add/add.component.html"),
            styles: [__webpack_require__("./src/app/orders/add/add.component.css")]
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__order_service__["a" /* OrderService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__order_service__["a" /* OrderService */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */]) === "function" && _d || Object])
    ], AddComponent);
    return AddComponent;
    var _a, _b, _c, _d;
}());

//# sourceMappingURL=add.component.js.map

/***/ }),

/***/ "./src/app/orders/edit/edit.component.css":
/***/ (function(module, exports) {

module.exports = ".FormClass\r\n{\r\n    text-align: right;\r\n    direction: rtl;\r\n}\r\n\r\n.row\r\n{\r\n    margin-top: 20px;\r\n}\r\n\r\n.textWhite\r\n{\r\n    background-color: white;\r\n}\r\n\r\ninput.ng-invalid.ng-touched\r\n{\r\n    border:1px solid red;\r\n}\r\n\r\n.SearchInput\r\n{\r\n    background-color: white;\r\n    text-align: right;\r\n}\r\n\r\n.p-3\r\n{\r\n    padding: 0px;\r\n    background-color: red;\r\n}\r\n\r\n.KitchensForm\r\n{\r\n    direction: rtl;\r\n    text-align: right;\r\n}\r\n\r\n.formCheck\r\n{\r\n    position: relative;\r\n    left:20px;\r\n    text-align: right;\r\n    width: 50px;\r\n    background-color: red;\r\n}"

/***/ }),

/***/ "./src/app/orders/edit/edit.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"row FormClass\">\r\n    <div class=\"col-lg-12\">\r\n        <div class=\"card\">\r\n            <div class=\"card-header\">\r\n                ערוך חברה\r\n            </div>\r\n            <div class=\"card-body\">\r\n                <form (ngSubmit)=\"onSubmit(f)\" #f=\"ngForm\" >\r\n                    <div class=\"row\">\r\n                        <div class=\"form-group\" class=\"col-lg-12\">\r\n                            <label for=\"formGroupExampleInput\">הכנס שם חברה </label>\r\n                            <input type=\"text\" class=\"form-control textWhite\" [(ngModel)]=\"Company.name\"\r\n                                   id=\"formGroupExampleInput\" name=\"name\" ngModel required>\r\n                        </div>\r\n\r\n                    </div>\r\n\r\n                    <div class=\"row\">\r\n                        <div class=\"form-group\" class=\"col-lg-6\">\r\n                            <label for=\"formGroupExampleInput\">הכנס כתובת </label>\r\n                            <input type=\"text\" class=\"form-control textWhite\" [(ngModel)]=\"Company.address\"\r\n                                   id=\"formGroupExampleInput\" name=\"address\"\r\n                                   ngModel>\r\n                        </div>\r\n                        <div class=\"form-group\" class=\"col-lg-6\">\r\n                            <label for=\"formGroupExampleInput2\">הכנס טלפון</label>\r\n                            <input type=\"text\" class=\"form-control textWhite\" [(ngModel)]=\"Company.phone\"\r\n                                   id=\"formGroupExampleInput2\" name=\"phone\"\r\n                                   ngModel>\r\n                        </div>\r\n                    </div>\r\n\r\n\r\n                    <div class=\"row\">\r\n                        <div class=\"form-group\" class=\"col-lg-6\">\r\n                            <label for=\"formGroupExampleInput\">הזן סיסמה</label>\r\n                            <input type=\"text\" class=\"form-control textWhite\" [(ngModel)]=\"Company.password\"\r\n                                   id=\"formGroupExampleInput\" name=\"pass\" ngModel>\r\n                        </div>\r\n                        <div class=\"form-group\" class=\"col-lg-6\">\r\n                            <label for=\"formGroupExampleInput2\">הכנס אימייל</label>\r\n                            <input type=\"text\" class=\"form-control textWhite\" [(ngModel)]=\"Company.email\"\r\n                                   id=\"formGroupExampleInput2\" name=\"email\" ngModel required email>\r\n                        </div>\r\n                    </div>\r\n\r\n                    <div class=\"row\">\r\n                        <div class=\"form-group\" class=\"col-lg-6\">\r\n                            <label for=\"formGroupExampleInput\">מחיר מנה</label>\r\n                            <input type=\"text\" class=\"form-control textWhite\" [(ngModel)]=\"Company.company_price\"\r\n                                   id=\"formGroupExampleInput\" name=\"web\"\r\n                                   ngModel>\r\n                        </div>\r\n                        <div class=\"form-group\" class=\"col-lg-6\">\r\n\r\n                        </div>\r\n                    </div>\r\n\r\n                    <hr>\r\n                    <div>\r\n                        <p>מטבחים השייכים לחברה</p>\r\n                        <label class=\"KitchensForm custom-control custom-radio\" style=\"overflow: hidden; margin-right: 50px;\">\r\n                            <label class=\"form-check-label\" *ngFor=\"let kitchen of Kitchens let i=index\"  style=\"margin-right: 20px;\">\r\n                                <input type=\"checkbox\" name=\"kitchen\"  (change)=\"changeKitchen(kitchen.index)\" [checked]=\"checkKitchen(i) == 1\" class=\"formCheck\"  style=\"background-color: red !important;\">\r\n                                <span style=\"position: relative; left: 30px; margin-top: -5px;\">{{kitchen.name}}</span>\r\n                            </label>\r\n                        </label>\r\n                    </div>\r\n                    <hr>\r\n\r\n                    <div class=\"row\">\r\n                        <div class=\"form-group\" class=\"col-lg-12\">\r\n                            <label for=\"formGroupExampleInput\">פרטים נוספים</label>\r\n                            <textarea rows=\"4\" cols=\"50\" class=\"form-control textWhite\" id=\"formGroupExampleInput\" [(ngModel)]=\"Company.desc\" name=\"description\" ngModel required> </textarea>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"row\">\r\n                        <label class=\"uploader\">\r\n                            <img [src]=\"imageSrc\" [class.loaded]=\"imageLoaded\" style=\"width: 100px;\"/>\r\n                            <input type=\"file\" name=\"file\" accept=\"image/*\" (change)=\"handleInputChange($event)\">\r\n                        </label>\r\n                    </div>\r\n                    <div class=\"row\" align=\"left\" style=\"float: left; margin-left: 5px\">\r\n                        <button [disabled]=\"!f.valid\" type=\"submit\"\r\n                                class=\"btn btn-primary btn-icon loading-demo mr-1 mb-1\"\r\n                                style=\"padding:0 20px !important; cursor: pointer; width: 205px; text-align: center; font-weight: bold; font-size: 16px\">\r\n                            <span style=\"text-align: center; padding: 10px;  width: 100% !important;\">שלח טופס</span>\r\n                        </button>\r\n                    </div>\r\n                </form>\r\n\r\n\r\n                <!-- <img src=\"{{imageSrc}}\" style=\"width: 100%\" /> <div class=\"row\" align=\"left\"  style=\"float: left; margin-left: 5px\">\r\n                     <button type=\"button\"  class=\"btn btn-primary btn-icon loading-demo mr-1 mb-1\" style=\"padding:0 20px !important; cursor: pointer; width: 205px; text-align: center; font-weight: bold; font-size: 16px\">\r\n                         <span style=\"text-align: center; padding: 10px;  width: 100% !important;\">שלח טופס</span>\r\n                     </button>\r\n                 </div>-->\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>"

/***/ }),

/***/ "./src/app/orders/edit/edit.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EditComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__("./node_modules/@angular/http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__order_service__ = __webpack_require__("./src/app/orders/order.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var URL = 'https://evening-anchorage-3159.herokuapp.com/api/';
var EditComponent = (function () {
    function EditComponent(route, http, service, router) {
        var _this = this;
        this.route = route;
        this.http = http;
        this.service = service;
        this.router = router;
        this.navigateTo = '/company/index';
        this.imageSrc = '';
        this.Company = [];
        this.CompanyKitchens = [];
        this.Kitchens = [];
        this.route.params.subscribe(function (params) {
            _this.Id = params['id'];
            _this.Company = _this.service.Companies[_this.Id];
            _this.Kitchens = _this.service.Kitchens;
            _this.CompanyKitchens = _this.Company['kitchens'];
            console.log("fkitchens ", _this.CompanyKitchens);
        });
    }
    EditComponent.prototype.onSubmit = function (form) {
        var _this = this;
        console.log("Edit : ", this.Company);
        this.service.EditCompany('EditCompany', this.Company).then(function (data) {
            console.log("AddCompany : ", data);
            _this.router.navigate([_this.navigateTo]);
        });
    };
    EditComponent.prototype.handleInputChange = function (e) {
        var file = e.dataTransfer ? e.dataTransfer.files[0] : e.target.files[0];
        var pattern = /image-*/;
        var reader = new FileReader();
        if (!file.type.match(pattern)) {
            alert('invalid format');
            return;
        }
        reader.onload = this._handleReaderLoaded.bind(this);
        reader.readAsDataURL(file);
        console.log("2 : ", reader);
    };
    EditComponent.prototype._handleReaderLoaded = function (e) {
        var reader = e.target;
        this.imageSrc = reader.result;
        console.log("3 : ", this.imageSrc);
    };
    EditComponent.prototype.checkKitchen = function (id) {
        var checked = 0;
        var kitchens1 = this.Company["kitchens"];
        for (var i = 0; i < kitchens1.length; i++) {
            if (kitchens1[i]['resturant_id'] == this.Kitchens[id]['index'])
                checked = 1;
        }
        return checked;
    };
    EditComponent.prototype.changeKitchen = function (id) {
        var exsist = 0;
        console.log("fkitchens1 ", this.CompanyKitchens);
        /*for(let i=0;i<this.CompanyKitchens.length; i++)
        {
            console.log(this.CompanyKitchens[i]['resturant_id']  + " : " + id)
            if(this.CompanyKitchens[i]['resturant_id'] == id)
            {
                console.log("splice" , this.CompanyKitchens);
                this.CompanyKitchens.splice(i, 1);
                exsist = 1;
                console.log("splice1" , this.CompanyKitchens);
            }
        }

        if(exsist == 0)
        {
            for(let j=0;j<this.Kitchens.length; j++)
            {
                console.log("Kitchen " , this.Kitchens[j]['index'] + " : "  + id );
                if(this.Kitchens[j]['index'] == id)
                {
                    this.CompanyKitchens.push(this.Kitchens[j])
                    console.log("splice2" , this.CompanyKitchens);
                }

            }
        }*/
    };
    EditComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-edit',
            template: __webpack_require__("./src/app/orders/edit/edit.component.html"),
            styles: [__webpack_require__("./src/app/icons/fontawesome/fontawesome.component.scss"), __webpack_require__("./src/app/components/buttons/buttons.component.scss"), __webpack_require__("./src/app/orders/edit/edit.component.css")]
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__order_service__["a" /* OrderService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__order_service__["a" /* OrderService */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */]) === "function" && _d || Object])
    ], EditComponent);
    return EditComponent;
    var _a, _b, _c, _d;
}());

//# sourceMappingURL=edit.component.js.map

/***/ }),

/***/ "./src/app/orders/index/index.component.css":
/***/ (function(module, exports) {

module.exports = ".card\r\n{\r\n    text-align: right;\r\n    direction: rtl;\r\n\r\n}\r\n\r\n.card-body\r\n{\r\n    border-bottom: 1px solid #f2f1f2;\r\n}\r\n\r\n.mr-auto\r\n{\r\n    text-align: right;\r\n    direction: rtl;\r\n    background-color: red;\r\n    float: right;\r\n}\r\n\r\n.mr-3\r\n{\r\n    background-color: green;\r\n    float: right;\r\n}\r\n\r\n.IconClass\r\n{\r\n    margin-top: 6px;\r\n    text-align: center;\r\n    padding-left: -13px !important;\r\n}\r\n\r\n.d-icon{\r\n    margin-top: -20px;\r\n}\r\n\r\n.titleImage\r\n{\r\n    width: 80px;\r\n    border-radius: 70%;\r\n    height:80px;\r\n    margin-top:3px;\r\n    border: 1px solid #f1f1f1;\r\n}\r\n\r\n.textHeader\r\n{\r\n    color: #337ab7;;\r\n    font-size: 15px;\r\n    font-weight: bold;\r\n}\r\n\r\n.SearchInput{\r\n    background-color: white;\r\n    text-align: right;\r\n    paddding:5px;\r\n    margin-bottom: -15px;\r\n    margin-top: -15px;\r\n}\r\n\r\n.p-3{\r\n    margin-top: 2px;\r\n    margin-bottom: 2px;\r\n}\r\n\r\n.sideButton\r\n{\r\n    width:90%;\r\n    cursor: pointer;\r\n    background-color: #3b5998;\r\n    color: white;\r\n    text-align: right;\r\n    padding: 3px;\r\n    overflow: hidden;\r\n}\r\n\r\n.sideButtonText\r\n{\r\n    margin-right: 10px;\r\n    font-size: 14px;\r\n    font-weight: bold;\r\n    top: 7px !important;\r\n    position: relative;\r\n}\r\n\r\n.sideButtonTextEmpty{\r\n    margin-right: 10px;\r\n    font-size: 14px;\r\n    font-weight: bold;\r\n    top: 0px !important;\r\n    position: relative;\r\n}\r\n\r\n.sideButtonBadge\r\n{\r\n    background-color: red;\r\n    border-radius:50%;\r\n    font-size: 12px;\r\n    margin-top: 5px;\r\n    padding: 3px;\r\n    width: 25px;\r\n    height: 25px;\r\n}\r\n\r\n.buttonDivBadge\r\n{\r\n    float: right;\r\n    width: 12%;\r\n}\r\n\r\n.buttonDivText\r\n{\r\n    float: right;\r\n    width: 60%;\r\n}\r\n\r\n.buttonDivIcon\r\n{\r\n    float: left;\r\n    width: 20%;\r\n}\r\n\r\n.badgeText\r\n{\r\n    top: 4px;\r\n    position: relative;\r\n}\r\n\r\n.fullscreen .datatable-row-center datatable-row-group\r\n{\r\n    border-bottom: 20px solid red;\r\n}\r\n\r\n.material {\r\n    vertical-align: bottom;\r\n    padding: 8px;\r\n    line-height: 1.42857143;\r\n    font-weight: bold;\r\n    color: rgb(51, 51, 51);\r\n    background-color: #ffffff;\r\n}\r\n\r\n.striped {\r\n    vertical-align: bottom;\r\n    padding: 8px;\r\n    line-height: 1.42857143;\r\n    font-weight: bold;\r\n    color: rgb(51, 51, 51);\r\n    height: 2000px;\r\n}\r\n\r\n.datatable-row-center\r\n{\r\n    float: right !important;\r\n    background-color: red !important;\r\n}\r\n\r\n.Rclass\r\n{\r\n    background-color: red !important;;\r\n}\r\n\r\n.Cell\r\n{\r\n    background-color: red !important;;\r\n}\r\n\r\n.getRowClass\r\n{\r\n    background-color: red !important;\r\n}\r\n\r\n.datatable,\r\n.datatable > div,\r\n.datatable.fixed-header .datatable-header .datatable-header-inner {\r\n    height: 100%;\r\n    direction: rtl;\r\n    text-align: right;\r\n    background-color: red;\r\n}\r\n\r\ndiv.dataTables_wrapper {\r\n    direction: rtl;\r\n    background-color: red;\r\n}\r\n\r\n/* Ensure that the demo table scrolls */\r\n\r\nth, td { white-space: nowrap; }\r\n\r\ndiv.dataTables_wrapper {\r\n    width: 800px;\r\n    margin: 0 auto;\r\n    background-color: red;\r\n}\r\n\r\n.datatable-header-inner\r\n{\r\n    direction: rtl;\r\n    text-align: right;\r\n    background-color: red;\r\n}\r\n\r\n.divRow\r\n{\r\n\r\n}\r\n\r\n.datatable-body-row.active .datatable-row-group {\r\n    background-color: red !important;\r\n}"

/***/ }),

/***/ "./src/app/orders/index/index.component.html":
/***/ (function(module, exports) {

module.exports = "\r\n<div class=\"row nopadding\" style=\"padding: 0px; background-color: white; margin-top: -15px; margin-left: -17px\">\r\n    <div class=\"col-lg-2 nopadding\" style=\"background-color: #eff1f1; padding: 0px\" >\r\n        <div style=\"margin-top: 20px; padding: 10px;\" align=\"center\">\r\n            <button [routerLink]=\"['/', 'company' , 'index']\"  class=\"btn btn-icon btn-facebook mb-1 mr-1 sideButton\" style=\"background-color: #666\">\r\n                <div class=\"buttonDivBadge\">\r\n\r\n                </div>\r\n                <div class=\"buttonDivText\">\r\n                    <span class=\"sideButtonTextEmpty\">חזר לעמוד חברות</span>\r\n                </div>\r\n                <div class=\"buttonDivIcon\">\r\n                    <i class=\"fa fa-chevron-left\"></i>\r\n                </div>\r\n            </button>\r\n\r\n            <hr>\r\n\r\n\r\n        </div>\r\n    </div>\r\n    <div class=\"col-lg-10 nopadding\" style=\"margin-top: 10px; height: 1000px\">\r\n        <div class=\"row\">\r\n            <div class=\"col-lg-12\">\r\n                <div class=\"col-lg-12\">\r\n                    <input type=\"text\" class=\"form-control mb-3\" placeholder=\"חפש חברה\" style=\"text-align: right; padding: 10px;\" required (keyup)='updateFilter($event)'>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"row\" style=\"margin-top: 10px; height: 1000px\">\r\n        <ngx-datatable\r\n                class=\"striped\"\r\n                [columnMode]=\"'force'\"\r\n                [headerHeight]=\"40\"\r\n                [footerHeight]=\"0\"\r\n                [rowHeight]=\"70\"\r\n                [scrollbarV]=\"true\"\r\n                [scrollbarH]=\"true\"\r\n                [rowClass]=\"getRowClass\"\r\n                (sort)=\"onSort($event)\"\r\n                [rows]=\"ItemsArray\">\r\n\r\n           <!-- <ngx-datatable-column name=\"icons\" [width]=\"100\" >\r\n                <ng-template let-column=\"column\" ngx-datatable-header-template>\r\n                    actions\r\n                </ng-template>\r\n                <ng-template let-rowIndex=\"rowIndex\" let-row=\"row\" let-value=\"value\" ngx-datatable-cell-template>\r\n                    <i class=\"fa fa-close IconClass\" style=\"\" (click)=\"DeleteItem()\"></i>\r\n                    <a [routerLink]=\"['/', 'company' , 'edit' , { id: rowIndex, foo: 'foo' }]\" class=\"navigation-link relative\"><i class=\"fa fa-edit IconClassMt3\" ></i></a>\r\n                </ng-template>\r\n            </ngx-datatable-column> -->\r\n            <ngx-datatable-column name=\"תאריך הזמנה\" prop=\"order_date\" [sortable]=\"true\" headerClass=\"Rclass\" cellClass=\"Cell\" [width]=\"130\" style=\"padding-top:30px !important; background-color: red\" >\r\n                <ng-template  let-row=\"row\" let-value=\"value\" ngx-datatable-cell-template>\r\n                    <div class=\"divRow\">{{row.order_date}}</div>\r\n                </ng-template>\r\n            </ngx-datatable-column>\r\n            <ngx-datatable-column  [sortable]=\"true\" style=\"padding-top:30px !important;\" [width]=\"100\" >\r\n                <ng-template   ngx-datatable-header-template><span >שתייה</span></ng-template>\r\n                <template  let-row=\"row\" let-value=\"value\" ngx-datatable-cell-template>\r\n                    <div *ngIf=\"row.DrinkArr[0]\">{{row.DrinkArr[0].name}}</div>\r\n                    <div *ngIf=\"!row.DrinkArr[0]\">---</div>\r\n                </template>\r\n            </ngx-datatable-column>\r\n            <ngx-datatable-column   [sortable]=\"true\" style=\"padding-top:30px !important;\" [width]=\"100\"  >\r\n                <ng-template   ngx-datatable-header-template><span >תוספת שנייה</span></ng-template>\r\n                <template  let-row=\"row\" let-value=\"value\" ngx-datatable-cell-template>\r\n                    <div *ngIf=\"row.Mod2firstArr[0]\">{{row.Mod2firstArr[0].name}}</div>\r\n                    <div *ngIf=\"!row.Mod2firstArr[0]\">לא נבחר</div>\r\n                </template>\r\n            </ngx-datatable-column>\r\n            <ngx-datatable-column  [sortable]=\"true\" style=\"padding-top:30px !important;\" >\r\n                <ng-template   ngx-datatable-header-template><span >תוספת ראשונה</span></ng-template>\r\n                <template  let-row=\"row\" let-value=\"value\" ngx-datatable-cell-template>\r\n                    <div *ngIf=\"row.Mod1Arr[0]\">{{row.Mod1Arr[0].name}}</div>\r\n                    <div *ngIf=\"!row.Mod1Arr[0]\">לא נבחר</div>\r\n                </template>\r\n            </ngx-datatable-column>\r\n            <ngx-datatable-column   style=\"padding-top:30px !important;\" [width]=\"150\">\r\n                <ng-template   ngx-datatable-header-template><span >מנה עיקרית</span></ng-template>\r\n                <template  let-row=\"row\" let-value=\"value\" ngx-datatable-cell-template>\r\n                    <div *ngIf=\"row.MainArr[0]\">{{row.MainArr[0].name}}</div>\r\n                    <div *ngIf=\"!row.MainArr[0]\">לא נבחר</div>\r\n                </template>\r\n            </ngx-datatable-column>\r\n            <ngx-datatable-column name=\"סוג הזמנה\" prop=\"FoodTypeName\"  [sortable]=\"true\" [width]=\"100\" style=\"padding-top:30px !important;\" >\r\n                <template let-row=\"row\" let-value=\"value\" ngx-datatable-cell-template>\r\n                    <div *ngIf=\"row.FoodTypeName\">{{row.FoodTypeName}}</div>\r\n                    <div *ngIf=\"!row.FoodTypeName\">לא נבחר</div>\r\n                </template>\r\n            </ngx-datatable-column>\r\n            <ngx-datatable-column  name=\"מסעדה\" prop=\"RestaurantName.name\"  style=\"padding-top:30px !important;\" [width]=\"120\" >\r\n                <template let-row=\"row\" let-value=\"value\" ngx-datatable-cell-template>\r\n                    <div *ngIf=\"row.RestaurantName\">{{row.RestaurantName[0].name}}</div>\r\n                    <div *ngIf=\"!row.RestaurantName\">לא נבחר</div>\r\n                </template>\r\n            </ngx-datatable-column>\r\n            <ngx-datatable-column  style=\"padding-top:30px !important;\" [width]=\"120\" >\r\n                <ng-template   ngx-datatable-header-template><span >שם לקוח</span></ng-template>\r\n                <template let-row=\"row\" let-value=\"value\" ngx-datatable-cell-template>\r\n                    <div *ngIf=\"row.MyUserId\">{{row.MyUserId[0].name}}</div>\r\n                    <div *ngIf=\"!row.MyUserId\">לא נבחר</div>\r\n                </template>\r\n            </ngx-datatable-column>\r\n            <ngx-datatable-column name=\"שם חברה\" prop=\"CompanyName[0].name\" style=\"padding-top:30px !important;\" [width]=\"120\">\r\n                <template let-row=\"row\" let-value=\"value\" ngx-datatable-cell-template>\r\n                    <div>{{row.CompanyName[0].name}}</div>\r\n                </template>\r\n            </ngx-datatable-column>\r\n            <ngx-datatable-column name=\"מספר הזמנה\" prop=\"index\" style=\"padding-top:30px !important;\" [width]=\"80\">\r\n                <template let-row=\"row\" let-value=\"value\" ngx-datatable-cell-template>\r\n                    <div>{{row.index}}</div>\r\n                </template>\r\n            </ngx-datatable-column>\r\n        </ngx-datatable>\r\n        </div>\r\n    </div>\r\n</div>\r\n\r\n\r\n\r\n\r\n\r\n\r\n"

/***/ }),

/***/ "./src/app/orders/index/index.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return IndexComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__order_service__ = __webpack_require__("./src/app/orders/order.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__settings_settings_service__ = __webpack_require__("./src/settings/settings.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__("./node_modules/@angular/router/@angular/router.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var IndexComponent = (function () {
    function IndexComponent(OrderService, settings, route) {
        var _this = this;
        this.OrderService = OrderService;
        this.settings = settings;
        this.route = route;
        this.ItemsArray = [];
        this.ItemsArray1 = [];
        this.host = '';
        this.route.params.subscribe(function (params) {
            _this.Id = params['id'];
            console.log("ssss : ", _this.Id);
            _this.OrderService.GetOrders('GetOrders', _this.Id).then(function (data) {
                console.log("Orders : ", data),
                    _this.ItemsArray = data,
                    _this.ItemsArray1 = data,
                    _this.host = settings.host;
                //console.log(this.ItemsArray[0].logo)
            });
        });
        this.OrderService.GetAllKitchens('GetAllKitchens').then(function (data) {
            console.log("GetAllKitchens : ", data);
        });
    }
    IndexComponent.prototype.ngOnInit = function () {
    };
    IndexComponent.prototype.DeleteItem = function (i) {
        console.log("Del 1 : ", this.ItemsArray[i].id);
        /* this.OrderService.DeleteCompany('DeleteCompany', this.ItemsArray[i].id).then((data: any) => {
             this.ItemsArray = data , console.log("Del 2 : ", data);
         })*/
    };
    IndexComponent.prototype.updateFilter = function (event) {
        /* const val = event.target.value;
         // filter our data
         const temp = this.ItemsArray1.filter(function (d) {
             return d.CompanyName[0]['name'].toLowerCase().indexOf(val) !== -1 || !val;
         });
         // update the rows
         this.ItemsArray = temp;*/
    };
    IndexComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-index',
            template: __webpack_require__("./src/app/orders/index/index.component.html"),
            styles: [__webpack_require__("./src/app/icons/fontawesome/fontawesome.component.scss"), __webpack_require__("./src/app/media/list/list.component.scss"), __webpack_require__("./src/app/orders/index/index.component.css")]
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__order_service__["a" /* OrderService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__order_service__["a" /* OrderService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__settings_settings_service__["a" /* SettingsService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__settings_settings_service__["a" /* SettingsService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__angular_router__["a" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__angular_router__["a" /* ActivatedRoute */]) === "function" && _c || Object])
    ], IndexComponent);
    return IndexComponent;
    var _a, _b, _c;
}());

//# sourceMappingURL=index.component.js.map

/***/ }),

/***/ "./src/app/orders/order.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrderModule", function() { return OrderModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common__ = __webpack_require__("./node_modules/@angular/common/@angular/common.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__order_routing__ = __webpack_require__("./src/app/orders/order.routing.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__swimlane_ngx_datatable__ = __webpack_require__("./node_modules/@swimlane/ngx-datatable/release/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__swimlane_ngx_datatable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4__swimlane_ngx_datatable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__order_service__ = __webpack_require__("./src/app/orders/order.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_http__ = __webpack_require__("./node_modules/@angular/http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__edit_edit_component__ = __webpack_require__("./src/app/orders/edit/edit.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__index_index_component__ = __webpack_require__("./src/app/orders/index/index.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__add_add_component__ = __webpack_require__("./src/app/orders/add/add.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ng_bootstrap_ng_bootstrap__ = __webpack_require__("./node_modules/@ng-bootstrap/ng-bootstrap/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_ng2_file_upload__ = __webpack_require__("./node_modules/ng2-file-upload/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_ng2_file_upload___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_11_ng2_file_upload__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__angular_forms__ = __webpack_require__("./node_modules/@angular/forms/@angular/forms.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};













var OrderModule = (function () {
    function OrderModule() {
    }
    OrderModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_2__angular_common__["CommonModule"],
                __WEBPACK_IMPORTED_MODULE_1__angular_router__["d" /* RouterModule */].forChild(__WEBPACK_IMPORTED_MODULE_3__order_routing__["a" /* OrderRoutes */]),
                __WEBPACK_IMPORTED_MODULE_4__swimlane_ngx_datatable__["NgxDatatableModule"],
                __WEBPACK_IMPORTED_MODULE_6__angular_http__["c" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_10__ng_bootstrap_ng_bootstrap__["g" /* NgbModule */],
                __WEBPACK_IMPORTED_MODULE_11_ng2_file_upload__["FileUploadModule"],
                __WEBPACK_IMPORTED_MODULE_12__angular_forms__["FormsModule"]
            ],
            declarations: [
                __WEBPACK_IMPORTED_MODULE_7__edit_edit_component__["a" /* EditComponent */],
                __WEBPACK_IMPORTED_MODULE_8__index_index_component__["a" /* IndexComponent */],
                __WEBPACK_IMPORTED_MODULE_9__add_add_component__["a" /* AddComponent */]
            ],
            providers: [__WEBPACK_IMPORTED_MODULE_5__order_service__["a" /* OrderService */]]
        })
    ], OrderModule);
    return OrderModule;
}());

//# sourceMappingURL=order.module.js.map

/***/ }),

/***/ "./src/app/orders/order.routing.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OrderRoutes; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__edit_edit_component__ = __webpack_require__("./src/app/orders/edit/edit.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__add_add_component__ = __webpack_require__("./src/app/orders/add/add.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__index_index_component__ = __webpack_require__("./src/app/orders/index/index.component.ts");



var OrderRoutes = [{
        path: '',
        children: [{
                path: 'index',
                component: __WEBPACK_IMPORTED_MODULE_2__index_index_component__["a" /* IndexComponent */],
                data: {
                    heading: 'Company'
                }
            }, {
                path: 'edit',
                component: __WEBPACK_IMPORTED_MODULE_0__edit_edit_component__["a" /* EditComponent */],
                data: {
                    heading: 'Edit Company'
                }
            }, {
                path: 'add',
                component: __WEBPACK_IMPORTED_MODULE_1__add_add_component__["a" /* AddComponent */],
                data: {
                    heading: 'Edit Company'
                }
            }]
    }];
//# sourceMappingURL=order.routing.js.map

/***/ }),

/***/ "./src/app/orders/order.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OrderService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_http__ = __webpack_require__("./node_modules/@angular/http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__ = __webpack_require__("./node_modules/rxjs/Rx.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__settings_settings_service__ = __webpack_require__("./src/settings/settings.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




//public this.ServerUrl = "";//http://www.tapper.co.il/salecar/laravel/public/api/";
var OrderService = (function () {
    function OrderService(http, settings) {
        this.http = http;
        this.ServerUrl = "";
        this.headers = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/x-www-form-urlencoded' });
        this.options = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["f" /* RequestOptions */]({ headers: this.headers });
        this.Orders = [];
        this.Companies = [];
        this.CompanyArray = [];
        this.Kitchens = [];
        this.ServerUrl = settings.ServerUrl;
    }
    ;
    OrderService.prototype.GetOrders = function (url, CompanyId) {
        var _this = this;
        var body = new FormData();
        body.append('uid', window.localStorage.identify);
        body.append('cid', CompanyId);
        return this.http.post(this.ServerUrl + '' + url, body).map(function (res) { return res.json(); }).do(function (data) { _this.Orders = data; }).toPromise();
    };
    OrderService.prototype.GetAllKitchens = function (url) {
        var _this = this;
        var body = new FormData();
        body.append('uid', window.localStorage.identify);
        return this.http.post(this.ServerUrl + '' + url, body).map(function (res) { return res.json(); }).do(function (data) { _this.Kitchens = data; }).toPromise();
    };
    OrderService.prototype.AddCompany = function (url, Company) {
        this.CompanyArray = Company;
        var body = 'company=' + JSON.stringify(this.CompanyArray);
        return this.http.post(this.ServerUrl + '' + url, body, this.options).map(function (res) { return res; }).do(function (data) { }).toPromise();
    };
    OrderService.prototype.EditCompany = function (url, Company) {
        this.CompanyArray = Company;
        var body = 'company=' + JSON.stringify(this.CompanyArray);
        return this.http.post(this.ServerUrl + '' + url, body, this.options).map(function (res) { return res; }).do(function (data) { }).toPromise();
    };
    OrderService.prototype.DeleteCompany = function (url, Id) {
        var body = 'id=' + Id;
        return this.http.post(this.ServerUrl + '' + url, body, this.options).map(function (res) { return res.json(); }).do(function (data) { }).toPromise();
    };
    OrderService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__angular_http__["b" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_http__["b" /* Http */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_3__settings_settings_service__["a" /* SettingsService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__settings_settings_service__["a" /* SettingsService */]) === "function" && _b || Object])
    ], OrderService);
    return OrderService;
    var _a, _b;
}());

;
//# sourceMappingURL=order.service.js.map

/***/ })

});
//# sourceMappingURL=order.module.chunk.js.map