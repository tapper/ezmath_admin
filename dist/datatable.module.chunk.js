webpackJsonp(["datatable.module"],{

/***/ "./src/app/datatable/datatable.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DatatableModule", function() { return DatatableModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common__ = __webpack_require__("./node_modules/@angular/common/@angular/common.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__swimlane_ngx_datatable__ = __webpack_require__("./node_modules/@swimlane/ngx-datatable/release/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__swimlane_ngx_datatable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3__swimlane_ngx_datatable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__datatable_routing__ = __webpack_require__("./src/app/datatable/datatable.routing.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__data_table_data_table_component__ = __webpack_require__("./src/app/datatable/data-table/data-table.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__table_editing_table_editing_component__ = __webpack_require__("./src/app/datatable/table-editing/table-editing.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__table_filter_table_filter_component__ = __webpack_require__("./src/app/datatable/table-filter/table-filter.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__table_paging_table_paging_component__ = __webpack_require__("./src/app/datatable/table-paging/table-paging.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__table_pinning_table_pinning_component__ = __webpack_require__("./src/app/datatable/table-pinning/table-pinning.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__table_selection_table_selection_component__ = __webpack_require__("./src/app/datatable/table-selection/table-selection.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__table_sorting_table_sorting_component__ = __webpack_require__("./src/app/datatable/table-sorting/table-sorting.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};












var DatatableModule = (function () {
    function DatatableModule() {
    }
    DatatableModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_2__angular_common__["CommonModule"],
                __WEBPACK_IMPORTED_MODULE_1__angular_router__["d" /* RouterModule */].forChild(__WEBPACK_IMPORTED_MODULE_4__datatable_routing__["a" /* DatatableRoutes */]),
                __WEBPACK_IMPORTED_MODULE_3__swimlane_ngx_datatable__["NgxDatatableModule"]
            ],
            declarations: [
                __WEBPACK_IMPORTED_MODULE_5__data_table_data_table_component__["a" /* DataTableComponent */],
                __WEBPACK_IMPORTED_MODULE_6__table_editing_table_editing_component__["a" /* TableEditingComponent */],
                __WEBPACK_IMPORTED_MODULE_7__table_filter_table_filter_component__["a" /* TableFilterComponent */],
                __WEBPACK_IMPORTED_MODULE_8__table_paging_table_paging_component__["a" /* TablePagingComponent */],
                __WEBPACK_IMPORTED_MODULE_9__table_pinning_table_pinning_component__["a" /* TablePinningComponent */],
                __WEBPACK_IMPORTED_MODULE_10__table_selection_table_selection_component__["a" /* TableSelectionComponent */],
                __WEBPACK_IMPORTED_MODULE_11__table_sorting_table_sorting_component__["a" /* TableSortingComponent */]
            ]
        })
    ], DatatableModule);
    return DatatableModule;
}());

//# sourceMappingURL=datatable.module.js.map

/***/ }),

/***/ "./src/app/datatable/datatable.routing.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DatatableRoutes; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__data_table_data_table_component__ = __webpack_require__("./src/app/datatable/data-table/data-table.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__table_editing_table_editing_component__ = __webpack_require__("./src/app/datatable/table-editing/table-editing.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__table_filter_table_filter_component__ = __webpack_require__("./src/app/datatable/table-filter/table-filter.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__table_paging_table_paging_component__ = __webpack_require__("./src/app/datatable/table-paging/table-paging.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__table_pinning_table_pinning_component__ = __webpack_require__("./src/app/datatable/table-pinning/table-pinning.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__table_selection_table_selection_component__ = __webpack_require__("./src/app/datatable/table-selection/table-selection.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__table_sorting_table_sorting_component__ = __webpack_require__("./src/app/datatable/table-sorting/table-sorting.component.ts");







var DatatableRoutes = [
    {
        path: '',
        children: [{
                path: 'fullscreen',
                component: __WEBPACK_IMPORTED_MODULE_0__data_table_data_table_component__["a" /* DataTableComponent */],
                data: {
                    heading: 'Fullscreen'
                }
            }, {
                path: 'editing',
                component: __WEBPACK_IMPORTED_MODULE_1__table_editing_table_editing_component__["a" /* TableEditingComponent */],
                data: {
                    heading: 'Editing'
                }
            }, {
                path: 'filter',
                component: __WEBPACK_IMPORTED_MODULE_2__table_filter_table_filter_component__["a" /* TableFilterComponent */],
                data: {
                    heading: 'Filter'
                }
            }, {
                path: 'paging',
                component: __WEBPACK_IMPORTED_MODULE_3__table_paging_table_paging_component__["a" /* TablePagingComponent */],
                data: {
                    heading: 'Paging'
                }
            }, {
                path: 'pinning',
                component: __WEBPACK_IMPORTED_MODULE_4__table_pinning_table_pinning_component__["a" /* TablePinningComponent */],
                data: {
                    heading: 'Pinning'
                }
            }, {
                path: 'selection',
                component: __WEBPACK_IMPORTED_MODULE_5__table_selection_table_selection_component__["a" /* TableSelectionComponent */],
                data: {
                    heading: 'Selection'
                }
            }, {
                path: 'sorting',
                component: __WEBPACK_IMPORTED_MODULE_6__table_sorting_table_sorting_component__["a" /* TableSortingComponent */],
                data: {
                    heading: 'Sorting'
                }
            }]
    }
];
//# sourceMappingURL=datatable.routing.js.map

/***/ })

});
//# sourceMappingURL=datatable.module.chunk.js.map