webpackJsonp(["components.module"],{

/***/ "./src/app/components/accordion/accordion.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"row mb-3\">\r\n  <div class=\"col-md-6\">\r\n    <ngb-accordion #acc=\"ngbAccordion\" activeIds=\"ngb-panel-0\">\r\n      <ngb-panel title=\"Simple\">\r\n        <ng-template ngbPanelContent>\r\n          Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia\r\n          aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor,\r\n          sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica,\r\n          craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings\r\n          occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus\r\n          labore sustainable VHS.\r\n        </ng-template>\r\n      </ngb-panel>\r\n      <ngb-panel>\r\n        <ng-template ngbPanelTitle>\r\n          <span>&#9733; <b>Fancy</b> title &#9733;</span>\r\n        </ng-template>\r\n        <ng-template ngbPanelContent>\r\n          Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia\r\n          aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor,\r\n          sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica,\r\n          craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings\r\n          occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus\r\n          labore sustainable VHS.\r\n        </ng-template>\r\n      </ngb-panel>\r\n      <ngb-panel title=\"Disabled\" [disabled]=\"true\">\r\n        <ng-template ngbPanelContent>\r\n          Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia\r\n          aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor,\r\n          sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica,\r\n          craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings\r\n          occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus\r\n          labore sustainable VHS.\r\n        </ng-template>\r\n      </ngb-panel>\r\n    </ngb-accordion>\r\n  </div>\r\n  <div class=\"col-md-6\">\r\n    <ngb-accordion [closeOthers]=\"true\" activeIds=\"1\">\r\n      <ngb-panel id=\"1\" title=\"Simple\">\r\n        <ng-template ngbPanelContent>\r\n          Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia\r\n          aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor,\r\n          sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica,\r\n          craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings\r\n          occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus\r\n          labore sustainable VHS.\r\n        </ng-template>\r\n      </ngb-panel>\r\n      <ngb-panel id=\"2\">\r\n        <ng-template ngbPanelTitle>\r\n          <span>&#9733; <b>Fancy</b> title &#9733;</span>\r\n        </ng-template>\r\n        <ng-template ngbPanelContent>\r\n          Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia\r\n          aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor,\r\n          sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica,\r\n          craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings\r\n          occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus\r\n          labore sustainable VHS.\r\n        </ng-template>\r\n      </ngb-panel>\r\n      <ngb-panel id=\"3\" title=\"Disabled\" [disabled]=\"true\">\r\n        <ng-template ngbPanelContent>\r\n          Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia\r\n          aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor,\r\n          sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica,\r\n          craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings\r\n          occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus\r\n          labore sustainable VHS.\r\n        </ng-template>\r\n      </ngb-panel>\r\n    </ngb-accordion>\r\n  </div>\r\n</div>\r\n<div class=\"row\">\r\n  <div class=\"col-md-6\">\r\n    <ngb-accordion #acc=\"ngbAccordion\">\r\n      <ngb-panel id=\"1\" title=\"First panel\">\r\n        <ng-template ngbPanelContent>\r\n          Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia\r\n          aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor,\r\n          sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica,\r\n          craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings\r\n          occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus\r\n          labore sustainable VHS.\r\n        </ng-template>\r\n      </ngb-panel>\r\n      <ngb-panel id=\"2\" title=\"Second\">\r\n        <ng-template ngbPanelContent>\r\n          Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia\r\n          aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor,\r\n          sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica,\r\n          craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings\r\n          occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus\r\n          labore sustainable VHS.\r\n        </ng-template>\r\n      </ngb-panel>\r\n      <ngb-panel title=\"Disabled\" [disabled]=\"true\">\r\n        <ng-template ngbPanelContent>\r\n          Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia\r\n          aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor,\r\n          sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica,\r\n          craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings\r\n          occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus\r\n          labore sustainable VHS.\r\n        </ng-template>\r\n      </ngb-panel>\r\n    </ngb-accordion>\r\n    <p class=\"mt-2\">\r\n      <button class=\"btn btn-secondary\" (click)=\"acc.toggle('1')\">Toggle first</button>\r\n      <button class=\"btn btn-secondary\" (click)=\"acc.toggle('2')\">Toggle second</button>\r\n    </p>\r\n  </div>\r\n  <div class=\"col-md-6\">\r\n    <ngb-accordion (panelChange)=\"beforeChange($event)\">\r\n      <ngb-panel id=\"1\" title=\"Simple\">\r\n        <ng-template ngbPanelContent>\r\n          Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia\r\n          aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor,\r\n          sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica,\r\n          craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings\r\n          occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus\r\n          labore sustainable VHS.\r\n        </ng-template>\r\n      </ngb-panel>\r\n      <ngb-panel id=\"2\" title=\"I can't be toggled...\">\r\n        <ng-template ngbPanelContent>\r\n          Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia\r\n          aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor,\r\n          sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica,\r\n          craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings\r\n          occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus\r\n          labore sustainable VHS.\r\n        </ng-template>\r\n      </ngb-panel>\r\n      <ngb-panel id=\"3\" title=\"I can be opened, but not closed...\">\r\n        <ng-template ngbPanelContent>\r\n          Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia\r\n          aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor,\r\n          sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica,\r\n          craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings\r\n          occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus\r\n          labore sustainable VHS.\r\n        </ng-template>\r\n      </ngb-panel>\r\n    </ngb-accordion>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./src/app/components/accordion/accordion.component.scss":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/components/accordion/accordion.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AccordionComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AccordionComponent = (function () {
    function AccordionComponent() {
    }
    AccordionComponent.prototype.beforeChange = function ($event) {
        if ($event.panelId === '2') {
            $event.preventDefault();
        }
        if ($event.panelId === '3' && $event.nextState === false) {
            $event.preventDefault();
        }
    };
    AccordionComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-accordion',
            template: __webpack_require__("./src/app/components/accordion/accordion.component.html"),
            styles: [__webpack_require__("./src/app/components/accordion/accordion.component.scss")]
        })
    ], AccordionComponent);
    return AccordionComponent;
}());

//# sourceMappingURL=accordion.component.js.map

/***/ }),

/***/ "./src/app/components/alert/alert.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"card\">\r\n  <div class=\"card-header\">Notification Alerts</div>\r\n  <div class=\"card-body\">\r\n    <p>Provide contextual feedback messages for typical user actions with the handful of available and flexible alert messages.<p>\r\n    <ngb-alert [dismissible]=\"false\">\r\n      <strong>Warning!</strong> Better check yourself, you're not looking too good.\r\n    </ngb-alert>\r\n\r\n    <p class=\"mt-4\">Closeable Alert</p>\r\n    <div *ngFor=\"let alert of alerts\">\r\n      <ngb-alert [type]=\"alert.type\" (close)=\"closeAlert(alert)\">{{ alert.message }}</ngb-alert>\r\n    </div>\r\n    <p>\r\n      <button type=\"button\" class=\"btn btn-primary\" (click)=\"reset()\">Reset</button>\r\n    </p>\r\n\r\n    <p class=\"mt-4\">Self-Closing Alert</p>\r\n    <div>Static self-closing alert that disappears after 20 seconds (refresh the page if it has already disappeared)</div>\r\n    <ngb-alert *ngIf=\"!staticAlertClosed\" (close)=\"staticAlertClosed = true\">Check out our awesome new features!</ngb-alert>\r\n\r\n    <hr/>\r\n\r\n    <p>Show a self-closing success message that disappears after 5 seconds.</p>\r\n    <ngb-alert *ngIf=\"successMessage\" type=\"success\" (close)=\"successMessage = null\">{{ successMessage }}</ngb-alert>\r\n    <p>\r\n      <button class=\"btn btn-primary\" (click)=\"changeSuccessMessage()\">Change message</button>\r\n    </p>\r\n\r\n    <p class=\"mt-4\">Closeable Alert</p>\r\n    <ngb-alert type=\"custom\" [dismissible]=\"false\"><strong>Whoa!</strong> This is a custom alert.</ngb-alert>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/components/alert/alert.component.scss":
/***/ (function(module, exports) {

module.exports = ":host > > > .alert-custom {\n  color: #99004d;\n  background-color: #f169b4;\n  border-color: #800040; }\n"

/***/ }),

/***/ "./src/app/components/alert/alert.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AlertComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Subject__ = __webpack_require__("./node_modules/rxjs/Subject.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Subject___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs_Subject__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_debounceTime__ = __webpack_require__("./node_modules/rxjs/add/operator/debounceTime.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_debounceTime___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_debounceTime__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AlertComponent = (function () {
    function AlertComponent() {
        this.alerts = [];
        this._success = new __WEBPACK_IMPORTED_MODULE_1_rxjs_Subject__["Subject"]();
        this.staticAlertClosed = false;
        this.alerts.push({
            id: 1,
            type: 'success',
            message: 'This is an success alert',
        }, {
            id: 2,
            type: 'info',
            message: 'This is an info alert',
        }, {
            id: 3,
            type: 'warning',
            message: 'This is a warning alert',
        }, {
            id: 4,
            type: 'danger',
            message: 'This is a danger alert',
        });
        this.backup = this.alerts.map(function (alert) { return Object.assign({}, alert); });
    }
    AlertComponent.prototype.closeAlert = function (alert) {
        var index = this.alerts.indexOf(alert);
        this.alerts.splice(index, 1);
    };
    AlertComponent.prototype.reset = function () {
        this.alerts = this.backup.map(function (alert) { return Object.assign({}, alert); });
    };
    AlertComponent.prototype.ngOnInit = function () {
        var _this = this;
        setTimeout(function () { return _this.staticAlertClosed = true; }, 20000);
        this._success.subscribe(function (message) { return _this.successMessage = message; });
        this._success.debounceTime(5000).subscribe(function () { return _this.successMessage = null; });
    };
    AlertComponent.prototype.changeSuccessMessage = function () {
        this._success.next(new Date() + " - Message successfully changed.");
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], AlertComponent.prototype, "alerts", void 0);
    AlertComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-alert',
            template: __webpack_require__("./src/app/components/alert/alert.component.html"),
            styles: [__webpack_require__("./src/app/components/alert/alert.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], AlertComponent);
    return AlertComponent;
}());

//# sourceMappingURL=alert.component.js.map

/***/ }),

/***/ "./src/app/components/buttons/buttons.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\r\n  <div class=\"col-md-6\">\r\n    <div class=\"card\">\r\n      <div class=\"card-header\">Button options</div>\r\n      <div class=\"card-body\">\r\n        <p>Bootstrap includes six predefined button styles, each serving its own semantic purpose.</p>\r\n        <button class=\"btn btn-secondary mr-1 mb-1\">Secondary</button>\r\n        <button type=\"button\" class=\"btn btn-primary mr-1 mb-1\">Primary</button>\r\n        <button type=\"button\" class=\"btn btn-success mr-1 mb-1\">Success</button>\r\n        <button type=\"button\" class=\"btn btn-info mr-1 mb-1\">Info</button>\r\n        <button type=\"button\" class=\"btn btn-warning mr-1 mb-1\">Warning</button>\r\n        <button type=\"button\" class=\"btn btn-danger mr-1 mb-1\">Danger</button>\r\n        <button type=\"button\" class=\"btn btn-primary mr-1 mb-1\" disabled=\"\">Disabled</button>\r\n        <button type=\"button\" class=\"btn btn-link mr-1 mb-1\">Link</button>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <div class=\"col-md-6\">\r\n    <div class=\"card\">\r\n      <div class=\"card-header\">Outline</div>\r\n      <div class=\"card-body\">\r\n        <p>\r\n          In need of a button, but not the hefty background colors they bring? Replace the default modifier classes with the\r\n          <code>\r\n            .btn-outline-*\r\n          </code>\r\n          ones to remove all background images and colors on any button.\r\n        </p>\r\n        <button class=\"btn btn-outline-secondary mr-1 mb-1\">Secondary</button>\r\n        <button type=\"button\" class=\"btn btn-outline-primary mr-1 mb-1\">Primary</button>\r\n        <button type=\"button\" class=\"btn btn-outline-success mr-1 mb-1\">Success</button>\r\n        <button type=\"button\" class=\"btn btn-outline-info mr-1 mb-1\">Info</button>\r\n        <button type=\"button\" class=\"btn btn-outline-warning mr-1 mb-1\">Warning</button>\r\n        <button type=\"button\" class=\"btn btn-outline-danger mr-1 mb-1\">Danger</button>\r\n        <button type=\"button\" class=\"btn btn-outline-primary mr-1 mb-1\" disabled=\"\">Disabled</button>\r\n        <button type=\"button\" class=\"btn btn-link mr-1 mb-1\">Link</button>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n\r\n<div class=\"row\">\r\n  <div class=\"col-md-6\">\r\n    <div class=\"card\">\r\n      <div class=\"card-header\">Checkbox and radio buttons</div>\r\n      <div class=\"card-body\">\r\n        <div [(ngModel)]=\"radioModel\" ngbRadioGroup name=\"radioBasic\">\r\n          <label class=\"btn btn-secondary btn-sm\">\r\n            <input type=\"radio\" [value]=\"1\"> Left (pre-checked)\r\n          </label>\r\n          <label class=\"btn btn-secondary btn-sm\">\r\n            <input type=\"radio\" value=\"middle\"> Middle\r\n          </label>\r\n          <label class=\"btn btn-secondary btn-sm\">\r\n            <input type=\"radio\" [value]=\"false\"> Right\r\n          </label>\r\n          <label class=\"btn btn-secondary btn-sm\">\r\n            <input type=\"radio\" disabled=\"\"> {{radioModel}}\r\n          </label>\r\n        </div>\r\n\r\n        <p>Check-box-like button group can be easily done with pure Angular, no custom directives necessary.\r\n          But we include demo here for completeness.</p>\r\n        <div class=\"btn-group btn-group-sm\" data-toggle=\"buttons\">\r\n          <label class=\"btn btn-secondary\" [class.active]=\"checkboxModel.left\">\r\n            <input type=\"checkbox\" [(ngModel)]=\"checkboxModel.left\"> Left (pre-checked)\r\n          </label>\r\n          <label class=\"btn btn-secondary\" [class.active]=\"checkboxModel.middle\">\r\n            <input type=\"checkbox\" [(ngModel)]=\"checkboxModel.middle\"> Middle\r\n          </label>\r\n          <label class=\"btn btn-secondary\" [class.active]=\"checkboxModel.right\">\r\n            <input type=\"checkbox\" [(ngModel)]=\"checkboxModel.right\"> Right\r\n          </label>\r\n        </div>\r\n        <pre>{{checkboxModel | json}}</pre>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <div class=\"col-md-6\">\r\n    <div class=\"card\">\r\n      <div class=\"card-header\">Sizing</div>\r\n      <div class=\"card-body\">\r\n        <p>Fancy larger or smaller buttons? Add <code>.btn-lg</code> or <code>.btn-sm</code> for additional sizes.</p>\r\n        <p>\r\n          <a href=\"javascript:;\" class=\"btn btn-secondary btn-lg mr-1\">Large button</a>\r\n          &nbsp;\r\n          <a href=\"javascript:;\" class=\"btn btn-outline-info btn-lg mr-1\">Large button</a>\r\n        </p>\r\n        <p>\r\n          <a href=\"javascript:;\" class=\"btn btn-secondary mr-1\">Default button</a>\r\n          &nbsp;\r\n          <a href=\"javascript:;\" class=\"btn btn-outline-primary mr-1\">Default button</a>\r\n        </p>\r\n        <p>\r\n          <a href=\"javascript:;\" class=\"btn btn-secondary btn-sm mr-1\">Small button</a>\r\n          &nbsp;\r\n          <a href=\"javascript:;\" class=\"btn btn-outline-danger btn-sm mr-1\">Small button</a>\r\n        </p>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n\r\n<div class=\"card\">\r\n  <div class=\"card-header\">Button styles</div>\r\n  <div class=\"card-body\">\r\n    <div class=\"row\">\r\n      <div class=\"col-md-6\">\r\n        <p>Button icons</p>\r\n        <p>Custom bootstrap buttons with icons</p>\r\n        <button type=\"button\" class=\"btn btn-primary btn-icon loading-demo mr-1 mb-1\">\r\n          <i class=\"icon icon-basic-paperplane\"></i>\r\n          <span>Submit</span>\r\n        </button>\r\n        <button type=\"button\" class=\"btn btn-danger btn-icon mr-1 mb-1\">\r\n          <i class=\"icon icon-basic-ban\"></i>\r\n          <span>Warning</span>\r\n        </button>\r\n        <button type=\"button\" class=\"btn btn-info btn-icon mr-1 mb-1\">\r\n          <i class=\"icon icon-basic-server-upload\"></i>\r\n          <span>Upload</span>\r\n        </button>\r\n        <button class=\"btn btn-success btn-icon-icon mr-1 mb-1\">\r\n          <i class=\"icon icon-basic-heart\"></i>\r\n        </button>\r\n      </div>\r\n      <div class=\"col-md-6\">\r\n        <p>Groups</p>\r\n        <p>Wrap a series of buttons with .btn in .btn-group.</p>\r\n        <div class=\"btn-group\">\r\n          <button type=\"button\" class=\"btn btn-secondary\">Left</button>\r\n          <button type=\"button\" class=\"btn btn-secondary\">Middle</button>\r\n          <button type=\"button\" class=\"btn btn-secondary\">Right</button>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n\r\n<div class=\"row\">\r\n  <div class=\"col-md-6\">\r\n    <div class=\"card\">\r\n      <div class=\"card-header\">Toolbar</div>\r\n      <div class=\"card-body\">\r\n        <p>Combine sets of button groups into button toolbars for more complex components.</p>\r\n        <div class=\"btn-toolbar\" role=\"toolbar\">\r\n          <div class=\"btn-group mr-1 mb-1\">\r\n            <button type=\"button\" class=\"btn btn-success\">1</button>\r\n            <button type=\"button\" class=\"btn btn-success\">2</button>\r\n            <button type=\"button\" class=\"btn btn-success\">3</button>\r\n            <button type=\"button\" class=\"btn btn-success\">4</button>\r\n          </div>\r\n          <div class=\"btn-group mr-1 mb-1\">\r\n            <button type=\"button\" class=\"btn btn-primary\">5</button>\r\n            <button type=\"button\" class=\"btn btn-primary\">6</button>\r\n            <button type=\"button\" class=\"btn btn-primary\">7</button>\r\n          </div>\r\n          <div class=\"btn-group mr-1 mb-1\">\r\n            <button type=\"button\" class=\"btn btn-warning\">8</button>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <div class=\"col-md-6\">\r\n    <div class=\"card\">\r\n      <div class=\"card-header\">Nesting</div>\r\n      <div class=\"card-body\">\r\n        <p>\r\n          Place a\r\n          <code>.btn-group</code>\r\n          within another\r\n          <code>.btn-group</code>\r\n          when you want dropdown menus mixed with a series of buttons.\r\n        </p>\r\n        <div class=\"btn-group\">\r\n          <button type=\"button\" class=\"btn btn-secondary\">1</button>\r\n          <button type=\"button\" class=\"btn btn-secondary\">2</button>\r\n          <div class=\"dropdown btn-group\" ngbDropdown>\r\n            <button type=\"button\" class=\"btn btn-secondary dropdown-toggle\" ngbDropdownToggle>\r\n              Dropdown\r\n              <span class=\"caret\"></span>\r\n            </button>\r\n            <div class=\"dropdown-menu\" role=\"menu\">\r\n              <a class=\"dropdown-item\" href=\"#\">Action</a>\r\n              <a class=\"dropdown-item\" href=\"#\">Another action</a>\r\n              <a class=\"dropdown-item\" href=\"#\">Something else here</a>\r\n              <div class=\"dropdown-divider\"></div>\r\n              <a class=\"dropdown-item\" href=\"#\">Separated link</a>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n\r\n<div class=\"row\">\r\n  <div class=\"col-md-6\">   \r\n    <div class=\"card\">\r\n      <div class=\"card-header\">Block level buttons</div>\r\n      <div class=\"card-body\">\r\n        <p>Create block level buttons—those that span the full width of a parent—by adding .btn-block.</p>\r\n        <button type=\"button\" class=\"btn btn-danger btn-lg btn-block mb-1\">\r\n          <span>Block level button</span>\r\n        </button>\r\n        <button type=\"button\" class=\"btn btn-info btn-block mb-1\">\r\n          <span>Block level button</span>\r\n        </button>\r\n        <button type=\"button\" class=\"btn btn-primary btn-sm btn-block mb-1\">\r\n          <span>Block level button</span>\r\n        </button>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <div class=\"col-md-6\">\r\n    <div class=\"card\">\r\n      <div class=\"card-header\">Vertical groups</div>\r\n      <div class=\"card-body\">\r\n        <p>Make a set of buttons appear vertically stacked rather than horizontally. Split button dropdowns are not supported here.</p>\r\n        <div class=\"btn-group-vertical\">\r\n          <button type=\"button\" class=\"btn btn-secondary\">Top</button>\r\n          <button type=\"button\" class=\"btn btn-secondary\">Middle</button>\r\n          <button type=\"button\" class=\"btn btn-secondary\">Bottom</button>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./src/app/components/buttons/buttons.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ButtonsComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var ButtonsComponent = (function () {
    function ButtonsComponent() {
        this.radioModel = 1;
        this.checkboxModel = {
            left: true,
            middle: false,
            right: false
        };
    }
    ButtonsComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-buttons',
            template: __webpack_require__("./src/app/components/buttons/buttons.component.html"),
            styles: [__webpack_require__("./src/app/components/buttons/buttons.component.scss")]
        })
    ], ButtonsComponent);
    return ButtonsComponent;
}());

//# sourceMappingURL=buttons.component.js.map

/***/ }),

/***/ "./src/app/components/carousel/carousel.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"card\">\r\n  <div class=\"card-header\">Carousel</div>\r\n  <div class=\"card-body\">\r\n    <div class=\"d-flex align-items-center justify-content-center\">\r\n      <ngb-carousel>\r\n        <ng-template ngbSlide>\r\n          <img src=\"assets/images/unsplash/16.jpg\" alt=\"Random first slide\">\r\n          <div class=\"carousel-caption\">\r\n            <h3 class=\"ff-serif text-uppercase\">First slide label</h3>\r\n            <p class=\"mb-0\">Nulla vitae elit libero, a pharetra augue mollis interdum.</p>\r\n          </div>\r\n        </ng-template>\r\n        <ng-template ngbSlide>\r\n          <img src=\"assets/images/unsplash/17.jpg\" alt=\"Random second slide\">\r\n          <div class=\"carousel-caption\">\r\n            <h3 class=\"ff-serif text-uppercase\">Second slide label</h3>\r\n            <p class=\"mb-0\">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>\r\n          </div>\r\n        </ng-template>\r\n        <ng-template ngbSlide>\r\n          <img src=\"assets/images/unsplash/21.jpg\" alt=\"Random third slide\">\r\n          <div class=\"carousel-caption\">\r\n            <h3 class=\"ff-serif text-uppercase\">Third slide label</h3>\r\n            <p class=\"mb-0\">Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>\r\n          </div>\r\n        </ng-template>\r\n      </ngb-carousel>\r\n    </div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./src/app/components/carousel/carousel.component.scss":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/components/carousel/carousel.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CarouselComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var CarouselComponent = (function () {
    function CarouselComponent() {
    }
    CarouselComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-carousel',
            template: __webpack_require__("./src/app/components/carousel/carousel.component.html"),
            styles: [__webpack_require__("./src/app/components/carousel/carousel.component.scss")]
        })
    ], CarouselComponent);
    return CarouselComponent;
}());

//# sourceMappingURL=carousel.component.js.map

/***/ }),

/***/ "./src/app/components/collapse/collapse.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"card\">\r\n  <div class=\"card-header\">Collapse</div>\r\n  <div class=\"card-body\">\r\n    <button type=\"button\" class=\"btn btn-primary mb-3\" (click)=\"isCollapsed = !isCollapsed\" [attr.aria-expanded]=\"!isCollapsed\" aria-controls=\"collapseExample\">Toggle</button>\r\n    <div id=\"collapseExample\" [ngbCollapse]=\"isCollapsed\">\r\n      <div class=\"card\">\r\n        <div>You can collapse this card by clicking Toggle.<br/> Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.</div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./src/app/components/collapse/collapse.component.scss":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/components/collapse/collapse.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CollapseComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var CollapseComponent = (function () {
    function CollapseComponent() {
        this.isCollapsed = false;
    }
    CollapseComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-collapse',
            template: __webpack_require__("./src/app/components/collapse/collapse.component.html"),
            styles: [__webpack_require__("./src/app/components/collapse/collapse.component.scss")]
        })
    ], CollapseComponent);
    return CollapseComponent;
}());

//# sourceMappingURL=collapse.component.js.map

/***/ }),

/***/ "./src/app/components/components.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ComponentsModule", function() { return ComponentsModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common__ = __webpack_require__("./node_modules/@angular/common/@angular/common.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__("./node_modules/@angular/forms/@angular/forms.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_http__ = __webpack_require__("./node_modules/@angular/http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ng_bootstrap_ng_bootstrap__ = __webpack_require__("./node_modules/@ng-bootstrap/ng-bootstrap/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__components_routing__ = __webpack_require__("./src/app/components/components.routing.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__buttons_buttons_component__ = __webpack_require__("./src/app/components/buttons/buttons.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__progress_progress_component__ = __webpack_require__("./src/app/components/progress/progress.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pagination_pagination_component__ = __webpack_require__("./src/app/components/pagination/pagination.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__spinners_spinners_component__ = __webpack_require__("./src/app/components/spinners/spinners.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__accordion_accordion_component__ = __webpack_require__("./src/app/components/accordion/accordion.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__alert_alert_component__ = __webpack_require__("./src/app/components/alert/alert.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__carousel_carousel_component__ = __webpack_require__("./src/app/components/carousel/carousel.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__collapse_collapse_component__ = __webpack_require__("./src/app/components/collapse/collapse.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__datepicker_datepicker_component__ = __webpack_require__("./src/app/components/datepicker/datepicker.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__dropdown_dropdown_component__ = __webpack_require__("./src/app/components/dropdown/dropdown.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__modal_modal_component__ = __webpack_require__("./src/app/components/modal/modal.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__popover_popover_component__ = __webpack_require__("./src/app/components/popover/popover.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__rating_rating_component__ = __webpack_require__("./src/app/components/rating/rating.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__tabs_tabs_component__ = __webpack_require__("./src/app/components/tabs/tabs.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__timepicker_timepicker_component__ = __webpack_require__("./src/app/components/timepicker/timepicker.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__tooltip_tooltip_component__ = __webpack_require__("./src/app/components/tooltip/tooltip.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__typeahead_typeahead_component__ = __webpack_require__("./src/app/components/typeahead/typeahead.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__button_icons_button_icons_component__ = __webpack_require__("./src/app/components/button-icons/button-icons.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

























var ComponentsModule = (function () {
    function ComponentsModule() {
    }
    ComponentsModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_2__angular_common__["CommonModule"],
                __WEBPACK_IMPORTED_MODULE_1__angular_router__["d" /* RouterModule */].forChild(__WEBPACK_IMPORTED_MODULE_6__components_routing__["a" /* ComponentsRoutes */]),
                __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormsModule"],
                __WEBPACK_IMPORTED_MODULE_3__angular_forms__["ReactiveFormsModule"],
                __WEBPACK_IMPORTED_MODULE_4__angular_http__["e" /* JsonpModule */],
                __WEBPACK_IMPORTED_MODULE_5__ng_bootstrap_ng_bootstrap__["g" /* NgbModule */]
            ],
            declarations: [
                __WEBPACK_IMPORTED_MODULE_7__buttons_buttons_component__["a" /* ButtonsComponent */],
                __WEBPACK_IMPORTED_MODULE_8__progress_progress_component__["a" /* ProgressComponent */],
                __WEBPACK_IMPORTED_MODULE_9__pagination_pagination_component__["a" /* PaginationComponent */],
                __WEBPACK_IMPORTED_MODULE_10__spinners_spinners_component__["a" /* SpinnersComponent */],
                __WEBPACK_IMPORTED_MODULE_11__accordion_accordion_component__["a" /* AccordionComponent */],
                __WEBPACK_IMPORTED_MODULE_12__alert_alert_component__["a" /* AlertComponent */],
                __WEBPACK_IMPORTED_MODULE_13__carousel_carousel_component__["a" /* CarouselComponent */],
                __WEBPACK_IMPORTED_MODULE_14__collapse_collapse_component__["a" /* CollapseComponent */],
                __WEBPACK_IMPORTED_MODULE_15__datepicker_datepicker_component__["a" /* DatepickerComponent */],
                __WEBPACK_IMPORTED_MODULE_16__dropdown_dropdown_component__["a" /* DropdownComponent */],
                __WEBPACK_IMPORTED_MODULE_17__modal_modal_component__["a" /* ModalComponent */],
                __WEBPACK_IMPORTED_MODULE_18__popover_popover_component__["a" /* PopoverComponent */],
                __WEBPACK_IMPORTED_MODULE_19__rating_rating_component__["a" /* RatingComponent */],
                __WEBPACK_IMPORTED_MODULE_20__tabs_tabs_component__["a" /* TabsComponent */],
                __WEBPACK_IMPORTED_MODULE_21__timepicker_timepicker_component__["a" /* TimepickerComponent */],
                __WEBPACK_IMPORTED_MODULE_22__tooltip_tooltip_component__["a" /* TooltipComponent */],
                __WEBPACK_IMPORTED_MODULE_23__typeahead_typeahead_component__["a" /* TypeaheadComponent */],
                __WEBPACK_IMPORTED_MODULE_24__button_icons_button_icons_component__["a" /* ButtonIconsComponent */]
            ]
        })
    ], ComponentsModule);
    return ComponentsModule;
}());

//# sourceMappingURL=components.module.js.map

/***/ }),

/***/ "./src/app/components/components.routing.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ComponentsRoutes; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__buttons_buttons_component__ = __webpack_require__("./src/app/components/buttons/buttons.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__progress_progress_component__ = __webpack_require__("./src/app/components/progress/progress.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__pagination_pagination_component__ = __webpack_require__("./src/app/components/pagination/pagination.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__spinners_spinners_component__ = __webpack_require__("./src/app/components/spinners/spinners.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__accordion_accordion_component__ = __webpack_require__("./src/app/components/accordion/accordion.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__alert_alert_component__ = __webpack_require__("./src/app/components/alert/alert.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__carousel_carousel_component__ = __webpack_require__("./src/app/components/carousel/carousel.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__collapse_collapse_component__ = __webpack_require__("./src/app/components/collapse/collapse.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__datepicker_datepicker_component__ = __webpack_require__("./src/app/components/datepicker/datepicker.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__dropdown_dropdown_component__ = __webpack_require__("./src/app/components/dropdown/dropdown.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__modal_modal_component__ = __webpack_require__("./src/app/components/modal/modal.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__popover_popover_component__ = __webpack_require__("./src/app/components/popover/popover.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__rating_rating_component__ = __webpack_require__("./src/app/components/rating/rating.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__tabs_tabs_component__ = __webpack_require__("./src/app/components/tabs/tabs.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__timepicker_timepicker_component__ = __webpack_require__("./src/app/components/timepicker/timepicker.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__tooltip_tooltip_component__ = __webpack_require__("./src/app/components/tooltip/tooltip.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__typeahead_typeahead_component__ = __webpack_require__("./src/app/components/typeahead/typeahead.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__button_icons_button_icons_component__ = __webpack_require__("./src/app/components/button-icons/button-icons.component.ts");


















var ComponentsRoutes = [
    {
        path: '',
        children: [{
                path: 'buttons',
                component: __WEBPACK_IMPORTED_MODULE_0__buttons_buttons_component__["a" /* ButtonsComponent */],
                data: {
                    heading: 'Buttons'
                }
            }, {
                path: 'buttonicons',
                component: __WEBPACK_IMPORTED_MODULE_17__button_icons_button_icons_component__["a" /* ButtonIconsComponent */],
                data: {
                    heading: 'Social Button Icons'
                }
            }, {
                path: 'progress',
                component: __WEBPACK_IMPORTED_MODULE_1__progress_progress_component__["a" /* ProgressComponent */],
                data: {
                    heading: 'Progress bars'
                }
            }, {
                path: 'pagination',
                component: __WEBPACK_IMPORTED_MODULE_2__pagination_pagination_component__["a" /* PaginationComponent */],
                data: {
                    heading: 'Pagination'
                }
            }, {
                path: 'spinners',
                component: __WEBPACK_IMPORTED_MODULE_3__spinners_spinners_component__["a" /* SpinnersComponent */],
                data: {
                    heading: 'Spinner'
                }
            }, {
                path: 'accordion',
                component: __WEBPACK_IMPORTED_MODULE_4__accordion_accordion_component__["a" /* AccordionComponent */],
                data: {
                    heading: 'Accordion'
                }
            }, {
                path: 'alert',
                component: __WEBPACK_IMPORTED_MODULE_5__alert_alert_component__["a" /* AlertComponent */],
                data: {
                    heading: 'Alert'
                }
            }, {
                path: 'carousel',
                component: __WEBPACK_IMPORTED_MODULE_6__carousel_carousel_component__["a" /* CarouselComponent */],
                data: {
                    heading: 'Carousel'
                }
            }, {
                path: 'collapse',
                component: __WEBPACK_IMPORTED_MODULE_7__collapse_collapse_component__["a" /* CollapseComponent */],
                data: {
                    heading: 'Collapse'
                }
            }, {
                path: 'datepicker',
                component: __WEBPACK_IMPORTED_MODULE_8__datepicker_datepicker_component__["a" /* DatepickerComponent */],
                data: {
                    heading: 'Datepicker'
                }
            }, {
                path: 'dropdown',
                component: __WEBPACK_IMPORTED_MODULE_9__dropdown_dropdown_component__["a" /* DropdownComponent */],
                data: {
                    heading: 'Dropdown'
                }
            }, {
                path: 'modal',
                component: __WEBPACK_IMPORTED_MODULE_10__modal_modal_component__["a" /* ModalComponent */],
                data: {
                    heading: 'Modal'
                }
            }, {
                path: 'popover',
                component: __WEBPACK_IMPORTED_MODULE_11__popover_popover_component__["a" /* PopoverComponent */],
                data: {
                    heading: 'Popovers'
                }
            }, {
                path: 'rating',
                component: __WEBPACK_IMPORTED_MODULE_12__rating_rating_component__["a" /* RatingComponent */],
                data: {
                    heading: 'Rating'
                }
            }, {
                path: 'tabs',
                component: __WEBPACK_IMPORTED_MODULE_13__tabs_tabs_component__["a" /* TabsComponent */],
                data: {
                    heading: 'Tabs'
                }
            }, {
                path: 'timepicker',
                component: __WEBPACK_IMPORTED_MODULE_14__timepicker_timepicker_component__["a" /* TimepickerComponent */],
                data: {
                    heading: 'Timepicker'
                }
            }, {
                path: 'tooltip',
                component: __WEBPACK_IMPORTED_MODULE_15__tooltip_tooltip_component__["a" /* TooltipComponent */],
                data: {
                    heading: 'Tooltips'
                }
            }, {
                path: 'typeahead',
                component: __WEBPACK_IMPORTED_MODULE_16__typeahead_typeahead_component__["a" /* TypeaheadComponent */],
                data: {
                    heading: 'Typeahead'
                }
            }]
    }
];
//# sourceMappingURL=components.routing.js.map

/***/ }),

/***/ "./src/app/components/datepicker/datepicker.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\r\n  <div class=\"col-lg-6\">\r\n    <div class=\"card\">\r\n      <div class=\"card-header\">Simple datepicker</div>\r\n      <div class=\"card-body\">\r\n        <ngb-datepicker #dp [(ngModel)]=\"model\" (navigate)=\"date = $event.next\"></ngb-datepicker>\r\n\r\n        <div class=\"block mt-3 mb-3\">\r\n          <button class=\"btn btn-sm btn-outline-primary\" (click)=\"selectToday()\">Select Today</button>\r\n          <button class=\"btn btn-sm btn-outline-primary\" (click)=\"dp.navigateTo()\">To current month</button>\r\n          <button class=\"btn btn-sm btn-outline-primary\" (click)=\"dp.navigateTo({year: 2013, month: 2})\">To Feb 2013</button>\r\n        </div>\r\n\r\n        <pre>Month: {{ date.month }}.{{ date.year }}</pre>\r\n        <pre>Model: {{ model | json }}</pre>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <div class=\"col-lg-6\">\r\n    <div class=\"card\">\r\n      <div class=\"card-header\">Multiple months</div>\r\n      <div class=\"card-body\">\r\n        <ngb-datepicker [displayMonths]=\"displayMonths\" [navigation]=\"navigation\"></ngb-datepicker>\r\n\r\n        <p class=\"pt-3\">Inline</p>\r\n\r\n        <form class=\"form-inline\">\r\n          <div class=\"form-group\">\r\n            <div class=\"input-group\">\r\n              <input class=\"form-control\" placeholder=\"yyyy-mm-dd\"\r\n                    name=\"dp\" [displayMonths]=\"displayMonths\" [navigation]=\"navigation\" ngbDatepicker #d=\"ngbDatepicker\">\r\n              <div class=\"input-group-addon\" (click)=\"d.toggle()\" >\r\n                <i class=\"icon icon-basic-calendar\" style=\"cursor: pointer;\"></i>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </form>\r\n\r\n        <p class=\"pt-3\">Options</p>\r\n\r\n        <select class=\"custom-select\" [(ngModel)]=\"displayMonths\">\r\n          <option [ngValue]=\"1\">One month</option>\r\n          <option [ngValue]=\"2\">Two months</option>\r\n          <option [ngValue]=\"3\">Three months</option>\r\n        </select>\r\n\r\n        <select class=\"custom-select\" [(ngModel)]=\"navigation\">\r\n          <option value=\"none\">Without navigation</option>\r\n          <option value=\"select\">With select boxes</option>\r\n          <option value=\"arrows\">Without select boxes</option>\r\n        </select>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n\r\n<div class=\"row\">\r\n  <div class=\"col-lg-6\">\r\n    <div class=\"card\">\r\n      <div class=\"card-header\">Datepicker in a popup</div>\r\n      <div class=\"card-body\">\r\n        <form class=\"form-inline\">\r\n          <div class=\"form-group\">\r\n            <div class=\"input-group\">\r\n              <input class=\"form-control\" placeholder=\"yyyy-mm-dd\"\r\n                     name=\"dp\" [(ngModel)]=\"model\" ngbDatepicker #d2=\"ngbDatepicker\">\r\n              <div class=\"input-group-addon\" (click)=\"d2.toggle()\" >\r\n                <i class=\"icon icon-basic-calendar\" style=\"cursor: pointer;\"></i>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </form>\r\n        <pre>Model: {{ popupModel | json }}</pre>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <div class=\"col-lg-6\">\r\n    <div class=\"card\">\r\n      <div class=\"card-header\">Custom day view</div>\r\n      <div class=\"card-body\">\r\n        <p>This datepicker uses a custom template to display days. All week-ends are displayed with an orange background.</p>\r\n\r\n        <form class=\"form-inline\">\r\n          <div class=\"form-group\">\r\n            <div class=\"input-group\">\r\n              <input class=\"form-control\" placeholder=\"yyyy-mm-dd\"\r\n                     name=\"dp\" [(ngModel)]=\"model\" ngbDatepicker [dayTemplate]=\"customDay\" [markDisabled]=\"isDisabled\" #d3=\"ngbDatepicker\">\r\n              <div class=\"input-group-addon\" (click)=\"d3.toggle()\" >\r\n                <i class=\"icon icon-basic-calendar\" style=\"cursor: pointer;\"></i>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </form>\r\n\r\n        <ng-template #customDay let-date=\"date\" let-currentMonth=\"currentMonth\" let-selected=\"selected\" let-disabled=\"disabled\">\r\n          <span class=\"custom-day\" [class.weekend]=\"isWeekend(date)\"\r\n                [class.bg-primary]=\"selected\" [class.hidden]=\"date.month !== currentMonth\" [class.text-muted]=\"disabled\">\r\n            {{ date.day }}\r\n          </span>\r\n        </ng-template>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n\r\n<div class=\"row\">\r\n  <div class=\"col-lg-6\">\r\n    <div class=\"card\">\r\n      <div class=\"card-header\">Disabled datepicker</div>\r\n      <div class=\"card-body\">\r\n        <ngb-datepicker [(ngModel)]=\"disabledModel\" [disabled]=\"disabled\"></ngb-datepicker>\r\n\r\n        <div class=\"block mt-3\">\r\n          <button class=\"btn btn-sm btn-outline-{{disabled ? 'danger' : 'success'}}\" (click)=\"disabled = !disabled\">\r\n            {{ disabled ? \"disabled\" : \"enabled\"}}\r\n          </button>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <div class=\"col-lg-6\">\r\n    <div class=\"card\">\r\n      <div class=\"card-header\">Internationalization of datepickers</div>\r\n      <div class=\"card-body\">\r\n        <div class=\"block mb-3\">\r\n          <div [(ngModel)]=\"language\" ngbRadioGroup>\r\n            <label class=\"btn btn-primary btn-sm\">\r\n              <input type=\"radio\" value=\"en\"> English\r\n            </label>\r\n            <label class=\"btn btn-primary btn-sm\">\r\n              <input type=\"radio\" value=\"fr\"> Français\r\n            </label>\r\n          </div>\r\n        </div>\r\n        <ngb-datepicker [(ngModel)]=\"intModel\"></ngb-datepicker>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./src/app/components/datepicker/datepicker.component.scss":
/***/ (function(module, exports) {

module.exports = ".custom-day {\n  text-align: center;\n  padding: 0.185rem 0.25rem;\n  border-radius: 0;\n  display: inline-block;\n  width: 2rem; }\n\n.custom-day:hover {\n  background-color: #e6e6e6; }\n\n.weekend {\n  background-color: #f0ad4e;\n  border-radius: 0;\n  color: white; }\n\n.hidden {\n  display: none; }\n"

/***/ }),

/***/ "./src/app/components/datepicker/datepicker.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export I18n */
/* unused harmony export CustomDatepickerI18n */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DatepickerComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ng_bootstrap_ng_bootstrap__ = __webpack_require__("./node_modules/@ng-bootstrap/ng-bootstrap/index.js");
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var now = new Date();
var I18N_VALUES = {
    en: {
        weekdays: ['Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa', 'Su'],
        months: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
    },
    fr: {
        weekdays: ['Lu', 'Ma', 'Me', 'Je', 'Ve', 'Sa', 'Di'],
        months: ['Jan', 'Fév', 'Mar', 'Avr', 'Mai', 'Juin', 'Juil', 'Aou', 'Sep', 'Oct', 'Nov', 'Déc'],
    }
};
// Define a service holding the language. You probably already have one if your app is i18ned.
var I18n = (function () {
    function I18n() {
        this.language = 'en';
    }
    I18n = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])()
    ], I18n);
    return I18n;
}());

// Define custom service providing the months and weekdays translations
var CustomDatepickerI18n = (function (_super) {
    __extends(CustomDatepickerI18n, _super);
    function CustomDatepickerI18n(_i18n) {
        var _this = _super.call(this) || this;
        _this._i18n = _i18n;
        return _this;
    }
    CustomDatepickerI18n.prototype.getWeekdayShortName = function (weekday) {
        return I18N_VALUES[this._i18n.language].weekdays[weekday - 1];
    };
    CustomDatepickerI18n.prototype.getMonthShortName = function (month) {
        return I18N_VALUES[this._i18n.language].months[month - 1];
    };
    CustomDatepickerI18n.prototype.getMonthFullName = function (month) {
        return this.getMonthShortName(month);
    };
    CustomDatepickerI18n = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [I18n])
    ], CustomDatepickerI18n);
    return CustomDatepickerI18n;
}(__WEBPACK_IMPORTED_MODULE_1__ng_bootstrap_ng_bootstrap__["e" /* NgbDatepickerI18n */]));

var DatepickerComponent = (function () {
    function DatepickerComponent(_i18n) {
        this._i18n = _i18n;
        this.displayMonths = 2;
        this.navigation = 'select';
        this.disabledModel = { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() };
        this.disabled = true;
    }
    Object.defineProperty(DatepickerComponent.prototype, "language", {
        get: function () {
            return this._i18n.language;
        },
        set: function (language) {
            this._i18n.language = language;
        },
        enumerable: true,
        configurable: true
    });
    DatepickerComponent.prototype.selectToday = function () {
        this.model = { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() };
    };
    DatepickerComponent.prototype.isWeekend = function (date) {
        var d = new Date(date.year, date.month - 1, date.day);
        return d.getDay() === 0 || d.getDay() === 6;
    };
    DatepickerComponent.prototype.isDisabled = function (date, current) {
        return date.month !== current.month;
    };
    DatepickerComponent.prototype.ngOnInit = function () {
        this.selectToday();
    };
    DatepickerComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-datepicker',
            template: __webpack_require__("./src/app/components/datepicker/datepicker.component.html"),
            styles: [__webpack_require__("./src/app/components/datepicker/datepicker.component.scss")],
            providers: [I18n, { provide: __WEBPACK_IMPORTED_MODULE_1__ng_bootstrap_ng_bootstrap__["e" /* NgbDatepickerI18n */], useClass: CustomDatepickerI18n }] // define custom NgbDatepickerI18n provider
        }),
        __metadata("design:paramtypes", [I18n])
    ], DatepickerComponent);
    return DatepickerComponent;
}());

//# sourceMappingURL=datepicker.component.js.map

/***/ }),

/***/ "./src/app/components/dropdown/dropdown.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"card\">\r\n  <div class=\"card-header\">Basic dropdown</div>\r\n  <div class=\"card-body\">\r\n    <div ngbDropdown class=\"d-inline-block\">\r\n      <button class=\"btn btn-outline-primary\" id=\"dropdownMenu1\" ngbDropdownToggle>Toggle dropdown</button>\r\n      <div ngbDropdownMenu class=\"dropdown-menu\" aria-labelledby=\"dropdownMenu1\">\r\n        <button class=\"dropdown-item\">Action - 1</button>\r\n        <button class=\"dropdown-item\">Another Action</button>\r\n        <button class=\"dropdown-item\">Something else is here</button>\r\n      </div>\r\n    </div>\r\n\r\n    <div ngbDropdown placement=\"top-right\" class=\"d-inline-block\">\r\n      <button class=\"btn btn-outline-primary\" id=\"dropdownMenu2\" ngbDropdownToggle>Toggle dropup</button>\r\n      <div ngbDropdownMenu class=\"dropdown-menu dropdown-menu-right\" aria-labelledby=\"dropdownMenu2\">\r\n        <button class=\"dropdown-item\">Action - 1</button>\r\n        <button class=\"dropdown-item\">Another Action</button>\r\n        <button class=\"dropdown-item\">Something else is here</button>\r\n      </div>\r\n    </div>\r\n\r\n    <p class=\"mt-4\">You can easily control dropdowns programmatically using the exported dropdown instance.</p>\r\n\r\n    <div class=\"d-inline-block\" ngbDropdown #myDrop=\"ngbDropdown\">\r\n      <button class=\"btn btn-outline-primary\" id=\"dropdownMenu1\" ngbDropdownToggle>Toggle dropdown</button>\r\n      <div ngbDropdownMenu class=\"dropdown-menu\" aria-labelledby=\"dropdownMenu1\">\r\n        <button class=\"dropdown-item\">Action - 1</button>\r\n        <button class=\"dropdown-item\">Another Action</button>\r\n        <button class=\"dropdown-item\">Something else is here</button>\r\n      </div>\r\n\r\n      <button class=\"btn btn-outline-success\" (click)=\"$event.stopPropagation(); myDrop.open();\">Open from outside</button>\r\n      <button class=\"btn btn-outline-danger\" (click)=\"$event.stopPropagation(); myDrop.close();\">Close from outside</button>\r\n    </div>\r\n\r\n    <p class=\"mt-4\">Manual triggers</p>\r\n    <p>You can easily control dropdowns programmatically using the exported dropdown instance.</p>\r\n\r\n    <div class=\"d-inline-block\" ngbDropdown #myDrop=\"ngbDropdown\">\r\n      <button class=\"btn btn-outline-primary\" id=\"dropdownMenu1\" ngbDropdownToggle>Toggle dropdown</button>\r\n      <div ngbDropdownMenu class=\"dropdown-menu\" aria-labelledby=\"dropdownMenu1\">\r\n        <button class=\"dropdown-item\">Action - 1</button>\r\n        <button class=\"dropdown-item\">Another Action</button>\r\n        <button class=\"dropdown-item\">Something else is here</button>\r\n      </div>\r\n\r\n      <button class=\"btn btn-outline-success\" (click)=\"$event.stopPropagation(); myDrop.open();\">Open from outside</button>\r\n      <button class=\"btn btn-outline-danger\" (click)=\"$event.stopPropagation(); myDrop.close();\">Close from outside</button>\r\n    </div>\r\n\r\n\r\n    <p class=\"mt-4\">Turn a button into a dropdown toggle with some basic markup changes.</p>\r\n    <div class=\"dropdown btn-group dropup mr-1 mb-1\" ngbDropdown>\r\n      <button type=\"button\" class=\"btn btn-danger\">Action</button>\r\n      <button type=\"button\" class=\"btn btn-danger dropdown-toggle\" ngbDropdownToggle aria-haspopup=\"true\" aria-expanded=\"false\">\r\n        <span class=\"sr-only\">Toggle Dropdown</span>\r\n      </button>\r\n      <div ngbDropdownMenu class=\"dropdown-menu\" role=\"menu\">\r\n        <a class=\"dropdown-item\" href=\"#\">Action</a>\r\n        <a class=\"dropdown-item\" href=\"#\">Another action</a>\r\n        <a class=\"dropdown-item\" href=\"#\">Something else here</a>\r\n        <div class=\"dropdown-divider\"></div>\r\n        <a class=\"dropdown-item\" href=\"#\">Separated link</a>\r\n      </div>\r\n    </div>\r\n    <div class=\"dropdown btn-group dropup mr-1 mb-1\" ngbDropdown>\r\n      <button type=\"button\" class=\"btn btn-outline-danger\">Action</button>\r\n      <button type=\"button\" class=\"btn btn-outline-danger dropdown-toggle\" ngbDropdownToggle aria-haspopup=\"true\" aria-expanded=\"false\">\r\n        <span class=\"sr-only\">Toggle Dropdown</span>\r\n      </button>\r\n      <div ngbDropdownMenu class=\"dropdown-menu\" role=\"menu\">\r\n        <a class=\"dropdown-item\" href=\"#\">Action</a>\r\n        <a class=\"dropdown-item\" href=\"#\">Another action</a>\r\n        <a class=\"dropdown-item\" href=\"#\">Something else here</a>\r\n        <div class=\"dropdown-divider\"></div>\r\n        <a class=\"dropdown-item\" href=\"#\">Separated link</a>\r\n      </div>\r\n    </div>\r\n    <div class=\"dropdown btn-group mr-1 mb-1\" ngbDropdown>\r\n      <button type=\"button\" class=\"btn btn-info dropdown-toggle\" ngbDropdownToggle>\r\n        Dropdown\r\n        <span class=\"caret\"></span>\r\n      </button>\r\n      <div ngbDropdownMenu class=\"dropdown-menu\" role=\"menu\">\r\n        <a class=\"dropdown-item\" href=\"#\">Action</a>\r\n        <a class=\"dropdown-item\" href=\"#\">Another action</a>\r\n        <a class=\"dropdown-item\" href=\"#\">Something else here</a>\r\n        <div class=\"dropdown-divider\"></div>\r\n        <a class=\"dropdown-item\" href=\"#\">Separated link</a>\r\n      </div>\r\n    </div>\r\n    <div class=\"dropdown btn-group mr-1 mb-1\" ngbDropdown>\r\n      <button type=\"button\" class=\"btn btn-outline-info dropdown-toggle\" ngbDropdownToggle>\r\n        Dropdown\r\n        <span class=\"caret\"></span>\r\n      </button>\r\n      <div ngbDropdownMenu class=\"dropdown-menu\" role=\"menu\">\r\n        <a class=\"dropdown-item\" href=\"#\">Action</a>\r\n        <a class=\"dropdown-item\" href=\"#\">Another action</a>\r\n        <a class=\"dropdown-item\" href=\"#\">Something else here</a>\r\n        <div class=\"dropdown-divider\"></div>\r\n        <a class=\"dropdown-item\" href=\"#\">Separated link</a>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/components/dropdown/dropdown.component.scss":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/components/dropdown/dropdown.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DropdownComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var DropdownComponent = (function () {
    function DropdownComponent() {
    }
    DropdownComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-dropdown',
            template: __webpack_require__("./src/app/components/dropdown/dropdown.component.html"),
            styles: [__webpack_require__("./src/app/components/dropdown/dropdown.component.scss")]
        })
    ], DropdownComponent);
    return DropdownComponent;
}());

//# sourceMappingURL=dropdown.component.js.map

/***/ }),

/***/ "./src/app/components/modal/modal.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"card\">\r\n  <div class=\"card-header\">Modal</div>\r\n  <div class=\"card-body\">\r\n    <button class=\"btn btn-secondary\" (click)=\"open(content)\">Modal with default options</button>\r\n    <pre>{{closeResult}}</pre>\r\n\r\n    <div role=\"document\" class=\"modal-dialog\">\r\n      <div class=\"modal-content\">\r\n        <div class=\"modal-header\">\r\n          <h6 class=\"modal-title text-uppercase\">Modal title</h6>\r\n          <button aria-label=\"Close\" class=\"close\" type=\"button\">\r\n            <span aria-hidden=\"true\">×</span>\r\n          </button>\r\n        </div>\r\n        <div class=\"modal-body\">\r\n          <p>One fine body…</p>\r\n        </div>\r\n        <div class=\"modal-footer\">\r\n          <button class=\"btn btn-secondary\" type=\"button\">Close</button>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n\r\n<ng-template ngbModalContainer></ng-template>\r\n<ng-template #content let-c=\"close\" let-d=\"dismiss\">\r\n  <div class=\"modal-header\">\r\n    <h6 class=\"modal-title text-uppercase\">Modal title</h6>\r\n    <button type=\"button\" class=\"close\" aria-label=\"Close\" (click)=\"d('Cross click')\">\r\n      <span aria-hidden=\"true\">&times;</span>\r\n    </button>\r\n  </div>\r\n  <div class=\"modal-body\">\r\n    <p>One fine body&hellip;</p>\r\n  </div>\r\n  <div class=\"modal-footer\">\r\n    <button type=\"button\" class=\"btn btn-secondary\" (click)=\"c('Close click')\">Close</button>\r\n  </div>\r\n</ng-template>"

/***/ }),

/***/ "./src/app/components/modal/modal.component.scss":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/components/modal/modal.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ModalComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ng_bootstrap_ng_bootstrap__ = __webpack_require__("./node_modules/@ng-bootstrap/ng-bootstrap/index.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ModalComponent = (function () {
    function ModalComponent(modalService) {
        this.modalService = modalService;
    }
    ModalComponent.prototype.open = function (content) {
        var _this = this;
        this.modalService.open(content).result.then(function (result) {
            _this.closeResult = "Closed with: " + result;
        }, function (reason) {
            _this.closeResult = "Dismissed " + _this.getDismissReason(reason);
        });
    };
    ModalComponent.prototype.getDismissReason = function (reason) {
        if (reason === __WEBPACK_IMPORTED_MODULE_1__ng_bootstrap_ng_bootstrap__["a" /* ModalDismissReasons */].ESC) {
            return 'by pressing ESC';
        }
        else if (reason === __WEBPACK_IMPORTED_MODULE_1__ng_bootstrap_ng_bootstrap__["a" /* ModalDismissReasons */].BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        }
        else {
            return "with: " + reason;
        }
    };
    ModalComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-modal',
            template: __webpack_require__("./src/app/components/modal/modal.component.html"),
            styles: [__webpack_require__("./src/app/components/modal/modal.component.scss")]
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__ng_bootstrap_ng_bootstrap__["f" /* NgbModal */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__ng_bootstrap_ng_bootstrap__["f" /* NgbModal */]) === "function" && _a || Object])
    ], ModalComponent);
    return ModalComponent;
    var _a;
}());

//# sourceMappingURL=modal.component.js.map

/***/ }),

/***/ "./src/app/components/pagination/pagination.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"card\">\r\n  <div class=\"card-header\">Pagination options</div>\r\n  <div class=\"card-body\">\r\n    <div class=\"row\">\r\n      <div class=\"col-lg-6\">\r\n        <div>Default pagination</div>\r\n        <ngb-pagination [collectionSize]=\"70\" [(page)]=\"page\"></ngb-pagination>\r\n\r\n        <div>directionLinks = false</div>\r\n        <ngb-pagination [collectionSize]=\"70\" [(page)]=\"page\" [directionLinks]=\"false\"></ngb-pagination>\r\n\r\n        <div>boundaryLinks = true</div>\r\n        <ngb-pagination [collectionSize]=\"70\" [(page)]=\"page\" [boundaryLinks]=\"true\"></ngb-pagination>\r\n\r\n        <div>Pagination sizes</div>\r\n        <ngb-pagination [collectionSize]=\"50\" [(page)]=\"currentPage\" size=\"lg\"></ngb-pagination>\r\n        <ngb-pagination [collectionSize]=\"50\" [(page)]=\"currentPage\"></ngb-pagination>\r\n        <ngb-pagination [collectionSize]=\"50\" [(page)]=\"currentPage\" size=\"sm\"></ngb-pagination>\r\n      </div>\r\n      <div class=\"col-lg-6\">\r\n        <div>maxSize = 5, rotate = false</div>\r\n        <ngb-pagination [collectionSize]=\"120\" [(page)]=\"page\" [maxSize]=\"5\" [boundaryLinks]=\"true\"></ngb-pagination>\r\n\r\n        <div>maxSize = 5, rotate = true</div>\r\n        <ngb-pagination [collectionSize]=\"120\" [(page)]=\"page\" [maxSize]=\"5\" [rotate]=\"true\" [boundaryLinks]=\"true\"></ngb-pagination>\r\n\r\n        <div>maxSize = 5, rotate = true, ellipses = false</div>\r\n        <ngb-pagination [collectionSize]=\"120\" [(page)]=\"page\" [maxSize]=\"5\" [rotate]=\"true\" [ellipses]=\"false\" [boundaryLinks]=\"true\"></ngb-pagination>\r\n\r\n        <pre>Current page: {{page}}</pre>\r\n\r\n        <div>Pagination control can be disabled</div>\r\n        <ngb-pagination [collectionSize]=\"70\" [(page)]=\"page\" [disabled]='isDisabled'></ngb-pagination>\r\n        <button class=\"btn btn-sm btn-outline-primary\" (click)=\"toggleDisabled()\">\r\n          Toggle disabled\r\n        </button>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./src/app/components/pagination/pagination.component.scss":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/components/pagination/pagination.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PaginationComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var PaginationComponent = (function () {
    function PaginationComponent() {
        this.page = 4;
        this.currentPage = 3;
        this.isDisabled = true;
    }
    PaginationComponent.prototype.toggleDisabled = function () {
        this.isDisabled = !this.isDisabled;
    };
    PaginationComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-pagination',
            template: __webpack_require__("./src/app/components/pagination/pagination.component.html"),
            styles: [__webpack_require__("./src/app/components/pagination/pagination.component.scss")]
        })
    ], PaginationComponent);
    return PaginationComponent;
}());

//# sourceMappingURL=pagination.component.js.map

/***/ }),

/***/ "./src/app/components/popover/popover.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\r\n  <div class=\"col-lg-6\">\r\n    <div class=\"card\">\r\n      <div class=\"card-header\">Quick and easy popovers</div>\r\n      <div class=\"card-body\">\r\n        <button type=\"button\" class=\"btn btn-secondary mr-1 mb-1\" placement=\"top\"\r\n                ngbPopover=\"Vivamus sagittis lacus vel augue laoreet rutrum faucibus.\" popoverTitle=\"Popover on top\">\r\n          Popover on top\r\n        </button>\r\n\r\n        <button type=\"button\" class=\"btn btn-secondary mr-1 mb-1\" placement=\"right\"\r\n                ngbPopover=\"Vivamus sagittis lacus vel augue laoreet rutrum faucibus.\" popoverTitle=\"Popover on right\">\r\n          Popover on right\r\n        </button>\r\n\r\n        <button type=\"button\" class=\"btn btn-secondary mr-1 mb-1\" placement=\"bottom\"\r\n                ngbPopover=\"Vivamus sagittis lacus vel augue laoreet rutrum faucibus.\" popoverTitle=\"Popover on bottom\">\r\n          Popover on bottom\r\n        </button>\r\n\r\n        <button type=\"button\" class=\"btn btn-secondary mr-1 mb-1\" placement=\"left\"\r\n                ngbPopover=\"Vivamus sagittis lacus vel augue laoreet rutrum faucibus.\" popoverTitle=\"Popover on left\">\r\n          Popover on left\r\n        </button>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <div class=\"col-lg-6\">\r\n    <div class=\"card\">\r\n      <div class=\"card-header\">HTML and bindings in popovers</div>\r\n      <div class=\"card-body\">\r\n        <p>\r\n          Popovers can contain any arbitrary HTML, Angular bindings and even directives!\r\n          Simply enclose desired content in a <code>&lt;template&gt;</code> element.\r\n        </p>\r\n\r\n        <ng-template #popContent>Hello, <b>{{name}}</b>!</ng-template>\r\n        <button type=\"button\" class=\"btn btn-secondary btn-sm\" [ngbPopover]=\"popContent\" popoverTitle=\"Fancy content\">\r\n          I've got markup and bindings in my popover!\r\n        </button>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n\r\n<div class=\"row\">\r\n  <div class=\"col-lg-6\">\r\n    <div class=\"card\">\r\n      <div class=\"card-header\">Custom and manual triggers</div>\r\n      <div class=\"card-body\">\r\n        <p>\r\n          You can easily override open and close triggers by specifying event names (separated by <code>:</code>) in the <code>triggers</code> property.\r\n        </p>\r\n\r\n        <button type=\"button\" class=\"btn btn-secondary\" ngbPopover=\"You see, I show up on hover!\" triggers=\"mouseenter:mouseleave\" popoverTitle=\"Pop title\">\r\n          Hover over me!\r\n        </button>\r\n\r\n        <p class=\"pt-3\">\r\n          Alternatively you can take full manual control over popover opening / closing events.\r\n        </p>\r\n\r\n        <button type=\"button\" class=\"btn btn-secondary\" ngbPopover=\"What a great tip!\" triggers=\"manual\" #p=\"ngbPopover\" (click)=\"p.open()\" popoverTitle=\"Pop title\">\r\n          Click me to open a popover\r\n        </button>\r\n        <button type=\"button\" class=\"btn btn-secondary\" (click)=\"p.close()\">\r\n          Click me to close a popover\r\n        </button>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <div class=\"col-lg-6\">\r\n    <div class=\"card\">\r\n      <div class=\"card-header\">Append popover in the body</div>\r\n      <div class=\"card-body\">\r\n        <p>\r\n          Set the <code>container</code> property to \"body\" to have the popover be appended to the body instead of the triggering element's parent. This option is useful if the element triggering the popover is inside an element that clips its contents (i.e. <code>overflow: hidden</code>).\r\n        </p>\r\n\r\n        <div class=\"row\">\r\n          <div class=\"card mb-0\" style=\"padding: 20px 0; text-align: center; overflow:hidden\">\r\n            <button type=\"button\" class=\"btn btn-secondary btn-sm mb-3\"\r\n                    ngbPopover=\"Vivamus sagittis lacus vel augue laoreet rutrum faucibus.\" container=\"body\">\r\n              Popover appended to body\r\n            </button>\r\n            <button type=\"button\" class=\"btn btn-secondary btn-sm\"\r\n                    ngbPopover=\"Vivamus sagittis lacus vel augue laoreet rutrum faucibus.\">\r\n              Default popover\r\n            </button>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n\r\n<div class=\"row\">\r\n  <div class=\"col-lg-6\">\r\n    <div class=\"card\">\r\n      <div class=\"card-header\">Context and manual triggers</div>\r\n      <div class=\"card-body\">\r\n        <p>\r\n          You can optionally pass in a context when manually triggering a popover.\r\n        </p>\r\n\r\n        <ng-template #popContent let-greeting=\"greeting\">{{greeting}}, <b>{{name}}</b>!</ng-template>\r\n        <p>\r\n          How would you like to greet <strong [ngbPopover]=\"popContent\" popoverTitle=\"Greeting\" #p=\"ngbPopover\" triggers=\"manual\">me</strong>?\r\n        </p>\r\n        <button type=\"button\" class=\"btn btn-secondary\" (click)=\"changeGreeting({ greeting: 'Bonjour' })\">\r\n          French\r\n        </button>\r\n        <button type=\"button\" class=\"btn btn-secondary\" (click)=\"changeGreeting({ greeting: 'Gutentag' })\">\r\n          German\r\n        </button>\r\n        <button type=\"button\" class=\"btn btn-secondary\" (click)=\"changeGreeting({ greeting: 'Hello' })\">\r\n          English\r\n        </button>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <div class=\"col-lg-6\">\r\n    <div class=\"card\">\r\n      <div class=\"card-header\">Popover visibility events</div>\r\n      <div class=\"card-body\">\r\n        <button type=\"button\" class=\"btn btn-secondary\" placement=\"top\"\r\n                ngbPopover=\"Vivamus sagittis lacus vel augue laoreet rutrum faucibus.\" popoverTitle=\"Popover on top\" \r\n                #popover=\"ngbPopover\">\r\n          Open Popover\r\n        </button>\r\n\r\n        <p class=\"pt-3\">\r\n          Popover is currently: <code>{{ popover.isOpen() ? 'open' : 'closed' }}</code>\r\n        </p>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./src/app/components/popover/popover.component.scss":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/components/popover/popover.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PopoverComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ng_bootstrap_ng_bootstrap__ = __webpack_require__("./node_modules/@ng-bootstrap/ng-bootstrap/index.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var PopoverComponent = (function () {
    function PopoverComponent() {
        this.greeting = {};
        this.name = 'World';
    }
    PopoverComponent.prototype.changeGreeting = function (greeting) {
        var isOpen = this.popover.isOpen();
        this.popover.close();
        if (greeting !== this.greeting || !isOpen) {
            this.greeting = greeting;
            this.popover.open(greeting);
        }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('p'),
        __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__ng_bootstrap_ng_bootstrap__["h" /* NgbPopover */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__ng_bootstrap_ng_bootstrap__["h" /* NgbPopover */]) === "function" && _a || Object)
    ], PopoverComponent.prototype, "popover", void 0);
    PopoverComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-popover',
            template: __webpack_require__("./src/app/components/popover/popover.component.html"),
            styles: [__webpack_require__("./src/app/components/popover/popover.component.scss")]
        })
    ], PopoverComponent);
    return PopoverComponent;
    var _a;
}());

//# sourceMappingURL=popover.component.js.map

/***/ }),

/***/ "./src/app/components/progress/progress.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"card\">\r\n  <div class=\"card-header\">Contextual progress bars</div>\r\n  <div class=\"card-body\">\r\n    <p>Stylize the HTML5 <code>progress</code> element with a few extra classes and some crafty browser-specific CSS. Be sure to read up on the browser support.</p>\r\n    <p><ngb-progressbar type=\"success\" [value]=\"25\"></ngb-progressbar></p>\r\n    <p><ngb-progressbar type=\"info\" [value]=\"50\"></ngb-progressbar></p>\r\n    <p><ngb-progressbar type=\"warning\" [value]=\"75\"></ngb-progressbar></p>\r\n    <p><ngb-progressbar type=\"danger\" [value]=\"100\"></ngb-progressbar></p>\r\n  </div>\r\n</div>\r\n\r\n<div class=\"card\">\r\n  <div class=\"card-header\">Progress bars with current value labels</div>\r\n  <div class=\"card-body\">\r\n    <p>Add labels to your progress bars by placing text within the <code class=\"highlighter-rouge\">.progress-bar</code>.</p>\r\n    <p><ngb-progressbar showValue=\"true\" type=\"success\" [value]=\"25\"></ngb-progressbar></p>\r\n    <p><ngb-progressbar [showValue]=\"true\" type=\"info\" [value]=\"50\"></ngb-progressbar></p>\r\n    <p><ngb-progressbar showValue=\"true\" type=\"warning\" [value]=\"150\" [max]=\"200\"></ngb-progressbar></p>\r\n    <p><ngb-progressbar [showValue]=\"true\" type=\"danger\" [value]=\"150\" [max]=\"150\"></ngb-progressbar></p>\r\n  </div>\r\n</div>\r\n\r\n<div class=\"card\">\r\n  <div class=\"card-header\">Striped progress bars</div>\r\n  <div class=\"card-body\">\r\n    <p>Uses a gradient to create a striped effect.</p>\r\n    <p><ngb-progressbar type=\"success\" [value]=\"25\" [striped]=\"true\"></ngb-progressbar></p>\r\n    <p><ngb-progressbar type=\"info\" [value]=\"50\" [striped]=\"true\"></ngb-progressbar></p>\r\n    <p><ngb-progressbar type=\"warning\" [value]=\"75\" [striped]=\"true\"></ngb-progressbar></p>\r\n    <p><ngb-progressbar type=\"danger\" [value]=\"100\" [striped]=\"true\"></ngb-progressbar></p>\r\n  </div>\r\n</div>\r\n\r\n<div class=\"card\">\r\n  <div class=\"card-header\">Progress bars with custom labels</div>\r\n  <div class=\"card-body\">\r\n    <p><ngb-progressbar type=\"success\" [value]=\"25\">25</ngb-progressbar></p>\r\n    <p><ngb-progressbar type=\"info\" [value]=\"50\">Copying file <b>2 of 4</b>...</ngb-progressbar></p>\r\n    <p><ngb-progressbar type=\"warning\" [value]=\"75\" [striped]=\"true\" [animated]=\"true\"><i>50%</i></ngb-progressbar></p>\r\n    <p><ngb-progressbar type=\"danger\" [value]=\"100\" [striped]=\"true\">Completed!</ngb-progressbar></p>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/components/progress/progress.component.scss":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/components/progress/progress.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProgressComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var ProgressComponent = (function () {
    function ProgressComponent() {
    }
    ProgressComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-progress',
            template: __webpack_require__("./src/app/components/progress/progress.component.html"),
            styles: [__webpack_require__("./src/app/components/progress/progress.component.scss")]
        })
    ], ProgressComponent);
    return ProgressComponent;
}());

//# sourceMappingURL=progress.component.js.map

/***/ }),

/***/ "./src/app/components/rating/rating.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\r\n  <div class=\"col-lg-6\">   \r\n    <div class=\"card\">\r\n      <div class=\"card-header\">Basic demo</div>\r\n      <div class=\"card-body\">\r\n        <ngb-rating [(rate)]=\"currentRate\"></ngb-rating>\r\n        <pre>Rate: <b>{{currentRate}}</b></pre>\r\n      </div>\r\n    </div>\r\n\r\n    <div class=\"card\">\r\n      <div class=\"card-header\">Events and readonly ratings</div>\r\n      <div class=\"card-body\">\r\n        <ngb-rating [(rate)]=\"selected\" (hover)=\"hovered=$event\" (leave)=\"hovered=0\" [readonly]=\"readonly\"></ngb-rating>\r\n        <pre>Selected: <b>{{selected}}</b>\r\nHovered: <b>{{hovered}}</b>\r\n        </pre>\r\n        <button class=\"btn btn-sm btn-outline-{{readonly ? 'danger' : 'success'}}\" (click)=\"readonly = !readonly\">\r\n          {{ readonly ? \"readonly\" : \"editable\"}}\r\n        </button>\r\n      </div>\r\n    </div>\r\n\r\n    <div class=\"card\">\r\n      <div class=\"card-header\">Custom star template</div>\r\n      <div class=\"card-body\">\r\n        <p>Custom rating template provided as child element</p>\r\n        <ngb-rating [(rate)]=\"currentRate\">\r\n          <ng-template let-fill=\"fill\">\r\n            <span class=\"star\" [class.filled]=\"fill === 100\">&#9733;</span>\r\n          </ng-template>\r\n        </ngb-rating>\r\n        <pre>Rate: <b>{{currentRate}}</b></pre>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <div class=\"col-lg-6\">\r\n    <div class=\"card\">\r\n      <div class=\"card-header\">Custom decimal rating</div>\r\n      <div class=\"card-body\">\r\n        <p>Custom rating template provided via a variable. Shows fine-grained rating display</p>\r\n\r\n        <ng-template #t let-fill=\"fill\">\r\n          <span *ngIf=\"fill === 100\" class=\"star full\">&hearts;</span>\r\n          <span *ngIf=\"fill === 0\" class=\"star\">&hearts;</span>\r\n          <span *ngIf=\"fill < 100 && fill > 0\" class=\"star\">\r\n            <span class=\"half\" [style.width.%]=\"fill\">&hearts;</span>&hearts;\r\n          </span>\r\n        </ng-template>\r\n\r\n        <ngb-rating [(rate)]=\"decimalCurrentRate\" [starTemplate]=\"t\" [readonly]=\"true\" max=\"5\"></ngb-rating>\r\n        <pre>Rate: <b>{{decimalCurrentRate}}</b></pre>\r\n        <button class=\"btn btn-sm btn-outline-primary\" (click)=\"decimalCurrentRate = 1.35\">1.35</button>\r\n        <button class=\"btn btn-sm btn-outline-primary\" (click)=\"decimalCurrentRate = 4.72\">4.72</button>\r\n      </div>\r\n    </div>\r\n\r\n    <div class=\"card\">\r\n      <div class=\"card-header\">Form integration</div>\r\n      <div class=\"card-body\">\r\n        <p>NgModel and reactive forms can be used without the 'rate' binding</p>\r\n\r\n        <div class=\"form-group\" [class.has-success]=\"ctrl.valid\" [class.has-danger]=\"ctrl.invalid\">\r\n          <ngb-rating [formControl]=\"ctrl\"></ngb-rating>\r\n          <div class=\"form-control-feedback\">\r\n            <div *ngIf=\"ctrl.valid\">Thanks!</div>\r\n            <div *ngIf=\"ctrl.errors\">Please rate us</div>\r\n          </div>\r\n        </div>\r\n\r\n        <pre>Model: <b>{{ ctrl.value }}</b></pre>\r\n        <button class=\"btn btn-sm btn-outline-{{ ctrl.disabled ? 'danger' : 'success'}}\" (click)=\"toggle()\">\r\n          {{ ctrl.disabled ? \"control disabled\" : \" control enabled\" }}\r\n        </button>\r\n        <button class=\"btn btn-sm btn-outline-primary\" (click)=\"ctrl.setValue(null)\">Clear</button>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./src/app/components/rating/rating.component.scss":
/***/ (function(module, exports) {

module.exports = ".star {\n  font-size: 1.5rem;\n  color: #b0c4de; }\n\n.filled {\n  color: #1e90ff; }\n\n.star {\n  position: relative;\n  display: inline-block;\n  font-size: 2rem;\n  color: #d3d3d3; }\n\n.full {\n  color: red; }\n\n.half {\n  position: absolute;\n  display: inline-block;\n  overflow: hidden;\n  color: red; }\n"

/***/ }),

/***/ "./src/app/components/rating/rating.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RatingComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("./node_modules/@angular/forms/@angular/forms.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


var RatingComponent = (function () {
    function RatingComponent() {
        this.currentRate = 8;
        this.selected = 0;
        this.hovered = 0;
        this.readonly = false;
        this.decimalCurrentRate = 3.14;
        this.ctrl = new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["FormControl"](null, __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required);
    }
    RatingComponent.prototype.toggle = function () {
        if (this.ctrl.disabled) {
            this.ctrl.enable();
        }
        else {
            this.ctrl.disable();
        }
    };
    RatingComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-rating',
            template: __webpack_require__("./src/app/components/rating/rating.component.html"),
            styles: [__webpack_require__("./src/app/components/rating/rating.component.scss")]
        })
    ], RatingComponent);
    return RatingComponent;
}());

//# sourceMappingURL=rating.component.js.map

/***/ }),

/***/ "./src/app/components/spinners/spinners.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"row demo-spinkit\">\r\n  <div class=\"col-xs-6 col-sm-4 col-md-3\">\r\n    <div class=\"card\">\r\n      <div class=\"card-header no-shadow block\"><span class=\"mr-auto\"></span>01</div>\r\n      <div class=\"card-body\">\r\n        <div class=\"loader01\"></div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <div class=\"col-xs-6 col-sm-4 col-md-3\">\r\n    <div class=\"card\">\r\n      <div class=\"card-header no-shadow block\"><span class=\"mr-auto\"></span>02</div>\r\n      <div class=\"card-body\">\r\n        <div class=\"loader02\"></div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <div class=\"col-xs-6 col-sm-4 col-md-3\">\r\n    <div class=\"card\">\r\n      <div class=\"card-header no-shadow block\"><span class=\"mr-auto\"></span>03</div>\r\n      <div class=\"card-body\">\r\n        <div class=\"loader03\"></div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <div class=\"col-xs-6 col-sm-4 col-md-3\">\r\n    <div class=\"card\">\r\n      <div class=\"card-header no-shadow block\"><span class=\"mr-auto\"></span>04</div>\r\n      <div class=\"card-body\">\r\n        <div class=\"loader04\"></div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <div class=\"col-xs-6 col-sm-4 col-md-3\">\r\n    <div class=\"card\">\r\n      <div class=\"card-header no-shadow block\"><span class=\"mr-auto\"></span>05</div>\r\n      <div class=\"card-body\">\r\n        <div class=\"loader05\"></div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <div class=\"col-xs-6 col-sm-4 col-md-3\">\r\n    <div class=\"card\">\r\n      <div class=\"card-header no-shadow block\"><span class=\"mr-auto\"></span>06</div>\r\n      <div class=\"card-body\">\r\n        <div class=\"loader06\"></div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <div class=\"col-xs-6 col-sm-4 col-md-3\">\r\n    <div class=\"card\">\r\n      <div class=\"card-header no-shadow block\"><span class=\"mr-auto\"></span>07</div>\r\n      <div class=\"card-body\">\r\n        <div class=\"loader07\"></div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <div class=\"col-xs-6 col-sm-4 col-md-3\">\r\n    <div class=\"card\">\r\n      <div class=\"card-header no-shadow block\"><span class=\"mr-auto\"></span>08</div>\r\n      <div class=\"card-body\">\r\n        <div class=\"loader08\"></div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <div class=\"col-xs-6 col-sm-4 col-md-3\">\r\n    <div class=\"card\">\r\n      <div class=\"card-header no-shadow block\"><span class=\"mr-auto\"></span>09</div>\r\n      <div class=\"card-body\">\r\n        <div class=\"loader09\"></div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <div class=\"col-xs-6 col-sm-4 col-md-3\">\r\n    <div class=\"card\">\r\n      <div class=\"card-header no-shadow block\"><span class=\"mr-auto\"></span>10</div>\r\n      <div class=\"card-body\">\r\n        <div class=\"loader10\"></div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <div class=\"col-xs-6 col-sm-4 col-md-3\">\r\n    <div class=\"card\">\r\n      <div class=\"card-header no-shadow block\"><span class=\"mr-auto\"></span>11</div>\r\n      <div class=\"card-body\">\r\n        <div class=\"loader11\"></div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <div class=\"col-xs-6 col-sm-4 col-md-3\">\r\n    <div class=\"card\">\r\n      <div class=\"card-header no-shadow block\"><span class=\"mr-auto\"></span>12</div>\r\n      <div class=\"card-body\">\r\n        <div class=\"loader12\"></div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./src/app/components/spinners/spinners.component.scss":
/***/ (function(module, exports) {

module.exports = "/* $colors\r\n ------------------------------------------*/\n.loader01 {\n  width: 56px;\n  height: 56px;\n  border: 8px solid #304ffe;\n  border-right-color: transparent;\n  border-radius: 50%;\n  position: relative;\n  -webkit-animation: loader-rotate 1s linear infinite;\n          animation: loader-rotate 1s linear infinite;\n  top: 50%;\n  margin: -28px auto 0; }\n.loader01::after {\n    content: '';\n    width: 8px;\n    height: 8px;\n    background: #304ffe;\n    border-radius: 50%;\n    position: absolute;\n    top: -1px;\n    left: 33px; }\n@-webkit-keyframes loader-rotate {\n  0% {\n    -webkit-transform: rotate(0);\n            transform: rotate(0); }\n  100% {\n    -webkit-transform: rotate(360deg);\n            transform: rotate(360deg); } }\n@keyframes loader-rotate {\n  0% {\n    -webkit-transform: rotate(0);\n            transform: rotate(0); }\n  100% {\n    -webkit-transform: rotate(360deg);\n            transform: rotate(360deg); } }\n.loader02 {\n  width: 56px;\n  height: 56px;\n  border: 8px solid rgba(48, 79, 254, 0.25);\n  border-top-color: #304ffe;\n  border-radius: 50%;\n  position: relative;\n  -webkit-animation: loader-rotate 1s linear infinite;\n          animation: loader-rotate 1s linear infinite;\n  top: 50%;\n  margin: -28px auto 0; }\n@keyframes loader-rotate {\n  0% {\n    -webkit-transform: rotate(0);\n            transform: rotate(0); }\n  100% {\n    -webkit-transform: rotate(360deg);\n            transform: rotate(360deg); } }\n.loader03 {\n  width: 56px;\n  height: 56px;\n  border: 8px solid transparent;\n  border-top-color: #304ffe;\n  border-bottom-color: #304ffe;\n  border-radius: 50%;\n  position: relative;\n  -webkit-animation: loader-rotate 1s linear infinite;\n          animation: loader-rotate 1s linear infinite;\n  top: 50%;\n  margin: -28px auto 0; }\n@keyframes loader-rotate {\n  0% {\n    -webkit-transform: rotate(0);\n            transform: rotate(0); }\n  100% {\n    -webkit-transform: rotate(360deg);\n            transform: rotate(360deg); } }\n.loader04 {\n  width: 56px;\n  height: 56px;\n  border: 2px solid rgba(48, 79, 254, 0.5);\n  border-radius: 50%;\n  position: relative;\n  -webkit-animation: loader-rotate 1s ease-in-out infinite;\n          animation: loader-rotate 1s ease-in-out infinite;\n  top: 50%;\n  margin: -28px auto 0; }\n.loader04::after {\n    content: '';\n    width: 10px;\n    height: 10px;\n    border-radius: 50%;\n    background: #304ffe;\n    position: absolute;\n    top: -6px;\n    left: 50%;\n    margin-left: -5px; }\n@keyframes loader-rotate {\n  0% {\n    -webkit-transform: rotate(0);\n            transform: rotate(0); }\n  100% {\n    -webkit-transform: rotate(360deg);\n            transform: rotate(360deg); } }\n.loader05 {\n  width: 56px;\n  height: 56px;\n  border: 4px solid #304ffe;\n  border-radius: 50%;\n  position: relative;\n  -webkit-animation: loader-scale 1s ease-out infinite;\n          animation: loader-scale 1s ease-out infinite;\n  top: 50%;\n  margin: -28px auto 0; }\n@-webkit-keyframes loader-scale {\n  0% {\n    -webkit-transform: scale(0);\n            transform: scale(0);\n    opacity: 0; }\n  50% {\n    opacity: 1; }\n  100% {\n    -webkit-transform: scale(1);\n            transform: scale(1);\n    opacity: 0; } }\n@keyframes loader-scale {\n  0% {\n    -webkit-transform: scale(0);\n            transform: scale(0);\n    opacity: 0; }\n  50% {\n    opacity: 1; }\n  100% {\n    -webkit-transform: scale(1);\n            transform: scale(1);\n    opacity: 0; } }\n.loader06 {\n  width: 56px;\n  height: 56px;\n  border: 4px solid transparent;\n  border-radius: 50%;\n  position: relative;\n  top: 50%;\n  margin: -28px auto 0; }\n.loader06::before {\n    content: '';\n    border: 4px solid rgba(48, 79, 254, 0.5);\n    border-radius: 50%;\n    width: 67.2px;\n    height: 67.2px;\n    position: absolute;\n    top: -9.6px;\n    left: -9.6px;\n    -webkit-animation: loader-scale 1s ease-out infinite;\n            animation: loader-scale 1s ease-out infinite;\n    -webkit-animation-delay: 1s;\n            animation-delay: 1s;\n    opacity: 0; }\n.loader06::after {\n    content: '';\n    border: 4px solid #304ffe;\n    border-radius: 50%;\n    width: 56px;\n    height: 56px;\n    position: absolute;\n    top: -4px;\n    left: -4px;\n    -webkit-animation: loader-scale 1s ease-out infinite;\n            animation: loader-scale 1s ease-out infinite;\n    -webkit-animation-delay: 0.5s;\n            animation-delay: 0.5s; }\n@keyframes loader-scale {\n  0% {\n    -webkit-transform: scale(0);\n            transform: scale(0);\n    opacity: 0; }\n  50% {\n    opacity: 1; }\n  100% {\n    -webkit-transform: scale(1);\n            transform: scale(1);\n    opacity: 0; } }\n.loader07 {\n  width: 16px;\n  height: 16px;\n  border-radius: 50%;\n  position: relative;\n  -webkit-animation: loader07-u480f46c4 1s linear infinite;\n          animation: loader07-u480f46c4 1s linear infinite;\n  top: 50%;\n  margin: -8px auto 0; }\n@-webkit-keyframes loader07-u480f46c4 {\n  0% {\n    -webkit-box-shadow: 0 -27px 0 0 rgba(48, 79, 254, 0.05), 19px -19px 0 0 rgba(48, 79, 254, 0.1), 27px 0 0 0 rgba(48, 79, 254, 0.2), 19px 19px 0 0 rgba(48, 79, 254, 0.3), 0 27px 0 0 rgba(48, 79, 254, 0.4), -19px 19px 0 0 rgba(48, 79, 254, 0.6), -27px 0 0 0 rgba(48, 79, 254, 0.8), -19px -19px 0 0 #304ffe;\n            box-shadow: 0 -27px 0 0 rgba(48, 79, 254, 0.05), 19px -19px 0 0 rgba(48, 79, 254, 0.1), 27px 0 0 0 rgba(48, 79, 254, 0.2), 19px 19px 0 0 rgba(48, 79, 254, 0.3), 0 27px 0 0 rgba(48, 79, 254, 0.4), -19px 19px 0 0 rgba(48, 79, 254, 0.6), -27px 0 0 0 rgba(48, 79, 254, 0.8), -19px -19px 0 0 #304ffe; }\n  12.5% {\n    -webkit-box-shadow: 0 -27px 0 0 #304ffe, 19px -19px 0 0 rgba(48, 79, 254, 0.05), 27px 0 0 0 rgba(48, 79, 254, 0.1), 19px 19px 0 0 rgba(48, 79, 254, 0.2), 0 27px 0 0 rgba(48, 79, 254, 0.3), -19px 19px 0 0 rgba(48, 79, 254, 0.4), -27px 0 0 0 rgba(48, 79, 254, 0.6), -19px -19px 0 0 rgba(48, 79, 254, 0.8);\n            box-shadow: 0 -27px 0 0 #304ffe, 19px -19px 0 0 rgba(48, 79, 254, 0.05), 27px 0 0 0 rgba(48, 79, 254, 0.1), 19px 19px 0 0 rgba(48, 79, 254, 0.2), 0 27px 0 0 rgba(48, 79, 254, 0.3), -19px 19px 0 0 rgba(48, 79, 254, 0.4), -27px 0 0 0 rgba(48, 79, 254, 0.6), -19px -19px 0 0 rgba(48, 79, 254, 0.8); }\n  25% {\n    -webkit-box-shadow: 0 -27px 0 0 rgba(48, 79, 254, 0.8), 19px -19px 0 0 #304ffe, 27px 0 0 0 rgba(48, 79, 254, 0.05), 19px 19px 0 0 rgba(48, 79, 254, 0.1), 0 27px 0 0 rgba(48, 79, 254, 0.2), -19px 19px 0 0 rgba(48, 79, 254, 0.3), -27px 0 0 0 rgba(48, 79, 254, 0.4), -19px -19px 0 0 rgba(48, 79, 254, 0.6);\n            box-shadow: 0 -27px 0 0 rgba(48, 79, 254, 0.8), 19px -19px 0 0 #304ffe, 27px 0 0 0 rgba(48, 79, 254, 0.05), 19px 19px 0 0 rgba(48, 79, 254, 0.1), 0 27px 0 0 rgba(48, 79, 254, 0.2), -19px 19px 0 0 rgba(48, 79, 254, 0.3), -27px 0 0 0 rgba(48, 79, 254, 0.4), -19px -19px 0 0 rgba(48, 79, 254, 0.6); }\n  37.5% {\n    -webkit-box-shadow: 0 -27px 0 0 rgba(48, 79, 254, 0.6), 19px -19px 0 0 rgba(48, 79, 254, 0.8), 27px 0 0 0 #304ffe, 19px 19px 0 0 rgba(48, 79, 254, 0.05), 0 27px 0 0 rgba(48, 79, 254, 0.1), -19px 19px 0 0 rgba(48, 79, 254, 0.2), -27px 0 0 0 rgba(48, 79, 254, 0.3), -19px -19px 0 0 rgba(48, 79, 254, 0.4);\n            box-shadow: 0 -27px 0 0 rgba(48, 79, 254, 0.6), 19px -19px 0 0 rgba(48, 79, 254, 0.8), 27px 0 0 0 #304ffe, 19px 19px 0 0 rgba(48, 79, 254, 0.05), 0 27px 0 0 rgba(48, 79, 254, 0.1), -19px 19px 0 0 rgba(48, 79, 254, 0.2), -27px 0 0 0 rgba(48, 79, 254, 0.3), -19px -19px 0 0 rgba(48, 79, 254, 0.4); }\n  50% {\n    -webkit-box-shadow: 0 -27px 0 0 rgba(48, 79, 254, 0.4), 19px -19px 0 0 rgba(48, 79, 254, 0.6), 27px 0 0 0 rgba(48, 79, 254, 0.8), 19px 19px 0 0 #304ffe, 0 27px 0 0 rgba(48, 79, 254, 0.05), -19px 19px 0 0 rgba(48, 79, 254, 0.1), -27px 0 0 0 rgba(48, 79, 254, 0.2), -19px -19px 0 0 rgba(48, 79, 254, 0.3);\n            box-shadow: 0 -27px 0 0 rgba(48, 79, 254, 0.4), 19px -19px 0 0 rgba(48, 79, 254, 0.6), 27px 0 0 0 rgba(48, 79, 254, 0.8), 19px 19px 0 0 #304ffe, 0 27px 0 0 rgba(48, 79, 254, 0.05), -19px 19px 0 0 rgba(48, 79, 254, 0.1), -27px 0 0 0 rgba(48, 79, 254, 0.2), -19px -19px 0 0 rgba(48, 79, 254, 0.3); }\n  62.5% {\n    -webkit-box-shadow: 0 -27px 0 0 rgba(48, 79, 254, 0.3), 19px -19px 0 0 rgba(48, 79, 254, 0.4), 27px 0 0 0 rgba(48, 79, 254, 0.6), 19px 19px 0 0 rgba(48, 79, 254, 0.8), 0 27px 0 0 #304ffe, -19px 19px 0 0 rgba(48, 79, 254, 0.05), -27px 0 0 0 rgba(48, 79, 254, 0.1), -19px -19px 0 0 rgba(48, 79, 254, 0.2);\n            box-shadow: 0 -27px 0 0 rgba(48, 79, 254, 0.3), 19px -19px 0 0 rgba(48, 79, 254, 0.4), 27px 0 0 0 rgba(48, 79, 254, 0.6), 19px 19px 0 0 rgba(48, 79, 254, 0.8), 0 27px 0 0 #304ffe, -19px 19px 0 0 rgba(48, 79, 254, 0.05), -27px 0 0 0 rgba(48, 79, 254, 0.1), -19px -19px 0 0 rgba(48, 79, 254, 0.2); }\n  75% {\n    -webkit-box-shadow: 0 -27px 0 0 rgba(48, 79, 254, 0.2), 19px -19px 0 0 rgba(48, 79, 254, 0.3), 27px 0 0 0 rgba(48, 79, 254, 0.4), 19px 19px 0 0 rgba(48, 79, 254, 0.6), 0 27px 0 0 rgba(48, 79, 254, 0.8), -19px 19px 0 0 #304ffe, -27px 0 0 0 rgba(48, 79, 254, 0.05), -19px -19px 0 0 rgba(48, 79, 254, 0.1);\n            box-shadow: 0 -27px 0 0 rgba(48, 79, 254, 0.2), 19px -19px 0 0 rgba(48, 79, 254, 0.3), 27px 0 0 0 rgba(48, 79, 254, 0.4), 19px 19px 0 0 rgba(48, 79, 254, 0.6), 0 27px 0 0 rgba(48, 79, 254, 0.8), -19px 19px 0 0 #304ffe, -27px 0 0 0 rgba(48, 79, 254, 0.05), -19px -19px 0 0 rgba(48, 79, 254, 0.1); }\n  87.5% {\n    -webkit-box-shadow: 0 -27px 0 0 rgba(48, 79, 254, 0.1), 19px -19px 0 0 rgba(48, 79, 254, 0.2), 27px 0 0 0 rgba(48, 79, 254, 0.3), 19px 19px 0 0 rgba(48, 79, 254, 0.4), 0 27px 0 0 rgba(48, 79, 254, 0.6), -19px 19px 0 0 rgba(48, 79, 254, 0.8), -27px 0 0 0 #304ffe, -19px -19px 0 0 rgba(48, 79, 254, 0.05);\n            box-shadow: 0 -27px 0 0 rgba(48, 79, 254, 0.1), 19px -19px 0 0 rgba(48, 79, 254, 0.2), 27px 0 0 0 rgba(48, 79, 254, 0.3), 19px 19px 0 0 rgba(48, 79, 254, 0.4), 0 27px 0 0 rgba(48, 79, 254, 0.6), -19px 19px 0 0 rgba(48, 79, 254, 0.8), -27px 0 0 0 #304ffe, -19px -19px 0 0 rgba(48, 79, 254, 0.05); }\n  100% {\n    -webkit-box-shadow: 0 -27px 0 0 rgba(48, 79, 254, 0.05), 19px -19px 0 0 rgba(48, 79, 254, 0.1), 27px 0 0 0 rgba(48, 79, 254, 0.2), 19px 19px 0 0 rgba(48, 79, 254, 0.3), 0 27px 0 0 rgba(48, 79, 254, 0.4), -19px 19px 0 0 rgba(48, 79, 254, 0.6), -27px 0 0 0 rgba(48, 79, 254, 0.8), -19px -19px 0 0 #304ffe;\n            box-shadow: 0 -27px 0 0 rgba(48, 79, 254, 0.05), 19px -19px 0 0 rgba(48, 79, 254, 0.1), 27px 0 0 0 rgba(48, 79, 254, 0.2), 19px 19px 0 0 rgba(48, 79, 254, 0.3), 0 27px 0 0 rgba(48, 79, 254, 0.4), -19px 19px 0 0 rgba(48, 79, 254, 0.6), -27px 0 0 0 rgba(48, 79, 254, 0.8), -19px -19px 0 0 #304ffe; } }\n@keyframes loader07-u480f46c4 {\n  0% {\n    -webkit-box-shadow: 0 -27px 0 0 rgba(48, 79, 254, 0.05), 19px -19px 0 0 rgba(48, 79, 254, 0.1), 27px 0 0 0 rgba(48, 79, 254, 0.2), 19px 19px 0 0 rgba(48, 79, 254, 0.3), 0 27px 0 0 rgba(48, 79, 254, 0.4), -19px 19px 0 0 rgba(48, 79, 254, 0.6), -27px 0 0 0 rgba(48, 79, 254, 0.8), -19px -19px 0 0 #304ffe;\n            box-shadow: 0 -27px 0 0 rgba(48, 79, 254, 0.05), 19px -19px 0 0 rgba(48, 79, 254, 0.1), 27px 0 0 0 rgba(48, 79, 254, 0.2), 19px 19px 0 0 rgba(48, 79, 254, 0.3), 0 27px 0 0 rgba(48, 79, 254, 0.4), -19px 19px 0 0 rgba(48, 79, 254, 0.6), -27px 0 0 0 rgba(48, 79, 254, 0.8), -19px -19px 0 0 #304ffe; }\n  12.5% {\n    -webkit-box-shadow: 0 -27px 0 0 #304ffe, 19px -19px 0 0 rgba(48, 79, 254, 0.05), 27px 0 0 0 rgba(48, 79, 254, 0.1), 19px 19px 0 0 rgba(48, 79, 254, 0.2), 0 27px 0 0 rgba(48, 79, 254, 0.3), -19px 19px 0 0 rgba(48, 79, 254, 0.4), -27px 0 0 0 rgba(48, 79, 254, 0.6), -19px -19px 0 0 rgba(48, 79, 254, 0.8);\n            box-shadow: 0 -27px 0 0 #304ffe, 19px -19px 0 0 rgba(48, 79, 254, 0.05), 27px 0 0 0 rgba(48, 79, 254, 0.1), 19px 19px 0 0 rgba(48, 79, 254, 0.2), 0 27px 0 0 rgba(48, 79, 254, 0.3), -19px 19px 0 0 rgba(48, 79, 254, 0.4), -27px 0 0 0 rgba(48, 79, 254, 0.6), -19px -19px 0 0 rgba(48, 79, 254, 0.8); }\n  25% {\n    -webkit-box-shadow: 0 -27px 0 0 rgba(48, 79, 254, 0.8), 19px -19px 0 0 #304ffe, 27px 0 0 0 rgba(48, 79, 254, 0.05), 19px 19px 0 0 rgba(48, 79, 254, 0.1), 0 27px 0 0 rgba(48, 79, 254, 0.2), -19px 19px 0 0 rgba(48, 79, 254, 0.3), -27px 0 0 0 rgba(48, 79, 254, 0.4), -19px -19px 0 0 rgba(48, 79, 254, 0.6);\n            box-shadow: 0 -27px 0 0 rgba(48, 79, 254, 0.8), 19px -19px 0 0 #304ffe, 27px 0 0 0 rgba(48, 79, 254, 0.05), 19px 19px 0 0 rgba(48, 79, 254, 0.1), 0 27px 0 0 rgba(48, 79, 254, 0.2), -19px 19px 0 0 rgba(48, 79, 254, 0.3), -27px 0 0 0 rgba(48, 79, 254, 0.4), -19px -19px 0 0 rgba(48, 79, 254, 0.6); }\n  37.5% {\n    -webkit-box-shadow: 0 -27px 0 0 rgba(48, 79, 254, 0.6), 19px -19px 0 0 rgba(48, 79, 254, 0.8), 27px 0 0 0 #304ffe, 19px 19px 0 0 rgba(48, 79, 254, 0.05), 0 27px 0 0 rgba(48, 79, 254, 0.1), -19px 19px 0 0 rgba(48, 79, 254, 0.2), -27px 0 0 0 rgba(48, 79, 254, 0.3), -19px -19px 0 0 rgba(48, 79, 254, 0.4);\n            box-shadow: 0 -27px 0 0 rgba(48, 79, 254, 0.6), 19px -19px 0 0 rgba(48, 79, 254, 0.8), 27px 0 0 0 #304ffe, 19px 19px 0 0 rgba(48, 79, 254, 0.05), 0 27px 0 0 rgba(48, 79, 254, 0.1), -19px 19px 0 0 rgba(48, 79, 254, 0.2), -27px 0 0 0 rgba(48, 79, 254, 0.3), -19px -19px 0 0 rgba(48, 79, 254, 0.4); }\n  50% {\n    -webkit-box-shadow: 0 -27px 0 0 rgba(48, 79, 254, 0.4), 19px -19px 0 0 rgba(48, 79, 254, 0.6), 27px 0 0 0 rgba(48, 79, 254, 0.8), 19px 19px 0 0 #304ffe, 0 27px 0 0 rgba(48, 79, 254, 0.05), -19px 19px 0 0 rgba(48, 79, 254, 0.1), -27px 0 0 0 rgba(48, 79, 254, 0.2), -19px -19px 0 0 rgba(48, 79, 254, 0.3);\n            box-shadow: 0 -27px 0 0 rgba(48, 79, 254, 0.4), 19px -19px 0 0 rgba(48, 79, 254, 0.6), 27px 0 0 0 rgba(48, 79, 254, 0.8), 19px 19px 0 0 #304ffe, 0 27px 0 0 rgba(48, 79, 254, 0.05), -19px 19px 0 0 rgba(48, 79, 254, 0.1), -27px 0 0 0 rgba(48, 79, 254, 0.2), -19px -19px 0 0 rgba(48, 79, 254, 0.3); }\n  62.5% {\n    -webkit-box-shadow: 0 -27px 0 0 rgba(48, 79, 254, 0.3), 19px -19px 0 0 rgba(48, 79, 254, 0.4), 27px 0 0 0 rgba(48, 79, 254, 0.6), 19px 19px 0 0 rgba(48, 79, 254, 0.8), 0 27px 0 0 #304ffe, -19px 19px 0 0 rgba(48, 79, 254, 0.05), -27px 0 0 0 rgba(48, 79, 254, 0.1), -19px -19px 0 0 rgba(48, 79, 254, 0.2);\n            box-shadow: 0 -27px 0 0 rgba(48, 79, 254, 0.3), 19px -19px 0 0 rgba(48, 79, 254, 0.4), 27px 0 0 0 rgba(48, 79, 254, 0.6), 19px 19px 0 0 rgba(48, 79, 254, 0.8), 0 27px 0 0 #304ffe, -19px 19px 0 0 rgba(48, 79, 254, 0.05), -27px 0 0 0 rgba(48, 79, 254, 0.1), -19px -19px 0 0 rgba(48, 79, 254, 0.2); }\n  75% {\n    -webkit-box-shadow: 0 -27px 0 0 rgba(48, 79, 254, 0.2), 19px -19px 0 0 rgba(48, 79, 254, 0.3), 27px 0 0 0 rgba(48, 79, 254, 0.4), 19px 19px 0 0 rgba(48, 79, 254, 0.6), 0 27px 0 0 rgba(48, 79, 254, 0.8), -19px 19px 0 0 #304ffe, -27px 0 0 0 rgba(48, 79, 254, 0.05), -19px -19px 0 0 rgba(48, 79, 254, 0.1);\n            box-shadow: 0 -27px 0 0 rgba(48, 79, 254, 0.2), 19px -19px 0 0 rgba(48, 79, 254, 0.3), 27px 0 0 0 rgba(48, 79, 254, 0.4), 19px 19px 0 0 rgba(48, 79, 254, 0.6), 0 27px 0 0 rgba(48, 79, 254, 0.8), -19px 19px 0 0 #304ffe, -27px 0 0 0 rgba(48, 79, 254, 0.05), -19px -19px 0 0 rgba(48, 79, 254, 0.1); }\n  87.5% {\n    -webkit-box-shadow: 0 -27px 0 0 rgba(48, 79, 254, 0.1), 19px -19px 0 0 rgba(48, 79, 254, 0.2), 27px 0 0 0 rgba(48, 79, 254, 0.3), 19px 19px 0 0 rgba(48, 79, 254, 0.4), 0 27px 0 0 rgba(48, 79, 254, 0.6), -19px 19px 0 0 rgba(48, 79, 254, 0.8), -27px 0 0 0 #304ffe, -19px -19px 0 0 rgba(48, 79, 254, 0.05);\n            box-shadow: 0 -27px 0 0 rgba(48, 79, 254, 0.1), 19px -19px 0 0 rgba(48, 79, 254, 0.2), 27px 0 0 0 rgba(48, 79, 254, 0.3), 19px 19px 0 0 rgba(48, 79, 254, 0.4), 0 27px 0 0 rgba(48, 79, 254, 0.6), -19px 19px 0 0 rgba(48, 79, 254, 0.8), -27px 0 0 0 #304ffe, -19px -19px 0 0 rgba(48, 79, 254, 0.05); }\n  100% {\n    -webkit-box-shadow: 0 -27px 0 0 rgba(48, 79, 254, 0.05), 19px -19px 0 0 rgba(48, 79, 254, 0.1), 27px 0 0 0 rgba(48, 79, 254, 0.2), 19px 19px 0 0 rgba(48, 79, 254, 0.3), 0 27px 0 0 rgba(48, 79, 254, 0.4), -19px 19px 0 0 rgba(48, 79, 254, 0.6), -27px 0 0 0 rgba(48, 79, 254, 0.8), -19px -19px 0 0 #304ffe;\n            box-shadow: 0 -27px 0 0 rgba(48, 79, 254, 0.05), 19px -19px 0 0 rgba(48, 79, 254, 0.1), 27px 0 0 0 rgba(48, 79, 254, 0.2), 19px 19px 0 0 rgba(48, 79, 254, 0.3), 0 27px 0 0 rgba(48, 79, 254, 0.4), -19px 19px 0 0 rgba(48, 79, 254, 0.6), -27px 0 0 0 rgba(48, 79, 254, 0.8), -19px -19px 0 0 #304ffe; } }\n.loader08 {\n  width: 20px;\n  height: 20px;\n  position: relative;\n  -webkit-animation: loader08-u9a43fb80 1s ease infinite;\n          animation: loader08-u9a43fb80 1s ease infinite;\n  top: 50%;\n  margin: -46px auto 0; }\n@-webkit-keyframes loader08-u9a43fb80 {\n  0%, 100% {\n    -webkit-box-shadow: -13px 20px 0 #304ffe, 13px 20px 0 rgba(48, 79, 254, 0.2), 13px 46px 0 rgba(48, 79, 254, 0.2), -13px 46px 0 rgba(48, 79, 254, 0.2);\n            box-shadow: -13px 20px 0 #304ffe, 13px 20px 0 rgba(48, 79, 254, 0.2), 13px 46px 0 rgba(48, 79, 254, 0.2), -13px 46px 0 rgba(48, 79, 254, 0.2); }\n  25% {\n    -webkit-box-shadow: -13px 20px 0 rgba(48, 79, 254, 0.2), 13px 20px 0 #304ffe, 13px 46px 0 rgba(48, 79, 254, 0.2), -13px 46px 0 rgba(48, 79, 254, 0.2);\n            box-shadow: -13px 20px 0 rgba(48, 79, 254, 0.2), 13px 20px 0 #304ffe, 13px 46px 0 rgba(48, 79, 254, 0.2), -13px 46px 0 rgba(48, 79, 254, 0.2); }\n  50% {\n    -webkit-box-shadow: -13px 20px 0 rgba(48, 79, 254, 0.2), 13px 20px 0 rgba(48, 79, 254, 0.2), 13px 46px 0 #304ffe, -13px 46px 0 rgba(48, 79, 254, 0.2);\n            box-shadow: -13px 20px 0 rgba(48, 79, 254, 0.2), 13px 20px 0 rgba(48, 79, 254, 0.2), 13px 46px 0 #304ffe, -13px 46px 0 rgba(48, 79, 254, 0.2); }\n  75% {\n    -webkit-box-shadow: -13px 20px 0 rgba(48, 79, 254, 0.2), 13px 20px 0 rgba(48, 79, 254, 0.2), 13px 46px 0 rgba(48, 79, 254, 0.2), -13px 46px 0 #304ffe;\n            box-shadow: -13px 20px 0 rgba(48, 79, 254, 0.2), 13px 20px 0 rgba(48, 79, 254, 0.2), 13px 46px 0 rgba(48, 79, 254, 0.2), -13px 46px 0 #304ffe; } }\n@keyframes loader08-u9a43fb80 {\n  0%, 100% {\n    -webkit-box-shadow: -13px 20px 0 #304ffe, 13px 20px 0 rgba(48, 79, 254, 0.2), 13px 46px 0 rgba(48, 79, 254, 0.2), -13px 46px 0 rgba(48, 79, 254, 0.2);\n            box-shadow: -13px 20px 0 #304ffe, 13px 20px 0 rgba(48, 79, 254, 0.2), 13px 46px 0 rgba(48, 79, 254, 0.2), -13px 46px 0 rgba(48, 79, 254, 0.2); }\n  25% {\n    -webkit-box-shadow: -13px 20px 0 rgba(48, 79, 254, 0.2), 13px 20px 0 #304ffe, 13px 46px 0 rgba(48, 79, 254, 0.2), -13px 46px 0 rgba(48, 79, 254, 0.2);\n            box-shadow: -13px 20px 0 rgba(48, 79, 254, 0.2), 13px 20px 0 #304ffe, 13px 46px 0 rgba(48, 79, 254, 0.2), -13px 46px 0 rgba(48, 79, 254, 0.2); }\n  50% {\n    -webkit-box-shadow: -13px 20px 0 rgba(48, 79, 254, 0.2), 13px 20px 0 rgba(48, 79, 254, 0.2), 13px 46px 0 #304ffe, -13px 46px 0 rgba(48, 79, 254, 0.2);\n            box-shadow: -13px 20px 0 rgba(48, 79, 254, 0.2), 13px 20px 0 rgba(48, 79, 254, 0.2), 13px 46px 0 #304ffe, -13px 46px 0 rgba(48, 79, 254, 0.2); }\n  75% {\n    -webkit-box-shadow: -13px 20px 0 rgba(48, 79, 254, 0.2), 13px 20px 0 rgba(48, 79, 254, 0.2), 13px 46px 0 rgba(48, 79, 254, 0.2), -13px 46px 0 #304ffe;\n            box-shadow: -13px 20px 0 rgba(48, 79, 254, 0.2), 13px 20px 0 rgba(48, 79, 254, 0.2), 13px 46px 0 rgba(48, 79, 254, 0.2), -13px 46px 0 #304ffe; } }\n.loader09 {\n  width: 10px;\n  height: 48px;\n  background: #304ffe;\n  position: relative;\n  -webkit-animation: loader09-u8f691a9e 1s ease-in-out infinite;\n          animation: loader09-u8f691a9e 1s ease-in-out infinite;\n  -webkit-animation-delay: 0.4s;\n          animation-delay: 0.4s;\n  top: 50%;\n  margin: -28px auto 0; }\n.loader09::after, .loader09::before {\n    content: '';\n    position: absolute;\n    width: 10px;\n    height: 48px;\n    background: #304ffe;\n    -webkit-animation: loader09-u8f691a9e 1s ease-in-out infinite;\n            animation: loader09-u8f691a9e 1s ease-in-out infinite; }\n.loader09::before {\n    right: 18px;\n    -webkit-animation-delay: 0.2s;\n            animation-delay: 0.2s; }\n.loader09::after {\n    left: 18px;\n    -webkit-animation-delay: 0.6s;\n            animation-delay: 0.6s; }\n@-webkit-keyframes loader09-u8f691a9e {\n  0%, 100% {\n    -webkit-box-shadow: 0 0 0 #304ffe, 0 0 0 #304ffe;\n            box-shadow: 0 0 0 #304ffe, 0 0 0 #304ffe; }\n  50% {\n    -webkit-box-shadow: 0 -8px 0 #304ffe, 0 8px 0 #304ffe;\n            box-shadow: 0 -8px 0 #304ffe, 0 8px 0 #304ffe; } }\n@keyframes loader09-u8f691a9e {\n  0%, 100% {\n    -webkit-box-shadow: 0 0 0 #304ffe, 0 0 0 #304ffe;\n            box-shadow: 0 0 0 #304ffe, 0 0 0 #304ffe; }\n  50% {\n    -webkit-box-shadow: 0 -8px 0 #304ffe, 0 8px 0 #304ffe;\n            box-shadow: 0 -8px 0 #304ffe, 0 8px 0 #304ffe; } }\n.loader10 {\n  width: 28px;\n  height: 28px;\n  border-radius: 50%;\n  position: relative;\n  -webkit-animation: loader10-ubf19b8bc 0.9s ease alternate infinite;\n          animation: loader10-ubf19b8bc 0.9s ease alternate infinite;\n  -webkit-animation-delay: 0.36s;\n          animation-delay: 0.36s;\n  top: 50%;\n  margin: -42px auto 0; }\n.loader10::after, .loader10::before {\n    content: '';\n    position: absolute;\n    width: 28px;\n    height: 28px;\n    border-radius: 50%;\n    -webkit-animation: loader10-ubf19b8bc 0.9s ease alternate infinite;\n            animation: loader10-ubf19b8bc 0.9s ease alternate infinite; }\n.loader10::before {\n    left: -40px;\n    -webkit-animation-delay: 0.18s;\n            animation-delay: 0.18s; }\n.loader10::after {\n    right: -40px;\n    -webkit-animation-delay: 0.54s;\n            animation-delay: 0.54s; }\n@-webkit-keyframes loader10-ubf19b8bc {\n  0% {\n    -webkit-box-shadow: 0 28px 0 -28px #304ffe;\n            box-shadow: 0 28px 0 -28px #304ffe; }\n  100% {\n    -webkit-box-shadow: 0 28px 0 #304ffe;\n            box-shadow: 0 28px 0 #304ffe; } }\n@keyframes loader10-ubf19b8bc {\n  0% {\n    -webkit-box-shadow: 0 28px 0 -28px #304ffe;\n            box-shadow: 0 28px 0 -28px #304ffe; }\n  100% {\n    -webkit-box-shadow: 0 28px 0 #304ffe;\n            box-shadow: 0 28px 0 #304ffe; } }\n.loader11 {\n  width: 20px;\n  height: 20px;\n  border-radius: 50%;\n  -webkit-box-shadow: 0 40px 0 #304ffe;\n          box-shadow: 0 40px 0 #304ffe;\n  position: relative;\n  -webkit-animation: loader11-u4c35e79b 0.8s ease-in-out alternate infinite;\n          animation: loader11-u4c35e79b 0.8s ease-in-out alternate infinite;\n  -webkit-animation-delay: 0.32s;\n          animation-delay: 0.32s;\n  top: 50%;\n  margin: -50px auto 0; }\n.loader11::after, .loader11::before {\n    content: '';\n    position: absolute;\n    width: 20px;\n    height: 20px;\n    border-radius: 50%;\n    -webkit-box-shadow: 0 40px 0 #304ffe;\n            box-shadow: 0 40px 0 #304ffe;\n    -webkit-animation: loader11-u4c35e79b 0.8s ease-in-out alternate infinite;\n            animation: loader11-u4c35e79b 0.8s ease-in-out alternate infinite; }\n.loader11::before {\n    left: -30px;\n    -webkit-animation-delay: 0.48s;\n            animation-delay: 0.48s; }\n.loader11::after {\n    right: -30px;\n    -webkit-animation-delay: 0.16s;\n            animation-delay: 0.16s; }\n@-webkit-keyframes loader11-u4c35e79b {\n  0% {\n    -webkit-box-shadow: 0 40px 0 #304ffe;\n            box-shadow: 0 40px 0 #304ffe; }\n  100% {\n    -webkit-box-shadow: 0 20px 0 #304ffe;\n            box-shadow: 0 20px 0 #304ffe; } }\n@keyframes loader11-u4c35e79b {\n  0% {\n    -webkit-box-shadow: 0 40px 0 #304ffe;\n            box-shadow: 0 40px 0 #304ffe; }\n  100% {\n    -webkit-box-shadow: 0 20px 0 #304ffe;\n            box-shadow: 0 20px 0 #304ffe; } }\n.loader12 {\n  width: 20px;\n  height: 20px;\n  border-radius: 50%;\n  position: relative;\n  -webkit-animation: loader12-u17b972f9 1s linear alternate infinite;\n          animation: loader12-u17b972f9 1s linear alternate infinite;\n  top: 50%;\n  margin: -50px auto 0; }\n@-webkit-keyframes loader12-u17b972f9 {\n  0% {\n    -webkit-box-shadow: -60px 40px 0 2px #304ffe, -30px 40px 0 0 rgba(48, 79, 254, 0.2), 0 40px 0 0 rgba(48, 79, 254, 0.2), 30px 40px 0 0 rgba(48, 79, 254, 0.2), 60px 40px 0 0 rgba(48, 79, 254, 0.2);\n            box-shadow: -60px 40px 0 2px #304ffe, -30px 40px 0 0 rgba(48, 79, 254, 0.2), 0 40px 0 0 rgba(48, 79, 254, 0.2), 30px 40px 0 0 rgba(48, 79, 254, 0.2), 60px 40px 0 0 rgba(48, 79, 254, 0.2); }\n  25% {\n    -webkit-box-shadow: -60px 40px 0 0 rgba(48, 79, 254, 0.2), -30px 40px 0 2px #304ffe, 0 40px 0 0 rgba(48, 79, 254, 0.2), 30px 40px 0 0 rgba(48, 79, 254, 0.2), 60px 40px 0 0 rgba(48, 79, 254, 0.2);\n            box-shadow: -60px 40px 0 0 rgba(48, 79, 254, 0.2), -30px 40px 0 2px #304ffe, 0 40px 0 0 rgba(48, 79, 254, 0.2), 30px 40px 0 0 rgba(48, 79, 254, 0.2), 60px 40px 0 0 rgba(48, 79, 254, 0.2); }\n  50% {\n    -webkit-box-shadow: -60px 40px 0 0 rgba(48, 79, 254, 0.2), -30px 40px 0 0 rgba(48, 79, 254, 0.2), 0 40px 0 2px #304ffe, 30px 40px 0 0 rgba(48, 79, 254, 0.2), 60px 40px 0 0 rgba(48, 79, 254, 0.2);\n            box-shadow: -60px 40px 0 0 rgba(48, 79, 254, 0.2), -30px 40px 0 0 rgba(48, 79, 254, 0.2), 0 40px 0 2px #304ffe, 30px 40px 0 0 rgba(48, 79, 254, 0.2), 60px 40px 0 0 rgba(48, 79, 254, 0.2); }\n  75% {\n    -webkit-box-shadow: -60px 40px 0 0 rgba(48, 79, 254, 0.2), -30px 40px 0 0 rgba(48, 79, 254, 0.2), 0 40px 0 0 rgba(48, 79, 254, 0.2), 30px 40px 0 2px #304ffe, 60px 40px 0 0 rgba(48, 79, 254, 0.2);\n            box-shadow: -60px 40px 0 0 rgba(48, 79, 254, 0.2), -30px 40px 0 0 rgba(48, 79, 254, 0.2), 0 40px 0 0 rgba(48, 79, 254, 0.2), 30px 40px 0 2px #304ffe, 60px 40px 0 0 rgba(48, 79, 254, 0.2); }\n  100% {\n    -webkit-box-shadow: -60px 40px 0 0 rgba(48, 79, 254, 0.2), -30px 40px 0 0 rgba(48, 79, 254, 0.2), 0 40px 0 0 rgba(48, 79, 254, 0.2), 30px 40px 0 0 rgba(48, 79, 254, 0.2), 60px 40px 0 2px #304ffe;\n            box-shadow: -60px 40px 0 0 rgba(48, 79, 254, 0.2), -30px 40px 0 0 rgba(48, 79, 254, 0.2), 0 40px 0 0 rgba(48, 79, 254, 0.2), 30px 40px 0 0 rgba(48, 79, 254, 0.2), 60px 40px 0 2px #304ffe; } }\n@keyframes loader12-u17b972f9 {\n  0% {\n    -webkit-box-shadow: -60px 40px 0 2px #304ffe, -30px 40px 0 0 rgba(48, 79, 254, 0.2), 0 40px 0 0 rgba(48, 79, 254, 0.2), 30px 40px 0 0 rgba(48, 79, 254, 0.2), 60px 40px 0 0 rgba(48, 79, 254, 0.2);\n            box-shadow: -60px 40px 0 2px #304ffe, -30px 40px 0 0 rgba(48, 79, 254, 0.2), 0 40px 0 0 rgba(48, 79, 254, 0.2), 30px 40px 0 0 rgba(48, 79, 254, 0.2), 60px 40px 0 0 rgba(48, 79, 254, 0.2); }\n  25% {\n    -webkit-box-shadow: -60px 40px 0 0 rgba(48, 79, 254, 0.2), -30px 40px 0 2px #304ffe, 0 40px 0 0 rgba(48, 79, 254, 0.2), 30px 40px 0 0 rgba(48, 79, 254, 0.2), 60px 40px 0 0 rgba(48, 79, 254, 0.2);\n            box-shadow: -60px 40px 0 0 rgba(48, 79, 254, 0.2), -30px 40px 0 2px #304ffe, 0 40px 0 0 rgba(48, 79, 254, 0.2), 30px 40px 0 0 rgba(48, 79, 254, 0.2), 60px 40px 0 0 rgba(48, 79, 254, 0.2); }\n  50% {\n    -webkit-box-shadow: -60px 40px 0 0 rgba(48, 79, 254, 0.2), -30px 40px 0 0 rgba(48, 79, 254, 0.2), 0 40px 0 2px #304ffe, 30px 40px 0 0 rgba(48, 79, 254, 0.2), 60px 40px 0 0 rgba(48, 79, 254, 0.2);\n            box-shadow: -60px 40px 0 0 rgba(48, 79, 254, 0.2), -30px 40px 0 0 rgba(48, 79, 254, 0.2), 0 40px 0 2px #304ffe, 30px 40px 0 0 rgba(48, 79, 254, 0.2), 60px 40px 0 0 rgba(48, 79, 254, 0.2); }\n  75% {\n    -webkit-box-shadow: -60px 40px 0 0 rgba(48, 79, 254, 0.2), -30px 40px 0 0 rgba(48, 79, 254, 0.2), 0 40px 0 0 rgba(48, 79, 254, 0.2), 30px 40px 0 2px #304ffe, 60px 40px 0 0 rgba(48, 79, 254, 0.2);\n            box-shadow: -60px 40px 0 0 rgba(48, 79, 254, 0.2), -30px 40px 0 0 rgba(48, 79, 254, 0.2), 0 40px 0 0 rgba(48, 79, 254, 0.2), 30px 40px 0 2px #304ffe, 60px 40px 0 0 rgba(48, 79, 254, 0.2); }\n  100% {\n    -webkit-box-shadow: -60px 40px 0 0 rgba(48, 79, 254, 0.2), -30px 40px 0 0 rgba(48, 79, 254, 0.2), 0 40px 0 0 rgba(48, 79, 254, 0.2), 30px 40px 0 0 rgba(48, 79, 254, 0.2), 60px 40px 0 2px #304ffe;\n            box-shadow: -60px 40px 0 0 rgba(48, 79, 254, 0.2), -30px 40px 0 0 rgba(48, 79, 254, 0.2), 0 40px 0 0 rgba(48, 79, 254, 0.2), 30px 40px 0 0 rgba(48, 79, 254, 0.2), 60px 40px 0 2px #304ffe; } }\n.card {\n  min-height: 160px; }\n"

/***/ }),

/***/ "./src/app/components/spinners/spinners.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SpinnersComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var SpinnersComponent = (function () {
    function SpinnersComponent() {
    }
    SpinnersComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-spinners',
            template: __webpack_require__("./src/app/components/spinners/spinners.component.html"),
            styles: [__webpack_require__("./src/app/components/spinners/spinners.component.scss")]
        })
    ], SpinnersComponent);
    return SpinnersComponent;
}());

//# sourceMappingURL=spinners.component.js.map

/***/ }),

/***/ "./src/app/components/tabs/tabs.component.html":
/***/ (function(module, exports) {

module.exports = "<p class=\"ff-headers text-uppercase mb-3 fw-600\">Tabset</p>\r\n<div class=\"mb-5\">\r\n  <ngb-tabset>\r\n    <ngb-tab title=\"Simple\">\r\n      <ng-template ngbTabContent>Simple content</ng-template>\r\n    </ngb-tab>\r\n    <ngb-tab>\r\n      <ng-template ngbTabTitle><b>Fancy</b> title</ng-template>\r\n      <ng-template ngbTabContent>Fancy title content</ng-template>\r\n    </ngb-tab>\r\n    <ngb-tab title=\"Disabled\" [disabled]=\"true\">\r\n      <ng-template ngbTabContent>Disabled content</ng-template>\r\n    </ngb-tab>\r\n  </ngb-tabset>\r\n</div>\r\n\r\n<p class=\"ff-headers text-uppercase mb-3 fw-600\">Pills</p>\r\n<div class=\"mb-5\">\r\n  <ngb-tabset type=\"pills\">\r\n    <ngb-tab title=\"Simple\">\r\n      <ng-template ngbTabContent>Simple content</ng-template>\r\n    </ngb-tab>\r\n    <ngb-tab>\r\n      <ng-template ngbTabTitle><b>Fancy</b> title</ng-template>\r\n      <ng-template ngbTabContent>Fancy title content</ng-template>\r\n    </ngb-tab>\r\n    <ngb-tab title=\"Disabled\" [disabled]=\"true\">\r\n      <ng-template ngbTabContent>Disabled content</ng-template>\r\n    </ngb-tab>\r\n  </ngb-tabset>\r\n</div>\r\n\r\n<p class=\"ff-headers text-uppercase mb-3 fw-600\">Select an active tab by id</p>\r\n<div class=\"mb-5\">\r\n  <ngb-tabset #t=\"ngbTabset\">\r\n    <ngb-tab title=\"Simple\" id=\"foo\">\r\n      <ng-template ngbTabContent>Simple content</ng-template>\r\n    </ngb-tab>\r\n    <ngb-tab id=\"bar\">\r\n      <ng-template ngbTabTitle><b>Fancy</b> title</ng-template>\r\n      <ng-template ngbTabContent>Fancy title content</ng-template>\r\n    </ngb-tab>\r\n  </ngb-tabset>\r\n  <p>\r\n    <button class=\"btn btn-outline-primary\" (click)=\"t.select('bar')\">Selected tab with \"bar\" id</button>\r\n  </p>\r\n</div>\r\n\r\n<p class=\"ff-headers text-uppercase mb-3 fw-600\">Prevent tab change</p>\r\n<div class=\"mb-5\">\r\n  <ngb-tabset (tabChange)=\"beforeChange($event)\">\r\n    <ngb-tab title=\"Simple\" id=\"foo\">\r\n      <ng-template ngbTabContent>Simple content</ng-template>\r\n    </ngb-tab>\r\n    <ngb-tab id=\"bar\" title=\"I can't be selected...\">\r\n      <ng-template ngbTabContent>I can't be selected content</ng-template>\r\n    </ngb-tab>\r\n    <ngb-tab title=\"But I can!\" >\r\n      <ng-template ngbTabContent>But I can content</ng-template>\r\n    </ngb-tab>\r\n  </ngb-tabset>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/components/tabs/tabs.component.scss":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/components/tabs/tabs.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TabsComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var TabsComponent = (function () {
    function TabsComponent() {
    }
    TabsComponent.prototype.beforeChange = function ($event) {
        if ($event.nextId === 'bar') {
            $event.preventDefault();
        }
    };
    TabsComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-tabs',
            template: __webpack_require__("./src/app/components/tabs/tabs.component.html"),
            styles: [__webpack_require__("./src/app/components/tabs/tabs.component.scss")]
        })
    ], TabsComponent);
    return TabsComponent;
}());

//# sourceMappingURL=tabs.component.js.map

/***/ }),

/***/ "./src/app/components/timepicker/timepicker.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\r\n  <div class=\"col-lg-4\"> \r\n    <div class=\"card\">\r\n      <div class=\"card-header\">Basic Timepicker</div>\r\n      <div class=\"card-body\">\r\n        <ngb-timepicker [(ngModel)]=\"time\"></ngb-timepicker>\r\n        <button class=\"btn mb-3 mt-3\" style=\"opacity: 0;\">&nbsp;</button>\r\n        <pre>Selected time: {{time | json}}</pre>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <div class=\"col-lg-4\"> \r\n    <div class=\"card\">\r\n      <div class=\"card-header\">Meridian</div>\r\n      <div class=\"card-body\">\r\n        <ngb-timepicker [(ngModel)]=\"time\" [meridian]=\"meridian\"></ngb-timepicker>\r\n        <button class=\"btn btn-outline-{{meridian ? 'success' : 'danger'}} mb-3 mt-3\" (click)=\"toggleMeridian()\">\r\n            Meridian - {{meridian ? \"ON\" : \"OFF\"}}\r\n        </button>\r\n        <pre>Selected time: {{time | json}}</pre>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <div class=\"col-lg-4\"> \r\n    <div class=\"card\">\r\n      <div class=\"card-header\">Seconds</div>\r\n      <div class=\"card-body\">\r\n        <ngb-timepicker [(ngModel)]=\"time\" [seconds]=\"seconds\"></ngb-timepicker>\r\n        <button class=\"btn btn-outline-{{seconds ? 'success' : 'danger'}} mb-3 mt-3\" (click)=\"toggleSeconds()\">\r\n            Seconds - {{seconds ? \"ON\" : \"OFF\"}}\r\n        </button>\r\n        <pre>Selected time: {{time | json}}</pre>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n\r\n<div class=\"row\">\r\n  <div class=\"col-lg-4\"> \r\n    <div class=\"card\">\r\n      <div class=\"card-header\">Spinners</div>\r\n      <div class=\"card-body\">\r\n        <ngb-timepicker [(ngModel)]=\"time\" [spinners]=\"spinners\"></ngb-timepicker>\r\n\r\n        <button class=\"m-t-1 btn btn-outline-{{spinners ? 'success' : 'danger'}} mb-3 mt-3\" (click)=\"toggleSpinners()\">\r\n            Spinners - {{spinners ? \"ON\" : \"OFF\"}}\r\n        </button>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <div class=\"col-lg-4\">\r\n    <div class=\"card\">\r\n      <div class=\"card-header\">Custom steps</div>\r\n      <div class=\"card-body\">\r\n        <ngb-timepicker [(ngModel)]=\"time\" [seconds]=\"true\" \r\n        [hourStep]=\"hourStep\" [minuteStep]=\"minuteStep\" [secondStep]=\"secondStep\"></ngb-timepicker>\r\n\r\n        <div class=\"row\">\r\n            <div class=\"col-sm-3\">\r\n                <label for=\"changeHourStep\">Hour Step</label>\r\n                <input type=\"number\" class=\"form-control form-control-sm\" [(ngModel)]=\"hourStep\" />\r\n            </div>    \r\n            <div class=\"col-sm-3\">\r\n                <label for=\"changeMinuteStep\">Minute Step</label>\r\n                <input type=\"number\" class=\"form-control form-control-sm\" [(ngModel)]=\"minuteStep\" />\r\n            </div>\r\n            <div class=\"col-sm-3\">\r\n                <label for=\"changeSecondStep\">Second Step</label>\r\n                <input type=\"number\" class=\"form-control form-control-sm\" [(ngModel)]=\"secondStep\" />\r\n            </div>\r\n        </div>\r\n        <pre>Selected time: {{time | json}}</pre>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <div class=\"col-lg-4\">\r\n    <div class=\"card\">\r\n      <div class=\"card-header\">Custom validation</div>\r\n      <div class=\"card-body\">\r\n        <p>Illustrates custom validation, you have to select time between 12:00 and 13:59</p>\r\n\r\n        <div class=\"form-group\" [class.has-success]=\"ctrl.valid\" [class.has-danger]=\"!ctrl.valid\">\r\n          <ngb-timepicker [(ngModel)]=\"time\" [formControl]=\"ctrl\" required></ngb-timepicker>\r\n          <div class=\"form-control-feedback\">\r\n            <div *ngIf=\"ctrl.valid\">Great choice</div>\r\n            <div *ngIf=\"ctrl.errors && ctrl.errors['required']\">Select some time during lunchtime</div>\r\n            <div *ngIf=\"ctrl.errors && ctrl.errors['tooLate']\">Oh no, it's way too late</div>\r\n            <div *ngIf=\"ctrl.errors && ctrl.errors['tooEarly']\">It's a bit too early</div>\r\n          </div>\r\n        </div>\r\n\r\n        <pre>Selected time: {{time | json}}</pre>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./src/app/components/timepicker/timepicker.component.scss":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/components/timepicker/timepicker.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TimepickerComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("./node_modules/@angular/forms/@angular/forms.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


var TimepickerComponent = (function () {
    function TimepickerComponent() {
        this.meridian = true;
        this.time = { hour: 13, minute: 30, second: 30 };
        this.seconds = true;
        this.spinners = true;
        this.hourStep = 1;
        this.minuteStep = 15;
        this.secondStep = 30;
        this.ctrl = new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["FormControl"]('', function (control) {
            var value = control.value;
            if (!value) {
                return null;
            }
            if (value.hour < 12) {
                return { tooEarly: true };
            }
            if (value.hour > 13) {
                return { tooLate: true };
            }
            return null;
        });
    }
    TimepickerComponent.prototype.toggleMeridian = function () {
        this.meridian = !this.meridian;
    };
    TimepickerComponent.prototype.toggleSeconds = function () {
        this.seconds = !this.seconds;
    };
    TimepickerComponent.prototype.toggleSpinners = function () {
        this.spinners = !this.spinners;
    };
    TimepickerComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-timepicker',
            template: __webpack_require__("./src/app/components/timepicker/timepicker.component.html"),
            styles: [__webpack_require__("./src/app/components/timepicker/timepicker.component.scss")]
        })
    ], TimepickerComponent);
    return TimepickerComponent;
}());

//# sourceMappingURL=timepicker.component.js.map

/***/ }),

/***/ "./src/app/components/tooltip/tooltip.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\r\n  <div class=\"col-lg-6\">\r\n    <div class=\"card\">\r\n      <div class=\"card-header\">Quick and easy tooltips</div>\r\n      <div class=\"card-body\">\r\n        <button type=\"button\" class=\"btn btn-secondary mr-1 mb-1\" placement=\"top\" ngbTooltip=\"Tooltip on top\">\r\n          Tooltip on top\r\n        </button>\r\n        <button type=\"button\" class=\"btn btn-secondary mr-1 mb-1\" placement=\"right\" ngbTooltip=\"Tooltip on right\">\r\n          Tooltip on right\r\n        </button>\r\n        <button type=\"button\" class=\"btn btn-secondary mr-1 mb-1\" placement=\"bottom\" ngbTooltip=\"Tooltip on bottom\">\r\n          Tooltip on bottom\r\n        </button>\r\n        <button type=\"button\" class=\"btn btn-secondary mr-1 mb-1\" placement=\"left\" ngbTooltip=\"Tooltip on left\">\r\n          Tooltip on left\r\n        </button>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <div class=\"col-lg-6\">\r\n    <div class=\"card\">\r\n      <div class=\"card-header\">HTML and bindings in tooltips</div>\r\n      <div class=\"card-body\">\r\n        <p>\r\n          Tooltips can contain any arbitrary HTML, Angular bindings and even directives!\r\n          Simply enclose desired content in a <code>&lt;template&gt;</code> element.\r\n        </p>\r\n\r\n        <ng-template #tipContent>Hello, <b>{{name}}</b>!</ng-template>\r\n        <button type=\"button\" class=\"btn btn-secondary\" [ngbTooltip]=\"tipContent\">\r\n          I've got markup and bindings in my tooltip!\r\n        </button>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n\r\n<div class=\"card\">\r\n  <div class=\"card-header\">Custom and manual triggers</div>\r\n  <div class=\"card-body\">\r\n    <p>\r\n      You can easily override open and close triggers by specifying event names (separated by <code>:</code>) in the <code>triggers</code> property.\r\n    </p>\r\n\r\n    <button type=\"button\" class=\"btn btn-secondary\" ngbTooltip=\"You see, I show up on click!\" triggers=\"click:blur\">\r\n      Click me!\r\n    </button>\r\n\r\n    <p class=\"pt-3\">\r\n      Alternatively you can take full manual control over tooltip opening / closing events.\r\n    </p>\r\n\r\n    <button type=\"button\" class=\"btn btn-secondary\" ngbTooltip=\"What a great tip!\" triggers=\"manual\" #t=\"ngbTooltip\" (click)=\"t.open()\">\r\n      Click me to open a tooltip\r\n    </button>\r\n    <button type=\"button\" class=\"btn btn-secondary\" (click)=\"t.close()\">\r\n      Click me to close a tooltip\r\n    </button>\r\n  </div>\r\n</div>\r\n\r\n<div class=\"row\">\r\n  <div class=\"col-lg-6\">\r\n    <div class=\"card\">\r\n      <div class=\"card-header\">Context and manual triggers</div>\r\n      <div class=\"card-body\">\r\n        <p>\r\n          You can optionally pass in a context when manually triggering a popover.\r\n        </p>\r\n\r\n        <ng-template #tipContent let-greeting=\"greeting\">{{greeting}}, <b>{{name}}</b>!</ng-template>\r\n        <p>\r\n          How would you like to greet <strong [ngbTooltip]=\"tipContent\" #t=\"ngbTooltip\" triggers=\"manual\">me</strong>?\r\n        </p>\r\n        <button type=\"button\" class=\"btn btn-secondary\" (click)=\"changeGreeting({ greeting: 'Bonjour' })\">\r\n          French\r\n        </button>\r\n        <button type=\"button\" class=\"btn btn-secondary\" (click)=\"changeGreeting({ greeting: 'Gutentag' })\">\r\n          German\r\n        </button>\r\n        <button type=\"button\" class=\"btn btn-secondary\" (click)=\"changeGreeting({ greeting: 'Hello' })\">\r\n          English\r\n        </button>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <div class=\"col-lg-6\">\r\n    <div class=\"card\">\r\n      <div class=\"card-header\">Modal</div>\r\n      <div class=\"card-body\">\r\n        <p>\r\n          Set the <code>container</code> property to \"body\" to have the tooltip be appended to the body instead of the triggering element's parent. This option is useful if the element triggering the tooltip is inside an element that clips its contents (i.e. <code>overflow: hidden</code>).\r\n        </p>\r\n\r\n        <div class='row'>\r\n          <div class='card' style=\"padding: 50px 0; text-align: center; overflow:hidden\">\r\n            <button type=\"button\" class=\"btn btn-secondary\"\r\n                    ngbTooltip=\"Vivamus sagittis lacus vel augue laoreet rutrum faucibus.\">\r\n              Default tooltip\r\n            </button>\r\n            <button type=\"button\" class=\"btn btn-secondary\"\r\n                    ngbTooltip=\"Vivamus sagittis lacus vel augue laoreet rutrum faucibus.\" container=\"body\">\r\n              Tooltip appended to body\r\n            </button>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/components/tooltip/tooltip.component.scss":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/components/tooltip/tooltip.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TooltipComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ng_bootstrap_ng_bootstrap__ = __webpack_require__("./node_modules/@ng-bootstrap/ng-bootstrap/index.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var TooltipComponent = (function () {
    function TooltipComponent() {
        this.greeting = {};
        this.name = 'World';
    }
    TooltipComponent.prototype.changeGreeting = function (greeting) {
        var isOpen = this.tooltip.isOpen();
        this.tooltip.close();
        if (greeting !== this.greeting || !isOpen) {
            this.greeting = greeting;
            this.tooltip.open(greeting);
        }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('t'),
        __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__ng_bootstrap_ng_bootstrap__["k" /* NgbTooltip */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__ng_bootstrap_ng_bootstrap__["k" /* NgbTooltip */]) === "function" && _a || Object)
    ], TooltipComponent.prototype, "tooltip", void 0);
    TooltipComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-tooltip',
            template: __webpack_require__("./src/app/components/tooltip/tooltip.component.html"),
            styles: [__webpack_require__("./src/app/components/tooltip/tooltip.component.scss")]
        })
    ], TooltipComponent);
    return TooltipComponent;
    var _a;
}());

//# sourceMappingURL=tooltip.component.js.map

/***/ }),

/***/ "./src/app/components/typeahead/typeahead.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\r\n  <div class=\"col-lg-6\">\r\n    <div class=\"card\">\r\n      <div class=\"card-header\">Simple Typeahead</div>\r\n      <div class=\"card-body\">\r\n        A typeahead example that gets values from a static <code>string[]</code>\r\n        <ul>\r\n          <li><code>debounceTime</code> operator</li>\r\n          <li>kicks in only if 2+ characters typed</li>\r\n          <li>limits to 10 results</li>\r\n        </ul>\r\n\r\n        <input type=\"text\" class=\"form-control mb-3\" [(ngModel)]=\"model\" [ngbTypeahead]=\"search\" />\r\n\r\n        <pre>Model: {{ model | json }}</pre>\r\n      </div>\r\n    </div>\r\n\r\n    <div class=\"card\">\r\n      <div class=\"card-header\">Formatted results</div>\r\n      <div class=\"card-body\">\r\n        <p>A typeahead example that uses a formatter function for string results</p>\r\n\r\n        <input type=\"text\" class=\"form-control mb-3\" [(ngModel)]=\"model\" [ngbTypeahead]=\"search\" [resultFormatter]=\"formatter\" />\r\n\r\n        <pre>Model: {{ model | json }}</pre>\r\n      </div>\r\n    </div>\r\n  </div>\r\n\r\n  <div class=\"col-lg-6\">\r\n    <div class=\"card\">\r\n      <div class=\"card-header\">Wikipedia search</div>\r\n      <div class=\"card-body\">\r\n        A typeahead example that gets values from the <code>WikipediaService</code>\r\n        <ul>\r\n          <li>remote data retrieval</li>\r\n          <li><code>debounceTime</code> operator</li>\r\n          <li><code>do</code> operator</li>\r\n          <li><code>distinctUntilChanged</code> operator</li>\r\n          <li><code>switchMap</code> operator</li>\r\n          <li><code>catch</code> operator to display an error message in case of connectivity issue</li>\r\n        </ul>\r\n\r\n        <div class=\"form-group\" [class.has-danger]=\"searchFailed\">\r\n          <input type=\"text\" class=\"form-control\" [(ngModel)]=\"model\" [ngbTypeahead]=\"searchWiki\" placeholder=\"Wikipedia search\" />\r\n          <span *ngIf=\"searching\">searching...</span>\r\n          <div class=\"form-control-feedback\" *ngIf=\"searchFailed\">Sorry, suggestions could not be loaded.</div>\r\n        </div>\r\n\r\n        <pre>Model: {{ model | json }}</pre>\r\n      </div>\r\n    </div>\r\n\r\n    <div class=\"card\">\r\n      <div class=\"card-header\">Template for results</div>\r\n      <div class=\"card-body\">\r\n        <p>A typeahead example that uses custom template for results display and uses object as a model</p>\r\n\r\n        <ng-template #rt let-r=\"result\" let-t=\"term\">\r\n          <img [src]=\"'https://upload.wikimedia.org/wikipedia/commons/thumb/' + r.flag\" width=\"16\">\r\n          {{ r.name}}\r\n        </ng-template>\r\n\r\n        <input type=\"text\" class=\"form-control mb-3\" [(ngModel)]=\"model\" [ngbTypeahead]=\"searchFlags\" [resultTemplate]=\"rt\" [inputFormatter]=\"formatter\" />\r\n\r\n        <pre>Model: {{ model | json }}</pre>\r\n      </div>\r\n    </div>\r\n  </div>\r\n\r\n  <div class=\"col-lg-6\">\r\n  </div>\r\n\r\n  <div class=\"col-lg-6\">  \r\n  </div>\r\n</div>"

/***/ }),

/***/ "./src/app/components/typeahead/typeahead.component.scss":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/components/typeahead/typeahead.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export WikipediaService */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TypeaheadComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("./node_modules/@angular/http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__ = __webpack_require__("./node_modules/rxjs/Observable.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__("./node_modules/rxjs/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_debounceTime__ = __webpack_require__("./node_modules/rxjs/add/operator/debounceTime.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_debounceTime___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_debounceTime__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_distinctUntilChanged__ = __webpack_require__("./node_modules/rxjs/add/operator/distinctUntilChanged.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_distinctUntilChanged___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_distinctUntilChanged__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_observable_of__ = __webpack_require__("./node_modules/rxjs/add/observable/of.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_observable_of___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_rxjs_add_observable_of__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_rxjs_add_operator_catch__ = __webpack_require__("./node_modules/rxjs/add/operator/catch.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_rxjs_add_operator_catch___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_rxjs_add_operator_catch__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_rxjs_add_operator_do__ = __webpack_require__("./node_modules/rxjs/add/operator/do.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_rxjs_add_operator_do___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_8_rxjs_add_operator_do__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_rxjs_add_operator_switchMap__ = __webpack_require__("./node_modules/rxjs/add/operator/switchMap.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_rxjs_add_operator_switchMap___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_9_rxjs_add_operator_switchMap__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var states = ['Alabama', 'Alaska', 'American Samoa', 'Arizona', 'Arkansas', 'California', 'Colorado',
    'Connecticut', 'Delaware', 'District Of Columbia', 'Federated States Of Micronesia', 'Florida', 'Georgia',
    'Guam', 'Hawaii', 'Idaho', 'Illinois', 'Indiana', 'Iowa', 'Kansas', 'Kentucky', 'Louisiana', 'Maine',
    'Marshall Islands', 'Maryland', 'Massachusetts', 'Michigan', 'Minnesota', 'Mississippi', 'Missouri', 'Montana',
    'Nebraska', 'Nevada', 'New Hampshire', 'New Jersey', 'New Mexico', 'New York', 'North Carolina', 'North Dakota',
    'Northern Mariana Islands', 'Ohio', 'Oklahoma', 'Oregon', 'Palau', 'Pennsylvania', 'Puerto Rico', 'Rhode Island',
    'South Carolina', 'South Dakota', 'Tennessee', 'Texas', 'Utah', 'Vermont', 'Virgin Islands', 'Virginia',
    'Washington', 'West Virginia', 'Wisconsin', 'Wyoming'];
var statesWithFlags = [
    { 'name': 'Alabama', 'flag': '5/5c/Flag_of_Alabama.svg/45px-Flag_of_Alabama.svg.png' },
    { 'name': 'Alaska', 'flag': 'e/e6/Flag_of_Alaska.svg/43px-Flag_of_Alaska.svg.png' },
    { 'name': 'Arizona', 'flag': '9/9d/Flag_of_Arizona.svg/45px-Flag_of_Arizona.svg.png' },
    { 'name': 'Arkansas', 'flag': '9/9d/Flag_of_Arkansas.svg/45px-Flag_of_Arkansas.svg.png' },
    { 'name': 'California', 'flag': '0/01/Flag_of_California.svg/45px-Flag_of_California.svg.png' },
    { 'name': 'Colorado', 'flag': '4/46/Flag_of_Colorado.svg/45px-Flag_of_Colorado.svg.png' },
    { 'name': 'Connecticut', 'flag': '9/96/Flag_of_Connecticut.svg/39px-Flag_of_Connecticut.svg.png' },
    { 'name': 'Delaware', 'flag': 'c/c6/Flag_of_Delaware.svg/45px-Flag_of_Delaware.svg.png' },
    { 'name': 'Florida', 'flag': 'f/f7/Flag_of_Florida.svg/45px-Flag_of_Florida.svg.png' },
    { 'name': 'Georgia', 'flag': '5/54/Flag_of_Georgia_%28U.S._state%29.svg/46px-Flag_of_Georgia_%28U.S._state%29.svg.png' },
    { 'name': 'Hawaii', 'flag': 'e/ef/Flag_of_Hawaii.svg/46px-Flag_of_Hawaii.svg.png' },
    { 'name': 'Idaho', 'flag': 'a/a4/Flag_of_Idaho.svg/38px-Flag_of_Idaho.svg.png' },
    { 'name': 'Illinois', 'flag': '0/01/Flag_of_Illinois.svg/46px-Flag_of_Illinois.svg.png' },
    { 'name': 'Indiana', 'flag': 'a/ac/Flag_of_Indiana.svg/45px-Flag_of_Indiana.svg.png' },
    { 'name': 'Iowa', 'flag': 'a/aa/Flag_of_Iowa.svg/44px-Flag_of_Iowa.svg.png' },
    { 'name': 'Kansas', 'flag': 'd/da/Flag_of_Kansas.svg/46px-Flag_of_Kansas.svg.png' },
    { 'name': 'Kentucky', 'flag': '8/8d/Flag_of_Kentucky.svg/46px-Flag_of_Kentucky.svg.png' },
    { 'name': 'Louisiana', 'flag': 'e/e0/Flag_of_Louisiana.svg/46px-Flag_of_Louisiana.svg.png' },
    { 'name': 'Maine', 'flag': '3/35/Flag_of_Maine.svg/45px-Flag_of_Maine.svg.png' },
    { 'name': 'Maryland', 'flag': 'a/a0/Flag_of_Maryland.svg/45px-Flag_of_Maryland.svg.png' },
    { 'name': 'Massachusetts', 'flag': 'f/f2/Flag_of_Massachusetts.svg/46px-Flag_of_Massachusetts.svg.png' },
    { 'name': 'Michigan', 'flag': 'b/b5/Flag_of_Michigan.svg/45px-Flag_of_Michigan.svg.png' },
    { 'name': 'Minnesota', 'flag': 'b/b9/Flag_of_Minnesota.svg/46px-Flag_of_Minnesota.svg.png' },
    { 'name': 'Mississippi', 'flag': '4/42/Flag_of_Mississippi.svg/45px-Flag_of_Mississippi.svg.png' },
    { 'name': 'Missouri', 'flag': '5/5a/Flag_of_Missouri.svg/46px-Flag_of_Missouri.svg.png' },
    { 'name': 'Montana', 'flag': 'c/cb/Flag_of_Montana.svg/45px-Flag_of_Montana.svg.png' },
    { 'name': 'Nebraska', 'flag': '4/4d/Flag_of_Nebraska.svg/46px-Flag_of_Nebraska.svg.png' },
    { 'name': 'Nevada', 'flag': 'f/f1/Flag_of_Nevada.svg/45px-Flag_of_Nevada.svg.png' },
    { 'name': 'New Hampshire', 'flag': '2/28/Flag_of_New_Hampshire.svg/45px-Flag_of_New_Hampshire.svg.png' },
    { 'name': 'New Jersey', 'flag': '9/92/Flag_of_New_Jersey.svg/45px-Flag_of_New_Jersey.svg.png' },
    { 'name': 'New Mexico', 'flag': 'c/c3/Flag_of_New_Mexico.svg/45px-Flag_of_New_Mexico.svg.png' },
    { 'name': 'New York', 'flag': '1/1a/Flag_of_New_York.svg/46px-Flag_of_New_York.svg.png' },
    { 'name': 'North Carolina', 'flag': 'b/bb/Flag_of_North_Carolina.svg/45px-Flag_of_North_Carolina.svg.png' },
    { 'name': 'North Dakota', 'flag': 'e/ee/Flag_of_North_Dakota.svg/38px-Flag_of_North_Dakota.svg.png' },
    { 'name': 'Ohio', 'flag': '4/4c/Flag_of_Ohio.svg/46px-Flag_of_Ohio.svg.png' },
    { 'name': 'Oklahoma', 'flag': '6/6e/Flag_of_Oklahoma.svg/45px-Flag_of_Oklahoma.svg.png' },
    { 'name': 'Oregon', 'flag': 'b/b9/Flag_of_Oregon.svg/46px-Flag_of_Oregon.svg.png' },
    { 'name': 'Pennsylvania', 'flag': 'f/f7/Flag_of_Pennsylvania.svg/45px-Flag_of_Pennsylvania.svg.png' },
    { 'name': 'Rhode Island', 'flag': 'f/f3/Flag_of_Rhode_Island.svg/32px-Flag_of_Rhode_Island.svg.png' },
    { 'name': 'South Carolina', 'flag': '6/69/Flag_of_South_Carolina.svg/45px-Flag_of_South_Carolina.svg.png' },
    { 'name': 'South Dakota', 'flag': '1/1a/Flag_of_South_Dakota.svg/46px-Flag_of_South_Dakota.svg.png' },
    { 'name': 'Tennessee', 'flag': '9/9e/Flag_of_Tennessee.svg/46px-Flag_of_Tennessee.svg.png' },
    { 'name': 'Texas', 'flag': 'f/f7/Flag_of_Texas.svg/45px-Flag_of_Texas.svg.png' },
    { 'name': 'Utah', 'flag': 'f/f6/Flag_of_Utah.svg/45px-Flag_of_Utah.svg.png' },
    { 'name': 'Vermont', 'flag': '4/49/Flag_of_Vermont.svg/46px-Flag_of_Vermont.svg.png' },
    { 'name': 'Virginia', 'flag': '4/47/Flag_of_Virginia.svg/44px-Flag_of_Virginia.svg.png' },
    { 'name': 'Washington', 'flag': '5/54/Flag_of_Washington.svg/46px-Flag_of_Washington.svg.png' },
    { 'name': 'West Virginia', 'flag': '2/22/Flag_of_West_Virginia.svg/46px-Flag_of_West_Virginia.svg.png' },
    { 'name': 'Wisconsin', 'flag': '2/22/Flag_of_Wisconsin.svg/45px-Flag_of_Wisconsin.svg.png' },
    { 'name': 'Wyoming', 'flag': 'b/bc/Flag_of_Wyoming.svg/43px-Flag_of_Wyoming.svg.png' }
];
var WikipediaService = (function () {
    function WikipediaService(_jsonp) {
        this._jsonp = _jsonp;
    }
    WikipediaService.prototype.search = function (term) {
        if (term === '') {
            return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"].of([]);
        }
        var wikiUrl = 'https://en.wikipedia.org/w/api.php';
        var params = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["g" /* URLSearchParams */]();
        params.set('search', term);
        params.set('action', 'opensearch');
        params.set('format', 'json');
        params.set('callback', 'JSONP_CALLBACK');
        return this._jsonp
            .get(wikiUrl, { search: params })
            .map(function (response) { return response.json()[1]; });
    };
    WikipediaService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* Jsonp */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* Jsonp */]) === "function" && _a || Object])
    ], WikipediaService);
    return WikipediaService;
    var _a;
}());

var TypeaheadComponent = (function () {
    function TypeaheadComponent(_service) {
        var _this = this;
        this._service = _service;
        this.searching = false;
        this.searchFailed = false;
        this.search = function (text$) {
            return text$
                .debounceTime(200)
                .distinctUntilChanged()
                .map(function (term) { return term.length < 2 ? []
                : states.filter(function (v) { return new RegExp(term, 'gi').test(v); }).splice(0, 10); });
        };
        this.searchWiki = function (text$) {
            return text$
                .debounceTime(300)
                .distinctUntilChanged()
                .do(function () { return _this.searching = true; })
                .switchMap(function (term) {
                return _this._service.search(term)
                    .do(function () { return _this.searchFailed = false; })
                    .catch(function () {
                    _this.searchFailed = true;
                    return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"].of([]);
                });
            })
                .do(function () { return _this.searching = false; });
        };
        this.searchFlags = function (text$) {
            return text$
                .debounceTime(200)
                .map(function (term) { return term === '' ? []
                : statesWithFlags.filter(function (v) { return new RegExp(term, 'gi').test(v.name); }).slice(0, 10); });
        };
        this.formatter = function (x) { return x.name; };
    }
    TypeaheadComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-typeahead',
            template: __webpack_require__("./src/app/components/typeahead/typeahead.component.html"),
            styles: [__webpack_require__("./src/app/components/typeahead/typeahead.component.scss")],
            providers: [WikipediaService]
        }),
        __metadata("design:paramtypes", [WikipediaService])
    ], TypeaheadComponent);
    return TypeaheadComponent;
}());

//# sourceMappingURL=typeahead.component.js.map

/***/ })

});
//# sourceMappingURL=components.module.chunk.js.map