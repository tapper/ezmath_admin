webpackJsonp(["icons.module"],{

/***/ "./src/app/icons/icons.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "IconsModule", function() { return IconsModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common__ = __webpack_require__("./node_modules/@angular/common/@angular/common.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__("./node_modules/@angular/forms/@angular/forms.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__icons_routing__ = __webpack_require__("./src/app/icons/icons.routing.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__linea_linea_component__ = __webpack_require__("./src/app/icons/linea/linea.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__fontawesome_fontawesome_component__ = __webpack_require__("./src/app/icons/fontawesome/fontawesome.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__sli_sli_component__ = __webpack_require__("./src/app/icons/sli/sli.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};








var IconsModule = (function () {
    function IconsModule() {
    }
    IconsModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_2__angular_common__["CommonModule"],
                __WEBPACK_IMPORTED_MODULE_1__angular_router__["d" /* RouterModule */].forChild(__WEBPACK_IMPORTED_MODULE_4__icons_routing__["a" /* IconsRoutes */]),
                __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormsModule"],
                __WEBPACK_IMPORTED_MODULE_3__angular_forms__["ReactiveFormsModule"]
            ],
            declarations: [__WEBPACK_IMPORTED_MODULE_5__linea_linea_component__["a" /* LineaComponent */], __WEBPACK_IMPORTED_MODULE_6__fontawesome_fontawesome_component__["a" /* FontawesomeComponent */], __WEBPACK_IMPORTED_MODULE_7__sli_sli_component__["a" /* SliComponent */]]
        })
    ], IconsModule);
    return IconsModule;
}());

//# sourceMappingURL=icons.module.js.map

/***/ }),

/***/ "./src/app/icons/icons.routing.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return IconsRoutes; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__linea_linea_component__ = __webpack_require__("./src/app/icons/linea/linea.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__fontawesome_fontawesome_component__ = __webpack_require__("./src/app/icons/fontawesome/fontawesome.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__sli_sli_component__ = __webpack_require__("./src/app/icons/sli/sli.component.ts");



var IconsRoutes = [
    {
        path: '',
        children: [{
                path: 'linea',
                component: __WEBPACK_IMPORTED_MODULE_0__linea_linea_component__["a" /* LineaComponent */],
                data: {
                    heading: 'Linea Icons'
                }
            }, {
                path: 'fontawesome',
                component: __WEBPACK_IMPORTED_MODULE_1__fontawesome_fontawesome_component__["a" /* FontawesomeComponent */],
                data: {
                    heading: 'FontAwesome Icons'
                }
            }, {
                path: 'sli',
                component: __WEBPACK_IMPORTED_MODULE_2__sli_sli_component__["a" /* SliComponent */],
                data: {
                    heading: 'Simple Line Icons'
                }
            }]
    }
];
//# sourceMappingURL=icons.routing.js.map

/***/ })

});
//# sourceMappingURL=icons.module.chunk.js.map