webpackJsonp(["dashboard.module"],{

/***/ "./src/app/dashboard/dashboard.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\r\n  <div class=\"col-sm-6 col-md-3\">\r\n    <div class=\"card card-body card-widget\">\r\n      <div class=\"widget-icon rounded-circle bg-green text-white icon icon-basic-headset\">\r\n      </div>\r\n      <div class=\"block ml-3 mr-3\">\r\n        <h5 class=\"mb-0 fw-400\">\r\n          576K\r\n        </h5>\r\n        <small class=\"text-muted text-uppercase \">\r\n          <strong>Users</strong>\r\n        </small>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <div class=\"col-sm-6 col-md-3\">\r\n    <div class=\"card card-body card-widget\">\r\n      <div class=\"widget-icon rounded-circle bg-blue text-white icon icon-basic-server2\">\r\n      </div>\r\n      <div class=\"block ml-3 mr-3\">\r\n        <h5 class=\"mb-0 fw-400\">\r\n          99.99%\r\n        </h5>\r\n        <small class=\"text-muted text-uppercase \">\r\n          <strong>Uptime</strong>\r\n        </small>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <div class=\"col-sm-6 col-md-3\">\r\n    <div class=\"card card-body card-widget\">\r\n      <div class=\"widget-icon rounded-circle bg-teal text-white icon icon-basic-magic-mouse\">\r\n      </div>\r\n      <div class=\"block ml-3 mr-3\">\r\n        <h5 class=\"mb-0 fw-400\">\r\n          465K\r\n        </h5>\r\n        <small class=\"text-muted text-uppercase \">\r\n          <strong>Visitors</strong>\r\n        </small>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <div class=\"col-sm-6 col-md-3\">\r\n    <div class=\"card card-body card-widget\">\r\n      <div class=\"widget-icon rounded-circle bg-red text-white icon icon-basic-heart\">\r\n      </div>\r\n      <div class=\"block ml-3 mr-3\">\r\n        <h5 class=\"mb-0 fw-400\">\r\n          7,578\r\n        </h5>\r\n        <small class=\"text-muted text-uppercase \">\r\n          <strong>Likes</strong>\r\n        </small>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n<div class=\"row\">\r\n  <div class=\"col-lg-6\">\r\n    <div class=\"card\">\r\n      <div class=\"card-header no-shadow\">\r\n        <div class=\"card-header-text w-100\">\r\n          <div class=\"card-title\">\r\n            GDP Per Capita\r\n          </div>\r\n          <div class=\"card-subtitle text-capitalize ff-sans\">\r\n            Last updated: 1 hour ago\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <div class=\"card-body\">\r\n        <div class=\"row text-center\">\r\n          <div class=\"col-xs-6 col-sm-3 text-success\">\r\n            <h5 class=\"mb-0\">\r\n              <strong>\r\n                34.45B\r\n              </strong>\r\n            </h5>\r\n            <small>\r\n              Income\r\n            </small>\r\n          </div>\r\n          <div class=\"col-xs-6 col-sm-3\">\r\n            <h5 class=\"mb-0\">\r\n              <strong>\r\n                34%\r\n              </strong>\r\n            </h5>\r\n            <small>\r\n              Growth\r\n            </small>\r\n          </div>\r\n          <div class=\"col-xs-6 col-sm-3 text-danger\">\r\n            <h5 class=\"mb-0\">\r\n              <strong>\r\n                45%\r\n              </strong>\r\n            </h5>\r\n            <small>\r\n              GNI\r\n            </small>\r\n          </div>\r\n          <div class=\"col-xs-6 col-sm-3\">\r\n            <h5 class=\"mb-0\">\r\n              <strong>\r\n                4.56M\r\n              </strong>\r\n            </h5>\r\n            <small>\r\n              Aid Flows\r\n            </small>\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <div class=\"mb-1\">\r\n        <ngx-charts-area-chart-stacked (legendlabelclick)=\"onLegendLabelClick($event)\" (select)=\"select($event)\" [curve]=\"curve\" [gradient]=\"gradient\" [legend]=\"showLegend\" [results]=\"dateData\" [roundDomains]=\"roundDomains\" [scheme]=\"colorScheme\" [schemeType]=\"schemeType\" [showGridLines]=\"showGridLines\" [showXAxisLabel]=\"showXAxisLabel\" [showYAxisLabel]=\"showYAxisLabel\" [timeline]=\"timeline\" [tooltipDisabled]=\"tooltipDisabled\" [xAxis]=\"showXAxis\" [xAxisLabel]=\"xAxisLabel\" [yAxis]=\"showYAxis\" [yAxisLabel]=\"yAxisLabel\" class=\"line-container\">\r\n        </ngx-charts-area-chart-stacked>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <div class=\"col-lg-6\">\r\n    <div class=\"card\">\r\n      <div class=\"card-header no-shadow\">\r\n        <div class=\"card-header-text w-100\">\r\n          <div class=\"card-title\">\r\n            Country spending\r\n          </div>\r\n          <div class=\"card-subtitle text-capitalize ff-sans\">\r\n            Last updated: 1 hour ago\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <div class=\"card-body\">\r\n        <div class=\"row text-center\">\r\n          <div class=\"col-sm-6 col-md-3 text-purple\">\r\n            <h5 class=\"mb-0\">\r\n              <strong>\r\n                34.45B\r\n              </strong>\r\n            </h5>\r\n            <small>\r\n              Health\r\n            </small>\r\n          </div>\r\n          <div class=\"col-sm-6 col-md-3\">\r\n            <h5 class=\"mb-0\">\r\n              <strong>\r\n                21.78B\r\n              </strong>\r\n            </h5>\r\n            <small>\r\n              Education\r\n            </small>\r\n          </div>\r\n          <div class=\"col-sm-6 col-md-3 text-indigo\">\r\n            <h5 class=\"mb-0\">\r\n              <strong>\r\n                2.89M\r\n              </strong>\r\n            </h5>\r\n            <small>\r\n              Research\r\n            </small>\r\n          </div>\r\n          <div class=\"col-sm-6 col-md-3\">\r\n            <h5 class=\"mb-0\">\r\n              <strong>\r\n                34.76B\r\n              </strong>\r\n            </h5>\r\n            <small>\r\n              Food\r\n            </small>\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <div class=\"mb-1\">\r\n        <ngx-charts-gauge (legendlabelclick)=\"onLegendLabelClick($event)\" (select)=\"select($event)\" [angleSpan]=\"gaugeAngleSpan\" [bigSegments]=\"gaugeLargeSegments\" [legend]=\"showLegend\" [margin]=\"margin ? [marginTop, marginRight, marginBottom, marginLeft] : null\" [max]=\"gaugeMax\" [min]=\"gaugeMin\" [results]=\"single\" [scheme]=\"colorScheme\" [showAxis]=\"gaugeShowAxis\" [smallSegments]=\"gaugeSmallSegments\" [startAngle]=\"gaugeStartAngle\" [textValue]=\"gaugeTextValue\" [tooltipDisabled]=\"tooltipDisabled\" [units]=\"gaugeUnits\" class=\"gauge-container\">\r\n        </ngx-charts-gauge>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n<div class=\"row\">\r\n  <div class=\"col-lg-5\">\r\n    <div class=\"card\">\r\n      <div class=\"card-header no-shadow\">\r\n        <div class=\"card-header-text w-100\">\r\n          <div class=\"card-title\">\r\n            Social Feed\r\n          </div>\r\n          <div class=\"card-subtitle text-capitalize ff-sans\">\r\n            Last updated: 1 hour ago\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <div class=\"activity-stream\">\r\n        <div class=\"card\">\r\n          <div class=\"card-header\">\r\n            <img class=\"card-avatar\" src=\"assets/images/face1.jpg\"/>\r\n            <div class=\"card-header-text\">\r\n              <div class=\"card-title\">\r\n                Received a call from Joel\r\n              </div>\r\n              <div class=\"card-subtitle\">\r\n                at 3:14PM\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <div class=\"card\">\r\n          <div class=\"card-header\">\r\n            <img class=\"card-avatar\" src=\"assets/images/face2.jpg\"/>\r\n            <div class=\"card-header-text\">\r\n              <div class=\"card-title\">\r\n                Added Ellie to Group\r\n              </div>\r\n              <div class=\"card-subtitle\">\r\n                5 mins ago\r\n              </div>\r\n            </div>\r\n          </div>\r\n          <div class=\"card-body\">\r\n            <p class=\"mb-0\">\r\n              Nullam id dolor id nibh ultricies vehicula ut id elit.\r\n            </p>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <div class=\"col-lg-3\">\r\n    <div class=\"card bg-purple card-inverse\">\r\n      <div class=\"card-header\">\r\n        Todos\r\n      </div>\r\n      <div class=\"card-body\">\r\n        <ul class=\"list-unstyled mb-0\">\r\n          <li>\r\n            <label class=\"custom-control custom-checkbox mb-2\">\r\n              <input class=\"custom-control-input\" type=\"checkbox\"/>\r\n              <span class=\"custom-control-indicator\">\r\n              </span>\r\n              <span class=\"custom-control-description\">\r\n                Learn Angular 4\r\n              </span>\r\n            </label>\r\n          </li>\r\n          <li>\r\n            <label class=\"custom-control custom-checkbox mb-2\">\r\n              <input class=\"custom-control-input\" type=\"checkbox\"/>\r\n              <span class=\"custom-control-indicator\">\r\n              </span>\r\n              <span class=\"custom-control-description\">\r\n                Update Vue.JS\r\n              </span>\r\n            </label>\r\n          </li>\r\n          <li>\r\n            <label class=\"custom-control custom-checkbox\">\r\n              <input class=\"custom-control-input\" type=\"checkbox\">\r\n              <span class=\"custom-control-indicator\">\r\n              </span>\r\n              <span class=\"custom-control-description\">\r\n                Add ReactJS\r\n              </span>\r\n            </label>\r\n          </li>\r\n          <li>\r\n            <label class=\"custom-control custom-checkbox mb-2\">\r\n              <input class=\"custom-control-input\" type=\"checkbox\"/>\r\n              <span class=\"custom-control-indicator\">\r\n              </span>\r\n              <span class=\"custom-control-description\">\r\n                Upgrade Ember\r\n              </span>\r\n            </label>\r\n          </li>\r\n          <li>\r\n            <label class=\"custom-control custom-checkbox mb-2\">\r\n              <input class=\"custom-control-input\" type=\"checkbox\"/>\r\n              <span class=\"custom-control-indicator\">\r\n              </span>\r\n              <span class=\"custom-control-description\">\r\n                Read on Meteor\r\n              </span>\r\n            </label>\r\n          </li>\r\n          <li>\r\n            <label class=\"custom-control custom-checkbox\">\r\n              <input class=\"custom-control-input\" type=\"checkbox\"/>\r\n              <span class=\"custom-control-indicator\">\r\n              </span>\r\n              <span class=\"custom-control-description\">\r\n                Commit Riot changes\r\n              </span>\r\n            </label>\r\n          </li>\r\n        </ul>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <div class=\"col-lg-4\">\r\n    <div class=\"card no-bg\">\r\n      <div class=\"card-body text-center rounded\" style=\"background-image: url('assets/images/unsplash/2.jpg')\">\r\n        <img alt=\"\" class=\"avatar-lg rounded-circle d-block mx-auto mb-3\" src=\"assets/images/face4.jpg\"/>\r\n        <button class=\"btn btn-secondary btn-sm mb-3\">\r\n          Follow\r\n        </button>\r\n      </div>\r\n      <div class=\"card-body pt-3 pb-3 ml-3 mr-3 bg-indigo text-white rounded\" style=\"margin-top: -15px;\">\r\n        <div class=\"row text-center\">\r\n          <div class=\"col\">\r\n            <div class=\"h6 no-margin\">\r\n              62\r\n            </div>\r\n            <div class=\"small bold text-uppercase\">\r\n              Followers\r\n            </div>\r\n          </div>\r\n          <div class=\"col\">\r\n            <div class=\"h6 no-margin\">\r\n              98\r\n            </div>\r\n            <div class=\"small bold text-uppercase\">\r\n              Posts\r\n            </div>\r\n          </div>\r\n          <div class=\"col\">\r\n            <div class=\"h6 no-margin\">\r\n              29\r\n            </div>\r\n            <div class=\"small bold text-uppercase\">\r\n              Likes\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n<div class=\"row\">\r\n  <div class=\"col-md-12 col-xl-4\">\r\n    <div class=\"card\">\r\n      <div class=\"card-header no-shadow\">\r\n        Messages\r\n      </div>\r\n      <ul class=\"list-group border-0\">\r\n        <li class=\"list-group-item border-top-0 border-left-0 border-right-0\">\r\n          <div class=\"d-flex align-items-center\">\r\n            <a class=\"pull-left avatar-sm\" href=\"javascript:;\">\r\n              <img class=\"img-fluid rounded-circle\" src=\"assets/images/face1.jpg\"/>\r\n            </a>\r\n            <div class=\"pl-3\">\r\n              <a class=\"d-block\" href=\"javascript:;\">\r\n                Amanda Alvarez\r\n              </a>\r\n              <span>\r\n                Donec ullamcorper nulla non metus auctor fringilla.\r\n              </span>\r\n            </div>\r\n          </div>\r\n        </li>\r\n        <li class=\"list-group-item border-top-0 border-left-0 border-right-0\">\r\n          <div class=\"d-flex align-items-center\">\r\n            <a class=\"pull-left avatar-sm\" href=\"javascript:;\">\r\n              <img class=\"img-fluid rounded-circle\" src=\"assets/images/face2.jpg\"/>\r\n            </a>\r\n            <div class=\"pl-3\">\r\n              <a class=\"d-block\" href=\"javascript:;\">\r\n                Debra Kelly\r\n              </a>\r\n              <span>\r\n                Donec ullamcorper nulla non metus auctor fringilla.\r\n              </span>\r\n            </div>\r\n          </div>\r\n        </li>\r\n        <li class=\"list-group-item border-top-0 border-left-0 border-right-0  border-bottom-0\">\r\n          <div class=\"d-flex align-items-center\">\r\n            <a class=\"pull-left avatar-sm\" href=\"javascript:;\">\r\n              <img class=\"img-fluid rounded-circle\" src=\"assets/images/face3.jpg\"/>\r\n            </a>\r\n            <div class=\"pl-3\">\r\n              <a class=\"d-block\" href=\"javascript:;\">\r\n                Walter Ryan\r\n              </a>\r\n              <span>\r\n                Donec ullamcorper nulla non metus auctor fringilla.\r\n              </span>\r\n            </div>\r\n          </div>\r\n        </li>\r\n      </ul>\r\n    </div>\r\n  </div>\r\n  <div class=\"col-md-6 col-xl-4\">\r\n    <div class=\"card bg-indigo card-inverse\">\r\n      <div class=\"card-header no-shadow\">\r\n        Tweets\r\n      </div>\r\n      <div class=\"list-group\">\r\n        <div class=\"list-group-item list-group-item-action flex-column align-items-start no-bg text-white\">\r\n          <p class=\"mb-1 w-100\">\r\n            Aenean lacinia bibendum nulla sed consectetur. Cras mattis consectetur purus sit amet fermentum.\r\n          </p>\r\n          <div class=\"d-flex w-100 justify-content-between\">\r\n            <div>\r\n              <a href=\"javascript:;\" class=\"text-white\">\r\n                <i class=\"icon icon-basic-heart\">\r\n                </i>\r\n              </a>\r\n              <a href=\"javascript:;\" class=\"text-white\">\r\n                <i class=\"icon icon-basic-share mr-3 ml-3\">\r\n                </i>\r\n              </a>\r\n            </div>\r\n            <small>\r\n              3m\r\n            </small>\r\n          </div>\r\n        </div>\r\n        <div class=\"list-group-item list-group-item-action flex-column align-items-start no-bg text-white\">\r\n          <p class=\"mb-1 w-100\">\r\n            Aenean lacinia bibendum nulla sed consectetur. Cras mattis consectetur purus sit amet fermentum.\r\n          </p>\r\n          <div class=\"d-flex w-100 justify-content-between\">\r\n            <div>\r\n              <a href=\"javascript:;\" class=\"text-white\">\r\n                <i class=\"icon icon-basic-heart\">\r\n                </i>\r\n              </a>\r\n              <a href=\"javascript:;\" class=\"text-white\">\r\n                <i class=\"icon icon-basic-share mr-3 ml-3\">\r\n                </i>\r\n              </a>\r\n            </div>\r\n            <small>\r\n              3m\r\n            </small>\r\n          </div>\r\n        </div>\r\n        <div class=\"list-group-item list-group-item-action flex-column align-items-start no-bg text-white  border-bottom-0\">\r\n          <p class=\"mb-1 w-100\">\r\n            Aenean lacinia bibendum nulla sed consectetur. Cras mattis consectetur purus sit amet fermentum.\r\n          </p>\r\n          <div class=\"d-flex w-100 justify-content-between\">\r\n            <div>\r\n              <a href=\"javascript:;\" class=\"text-white\">\r\n                <i class=\"icon icon-basic-heart\">\r\n                </i>\r\n              </a>\r\n              <a href=\"javascript:;\" class=\"text-white\">\r\n                <i class=\"icon icon-basic-share mr-3 ml-3\">\r\n                </i>\r\n              </a>\r\n            </div>\r\n            <small>\r\n              3m\r\n            </small>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <div class=\"col-md-6 col-xl-4\">\r\n    <div class=\"card\">\r\n      <div class=\"card-header no-shadow\">\r\n        News Feed\r\n      </div>\r\n      <ul class=\"list-group border-0\">\r\n        <li class=\"list-group-item border-left-0 border-right-0\">\r\n          <div class=\"d-flex justify-content-center\">\r\n            <a class=\"pull-left avatar-sm\" href=\"javascript:;\">\r\n              <img class=\"img-fluid\" src=\"assets/images/face4.jpg\"/>\r\n            </a>\r\n            <div class=\"pl-3\">\r\n              <span class=\"d-block\">\r\n                Donec ullamcorper nulla non metus auctor fringilla.\r\n              </span>\r\n              <small class=\"text-muted\">\r\n                Yesterday\r\n              </small>\r\n            </div>\r\n          </div>\r\n        </li>\r\n        <li class=\"list-group-item border-left-0 border-right-0\">\r\n          <div class=\"d-flex justify-content-center\">\r\n            <a class=\"pull-left avatar-sm\" href=\"javascript:;\">\r\n              <img class=\"img-fluid\" src=\"assets/images/face5.jpg\"/>\r\n            </a>\r\n            <div class=\"pl-3\">\r\n              <span class=\"d-block\">\r\n                Donec ullamcorper nulla non metus auctor fringilla.\r\n              </span>\r\n              <small class=\"text-muted\">\r\n                Yesterday\r\n              </small>\r\n            </div>\r\n          </div>\r\n        </li>\r\n        <li class=\"list-group-item border-left-0 border-right-0  border-bottom-0\">\r\n          <div class=\"d-flex justify-content-center\">\r\n            <a class=\"pull-left avatar-sm\" href=\"javascript:;\">\r\n              <img class=\"img-fluid\" src=\"assets/images/face1.jpg\">\r\n            </a>\r\n            <div class=\"pl-3\">\r\n              <span class=\"d-block\">\r\n                Donec ullamcorper nulla non metus auctor fringilla.\r\n              </span>\r\n              <small class=\"text-muted\">\r\n                Yesterday\r\n              </small>\r\n            </div>\r\n          </div>\r\n        </li>\r\n      </ul>\r\n    </div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./src/app/dashboard/dashboard.component.scss":
/***/ (function(module, exports) {

module.exports = ".line-container {\n  height: 300px;\n  width: 100%;\n  display: block; }\n\n.gauge-container {\n  height: 300px;\n  width: 100%;\n  display: block; }\n"

/***/ }),

/***/ "./src/app/dashboard/dashboard.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DashboardComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_d3_shape__ = __webpack_require__("./node_modules/d3-shape/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__shared_chartData__ = __webpack_require__("./src/app/shared/chartData.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var DashboardComponent = (function () {
    function DashboardComponent() {
        // options
        this.showXAxis = true;
        this.showYAxis = true;
        this.gradient = false;
        this.showLegend = false;
        this.showXAxisLabel = false;
        this.tooltipDisabled = false;
        this.xAxisLabel = 'Country';
        this.showYAxisLabel = false;
        this.yAxisLabel = 'GDP Per Capita';
        this.showGridLines = true;
        this.roundDomains = false;
        this.colorScheme = {
            domain: [
                '#0099cc', '#2ECC71', '#4cc3d9', '#ffc65d', '#d96557', '#ba68c8'
            ]
        };
        this.schemeType = 'ordinal';
        // line interpolation
        this.curve = __WEBPACK_IMPORTED_MODULE_1_d3_shape__["curveLinear"];
        // line, area
        this.timeline = false;
        // margin
        this.margin = false;
        this.marginTop = 40;
        this.marginRight = 40;
        this.marginBottom = 40;
        this.marginLeft = 40;
        // gauge
        this.gaugeMin = 0;
        this.gaugeMax = 50;
        this.gaugeLargeSegments = 10;
        this.gaugeSmallSegments = 5;
        this.gaugeTextValue = '';
        this.gaugeUnits = 'alerts';
        this.gaugeAngleSpan = 240;
        this.gaugeStartAngle = -120;
        this.gaugeShowAxis = true;
        this.gaugeValue = 50; // linear gauge value
        this.gaugePreviousValue = 70;
        Object.assign(this, {
            single: __WEBPACK_IMPORTED_MODULE_2__shared_chartData__["e" /* single */]
        });
        this.dateData = Object(__WEBPACK_IMPORTED_MODULE_2__shared_chartData__["b" /* generateData */])(5, false);
    }
    DashboardComponent.prototype.select = function (data) {
        console.log('Item clicked', data);
    };
    DashboardComponent.prototype.onLegendLabelClick = function (entry) {
        console.log('Legend clicked', entry);
    };
    DashboardComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-dashboard',
            template: __webpack_require__("./src/app/dashboard/dashboard.component.html"),
            styles: [__webpack_require__("./src/app/dashboard/dashboard.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], DashboardComponent);
    return DashboardComponent;
}());

//# sourceMappingURL=dashboard.component.js.map

/***/ }),

/***/ "./src/app/dashboard/dashboard.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardModule", function() { return DashboardModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common__ = __webpack_require__("./node_modules/@angular/common/@angular/common.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__swimlane_ngx_charts__ = __webpack_require__("./node_modules/@swimlane/ngx-charts/release/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__swimlane_ngx_charts___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3__swimlane_ngx_charts__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__dashboard_component__ = __webpack_require__("./src/app/dashboard/dashboard.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__dashboard_routing__ = __webpack_require__("./src/app/dashboard/dashboard.routing.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var DashboardModule = (function () {
    function DashboardModule() {
    }
    DashboardModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [__WEBPACK_IMPORTED_MODULE_2__angular_common__["CommonModule"], __WEBPACK_IMPORTED_MODULE_1__angular_router__["d" /* RouterModule */].forChild(__WEBPACK_IMPORTED_MODULE_5__dashboard_routing__["a" /* DashboardRoutes */]), __WEBPACK_IMPORTED_MODULE_3__swimlane_ngx_charts__["NgxChartsModule"]],
            declarations: [__WEBPACK_IMPORTED_MODULE_4__dashboard_component__["a" /* DashboardComponent */]]
        })
    ], DashboardModule);
    return DashboardModule;
}());

//# sourceMappingURL=dashboard.module.js.map

/***/ }),

/***/ "./src/app/dashboard/dashboard.routing.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DashboardRoutes; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__dashboard_component__ = __webpack_require__("./src/app/dashboard/dashboard.component.ts");

var DashboardRoutes = [{
        path: '',
        component: __WEBPACK_IMPORTED_MODULE_0__dashboard_component__["a" /* DashboardComponent */],
        data: {
            heading: 'Dashboard'
        }
    }];
//# sourceMappingURL=dashboard.routing.js.map

/***/ })

});
//# sourceMappingURL=dashboard.module.chunk.js.map