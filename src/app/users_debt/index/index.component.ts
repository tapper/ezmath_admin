import {Component, OnInit} from '@angular/core';
import {MainService} from "../MainService.service";
import {SettingsService} from "../../../settings/settings.service";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
    selector: 'app-index',
    templateUrl: './index.component.html',
    styleUrls: ['../../icons/fontawesome/fontawesome.component.scss', '../../media/list/list.component.scss', './index.component.css']
})


export class IndexComponent implements OnInit {

    ItemsArray: any[] = [];
    ItemsArray1: any[] = [];
    host: string = '';
    settings = '';
    avatar = '';
    public folderName:string = 'users_debt';
    public addButton:string = 'הוסף משתמש'
    deleteModal: any;
    detailsModal: any;
    selectedItem: any;
    companyToDelete: any;
    SubCatId:any;
    UserType :any = ["תלמיד","מורה",'מנהל'];
    pushModal: any;
    companyToPush: any;
    pushText: string = '';
    sent: boolean = false;
    inProcess: boolean = false;

    public senddetails:any = {
        'id' : '',
        'user_id' : '',
        'class_id' : '',
        'approve_status' : '',
        'payment_method' : '',
        'reset_debt' : '',
    }


    constructor(public MainService: MainService, settings: SettingsService , private modalService: NgbModal , private route: ActivatedRoute, public router:Router) {
        this.route.params.subscribe(params => {
            this.SubCatId = params['id'];
            if(!this.SubCatId)
                this.SubCatId = "-1";
            console.log("11 : " , this.SubCatId)
            this.host = settings.host;
            this.avatar = settings.avatar;
            this.getItems();
        });
    }

    ngOnInit() {
    }

    getItems()
    {
        this.MainService.GetItems('getUsersDebt', this.SubCatId ).then((data: any) => {
            console.log("getUsersDebt : ", data)
            this.ItemsArray = data;
            this.ItemsArray1 = data;
        })
    }

    paymentCash(row)
    {
        this.senddetails.user_id = row.user_id;
        this.senddetails.class_id = row.class_id;
        this.senddetails.payment_method = 0;
        this.senddetails.reset_debt = 1;

        let confirmBox = confirm("האם לאשר רכישה במזומן?");
        if (confirmBox){

            this.MainService.ClassPayment('ClassCashPayment',this.senddetails,'0').then((data: any) => {
                this.closeDetailsModal();
                alert ("תשלום במזומן בוצע בהצלחה");
                this.getItems();
            })
        }
    }

    paymentTicket(row)
    {
        this.senddetails.user_id = row.user_id;
        this.senddetails.class_id = row.class_id;
        this.senddetails.payment_method = 1;
        this.senddetails.reset_debt = 1;

        let confirmBox1 = confirm("האם לאשר רכישה בכרטיסיה?");
        if (confirmBox1){
            this.MainService.ClassPayment('ClassTicketPaymentConfirm',this.senddetails,'0').then((data: any) => {
                let dataReponse = data;
                let alertResponse = '';


                switch(dataReponse) {
                    case 0:
                        alertResponse = 'רכישה בכרטיסיה בוצעה בהצלחה';
                        break;
                    case 1:
                        alertResponse = 'תשלום כבר בוצעה';
                        break;
                    case 2:
                        alertResponse = 'למשתמש זה לא שויך קבוצת כרטסיות , יש לפתוח קבוצה במערכת ניהול';
                        break;
                    case 3:
                        alertResponse = 'לא נותרה יתרה בכרטיסיה של המשתמש';
                        break;
                    default:
                        alertResponse = '';
                }

                if (dataReponse == 0 || dataReponse == 1 || dataReponse == 2) {
                    alert (alertResponse);
                    this.closeDetailsModal();
                    this.getItems();

                    if (dataReponse == 0 || dataReponse == 1)
                    {
                        row.paid_count = 1;
                        row.paid_method = 1;
                    }

                }
                else {
                    let confirmBox2 = confirm("לא נותרה יתרה - קניית כרטיסיה ותשלום?");
                    if (confirmBox2)
                    {

                        this.MainService.ClassPayment('ClassCashPaymentTicketBuy',this.senddetails,'0').then((data: any) => {
                            this.closeDetailsModal();
                            alert ("קניית כרטיסיה ותשלום בוצע בהצלחה");
                            this.getItems();
                        })
                    }
                }
            })
        }

    }

    DeleteItem() {
        this.MainService.DeleteItem('deleteUserDebt', this.ItemsArray[this.companyToDelete].id).then((data: any) => {
            this.getItems();
        })
    }

    updateFilter(event) {
        const val = event.target.value;
        // filter our data
        const temp = this.ItemsArray1.filter(function (d) {
            return d.student_name.toLowerCase().indexOf(val) !== -1 || !val;
        });
        // update the rows
        this.ItemsArray = temp;
    }

    openDetailsModal(content, item){
        console.log("DM : " , content , item)
        this.detailsModal = this.modalService.open(content);
        this.selectedItem = item;
    }

    closeDetailsModal(){
        this.detailsModal.close();
    }

    openDeleteModal(content,index)
    {
        this.deleteModal = this.modalService.open(content);
        this.companyToDelete = index;
    }

    openPushModal (content, company) {
        this.pushModal = this.modalService.open(content);
        this.companyToPush = company;
    }

    closePushModal () {
        this.pushModal.close();
        this.sent = false;
    }

    sendPush() {

        this.inProcess = true;
        this.sent = true;

        this.MainService.SendPush('WebSendUserPush', this.companyToPush.id,this.pushText).then((data: any) => {
            this.pushText = '';
            this.sent = true;
            this.inProcess = false;
            this.closePushModal();
        })
    }

    async deleteCompany()
    {
        this.deleteModal.close();
        this.DeleteItem();
        console.log("Company To Delete : " , this.companyToDelete)
    }

    openTicketsPage(user_id,has_group) {
        if (has_group == 0) {
            alert ("יש תחילה לשייך משתמש זה לקבוצת כרטיסיות")
        }
        else {
            this.router.navigate(['/','user_tickets','index'], { queryParams: { user_id: user_id } });
        }
    }
}
