import {Component, OnInit, ElementRef, ViewChild, NgZone} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {FileUploader} from "ng2-file-upload";
import {Http, RequestOptions} from "@angular/http";
import {Observable} from "rxjs/Observable";
import {Headers} from '@angular/http';
import {FormBuilder,FormControl, FormGroup, NgForm, Validators} from "@angular/forms";
import {Ng2UploaderModule} from 'ng2-uploader';
import {MainService} from "../MainService.service";
import {SettingsService} from "../../../settings/settings.service";



@Component({
    selector: 'app-edit',
    templateUrl: './edit.component.html',
    styleUrls: ['../../icons/fontawesome/fontawesome.component.scss', '../../components/buttons/buttons.component.scss', './edit.component.css']
})

export class EditComponent implements OnInit {
    registerForm: FormGroup;
    public Id;
    public navigateTo: string = '/users/index';
    public imageSrc: string = '';
    public Items: any[] = [];
    public changeImage;
    public rowsNames: any[] = ['שם התלמיד','סיסמה','טלפון','אימייל','עיר','שם בית ספר','שם הורה','טלפון הורה'];
    public rows: any[] = ['student_name','password','phone','email','city','scool_name','parent_name','parent_phone'];
    public Item;
    public schoolgradeArray : any = [];
    public teachinglevelArray : any = [];
    public branchesArray : any = [];

    dropdownSettings = {
        singleSelection: false,
        text:"סניפים",
        selectAllText:'Select All',
        unSelectAllText:'UnSelect All',
        classes:"custom-select-item",
        //limitSelection : 4,
    };

    public host;
    public Image;
    public SubCategories;
    public isReady;
    public Change: boolean = false;
    public rowIndex:any = '';
    @ViewChild("fileInput") fileInput;

    constructor(private route: ActivatedRoute, private http: Http, public service: MainService, public router: Router, public settings: SettingsService) {

        this.route.params.subscribe(params => {
            this.Id = params['id'];
            for (let i = 0; i < this.service.Items.length; i++) {
                if (this.service.Items[i].id == this.Id) {
                    this.rowIndex = i;
                }
            }


            this.Item = this.service.Items[this.rowIndex];
            //this.Item = this.service.Items[this.Id];
            this.host = settings.host;
            this.getSchoolGrade();
            this.getTeachingLevel();
            this.getBranches();
            //this.Item.Change  = false;
            //this.isReady = true;
            console.log("Product", this.Item)
            console.log("Item : " , this.Item)
        });
    }

    async getSchoolGrade() {
        await this.service.GetItems('webGetSchoolGrade',-1).then((data: any) => {
            this.schoolgradeArray = data;
            console.log("webGetSchoolGrade : " , data);
        });
    }

    async getTeachingLevel() {
        await this.service.GetItems('webGetTeachingLevel',-1).then((data: any) => {
            console.log("webGetTeachingLevel : " , data);
            this.teachinglevelArray = data;
        });
    }

    async getBranches() {
        await this.service.GetItems('GetBranches',-1).then((data: any) => {
            console.log("GetBranches : " , data);
            this.branchesArray = data;
        });
    }

    onSubmit(form: NgForm) {


        let fi = this.fileInput.nativeElement;
        let fileToUpload;
        if (fi.files && fi.files[0]) {fileToUpload = fi.files[0]; console.log("fff : ",fileToUpload);}
        this.Item.Change = this.Change;

        console.log("EditUser",form.value);

        this.service.EditItem('EditUser',form.value,fileToUpload).then((data: any) => {
            console.log("EditUser : " , data);
            this.router.navigate([this.navigateTo]);
        });

    }

    ngOnInit() {

        this.registerForm = new FormGroup({
            'id':new FormControl(this.Item.id),
            'Change':new FormControl(this.Item.Change),
            //'username':new FormControl(this.Item.username,Validators.required),
            'password':new FormControl(this.Item.password,Validators.required),
            'student_name':new FormControl(this.Item.student_name,Validators.required),
            'phone':new FormControl(this.Item.phone,[Validators.required, Validators.minLength(9),Validators.pattern('^[0-9]+$')]),
            'email':new FormControl(this.Item.email,[Validators.required, Validators.email]),
            'city':new FormControl(this.Item.city),
            'schoolgrade':new FormControl(this.Item.schoolgrade),
            'teachinglevel':new FormControl(this.Item.teachinglevel),
            'branch_id':new FormControl(this.Item.branch_id),
            'scool_name':new FormControl(this.Item.scool_name),
            'parent_name':new FormControl(this.Item.parent_name),
            'parent_phone':new FormControl(this.Item.parent_phone),
            'selectedBranches':new FormControl(this.Item.selectedBranches),
            'userType':new FormControl(this.Item.userType,Validators.required),
        })

    }


    onChange(event) {

        this.Change = true;
        var files = event.srcElement.files;
        this.changeImage = event.target.files[0];

        let reader = new FileReader();
        reader.onload = (e: any) => {
            this.changeImage = e.target.result;
        }
        reader.readAsDataURL(event.target.files[0]);
    }
}
