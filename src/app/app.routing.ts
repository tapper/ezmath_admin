import { Routes } from '@angular/router';

import { AdminLayoutComponent } from './layouts/admin/admin-layout.component';
import { AuthLayoutComponent } from './layouts/auth/auth-layout.component';
import {AuthGuard} from "./_guards/auth.guard";

export const AppRoutes: Routes = [{
  path: '',
  component: AdminLayoutComponent,
  children: [{
    path: '',
    loadChildren: './dashboard/dashboard.module#DashboardModule'
  }, {
    path: 'email',
    loadChildren: './email/email.module#EmailModule'
  },{
      path: 'subCategory',
      loadChildren: './subCategory/Main.module#MainModule'
  },{
      path: 'products',
      loadChildren: './products/Main.module#MainModule'
  },{
      path: 'users',
      loadChildren: './users/Main.module#MainModule'
  },{
      path: 'users_debt',
      loadChildren: './users_debt/Main.module#MainModule'
  },{
      path: 'teachers_classes',
      loadChildren: './teachers_classes/Main.module#MainModule'
  },{
      path: 'professions',
      loadChildren: './professions/Main.module#MainModule'
  },{
      path: 'branches',
      loadChildren: './branches/Main.module#MainModule'
  },
  {
      path: 'classes2',
      loadChildren: './classes2/Main.module#MainModule'
  },
  {
      path: 'tickets',
      loadChildren: './tickets/Main.module#MainModule'
  },
  {
      path: 'ticket_groups',
      loadChildren: './ticket_groups/Main.module#MainModule'
  },
  {
      path: 'user_tickets',
      loadChildren: './user_tickets/Main.module#MainModule'
  },
      {
          path: 'schoolgrade',
          loadChildren: './schoolgrade/Main.module#MainModule'
      },
      {
          path: 'teachinglevel',
          loadChildren: './teachinglevel/Main.module#MainModule'
      },
  {
      path: 'user_classes',
      loadChildren: './user_classes/Main.module#MainModule'
  },
  {
      path: 'classes',
      loadChildren: './classes/Main.module#MainModule'
  },
  {
      path: 'class_contact_leads',
      loadChildren: './class_contact_leads/Main.module#MainModule'
  },
  {
      path: 'push',
      loadChildren: './push/Main.module#MainModule'
  },
   {
      path: 'productImages',
      loadChildren: './productImages/Main.module#MainModule'
  },{
      path: 'car',
      loadChildren: './cars/car.module#CarModule'
  },{
      path: 'employee',
      loadChildren: './employee/employee.module#EmployeeModule'
  },{
      path: 'order',
      loadChildren: './orders/order.module#OrderModule'
  },{
      path: 'kitchen',
      loadChildren: './kitchens/kitchen.module#KitchenModule'
  },{
      path: 'company',
      loadChildren: './company/company.module#CompanyModule'
  },{
      path: 'category',
      loadChildren: './category/Main.module#MainModule'
  },{
    path: 'components',
    loadChildren: './components/components.module#ComponentsModule'
  }, {
    path: 'icons',
    loadChildren: './icons/icons.module#IconsModule'
  }, {
    path: 'cards',
    loadChildren: './cards/cards.module#CardsModule'
  }, {
    path: 'forms',
    loadChildren: './form/form.module#FormModule'
  }, {
    path: 'tables',
    loadChildren: './tables/tables.module#TablesModule'
  }, {
    path: 'datatable',
    loadChildren: './datatable/datatable.module#DatatableModule'
  }, {
    path: 'charts',
    loadChildren: './charts/charts.module#ChartsModule'
  }, {
    path: 'maps',
    loadChildren: './maps/maps.module#MapsModule'
  }, {
    path: 'pages',
    loadChildren: './pages/pages.module#PagesModule'
  }, {
    path: 'taskboard',
    loadChildren: './taskboard/taskboard.module#TaskboardModule'
  }, {
    path: 'calendar',
    loadChildren: './fullcalendar/fullcalendar.module#FullcalendarModule'
  }, {
    path: 'media',
    loadChildren: './media/media.module#MediaModule'
  }, {
    path: 'widgets',
    loadChildren: './widgets/widgets.module#WidgetsModule'
  }, {
    path: 'social',
    loadChildren: './social/social.module#SocialModule'
  }, {
    path: 'docs',
    loadChildren: './docs/docs.module#DocsModule'
  }] , canActivate: [AuthGuard]
}, {
  path: '',
  component: AuthLayoutComponent,
  children: [{
    path: 'authentication',
    loadChildren: './authentication/authentication.module#AuthenticationModule'
  }, {
    path: 'error',
    loadChildren: './error/error.module#ErrorModule'
  }, {
    path: 'landing',
    loadChildren: './landing/landing.module#LandingModule'
  }]
}, {
  path: '**',
  redirectTo: 'error/404'
}];

