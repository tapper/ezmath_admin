import {Component, OnInit} from '@angular/core';
import {MainService} from "../MainService.service";
import {SettingsService} from "../../../settings/settings.service";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {ActivatedRoute} from "@angular/router";
import {FormControl, FormGroup} from "@angular/forms";

@Component({
    selector: 'app-index',
    templateUrl: './index.component.html',
    styleUrls: ['../../icons/fontawesome/fontawesome.component.scss', '../../media/list/list.component.scss', './index.component.css']
})


export class IndexComponent implements OnInit {

    ItemsArray: any[] = [];
    ItemsArray1: any[] = [];
    host: string = '';
    settings = '';
    avatar = '';
    public folderName:string = '';
    public addButton:string = ''
    deleteModal: any;
    detailsModal: any;
    selectedItem: any;
    companyToDelete: any;
    SubCatId:any;
    now = new Date();
    searchDate:any;
    hours : any = ["14:00-15:00","15:00-16:00","16:00-17:00","17:00-18:00","18:00-19:00","19:00-20:00","20:00-21:00"];

    public senddetails:any =
    {
        'user_id' : '',
        'class_id' : '',
        'approve_status' : '',
        'payment_method' : '',
    }


    constructor(public MainService: MainService, settings: SettingsService , private modalService: NgbModal , private route: ActivatedRoute) {
        this.route.params.subscribe(params =>
        {
            this.SubCatId = params['id'];
            if(!this.SubCatId)
                this.SubCatId = "-1";
            console.log("11 : " , this.SubCatId)
            this.host = settings.host;
            this.avatar = settings.avatar;
        });
    }

    ngOnInit() {
        this.searchDate = {year: this.now.getFullYear(), month: this.now.getMonth() + 1, day: this.now.getDate()};
        this.getItems(this.searchDate);
    }

    onDateChange(evt) {
        this.getItems(this.searchDate);
    }



    getItems(date)
    {
        this.MainService.GetItems('TeachersClasses',date).then((data: any) =>
        {
            console.log("TeachersClasses : ", data)
            this.ItemsArray = data;
            this.ItemsArray1 = data;
        })
    }

    changeClassOption()
    {
        this.closeDetailsModal();
        this.changeClass(this.selectedItem['student']['user_data']['id']);
    }


    paymentTicket() {
        this.senddetails.user_id = this.selectedItem['student']['user_data']['id'];
        this.senddetails.class_id = this.selectedItem['id'];
        this.senddetails.payment_method = 1;
    }

    paymentCash() {

    }

    paymentDebt() {

    }

    didntShowClass(type)
    {

        console.log("this.selectedItem",this.selectedItem)

        this.senddetails.user_id = this.selectedItem['student']['user_data']['id'];
        this.senddetails.class_id = this.selectedItem['id'];


        this.MainService.userDidntShow('userDidntShow', this.senddetails,'0').then((data: any) => {
            let response = data.json();
            if (type == 0) {
                //this.selectedItem['student']['paid_count'] = 1;
                this.selectedItem['student']['didnt_show'] = 1;
            }
            else
                this.selectedItem['student']['didnt_show'] = 0;
        })
    }

    changeClass(id){
        let newclass = prompt("אנא הזן מספר שיעור");
        if (newclass) {
            this.MainService.ChangeClass('webChangeUserClass', id,newclass).then((data: any) => {
                let response = data.json();
                if (response == 0) {
                    alert ("מספר שיעור לא נמצא יש לנסות שוב");

                }
                else if (response == 1) {
                    alert ("שיעור הוחלף בהצלחה");
                    this.getItems(this.searchDate);
                }
                // else if (response == 2) {
                //     alert('לא ניתן להחליף לשיעור זה, מכסת תלמידים לשיעור הסתיים');
                // }
            })
        } else {
            alert ("לא הוזן מספר שיעור")
        }
    }

    DeleteItem() {
        // this.MainService.DeleteItem('old', this.ItemsArray[this.companyToDelete].itemId).then((data: any) => {
        //     this.getItems(this.searchDate);
        // })
    }

    updateFilter(event) {
        const val = event.target.value;
        // filter our data
        const temp = this.ItemsArray1.filter(function (d) {
            return d.fullname.toLowerCase().indexOf(val) !== -1 || !val;
        });
        // update the rows
        this.ItemsArray = temp;
    }

    openDetailsModal(content, item,student){

        this.detailsModal = this.modalService.open(content);
        item.student = student;
        this.selectedItem = item;
        console.log("DM : " , content , item)
    }

    closeDetailsModal(){
        this.detailsModal.close();
    }

    openDeleteModal(content,index)
    {
        this.deleteModal = this.modalService.open(content);
        this.companyToDelete = index;
    }

    async deleteCompany()
    {
        this.deleteModal.close();
        this.DeleteItem();
        console.log("Company To Delete : " , this.companyToDelete)
    }

}
