import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import {SettingsService} from "../../../settings/settings.service";

import {Http,Headers,Response, RequestOptions} from "@angular/http";
import {Injectable} from "@angular/core";
import { HttpModule } from '@angular/http';
import 'rxjs/Rx';


@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss']
})
export class SigninComponent implements OnInit {

    ServerUrl: string = '';
    public form: FormGroup;
    public wrongCredentials: boolean = false;
    public headers = new Headers({'Content-Type': 'application/x-www-form-urlencoded'});
    public options = new RequestOptions({headers:this.headers});

  constructor(private fb: FormBuilder, private router: Router, settings: SettingsService,private http:Http) {
      this.ServerUrl = settings.ServerUrl;
  }

  ngOnInit() {
    this.form = this.fb.group ( {
      uname: [null , Validators.compose ( [ Validators.required ] )] , password: [null , Validators.compose ( [ Validators.required ] )]
    } );
  }

  onSubmit() {

      let body = new FormData();
      body.append('uname', this.form.value.uname);
      body.append('password', this.form.value.password);
      return this.http.post(this.ServerUrl + '' + 'WebAdminLogin', body).map(res => res.json()).do((data)=>{
       if  (data.length == 0)
           this.wrongCredentials = true;
       else {
           this.wrongCredentials = false;
           localStorage.setItem('id', data['0'].id);
           //localStorage.setItem('type', data['0'].userType);
           this.router.navigate ( [ '/' ] );
       }
      }).toPromise();
  }

}
