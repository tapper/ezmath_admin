import {Component, OnInit, ElementRef, ChangeDetectionStrategy , ViewChild, NgZone} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";

import {Http, RequestOptions} from "@angular/http";
import {Observable} from "rxjs/Observable";
import {Headers} from '@angular/http';
import {FormBuilder, FormControl , FormGroup, NgForm, Validators} from "@angular/forms";
import { Ng2UploaderModule } from 'ng2-uploader';
import {MainService} from "../MainService.service";
import { FileUploader, FileUploaderOptions } from 'ng2-file-upload/ng2-file-upload';
import {NgbTimeStruct} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})

export class AddComponent implements OnInit {

    registerForm: FormGroup;
    public navigateTo:string = '/users/index';
    public paramsSub;
    public id: number;
    public SubCategories;
    public isReady;
    public imageSrc: string = '';
    public folderName:string = 'user_tickets';
    //public rowsNames: any[] = ['שם התלמיד','שם משתמש','סיסמה','טלפון','אימייל','עיר','שם בית ספר','שם הורה','טלפון הורה'];
    //public rows: any[] = ['student_name','username','password','phone','email','city','scool_name','parent_name','parent_phone'];
    public User_Id;

    now = new Date();
    time: NgbTimeStruct = {hour: 13, minute: 30, second: 30};

    public fields:any = {
        "user_id" : "",
        "ticket_date" : "",
    }



    @ViewChild("fileInput") fileInput;

    constructor(private route: ActivatedRoute,private http: Http, public service:MainService , public router:Router) {
        //console.log("Row : " , this.rows)
        this.route.queryParams.subscribe(params => {
            this.User_Id = params['user_id'];
        });
    }

    onSubmit(form:NgForm)
    {

        //console.log(form.value);
       // let fi = this.fileInput.nativeElement;
        let fileToUpload;
        //if (fi.files && fi.files[0]) {fileToUpload = fi.files[0];}

        this.service.AddItem('WebAddUserTicket',this.fields,fileToUpload).then((data: any) => {
            console.log("WebAddUserTicket : " , data);
            this.router.navigate(['/','user_tickets','index'], { queryParams: { user_id: this.User_Id } });
        });

    }
    
    ngOnInit() {

        this.paramsSub = this.route.queryParams.subscribe(params => this.User_Id = params['user_id']);
        this.fields.user_id = this.User_Id;
        this.fields.ticket_date = {year: this.now.getFullYear(), month: this.now.getMonth() + 1, day: this.now.getDate()};


    }
    
    ngOnDestroy() {
        this.paramsSub.unsubscribe();
    }
}
