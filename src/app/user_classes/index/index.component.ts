import { Component, OnInit } from '@angular/core';
import { MainService } from "../MainService.service";
import { SettingsService } from "../../../settings/settings.service";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { ActivatedRoute } from "@angular/router";

@Component({
    selector: 'app-index',
    templateUrl: './index.component.html',
    styleUrls: ['../../icons/fontawesome/fontawesome.component.scss', '../../media/list/list.component.scss', './index.component.css']
})


export class IndexComponent implements OnInit {

    ItemsArray: any[] = [];
    ItemsArray1: any[] = [];
    host: string = '';
    settings = '';
    avatar = '';
    public folderName: string = 'classes2';
    public addButton: string = 'הוסף שיעור'
    deleteModal: any;
    detailsModal: any;
    selectedItem: any;
    companyToDelete: any;
    SubCatId: any;
    now = new Date();
    searchDate: any;

    public senddetails: any = {
        'user_id': '',
        'class_id': '',
        'approve_status': '',
        'payment_method': '',
    }

    constructor(public MainService: MainService, settings: SettingsService, private modalService: NgbModal, private route: ActivatedRoute) {
        this.route.params.subscribe(params => {
            this.SubCatId = params['id'];
            if (!this.SubCatId)
                this.SubCatId = "-1";
            console.log("11 : ", this.SubCatId)
            this.host = settings.host;
            this.avatar = settings.avatar;
        });
    }

    ngOnInit() {
        this.searchDate = { year: this.now.getFullYear(), month: this.now.getMonth() + 1, day: this.now.getDate() };
        this.getItems(this.searchDate);
    }

    onDateChange(evt) {
        this.getItems(this.searchDate);
    }

    getItems(date) {
        this.MainService.GetItems('WebGetUserClasses', this.SubCatId, date).then((data: any) => {
            console.log("WebGetUserClasses : ", data)
            this.ItemsArray = data;
            this.ItemsArray1 = data;
        })
    }

    DeleteItem() {
        this.MainService.DeleteItem('WebDeleteUserClass', this.ItemsArray[this.companyToDelete].itemId).then((data: any) => {
            this.getItems(this.searchDate);
        })
    }

    didntShowClass(type) {

        console.log("selectedItem", this.selectedItem)

        this.senddetails.user_id = this.selectedItem.user_data.id;
        this.senddetails.class_id = this.selectedItem.class_id;


        this.MainService.userDidntShow('userDidntShow', this.senddetails, 0).then((data: any) => {

            if (type == 0)
                this.selectedItem['didnt_show'] = 1;
            else
                this.selectedItem['didnt_show'] = 0;
        })
    }

    paymentTicket() {
        this.senddetails.user_id = this.selectedItem.user_data.id;
        this.senddetails.class_id = this.selectedItem.class_id;
        this.senddetails.payment_method = 1;


        let confirmBox1 = confirm("האם לאשר רכישה בכרטיסיה?");
        if (confirmBox1) {
            this.MainService.ClassPayment('ClassTicketPaymentConfirm', this.senddetails, '0').then((data: any) => {
                let dataReponse = data;
                let alertResponse = '';

                switch (dataReponse) {
                    case 0:
                        alertResponse = 'רכישה בכרטיסיה בוצעה בהצלחה';
                        break;
                    case 1:
                        alertResponse = 'תשלום כבר בוצעה';
                        break;
                    case 2:
                        alertResponse = 'למשתמש זה לא שויך קבוצת כרטסיות , יש לפתוח קבוצה במערכת ניהול';
                        break;
                    case 3:
                        alertResponse = 'לא נותרה יתרה בכרטיסיה של המשתמש';
                        break;
                    default:
                        alertResponse = '';
                }

                if (dataReponse == 0 || dataReponse == 1 || dataReponse == 2) {
                    alert(alertResponse);

                    if (dataReponse == 0 || dataReponse == 1) {
                        this.selectedItem['paid_count'] = 1;
                        this.selectedItem['paid_method'] = 1;
                    }

                }
                else {
                    let confirmBox2 = confirm("לא נותרה יתרה - קניית כרטיסיה ותשלום?");
                    if (confirmBox2) {
                        this.MainService.ClassPayment('ClassCashPaymentTicketBuy', this.senddetails, '0').then((data: any) => {
                            alert("קניית כרטיסיה ותשלום בוצע בהצלחה");
                            this.selectedItem['paid_count'] = 1;
                            this.selectedItem['paid_method'] = 1;
                        })
                    }
                }
            })
        }
    }

    paymentCash() {
        this.senddetails.user_id = this.selectedItem.user_data.id;
        this.senddetails.class_id = this.selectedItem.class_id;
        this.senddetails.payment_method = 0;

        let confirmBox1 = confirm("האם לאשר רכישה במזומן?");
        if (confirmBox1) {


            this.MainService.ClassPayment('ClassCashPayment', this.senddetails, 0).then((data: any) => {

                let dataReponse = data.json();
                let alertResponse = '';

                switch (dataReponse) {
                    case 0:
                        alertResponse = 'רכישה במזומן בוצעה בהצלחה';
                        break;
                    case 1:
                        alertResponse = 'תשלום כבר בוצעה';
                        break;
                    default:
                        alertResponse = '';
                }

                if (dataReponse == 0) {
                    this.selectedItem['paid_count'] = 1;
                    this.selectedItem['paid_method'] = 0;
                }

                alert(alertResponse);
            })
        }
    }

    paymentDebt() {
        this.senddetails.user_id = this.selectedItem.user_data.id;
        this.senddetails.class_id = this.selectedItem.class_id;
        this.senddetails.payment_method = 2;

        let confirmBox1 = confirm("האם לאשר תשלום בחוב?");
        if (confirmBox1) {
            this.MainService.ClassPayment('ClassDebtPayment', this.senddetails, 0).then((data: any) => {
                let dataReponse = data.json();
                if (dataReponse == 0) {
                    this.selectedItem['paid_count'] = 1;
                    this.selectedItem['paid_method'] = 2;
                }
            })
        }
    }

    //
    cashToTicket() {
        this.senddetails.user_id = this.selectedItem.user_data.id;
        this.senddetails.class_id = this.selectedItem.class_id;
        this.senddetails.payment_method = 1;

        let confirmBox1 = confirm("האם לאשר רכישה בכרטיסיה?");
        if (confirmBox1) {
            this.MainService.ClassPayment('cashToTicket', this.senddetails, 0).then((data: any) => {
                let dataReponse = data.json();
                let alertResponse = '';
                console.log("dataReponse:", dataReponse)
                switch (dataReponse) {
                    case 0:
                        alertResponse = 'רכישה בכרטיסיה בוצעה בהצלחה';
                        break;
                    case 1:
                        alertResponse = 'תשלום כבר בוצעה';
                        break;
                    case 2:
                        alertResponse = 'למשתמש זה לא שויך קבוצת כרטסיות , יש לפתוח קבוצה במערכת ניהול';
                        break;
                    case 3:
                        alertResponse = 'לא נותרה יתרה בכרטיסיה של המשתמש';
                        break;
                    default:
                        alertResponse = '';
                }

                if (dataReponse == 0 || dataReponse == 1 || dataReponse == 2) {
                    alert(alertResponse);

                    if (dataReponse == 0 || dataReponse == 1) {
                        this.selectedItem['paid_count'] = 1;
                        this.selectedItem['paid_method'] = 1;
                    }
                }
                // else {
                //     let confirmBox2 = confirm("לא נותרה יתרה - קניית כרטיסיה ותשלום?");
                //     if (confirmBox2) {
                //         this.MainService.ClassPayment('ClassCashPaymentTicketBuy', this.senddetails, '0').then((data: any) => {
                //             alert("קניית כרטיסיה ותשלום בוצע בהצלחה");
                //             this.selectedItem['paid_count'] = 1;
                //             this.selectedItem['paid_method'] = 1;
                //         })
                //     }
                // }
            })
        }
    }

    CashtoDebt() {
        let confirmBox1 = confirm("האם לאשר תשלום בחוב?");
        if (confirmBox1) {
            this.senddetails.user_id = this.selectedItem.user_data.id;
            this.senddetails.class_id = this.selectedItem.class_id;
            this.senddetails.payment_method = 2;
            this.MainService.ClassPayment('CashtoDebt', this.senddetails, 0).then((data: any) => {
                let dataReponse = data.json();
                console.log("dataReponse:", dataReponse)
                if (dataReponse == 0) {
                    this.selectedItem['paid_count'] = 1;
                    this.selectedItem['paid_method'] = 2;
                }
            })
        }
    }

    debtToCash() {
        let confirmBox1 = confirm("האם לאשר תשלום במזומן?");
        if (confirmBox1) {
            this.senddetails.user_id = this.selectedItem.user_data.id;
            this.senddetails.class_id = this.selectedItem.class_id;
            this.senddetails.payment_method = 0;
            this.MainService.ClassPayment('debtToCash', this.senddetails, 0).then((data: any) => {
                let dataReponse = data.json();
                console.log("dataReponse:", dataReponse)
                if (dataReponse == 0) {
                    this.selectedItem['paid_count'] = 1;
                    this.selectedItem['paid_method'] = 0;
                }
            })
        }
    }

    debtToTicket()
    {
        let confirmBox1 = confirm("האם לאשר תשלום בכרטיסיה?");
        if (confirmBox1) {
            this.senddetails.user_id = this.selectedItem.user_data.id;
            this.senddetails.class_id = this.selectedItem.class_id;
            this.senddetails.payment_method = 1;
            this.MainService.ClassPayment('debtToTicket', this.senddetails, 0).then((data: any) => {
                let dataReponse = data.json();
                let alertResponse = '';
                console.log("dataReponse:", dataReponse)
                switch (dataReponse) {
                    case 0:
                        alertResponse = 'רכישה בכרטיסיה בוצעה בהצלחה';
                        break;
                    case 1:
                        alertResponse = 'תשלום כבר בוצעה';
                        break;
                    case 2:
                        alertResponse = 'למשתמש זה לא שויך קבוצת כרטסיות , יש לפתוח קבוצה במערכת ניהול';
                        break;
                    case 3:
                        alertResponse = 'לא נותרה יתרה בכרטיסיה של המשתמש';
                        break;
                    default:
                        alertResponse = '';
                }

                if (dataReponse == 0 || dataReponse == 1 || dataReponse == 2) {
                    alert(alertResponse);

                    if (dataReponse == 0 || dataReponse == 1) {
                        this.selectedItem['paid_count'] = 1;
                        this.selectedItem['paid_method'] = 1;
                    }
                }
                // else {
                //     let confirmBox2 = confirm("לא נותרה יתרה - קניית כרטיסיה ותשלום?");
                //     if (confirmBox2) {
                //         this.MainService.ClassPayment('ClassCashPaymentTicketBuy', this.senddetails, '0').then((data: any) => {
                //             alert("קניית כרטיסיה ותשלום בוצע בהצלחה");
                //             this.selectedItem['paid_count'] = 1;
                //             this.selectedItem['paid_method'] = 1;
                //         })
                //     }
                // }
            })
        }     
    }

    updateFilter(event) {
        const val = event.target.value;
        // filter our data
        const temp = this.ItemsArray1.filter(function (d) {
            return d.fullname.toLowerCase().indexOf(val) !== -1 || !val;
        });
        // update the rows
        this.ItemsArray = temp;
    }

    openDetailsModal(content, item) {
        console.log("DM11 : ", content, item)
        this.detailsModal = this.modalService.open(content);
        this.selectedItem = item;
    }

    openDeleteModal(content, index) {
        this.deleteModal = this.modalService.open(content);
        this.companyToDelete = index;
    }

    async deleteCompany() {
        this.deleteModal.close();
        this.DeleteItem();
        console.log("Company To Delete : ", this.companyToDelete)
    }

}
